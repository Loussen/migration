<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 12/3/16
 * Time: 10:29 AM
 */
?>
<?php
include "8w4_migrationadm/pages/includes/config.php";
require "phpmailer/class.phpmailer.php";

$response = json_encode(array("code"=>0, "content" => "Error system", "err_param" => ''));

$queue_date_count = 10;

//move_uploaded_file($_FILES['file']['tmp_name'],'images/file/1.jpg');

if($_POST)
{
    if($_POST['apply_form_letter'])
    {
        $_POST = array_map("strip_tags", $_POST);
        extract($_POST);

        $description = nl2br($description);

//        if(strlen($applytype)<3)
//            $response = json_encode(array("code"=>0, "content" => "Error apply type", "err_param" => 'applytype'));
//        elseif(strlen($sendto)<3)
//            $response = json_encode(array("code"=>0, "content" => "Error send to", "err_param" => 'sendto'));
        if(strlen($name)<3)
            $response = json_encode(array("code"=>0, "content" => "Error name", "err_param" => 'name'));
//        elseif(strlen($surname)<3)
//            $response = json_encode(array("code"=>0, "content" => "Error surname", "err_param" => 'surname'));
//        elseif(strlen($socialstatus)<3)
//            $response = json_encode(array("code"=>0, "content" => "Error social status", "err_param" => 'socialstatus'));
//        elseif(strlen($country)<3)
//            $response = json_encode(array("code"=>0, "content" => "Error country", "err_param" => 'country'));
//        elseif(strlen($birthday)<3)
//            $response = json_encode(array("code"=>0, "content" => "Error birthday", "err_param" => 'birthday'));
        elseif(strlen($email)<3 || !filter_var($email, FILTER_VALIDATE_EMAIL))
            $response = json_encode(array("code"=>0, "content" => "Error email", "err_param" => 'email'));
        elseif(strlen($address)<3)
            $response = json_encode(array("code"=>0, "content" => "Error address", "err_param" => 'address'));
        elseif(strlen($phone)<3 || !filter_var($phone, FILTER_SANITIZE_NUMBER_FLOAT))
            $response = json_encode(array("code"=>0, "content" => "Error phone", "err_param" => 'phone'));
        elseif(strlen($description)<3)
            $response = json_encode(array("code"=>0, "content" => "Error description", "err_param" => 'description'));
        else
        {
            $file_upload = false;
            if(is_array($_FILES) && !empty($_FILES['file']['tmp_name']))
            {
                $file_types = ['jpeg', 'jpg', 'bmp', 'png', 'doc', 'pdf', 'docx'];
                $file_name = $_FILES['file']['name'];
                $type = end(explode(".",$file_name));
                if(!in_array($type,$file_types))
                {
                    $response = json_encode(array("code"=>0, "content" => "Error file type", "err_param" => 'file'));
                    echo $response;
                    exit;
                }
                else
                {
                    $file_upload = true;
                    $file_name_uniq = uniqid().'.'.$type;
                    move_uploaded_file($_FILES['file']['tmp_name'],'images/file/'.$file_name_uniq);
                }
            }

            $mail = new PHPMailer();
            $mail -> IsSMTP();
            $mail -> SMTPAuth = true;
            $mail -> Host = 'smtp.yandex.com';
            $mail -> Port = 587;
            $mail -> SMTPSecure = 'tls';
            $mail -> Username = 'contact@laennec.az';
            $mail -> Password = '159357ct!)(';
            $mail -> AddReplyTo($email,"Məktub");
            $mail -> SetFrom($mail -> Username, 'Miqrasiya (Məktub)');
            $mail -> AddAddress('miqrasiya@nakhchivan.az');

            if($file_upload==true)
                $mail -> AddAttachment($_SERVER['DOCUMENT_ROOT']."/images/file/".$file_name_uniq);

            $mail -> CharSet = 'UTF-8';
            $mail -> Subject = 'Müraciət formu';

//            $message = $lang53." : ".$applytype."<br />
//                        ".$lang54." : ".$sendto."<br />
//                        ".$lang55." : ".$doctype."<br />
//                        ".$lang56." : ".$docnum."<br />
//                        ".$lang42." : ".$name."<br />
//                        ".$lang43." : ".$surname."<br />
//                        ".$lang44." : ".$patronymicname."<br />
//                        ".$lang57." : ".$socialstatus."<br />
//                        ".$lang63." : ".$country."<br />
//                        ".$lang64." : ".$region."<br />
//                        ".$lang48." : ".$email."<br />
//                        ".$lang46." : ".$address."<br />
//                        ".$lang23." : ".$phone."<br />
//                        ".$lang60." : ".$description."<br /><br />
//
//                        * ".$lang65."
//                        ";

            $message =  $lang42.",".$lang43.",".$lang44." : ".$name."<br />
                        ".$lang48." : ".$email."<br />
                        ".$lang46." : ".$address."<br />
                        ".$lang23." : ".$phone."<br />
                        ".$lang60." : ".$description."<br /><br />
                        ";

            $mail -> MsgHTML($message);

            $insert = mysqli_query($db,"insert into `mektublar` values (0,'$applytype','$sendto','$doctype','$docnum','$name','$surname', '$patronymicname','$socialstatus','$country', '$region', '$birthday', '$email', '$address', '$phone','$file_name_uniq','$description') ");

            if($mail->Send() && $insert)
                $response = json_encode(array("code"=>1, "content" => "Success", "err_param" => ''));
            else
                $response = json_encode(array("code"=>-1, "content" => "Try again", "err_param" => ''));

        }
    }
    elseif($_POST['apply_form_queue'])
    {
        $_POST = array_map("strip_tags", $_POST);
        extract($_POST);

        if(strlen($name)<3)
            $response = json_encode(array("code"=>0, "content" => "Error name", "err_param" => 'name'));
        elseif(strlen($surname)<3)
            $response = json_encode(array("code"=>0, "content" => "Error surname", "err_param" => 'surname'));
        elseif(strlen($patronymicname)<3)
            $response = json_encode(array("code"=>0, "content" => "Error father name", "err_param" => 'patronymicname'));
        elseif(strlen($docnum)<3)
            $response = json_encode(array("code"=>0, "content" => "Error document number", "err_param" => 'docnum'));
        elseif(strlen($address)<3)
            $response = json_encode(array("code"=>0, "content" => "Error address", "err_param" => 'address'));
        elseif(strlen($applytype)<3)
            $response = json_encode(array("code"=>0, "content" => "Error apply type", "err_param" => 'applytype'));
        elseif(strlen($email)<3 || !filter_var($email, FILTER_VALIDATE_EMAIL))
            $response = json_encode(array("code"=>0, "content" => "Error email", "err_param" => 'email'));
        elseif(strlen($date)<3)
            $response = json_encode(array("code"=>0, "content" => "Error date", "err_param" => 'date'));
        elseif(strlen($hour)<3)
            $response = json_encode(array("code"=>0, "content" => "Error hour", "err_param" => 'hour'));
        else
        {
            $queue = mysqli_query($db,"select `hour` from `novbeler` where `date`='$date' and `hour`='$hour'");

            if(mysqli_num_rows($queue)>=$queue_date_count)
            {
                $response = json_encode(array("code"=>0, "content" => "Queue count limit", "err_param" => 'hour'));
                echo $response;
                exit;
            }

            $insert = mysqli_query($db,"insert into `novbeler` values (0,'$applytype','$docnum','$name','$surname', '$patronymicname','$email', '$address', '$date', '$hour') ");

            $mail = new PHPMailer();
            $mail -> IsSMTP();
            $mail -> SMTPAuth = true;
            $mail -> Host = 'smtp.yandex.com';
            $mail -> Port = 587;
            $mail -> SMTPSecure = 'tls';
            $mail -> Username = 'contact@laennec.az';
            $mail -> Password = '159357ct!)(';
            $mail -> AddReplyTo($email,"Elektron növbə");
            $mail -> SetFrom($mail -> Username, 'Miqrasiya (Elektron növbə)');
            $mail -> AddAddress($email);
            $mail -> AddCC('miqrasiya@nakhchivan.az.az');

            $mail -> CharSet = 'UTF-8';
            $mail -> Subject = 'Müraciət formu';

            $message = $lang42." : ".$name."<br />
                        ".$lang43." : ".$surname."<br />
                        ".$lang44." : ".$patronymicname."<br />
                        ".$lang45." : ".$docnum."<br />
                        ".$lang46." : ".$address."<br />
                        ".$lang53." : ".$applytype."<br />
                        ".$lang48." : ".$email."<br />
                        ".$lang49." : ".$date."<br />
                        ".$lang50." : ".$hour."<br /><br />
                        ".$lang67." : ".mysqli_insert_id($db);

            $mail -> MsgHTML($message);

            if($mail->Send() && $insert)
                $response = json_encode(array("code"=>1, "content" => "Success", "err_param" => '', "response_info"=>$_POST));
            else
                $response = json_encode(array("code"=>-1, "content" => "Try again", "err_param" => ''));

        }
    }
    elseif($_POST['date_val'])
    {
        $date_val = strip_tags($_POST['date_val']);
        $queue = mysqli_query($db,"select `hour` from `novbeler` where `date`='$date_val'");

        if(mysqli_num_rows($queue))
        {
            while($row=mysqli_fetch_assoc($queue))
            {
                $array[] = $row['hour'];
            }

            $count = array_count_values($array);

            $array_count = [];

            foreach ($count as $key=>$value)
            {
                if($value>=$queue_date_count)
                {
                    $array_count[] = $key;
                }
            }

            if(count($array_count)>0)
                $response = json_encode(array("code"=>1, "content" => "Success", "err_param" => '', "response_info"=>$array_count));
            else
                $response = json_encode(array("code"=>0, "content" => "", "err_param" => ''));
        }
        else
            $response = json_encode(array("code"=>0, "content" => "", "err_param" => ''));
    }
}

echo $response;
?>
