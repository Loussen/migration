<?php
include "pages/includes/config.php";
if(isset($_GET["do"])) $do=safe($_GET["do"]); else $do='';
$do=str_replace(array("../","./","/"),"",$do);
include "pages/includes/check.php";
if(intval($user["id"])==0){header("Location: login.php"); exit('Go go go...'); die('Go go go...');}
include "pages/includes/function_thumb.php";
include "pages/includes/resize_class.php";
//$esas_dil=1;

$checkboxes=safe($_GET["checkboxes"]);
if($checkboxes!=''){
	$checkboxes=substr($checkboxes,1,-1);	$checkboxes=str_replace("-",",",$checkboxes);
	$forId=intval($_GET["forId"]);		if($forId==1) $column='id'; else $column='auto_id';
	$checkbox_del=intval($_GET["checkbox_del"]);
	$active=intval($_GET["active"]);
	if($checkbox_del==1){
		mysqli_query($db,"delete from $do where $column in (".$checkboxes.") ");
		if($do=='mehsullar'){
			$sql=mysqli_query($db,"select id,tip,xeber_id from sekiller2 where xeber_id in (".$checkboxes.") ");
			while($row=mysqli_fetch_assoc($sql)){
				@unlink('../images/gallery2/'.$row["xeber_id"].'/'.$row["id"].'.'.$row["tip"]);
				@unlink('../images/gallery2/'.$row["xeber_id"].'/'.$row["id"].'_thumb.'.$row["tip"]);
			}
			mysqli_query($db,"delete from sekiller2 where xeber_id='$delete' ");
		}
	}
	elseif($active>0){
		if($active==2) $active=0; else $active=1;
		if($do=='mehsullar' && $active==1){
			$sql=mysqli_query($db,"select id,xeber_id from sekiller2 where xeber_id in (".$checkboxes.") and aktivlik=1 ");
			while($row=mysqli_fetch_assoc($sql)){
				mysqli_query($db,"update $do set aktivlik='$active' where auto_id='$row[xeber_id]' ");
			}
		}
		else mysqli_query($db,"update $do set aktivlik='$active' where $column in (".$checkboxes.") ");
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"> 
<html> 
<head><?php include "pages/layouts/head.php"; ?></head>
<body>
<div class="content_wrapper">
	<?php include "pages/layouts/top.php"; ?>
	<?php include "pages/layouts/menu.php"; ?>
	<div id="content">
		<div class="inner">
            <a style="padding: 10px 50px; background: green; color: #fff; border-radius: 50px;" href="index.php?do=uploader" target="_blank">Serverə fayl yükləmək</a>
			<?php
			if(is_file("pages/".$do.".php")) include "pages/".$do.".php";
			else echo '<center><strong id="basliq1">Azərbaycan Respublikası<br>
                    DÖVLƏT MİQRASİYA XİDMƏTİ</strong></center>
            <br>
            <strong id="basliq2">Dövlət miqrasiya siyasəti</strong><br>
            Azərbaycan Respublikasında miqrasiya sahəsində dövlət siyasətinin həyata keçirilməsi, qanunvericiliyin beynəlxalq normalara və müasir dövrün tələblərinə uyğun təkmilləşdirilməsi, qanunların tətbiqində Azərbaycan Respublikasının milli təhlükəsizliyinin və sabit sosial-iqtisadi, demoqrafik inkişafının təmin edilməsi, əmək ehtiyatlarından səmərəli istifadə edilməsi, ölkə ərazisində əhalinin mütənasib yerləşdirilməsi, miqrantların intellektual və əmək potensialından istifadə edilməsi, tənzimlənməyən miqrasiya proseslərinin neqativ təsirinin aradan qaldırılması, insan alveri də daxil olmaqla, qeyri-qanuni miqrasiyanın qarşısının alınması məqsədi ilə Azərbaycan Respublikası Prezidentinin 25 iyul 2006-cı il tarixli Sərəncamı ilə “Azərbaycan Respublikasının Dövlət Miqrasiya Proqramı (2006-2008-ci illər)” təsdiq edilmişdir.
        Bu Dövlət Proqramının əsas məqsədi miqrasiya sahəsində dövlət siyasətinin həyata keçirilməsi, miqrasiya idarəetmə sisteminin inkişaf etdirilməsi, miqrasiya proseslərinin tənzimlənməsi və proqnozlaşdırılması, bu sahədə qanunvericiliyin beynəlxalq normalara və müasir dövrün tələblərinə uyğun təkmilləşdirilməsi, qanunların tətbiqinin effektivliyinin təmin edilməsi, miqrasiya sahəsində vahid məlumat bankının, müasir avtomatlaşdırılmış nəzarət sisteminin yaradılması, qeyri-qanuni miqrasiyanın qarşısının alınması, beynəlxalq əməkdaşlığın inkişaf etdirilməsi tədbirlərinin həyata keçirilməsindən ibarətdir. Proqramda miqrasiya proseslərinin xüsusiyyətləri, dövlət siyasətinin prioritet istiqamətləri öz əksini tapmışdır. Bununla yanaşı proqramın əhatə etdiyi dövr ərzində ayrı-ayrı dövlət qurumları ilə əlaqəli şəkildə həyata keçirməli olduqları tədbirlər qeyd edilmişdir<br>
            <strong id="basliq2">Dövlət Miqrasiya Xidməti haqqında</strong><br>
            Təsdiq edilmiş “Azərbaycan Respublikasının Dövlət Miqrasiya Proqramı” əsasında miqrasiya sahəsində vahid
            dövlət siyasətini həyata keçirən xüsusi dövlət orqanının yaradılması zərurətini nəzərə alan Azərbaycan
            Respublikasının Prezidenti cənab İlham Əliyev 19 mart 2007-ci il tarixdə Azərbaycan Respublikası Dövlət
            Miqrasiya Xidmətinin yaradılması haqqında 560 nömrəli Fərman imzalamışdır. Eyni zamanda, Fərmana əsasən
            Xidmətin Əsasnaməsi təsdiq edilmişdir.

            Dövlət Miqrasiya Xidmətinin yaradıldığı vaxtdan miqrasiya proseslərinə dövlət nəzarətinin gücləndirilməsi
            məqsədi ilə zəruri normativ hüquqi aktlar qəbul edilmiş, bir sıra institusional və təşkilati tədbirlər
            həyata keçirilmişdir.

            Miqrasiya proseslərinin vahid və çevik prosedurlar əsasında tənzimlənməsi, sənədləşmənin sadələşdirilməsi
            məqsədilə Azərbaycan Respublikasının Prezidenti cənab İlham Əliyev tərəfindən "Miqrasiya proseslərinin
            idarəolunmasında "bir pəncərə" prinsipinin tətbiqi haqqında" 4 mart 2009-cu il tarixli 69 nömrəli Fərman
            imzalanmışdır. Adıçəkilən Fərmanın tətbiqi ölkədə miqrasiya proseslərinin daha çevik və işlək mexanizmlər
            əsasında idarə edilməsinə, bu sahədə operativliyin təmin edilməsinə və həllini gözləyən problemlərin aradan
            qaldırılmasına səbəb olmuşdur. Fərmana əsasən, 2009-cu il iyulun 1-dən miqrasiya proseslərinin
            idarəolunmasında «bir pəncərə» prinsipinin tətbiq edilməsinə başlanılmış və bu prinsip üzrə vahid dövlət
            orqanının səlahiyyətləri Azərbaycan Respublikasının Dövlət Miqrasiya Xidmətinə həvalə edilmişdir.

            “Bir pəncərə” prinsipi çərçivəsində Dövlət Miqrasiya Xidməti əcnəbilərə və vətəndaşlığı olmayan şəxslərə
            Azərbaycan Respublikasının ərazisində müvəqqəti və daimi yaşamaq üçün icazələrin və müvafiq vəsiqələrin
            verilməsini, onların qeydiyyata alınmasını, Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı
            olmayan şəxslərin ölkədə müvəqqəti olma müddətinin uzadılmasını, həmçinin ölkə ərazisində haqqı ödənilən
            əmək fəaliyyəti ilə məşğul olmaq üçün iş icazələrinin verilməsini həyata keçirir. Bununla belə, Xidmət
            vətəndaşlıq məsələlərində iştirak edir və qaçqın statusunu müəyyənləşdirir.

            Qeyri-qanuni miqrasiyaya qarşı mübarizə tədbirlərinin gücləndirilməsi, Xidmətin fəaliyyətinin
            təkmilləşdirilməsi məqsədi ilə Prezident cənab İlham Əliyevin 8 aprel 2009-cu il tarixdə imzaladığı 76
            nömrəli Fərmanla Dövlət Miqrasiya Xidmətinə hüquq-mühafizə orqanı statusu verilmişdir.';
			?>
		</div>
		<?php include "pages/layouts/footer.php"; ?>		
	</div>
</div>
<input type="hidden" id="link_url" value="-" />
<input type="hidden" value="0" id="all_check_changed" />
<input type="hidden" id="delete_text1" value="<?=$lang116?>"/>
<input type="hidden" id="delete_text2" value="<?=$lang117?>"/>
<input type="hidden" id="tab_lang100" onclick="tab_select(this.id)" class="left_switch" value="test" style="width:50px">
</body>
</html>
