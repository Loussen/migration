<a href="javascript:;" id="show_menu">&raquo;</a>
<div id="left_menu">
	<a href="javascript:;" id="hide_menu">&laquo;</a>
	<ul id="main_menu">
		<li><a href="<?=SITE_PATH.'/8w4_migrationadm'?>"><img src="images/home.png" alt="" width="25"/>Ana səhifə</a></li>
		<li><a href="index.php?do=menyular"><img src="images/menus.png" alt="" width="25"/>Menyular</a></li>
		<li><a href="index.php?do=dmx"><img src="images/about.png" alt="" width="25"/>DMX haqqında</a></li>
		<li><a href="index.php?do=structure"><img src="images/structure.png" alt="" width="25"/>Struktur</a></li>
		<li><a href="index.php?do=rehber"><img src="images/leader.png" alt="" width="25"/>Rəhbərlik</a></li>
<!--		<li><a href="index.php?do=slider"><img src="images/sliders.png" alt="" width="25"/>Sliders</a></li>-->
		<li><a href="index.php?do=alboms"><img src="images/sliders.png" alt="" width="25"/>Albomlar</a></li>
		<li><a href="index.php?do=gallery"><img src="images/sliders.png" alt="" width="25"/>Qalereya</a></li>
		<li><a href="index.php?do=videos"><img src="images/video.png" alt="" width="25"/>Videolar</a></li>
		<li><a href="index.php?do=acts"><img src="images/acts.png" alt="" width="25"/>Aktlar</a></li>
		<li><a href="index.php?do=acts_sub"><img src="images/acts.png" alt="" width="25"/>Aktlar (Alt kateqoriya)</a></li>
		<li><a href="index.php?do=audio"><img src="images/audio.png" alt="" width="25"/>Audiolar</a></li>
        <li><a href="index.php?do=blog"><img src="images/news.png" alt="" width="25"/>Xəbərlər</a></li>
        <li><a href="index.php?do=news_gallery"><img src="images/sliders.png" alt="" width="25"/>Xəbərlər (Qalereya)</a></li>
        <li><a href="index.php?do=beledci"><img src="images/beledci.png" alt="" width="25"/>Bələdçi</a></li>
        <li><a href="index.php?do=sual"><img src="images/faq.png" alt="" width="25"/>Sual-Cavab</a></li>
        <li><a href="index.php?do=rusum"><img src="images/tax.png" alt="" width="25"/>Rüsumlar</a></li>
        <li><a href="index.php?do=calculator"><img src="images/calculator.png" alt="" width="25"/>Kalkulyator (Text)</a></li>
        <li><a href="index.php?do=days"><img src="images/days.png" alt="" width="25"/>Qəbul günləri</a></li>
        <li><a href="index.php?do=interviews"><img src="images/interviews.png" alt="" width="25"/>Müsahibələr</a></li>
        <li><a href="index.php?do=articles"><img src="images/articles.png" alt="" width="25"/>Məqalələr</a></li>
        <li><a href="index.php?do=faydali"><img src="images/help.png" alt="" width="25"/>Faydalı məlumatlar</a></li>
        <li><a href="index.php?do=faydali_alt"><img src="images/help.png" alt="" width="25"/>Faydalı məlumatlar (ic)</a></li>
        <li><a href="index.php?do=vetendasliq"><img src="images/citizenship.png" alt="" width="25"/>Vətəndaşlıq məs.</a></li>
        <li><a href="index.php?do=elanlar"><img src="images/elanlar.png" alt="" width="25"/>Elanlar</a></li>
        <li><a href="index.php?do=links"><img src="images/links.png" alt="" width="25"/>Digər linklər</a></li>
        <li><a href="index.php?do=senedler"><img src="images/documents.png" alt="" width="25"/>Tələb olunan sənədlər</a></li>
        <li><a href="index.php?do=rightlinks"><img src="images/links.png" alt="" width="25"/>Sağ linklər</a></li>
        <li><a href="index.php?do=gallery_footer"><img src="images/sliders.png" alt="" width="25"/>Aşağı fotolar</a></li>
        <li><a href="index.php?do=partners"><img src="images/partners.png" alt="" width="25"/>Partnyorlar</a></li>
        <li><a href="index.php?do=karabag"><img src="images/karabag.png" alt="" width="25"/>Qarabağ</a></li>
        <li><a href="index.php?do=callcenter"><img src="images/callcenter.png" alt="" width="25"/>Məlumat mərkəzi</a></li>
        <li><a href="index.php?do=naxcivan"><img src="images/naxcivan.png" alt="" width="25"/>Naxçıvan</a></li>
        <li><a href="index.php?do=countries"><img src="images/countries.png" alt="" width="25"/>Ölkələr</a></li>
        <li><a href="index.php?do=regions"><img src="images/regions.png" alt="" width="25"/>Regionlar</a></li>
		<li><a href="index.php?do=statistics"><img src="images/statistics.png" alt="" width="25"/>Statistika</a></li>
		<li><a href="index.php?do=elaqe"><img src="images/contacts.png" alt="" width="25"/>Əlaqələr</a></li>

        <li><a href="javascript::void(0);">&nbsp;&nbsp;</a></li>

        <li><a href="javascript::void(0);"><img src="images/letter.png" alt="" width="25"/>Məktub yazmaq (Form)</a></li>
        <li><a href="index.php?do=muraciet_mektub">&nbsp;&nbsp;&nbsp;<img src="images/apply.png" alt="" width="25"/>Müraciətin növləri</a></li>
        <li><a href="index.php?do=kime_mektub">&nbsp;&nbsp;&nbsp;<img src="images/who.png" alt="" width="25"/>Kimə</a></li>
        <li><a href="index.php?do=sened_mektub">&nbsp;&nbsp;&nbsp;<img src="images/documents.png" alt="" width="25"/>Sənədin növü</a></li>
        <li><a href="index.php?do=social_mektub">&nbsp;&nbsp;&nbsp;<img src="images/socialstatus.png" alt="" width="25"/>Sosial status</a></li>

        <li><a href="javascript::void(0);">&nbsp;&nbsp;</a></li>

        <li><a href="javascript::void(0);"><img src="images/novbe.png" alt="" width="25"/>Elektron növbə (Form)</a></li>
        <li><a href="index.php?do=novbe">&nbsp;&nbsp;&nbsp;<img src="images/about.png" alt="" width="25"/>Text</a></li>
        <li><a href="index.php?do=muraciet_novbe">&nbsp;&nbsp;&nbsp;<img src="images/apply.png" alt="" width="25"/>Müraciətin növləri</a></li>

        <li><a href="javascript::void(0);">&nbsp;&nbsp;</a></li>

		<li><a href="index.php?do=description"><img src="images/seoopt.png" alt="" width="25"/>Seo Opt.</a></li>

		<li><a href="javascript::void(0);">&nbsp;&nbsp;</a></li>

		<li><a href="index.php?do=diller"><img src="images/lang.png" alt="" width="25"/>Dillər</a></li>
		<li><a href="index.php?do=admin_pass"><img src="images/admin.png" alt="" width="25"/>Administrator</a></li>
		<li><a href="logout.php"><img src="images/logout.png" alt="" width="25"/>Logout</a></li>
<!--		<li><a href="index.php?do=users"><img src="images/users.png" alt="" width="25"/>İstifadəçilər</a></li>-->
	</ul>
	<br class="clear"/>	
</div>
<?php
/*

*/
?>