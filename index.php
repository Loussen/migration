<?php
    include "8w4_migrationadm/pages/includes/config.php";
    $do=safe($_GET["do"]);
    if(!is_file("includes/pages/".$do.".php")) $do='index';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include "includes/head.php"; ?>
</head>
<body>
<?php
    $sql_contact = mysqli_fetch_assoc(mysqli_query($db,"SELECT * FROM `elaqe` WHERE `lang_id`='$esas_dil' order by `id` DESC"));
?>
<input type="hidden" name="csrf_" value="<?=set_csrf_()?>" />

    <?php include "includes/header.php"; ?>
    <section class="center in-page">
        <div class="container">
            <?php include "includes/pages/".$do.".php"; ?>
        </div>
    </section>
    <section class="slide-foot">
        <div class="container">
            <div class="partner">
                <ul class="navbar">
                    <?php
                        $sql_partners = mysqli_query($db, "SELECT `auto_id`,`tip`,`link` FROM `partners` WHERE `lang_id`='$esas_dil' and `aktivlik`=1 order by `sira`");
                        while($row_partners=mysqli_fetch_assoc($sql_partners))
                        {
                            ?>
                            <li>
                                <a target="_blank" href="<?=$row_partners['link']?>" title="<?=$row_partners['link']?>">
                                    <img style="max-width: 200px;" src="<?=SITE_PATH?>/images/partners/<?=$row_partners['auto_id'].".".$row_partners['tip']?>" title="" alt="">
                                </a>
                            </li>
                            <?php
                        }
                    ?>
                </ul>
            </div>
        </div>
        <div id="carouselExampleControls1" class="carousel slide" data-ride="carousel" style="text-align: center;">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <ul class="navbar gallerybox">
                        <?php
                            $sql_galleryfooter = mysqli_query($db, "SELECT `auto_id`,`tip` FROM `gallery_footer` WHERE `lang_id`='$esas_dil' and `aktivlik`=1 order by `sira`");
                            $i=1;
                            while($row_galleryfooter=mysqli_fetch_assoc($sql_galleryfooter))
                            {

                                ?>
                                <li>
                                    <a href="<?=SITE_PATH?>/images/gallery_footer/<?=$row_galleryfooter['auto_id'].".".$row_galleryfooter['tip']?>" class="image-link" title="Foto">
                                        <img style="width: 270px !important; height: 220px; padding: 2px; object-fit: inherit;" class="d-block w-100" src="<?=SITE_PATH?>/images/gallery_footer/<?=$row_galleryfooter['auto_id'].".".$row_galleryfooter['tip']?>" alt="Foto">
                                    </a>
                                </li>
                                <?php
                                if($i%5==0)
                                {
                                    ?>
                                        </ul>
                                    </div>
                                    <div class="carousel-item">
                                        <ul class="navbar gallerybox">
                                    <?php
                                }
                                $i++;
                            }
                        ?>
                    </ul> 
                </div>
            </div>
        </div>
    </section>
    <?php include "includes/footer.php"; ?>

    <?php
        if($do=='letter')
        {
            ?>
            <script>
                // $(document).on('ready', function () {
                //     $("input[name=file]").fileinput({
                //         showUpload: false,
                //         maxFileCount: 1,
                //         language: 'az'
                //     });
                // });
                $(document).on('submit','form#letter-form',function(e){

                    e.preventDefault();

                    $('#loading-image').show();
                    $('#loading').css('opacity','0.3');
                    $('.has-error').removeClass('has-error');

                    var formData = new FormData(this);

                    $.ajax({
                        url: base_url+'/ajax.php',
                        type: 'POST',
                        data: formData,
                        mimeType: "multipart/form-data",
                        contentType: false,
                        cache: false,
                        processData: false,
                        dataType: 'json',
                        success: function (data, textStatus, jqXHR) {
                            // console.log(data.content);
                            if(data.code==0)
                            {
                                $('html, body').animate({
                                    scrollTop: $('[name="'+data.err_param+'"]').offset().top-80
                                }, 500);
                                $('[name="'+data.err_param+'"]').addClass('has-error');
                            }
                            else if(data.code==-1)
                            {
                                window.scrollTo(0,0);
                                $('div.error_letter').show();
                            }
                            else
                            {
                                window.scrollTo(0,0);
                                $("form#letter-form").css("display","none");
                                $('div.success_letter').show();
                            }

                            $('#loading-image').hide();
                            $('#loading').css('opacity','1');
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $('#loading-image').hide();
                            $('#loading').css('opacity','1');

                        }
                    });
                });
            </script>
            <?php
        }

        if($do=='onlinequeue')
        {
            ?>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
            <script>
                $(document).on('submit','form#queue-form',function(e){

                    e.preventDefault();

                    $('#loading-image').show();
                    $('#loading').css('opacity','0.3');
                    $('.has-error').removeClass('has-error');

                    var formData = new FormData(this);

                    $.ajax({
                        url: base_url+'/ajax.php',
                        type: 'POST',
                        data: formData,
                        mimeType: "multipart/form-data",
                        contentType: false,
                        cache: false,
                        processData: false,
                        dataType: 'json',
                        success: function (data, textStatus, jqXHR) {
                            if(data.code==0)
                            {
                                $('html, body').animate({
                                    scrollTop: $('[name="'+data.err_param+'"]').offset().top-80
                                }, 500);
                                $('[name="'+data.err_param+'"]').addClass('has-error');
                            }
                            else if(data.code==-1)
                            {
                                window.scrollTo(0,0);
                                $('div.error_letter').show();
                            }
                            else
                            {
                                window.scrollTo(0,0);
                                $("table.queue_info").show();

                                $("table.queue_info tr#response_info td#name").html(data.response_info.name);
                                $("table.queue_info tr#response_info td#surname").html(data.response_info.surname);
                                $("table.queue_info tr#response_info td#patronymicname").html(data.response_info.patronymicname);
                                $("table.queue_info tr#response_info td#docnum").html(data.response_info.docnum);
                                $("table.queue_info tr#response_info td#address").html(data.response_info.address);
                                $("table.queue_info tr#response_info td#applytype").html(data.response_info.applytype);
                                $("table.queue_info tr#response_info td#email").html(data.response_info.email);
                                $("table.queue_info tr#response_info td#date").html(data.response_info.date);
                                $("table.queue_info tr#response_info td#hour").html(data.response_info.hour);

                                $("form#queue-form").css("display","none");
                                $('div.success_letter').show();
                            }

                            $('#loading-image').hide();
                            $('#loading').css('opacity','1');
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $('#loading-image').hide();
                            $('#loading').css('opacity','1');

                        }
                    });
                });

                $(document).ready(function(){
                    var date_input=$('input[name="date"]'); //our date input has the name "date"
                    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                    date_input.datepicker({
                        format: 'mm/dd/yyyy',
                        container: container,
                        todayHighlight: true,
                        autoclose: true,
                    });
                })

                $(document).on('change','form#queue-form input#date',function(){

                    $('#loading-image').show();
                    $('#loading').css('opacity','0.3');
                    $('.has-error').removeClass('has-error');

                    var date_val = $(this).val();

                    $.ajax({
                        type: "POST",
                        url: base_url+'/ajax.php',
                        data: {date_val: date_val},
                        dataType: 'json',
                        success: function(data){
                            if(data.code==1)
                            {
                                var hours = data.response_info;
                                $("select[name='hour'] > option").each(function() {

                                    if($.inArray($(this).text(), hours )!==-1)
                                    {
                                        $(this).attr('disabled', 'disabled');
                                    }
                                });
                            }
                            else
                            {
                                $("select[name='hour'] > option").attr("disabled", false);
                            }

                            $('#loading-image').hide();
                            $('#loading').css('opacity','1');
                        }
                    });
                });
            </script>
            <?php
        }
    ?>

</body>
</html>