<header>
    <div class="ulu">
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <a href="<?=SITE_PATH?>" title="<?=$lang14." ".$lang15?>">
                        <?php
                            if($esas_dil==2)
                                $logo = 'logo-ru.png';
                            elseif($esas_dil==3)
                                $logo = 'logo-en.png';
                            else
                                $logo = 'logo-az.png';
                        ?>
                        <img src="<?=SITE_PATH?>/assets/img/logos/<?=$logo?>" title="<?=$lang14." ".$lang15?>" alt="<?=$lang14." ".$lang15?>">
                    </a>
                </div>
                <div class="col-6">
                    <div class="row">
                        <div class="col-9">
                            <p><?=$lang1?></p>
                            <h5><?=$lang2?></h5>
                        </div>
                        <div class="col-3">
                            <div class="float-right">
                                <img src="<?=SITE_PATH?>/assets/img/heydar-aliyev.png" title="Heydər Əliyev" alt="Ulu Öndər">
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light" id="grad">
        <div class="container">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto menu-mig">
                    <?php
                        $sql_menus = mysqli_query($db,"select `id`,`name`,`link`,`parent_auto_id`,`auto_id` from `menyular` where `lang_id`='$esas_dil' and `aktivlik`=1 and `parent_auto_id`=0 order by `sira`");
                        while($row_menus=mysqli_fetch_assoc($sql_menus))
                        {
                            $link = SITE_PATH."/".$row_menus['link'];
                            if($row_menus['link']=='/') $link = SITE_PATH;
                            $sql_submenus = mysqli_query($db,"select `id`,`name`,`link`,`parent_auto_id` from `menyular` where `lang_id`='$esas_dil' and `aktivlik`=1 and `parent_auto_id`='$row_menus[auto_id]' order by `sira`");
                            if(mysqli_num_rows($sql_submenus)>0)
                                $sub_menu = true;
                            else
                                $sub_menu = false;
                            ?>
                            <li class="nav-item <?=($sub_menu==true) ? 'dropdown' : ''?>">
                                <a class="nav-link <?=($sub_menu==true) ? 'dropdown-toggle' : ''?>" href="<?=$link?>" id="<?=($sub_menu==true) ? 'navbarDropdown' : ''?>" role="button" data-toggle="<?=($sub_menu==true) ? 'dropdown' : ''?>" aria-haspopup="false" aria-expanded="false" title="<?=$row_menus['name']?>">
                                    <?=$row_menus['name']?>
                                </a>
                                <?php
                                    if($sub_menu==true)
                                    {
                                        ?>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <?php
                                                while($row_sub_menus=mysqli_fetch_assoc($sql_submenus))
                                                {
                                                    ?>
                                                    <a class="dropdown-item" href="<?=SITE_PATH."/".$row_sub_menus['link']?>"><?=$row_sub_menus['name']?></a>
                                                    <?php
                                                }
                                            ?>
                                        </div>
                                        <?php
                                    }
                                ?>
                            </li>
                            <?php
                        }
                    ?>
                </ul>
                <form class="form-inline my-2 my-lg-0 form-mig" method="GET" action="<?=SITE_PATH?>/search">
                    <input class="form-control mr-sm-2" type="search" placeholder="" name="data" aria-label="Axtarış">
                </form>
                <ul class="menu-migra navbar-nav">
                    <li>
                        <a href="<?=SITE_PATH."/index.php?lang=1"?>" title="">AZ</a>
                    </li>
                    <li>
                        <a href="<?=SITE_PATH."/index.php?lang=2"?>" title="">RU</a>
                    </li>
                    <li>
                        <a href="<?=SITE_PATH."/index.php?lang=3"?>" title="">EN</a>
                    </li>
                </ul> 
            </div>
        </div>
    </nav>
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top" id="topx">
        <div class="container">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto menu-mig">
                    <?php
                    $sql_menus = mysqli_query($db,"select `id`,`name`,`link`,`parent_auto_id`,`auto_id` from `menyular` where `lang_id`='$esas_dil' and `aktivlik`=1 and `parent_auto_id`=0 order by `sira`");
                    while($row_menus=mysqli_fetch_assoc($sql_menus))
                    {
                        $link = SITE_PATH."/".$row_menus['link'];
                        if($row_menus['link']=='/') $link = SITE_PATH;
                        $sql_submenus = mysqli_query($db,"select `id`,`name`,`link`,`parent_auto_id` from `menyular` where `lang_id`='$esas_dil' and `aktivlik`=1 and `parent_auto_id`='$row_menus[auto_id]' order by `sira`");
                        if(mysqli_num_rows($sql_submenus)>0)
                            $sub_menu = true;
                        else
                            $sub_menu = false;
                        ?>
                        <li class="nav-item <?=($sub_menu==true) ? 'dropdown' : ''?>">
                            <a class="nav-link <?=($sub_menu==true) ? 'dropdown-toggle' : ''?>" href="<?=$link?>" id="<?=($sub_menu==true) ? 'navbarDropdown' : ''?>" role="button" data-toggle="<?=($sub_menu==true) ? 'dropdown' : ''?>" aria-haspopup="false" aria-expanded="false" title="<?=$row_menus['name']?>">
                                <?=$row_menus['name']?>
                            </a>
                            <?php
                            if($sub_menu==true)
                            {
                                ?>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <?php
                                    while($row_sub_menus=mysqli_fetch_assoc($sql_submenus))
                                    {
                                        ?>
                                        <a class="dropdown-item" href="<?=SITE_PATH."/".$row_sub_menus['link']?>"><?=$row_sub_menus['name']?></a>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <?php
                            }
                            ?>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
                <form class="form-inline my-2 my-lg-0 form-mig" method="GET" action="<?=SITE_PATH?>/search">
                    <input class="form-control mr-sm-2" type="search" name="data" placeholder="" aria-label="Axtarış">
                </form>
                <ul class="menu-migra navbar-nav">
                    <li>
                        <a href="<?=SITE_PATH."/index.php?lang=1"?>" title="AZ">AZ</a>
                    </li>
                    <li>
                        <a href="<?=SITE_PATH."/index.php?lang=2"?>" title="RU">RU</a>
                    </li>
                    <li>
                        <a href="<?=SITE_PATH."/index.php?lang=3"?>" title="EN">EN</a>
                    </li>
                </ul> 
            </div>
        </div>
    </nav>
</header>