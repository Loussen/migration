<div class="col-4">
    <div class="well">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-3">
                        <a href="<?=SITE_PATH?>/naxcivan" title="<?=$lang3?>">
                            <img src="<?=SITE_PATH?>/assets/img/flag.jpg" title="<?=$lang3?>" alt="<?=$lang3?>">
                        </a>
                    </div>
                    <div class="col-9">
                        <a href="<?=SITE_PATH?>/naxcivan" title="<?=$lang3?>">
                            <?=$lang3?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <iframe width="272" height="160" style="border: 0px;
    padding: 0 5px 0 0;"
                        src="https://www.youtube.com/embed/tgbNymZ7vqY">
                </iframe>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-3">
                        <a href="http://az.e.migration.gov.az/" target="_blank" title="<?=$lang4?>">
                            <img src="<?=SITE_PATH?>/assets/img/exidmet.png" title="<?=$lang4?>" alt="<?=$lang4?>">
                        </a>
                    </div>
                    <div class="col-9">
                        <a href="http://az.e.migration.gov.az/" target="_blank" title="<?=$lang4?>">
                            <?=$lang4?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-3">
                        <a href="" title="<?=$lang5?>">
                            <img src="<?=SITE_PATH?>/assets/img/odeme.png" title="<?=$lang5?>" alt="<?=$lang5?>">
                        </a>
                    </div>
                    <div class="col-9">
                        <a href="" title="<?=$lang5?>">
                            <?=$lang5?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-3">
                        <a href="<?=SITE_PATH?>/onlinequeue" title="<?=$lang6?>">
                            <img src="<?=SITE_PATH?>/assets/img/novbe.png" title="<?=$lang6?>" alt="<?=$lang6?>">
                        </a>
                    </div>
                    <div class="col-9">
                        <a href="<?=SITE_PATH?>/onlinequeue" title="<?=$lang6?>">
                            <?=$lang6?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-3">
                        <a href="<?=SITE_PATH."/callcenter"?>" title="<?=$lang7?>">
                            <img src="<?=SITE_PATH?>/assets/img/cagri.png" title="<?=$lang7?>" alt="<?=$lang7?>">
                        </a>
                    </div>
                    <div class="col-9">
                        <a href="<?=SITE_PATH."/callcenter"?>" title="<?=$lang7?>">
                            <?=$lang7?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-3">
                        <a href="" title="<?=$lang8?>">
                            <img src="<?=SITE_PATH?>/assets/img/mehdudiyyet.png" title="<?=$lang8?>" alt="<?=$lang8?>">
                        </a>
                    </div>
                    <div class="col-9">
                        <a href="" title="<?=$lang8?>">
                            <?=$lang8?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="accardion-mig">
        <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <?=$lang9?>
                        </button>
                    </h5>
                </div>

                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul class="navbar">
                            <?php
                                $sql_faydali = mysqli_query($db, "SELECT `auto_id`,`name` FROM `faydali` WHERE `lang_id`='$esas_dil' and `aktivlik`=1 order by `sira`");
                                while($row_faydali=mysqli_fetch_assoc($sql_faydali))
                                {
                                    ?>
                                    <li>
                                        <a href="<?= SITE_PATH . '/helpful/' . slugGenerator($row_faydali['name']) . '-' . $row_faydali['auto_id'] ?>" title="<?=$row_faydali['name']?>" target="blank">
                                            <?=more_string($row_faydali['name'],70)?>
                                        </a>
                                    </li>
                                    <?php
                                }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <?=$lang10?>
                        </button>
                    </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul class="navbar">
                            <?php
                                $sql_citizenship = mysqli_query($db, "SELECT `auto_id`,`name` FROM `vetendasliq` WHERE `lang_id`='$esas_dil' and `aktivlik`=1 order by `sira`");

                                while($row_citizenship=mysqli_fetch_assoc($sql_citizenship))
                                {
                                    ?>
                                    <li>
                                        <a href="<?= SITE_PATH . '/citizenship/' . slugGenerator($row_citizenship['name']) . '-' . $row_citizenship['auto_id'] ?>" title="" target="blank">
                                            <?=more_string($row_citizenship['name'],70)?>
                                        </a>
                                    </li>
                                    <?php
                                }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <?=$lang11?>
                        </button>
                    </h5>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul class="navbar">
                            <?php
                                $sql_elanlar = mysqli_query($db, "SELECT `auto_id`,`name` FROM `elanlar` WHERE `lang_id`='$esas_dil' and `aktivlik`=1 order by `sira`");
                                while($row_elanlar=mysqli_fetch_assoc($sql_elanlar))
                                {
                                    ?>
                                    <li>
                                        <a href="<?= SITE_PATH . '/announcement/' . slugGenerator($row_elanlar['name']) . '-' . $row_elanlar['auto_id'] ?>" title="" target="blank">
                                            <?=more_string($row_elanlar['name'],70)?>
                                        </a>
                                    </li>
                                    <?php
                                }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingFour">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            <?=$lang12?>
                        </button>
                    </h5>
                </div>
                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul class="navbar">
                            <?php
                                $sql_links = mysqli_query($db, "SELECT `name`,`link` FROM `links` WHERE `lang_id`='$esas_dil' and `aktivlik`=1 order by `sira`");
                                while($row_links=mysqli_fetch_assoc($sql_links))
                                {
                                    ?>
                                    <li>
                                        <a href="<?=$row_links['link']?>" title="<?=$row_links['name']?>" target="_blank">
                                            <?=$row_links['name']?>
                                        </a>
                                    </li>
                                    <?php
                                }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingFive">
                    <a href="<?=SITE_PATH."/occupy"?>" title="<?=$lang41?>">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <?=html_entity_decode($lang13)?>
                            </button>
                        </h5>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>