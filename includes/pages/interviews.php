<div class="row">
	<?php include "includes/left.php"; ?>
	<div class="col-8">
		<div class="news-mig"> 
			<div class="in-mig">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=SITE_PATH."/".$parent_menu['link']?>"><?=$parent_menu['name']?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span><?=$menyu['name']?></span></li>
					</ol>
				</nav>
				<div class="card beledci">
                    <?php
                        while($row_interviews=mysqli_fetch_assoc($sql_interviews))
                        {
                            ?>
                            <a href="<?= SITE_PATH . '/interviews-inner/' . slugGenerator($row_interviews['name']) . '-' . $row_interviews['auto_id'] ?>" title="<?=$row_interviews['name']?>">
                                <i class="fa fa-microphone" aria-hidden="true"></i>
                                <?=more_string($row_interviews['name'],150)?>
                                <span class="pull-right btn btn-primary btn-xs"><?=$lang34?></span>
                            </a>
                            <?php
                        }
                    ?>
				</div> 
			</div>  
		</div>
	</div>
</div>