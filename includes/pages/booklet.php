<div class="row">
	<?php include "includes/left.php"; ?>
	<div class="col-8">
		<div class="news-mig"> 
			<div class="in-mig">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?=$main_menu['link']?>"><?=$main_menu['name']?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><span><?=$lang27?></span></li>
					</ol>
				</nav>
				<div class="card beledci">
                    <?php
                        while($row_booklet=mysqli_fetch_assoc($sql_booklet))
                        {
                            ?>
                            <a href="<?= SITE_PATH . '/booklet-inner/' . slugGenerator($row_booklet['name']) . '-' . $row_booklet['auto_id'] ?>" title="<?=$row_booklet['name']?>">
                                <i class="fa fa-bookmark" aria-hidden="true"></i>
                                <?=$row_booklet['name']?>
                                <span class="pull-right btn btn-primary btn-xs"><?=$lang34?></span>
                            </a>
                            <?php
                        }
                    ?>
				</div> 
			</div>  
		</div>
	</div>
</div>