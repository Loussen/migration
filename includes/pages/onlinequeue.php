<div class="row">
    <?php include "includes/left.php"; ?>
    <div class="col-8">
        <div class="news-mig">
            <div class="in-mig novbe" id="loading">
                <img align="center" id="loading-image" src="<?=SITE_PATH?>/assets/img/load.gif   " alt="Loading..." style="display: none; position: fixed; z-index: 1; margin-left: 250px; margin-top: 0; ">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=SITE_PATH?>"><?=$main_menu['name']?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span><?=$lang6?></span></li>
                    </ol>
                </nav>
                <div class="alert alert-info" role="alert">
                    <?=html_entity_decode($sql_novbe['text'])?>
                </div>
                <div class="alert alert-success success_letter" role="alert" style="display: none;">
                    <?=$lang39?>
                </div>
                <div class="alert alert-warning error_letter" role="alert" style="display: none;">
                    <?=$lang40?>
                </div>
                <div style="margin-bottom: 15px;">
                    <style>
                        table.queue_info {
                            font-family: arial, sans-serif;
                            border-collapse: collapse;
                            width: 100%;
                            font-size: 10px;
                            overflow: auto;
                            display: inline-block;
                        }

                        table.queue_info td, th {
                            border: 1px solid #dddddd;
                            text-align: left;
                            padding: 8px;
                        }

                        /*table.queue_info tr:nth-child(even) {*/
                            /*background-color: #dddddd;*/
                        /*}*/
                    </style>
                    <table class="queue_info" style="display: none;">
                        <tr>
                            <th><?=$lang42?></th>
                            <th><?=$lang43?></th>
                            <th><?=$lang44?></th>
                            <th><?=$lang45?></th>
                            <th><?=$lang46?></th>
                            <th><?=$lang47?></th>
                            <th><?=$lang48?></th>
                            <th><?=$lang49?></th>
                            <th><?=$lang50?></th>
                        </tr>
                        <tr id="response_info">
                            <td id="name"></td>
                            <td id="surname"></td>
                            <td id="patronymicname"></td>
                            <td id="docnum"></td>
                            <td id="address"></td>
                            <td id="applytype"></td>
                            <td id="email"></td>
                            <td id="date"></td>
                            <td id="hour"></td>
                        </tr>
                    </table>
                </div>
                <form id="queue-form" method="post">
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="formGroupExampleInput"><?=$lang42?> *</label>
                            <input type="text" name="name" maxlength="30" class="form-control" id="formGroupExampleInput" placeholder="Example input">
                        </div>
                        <div class="form-group col-4">
                            <label for="formGroupExampleInput2"><?=$lang43?> *</label>
                            <input type="text" name="surname" maxlength="30" class="form-control" id="formGroupExampleInput2" placeholder="Another input">
                        </div>
                        <div class="form-group col-4">
                            <label for="formGroupExampleInput2"><?=$lang44?> *</label>
                            <input type="text" name="patronymicname" maxlength="30" class="form-control" id="formGroupExampleInput2" placeholder="Another input">
                        </div>
                        <div class="form-group col-6">
                            <label for="formGroupExampleInput2"><?=$lang45?> *</label>
                            <input type="text" name="docnum" maxlength="30" class="form-control" id="formGroupExampleInput2" placeholder="Another input">
                        </div>
                        <div class="form-group col-6">
                            <label for="formGroupExampleInput2"><?=$lang46?> *</label>
                            <input type="text" name="address" maxlength="50" class="form-control" id="formGroupExampleInput2" placeholder="Another input">
                        </div>
                        <div class="form-group col-6">
                            <label for="exampleFormControlSelect1"><?=$lang47?> *</label>
                            <select class="form-control" name="applytype" id="exampleFormControlSelect1">
                                <option value="0"><?=$lang62?></option>
                                <?php
                                    $sql_apply = mysqli_query($db,"select `name` from `muraciet_novbe` where `aktivlik`=1 and `lang_id`='$esas_dil' order by `sira`");
                                    while($row_apply=mysqli_fetch_assoc($sql_apply))
                                    {
                                        ?>
                                        <option value="<?=$row_apply['name']?>"><?=$row_apply['name']?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-6">
                            <label for="exampleFormControlInput1"><?=$lang48?> *</label>
                            <input type="email" name="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
                        </div>
                        <div class="form-group col-6">
                            <label class="control-label requiredField" for="date">
                                <?=$lang49?> *
                            </label>
                            <div>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar">
                                        </i>
                                    </div>
                                    <input class="form-control" id="date" autocomplete="off" name="date" placeholder="MM/DD/YYYY" type="text"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-6">
                            <label for="exampleFormControlSelect1"><?=$lang50?> *</label>
                            <select class="form-control" name="hour" id="exampleFormControlSelect1">
                                <option value="0"><?=$lang62?></option>
                                <?php
                                    date_default_timezone_set("Asia/Baku");
                                    $range=range(strtotime("09:30"),strtotime("18:00"),30*60);
                                    foreach($range as $time)
                                    {
                                        if(date("H:i",$time)=="13:00" || date("H:i",$time)=="13:30" || date("H:i",$time)=="14:00") continue;
                                        ?>
                                        <option value="<?=date("H:i",$time)?>"><?=date("H:i",$time)?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-12">
                            <input type="hidden" name="apply_form_queue" value="send_form">
<!--                            <button type="button" class="btn btn-primary">--><?//=$lang51?><!--</button>-->
                            <button type="submit" class="btn btn-primary"><?=$lang51?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>