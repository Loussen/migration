<div class="row">
	<?php include "includes/left.php"; ?>
	<div class="col-8">
		<div class="news-mig"> 
			<div class="in-mig">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=SITE_PATH."/".$parent_menu['link']?>"><?=$parent_menu['name']?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span><?=$menyu['name']?></span></li>
					</ol>
				</nav>
                <?=html_entity_decode($sql_structure['text'])?>
				<img src="<?=SITE_PATH?>/images/structure/<?=$sql_structure['auto_id'].'.'.$sql_structure['tip']?>" title="<?=$menyu['name']?>" alt="<?=$menyu['name']?>" class="img-fluid">
			</div>  
		</div>
	</div>
</div> 