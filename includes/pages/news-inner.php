<div class="row">
	<?php include "includes/left.php"; ?>
	<div class="col-8">
		<div class="news-mig"> 
			<div class="in-mig">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?=SITE_PATH."/".$parent_menu['link']?>"><?=$parent_menu['name']?></a></li>
						<li class="breadcrumb-item"><a href="<?=SITE_PATH."/".$menyu['link']?>"><?=$menyu['name']?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><span><?=date("d-m-Y",$sql_news_inner['created_at'])?></span></li>
						<li class="breadcrumb-item active" aria-current="page"><span><?=date("H:i",$sql_news_inner['created_at'])?></span></li>
					</ol>
				</nav>
				<img src="<?=SITE_PATH?>/images/blog/<?=$sql_news_inner['auto_id'].".".$sql_news_inner['tip']?>" title="<?=$sql_news_inner['name']?>" alt="<?=$sql_news_inner['name']?>" class="img-fluid">
				<h6>
					<?=$sql_news_inner['name']?>
				</h6>
				<?=html_entity_decode($sql_news_inner['text'])?>
				<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
                        <?php
                            for($i=1;$i<=mysqli_num_rows($sql_news_gallery);$i++)
                            {
                                $class_active = ($i==1) ? 'active' : '';
                                ?>
                                <li data-target="#carouselExampleIndicators" data-slide-to="<?=$i?>" class="<?=$class_active?>"><?=$i?></li>
                                <?php
                            }
                        ?>
					</ol>
					<div class="carousel-inner">
                        <?php
                            $i=1;
                            while($row_news_gallery=mysqli_fetch_assoc($sql_news_gallery))
                            {
                                $class_active = ($i==1) ? 'active' : '';
                                ?>
                                <div class="carousel-item <?=$class_active?>">
                                    <img class="d-block w-100" src="<?=SITE_PATH?>/images/news_gallery/<?=$row_news_gallery['id'].'.'.$row_news_gallery['tip']?>" alt="<?=$sql_news_inner['name']?>">
                                </div>
                                <?php
                                $i++;
                            }
                        ?>
					</div>
					<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>   
		</div>  
	</div>
</div>