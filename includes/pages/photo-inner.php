<div class="row">
	<?php include "includes/left.php"; ?>
	<div class="col-8">
		<div class="news-mig"> 
			<div class="in-mig foto-ic">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?=SITE_PATH."/".$parent_menu['link']?>"><?=$parent_menu['name']?></a></li>
						<li class="breadcrumb-item"><a href="<?=SITE_PATH."/".$menyu['link']?>"><?=$menyu['name']?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><span><?=date("d-m-Y",$row_photo['tarix'])?></span></li>
						<li class="breadcrumb-item active" aria-current="page"><span><?=date("H:i",$row_photo['tarix'])?></span></li>
					</ol>
				</nav>  
				<h6>
					<?=$row_photo['basliq']?>
				</h6>
				<p>
					<?=html_entity_decode($row_photo['tam_metn'])?>
				</p> 
				<ul class="navbar gallerybox">
					<?php
						while($row_photo_inner=mysqli_fetch_assoc($sql_photo_inner))
						{
							?>
							<li>
								<a href="<?=SITE_PATH?>/images/gallery/<?=$row_photo_inner['id'].".".$row_photo_inner['tip']?>" class="image-link" title="<?=$row_photo['basliq']?>">
									<img src="<?=SITE_PATH?>/images/gallery/<?=$row_photo_inner['id']."_thumb.".$row_photo_inner['tip']?>" title="" alt="">
								</a>
							</li>
							<?php
						}
					?>
				</ul> 
			</div>   
		</div>  
	</div>
</div>