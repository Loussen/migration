<div class="row">
	<?php include "includes/left.php"; ?>
	<div class="col-8">
		<div class="news-mig"> 
			<div class="in-mig">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?=$main_menu['link']?>"><?=$main_menu['name']?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><span><?=$lang68?></span></li>
					</ol>
				</nav>
                <?php
                    if($count_rows<=0)
                    {
                        ?>
                        <div class="alert alert-warning"><?=$lang71?></div>
                        <?php
                    }
                    elseif(strlen($data)<3)
                    {
                        ?>
                        <div class="alert alert-warning"><?=$lang70?></div>
                        <?php
                    }
                    else
                    {
                        ?>
                        <div class="alert alert-warning"><?=$count_rows." ".$lang69?></div>
                        <div class="card beledci">
                            <?php
                            while($row_search=mysqli_fetch_assoc($sql_search))
                            {
                                ?>
                                <a href="<?= SITE_PATH . '/news-inner/' . slugGenerator($row_search['name']) . '-' . $row_search['auto_id'] ?>" title="<?=$row_search['name']?>">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                    <?=$row_search['name']?>
                                    <span class="pull-right btn btn-primary btn-xs"><?=$lang34?></span>
                                </a>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }

                ?>
			</div>  
		</div>
	</div>
</div>