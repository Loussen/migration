<div class="in-mig">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?=$parent_menu['link']?>"><?=$parent_menu['name']?></a></li>
			<li class="breadcrumb-item active" aria-current="page"><span><?=$menyu['name']?></span></li>
		</ol>
	</nav>
	<div class="row">
		<div class="col-3">
			<img src="<?=SITE_PATH?>/images/about/<?=$sql_leadership['auto_id'].".".$sql_leadership['tip']?>" class="img-fluid img-rounded img-thumbnail" title="<?=$sql_leadership['name']?>" alt="<?=$sql_leadership['name']?>">
		</div>
		<div class="col-9">
			<center>
				<h5>
					<?=$sql_leadership['name']?>
				</h5>
			</center>
			<p>
                <?=html_entity_decode($sql_leadership['text'])?>
			</p>
		</div>
	</div>
</div>