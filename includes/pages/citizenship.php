<div class="row">
	<?php include "includes/left.php"; ?>
	<div class="col-8">
		<div class="news-mig"> 
			<div class="in-mig">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?=$main_menu['link']?>"><?=$main_menu['name']?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><span><?=more_string($sql_citizenship_inner['name'],100)?></span></li>
					</ol>
				</nav> 
				<strong><?=$sql_citizenship_inner['name']?></strong>
                <?=html_entity_decode($sql_citizenship_inner['text'])?>
			</div>   
		</div>
	</div>
</div>