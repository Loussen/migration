<div class="in-mig" id="loading">
    <img align="center" id="loading-image" src="<?=SITE_PATH?>/assets/img/load.gif   " alt="Loading..." style="display: none; position: fixed; z-index: 1; margin-left: 450px; margin-top: 0; ">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?=SITE_PATH?>/letter"><?=$parent_menu['name']?></a></li>
			<li class="breadcrumb-item active" aria-current="page"><span><?=$menyu['name']?></span></li>
		</ol>
	</nav>
    <div class="alert alert-success success_letter" role="alert" style="display: none;">
        <?=$lang39?>
    </div>
    <div class="alert alert-warning error_letter" role="alert" style="display: none;">
        <?=$lang40?>
    </div>
	<form enctype="multipart/form-data" id="letter-form" method="post">
		<div class="mektub"> 
			<div class="row">
<!--				<div class="form-group col-6">-->
<!--					<label for="exampleFormControlInput1">--><?//=$lang54?><!-- *</label>-->
<!--					<select class="custom-select" name="sendto" id="inputGroupSelect01">-->
<!--                        <option value="0">--><?//=$lang62?><!--</option>-->
<!--                        --><?php
//                            $sql_persons = mysqli_query($db,"select `name` from `kime_mektub` where `aktivlik`=1 and `lang_id`='$esas_dil' order by `sira`");
//                            while($row_persons=mysqli_fetch_assoc($sql_persons))
//                            {
//                                ?>
<!--                                <option value="--><?//=$row_persons['name']?><!--">--><?//=$row_persons['name']?><!--</option>-->
<!--                                --><?php
//                            }
//                        ?>
<!--					</select>-->
<!--				</div>-->
<!--				<div class="form-group col-6">-->
<!--					<label for="exampleFormControlInput1">--><?//=$lang55?><!--</label>-->
<!--					<select class="custom-select" name="doctype" id="inputGroupSelect01">-->
<!--                        <option value="0">--><?//=$lang62?><!--</option>-->
<!--                        --><?php
//                            $sql_docs = mysqli_query($db,"select `name` from `sened_mektub` where `aktivlik`=1 and `lang_id`='$esas_dil' order by `sira`");
//                            while($row_doc=mysqli_fetch_assoc($sql_docs))
//                            {
//                                ?>
<!--                                <option value="--><?//=$row_doc['name']?><!--">--><?//=$row_doc['name']?><!--</option>-->
<!--                                --><?php
//                            }
//                        ?>
<!--					</select>-->
<!--				</div>-->
<!--				<div class="form-group col-6">-->
<!--					<label for="exampleFormControlInput1">--><?//=$lang56?><!--</label>-->
<!--					<input type="text" name="docnum" maxlength="13" class="form-control" aria-label="Text input with dropdown button">-->
<!--				</div>-->
				<div class="form-group col-6">
					<label for="exampleFormControlInput1"><?=$lang42?>, <?=$lang43?>, <?=$lang44?> *</label>
					<input type="text" name="name" maxlength="30" class="form-control" aria-label="Text input with dropdown button">
				</div>
                <div class="form-group col-6">
                    <label for="exampleFormControlInput1"><?=$lang53?> *</label>
                    <select name="applytype" class="custom-select" id="inputGroupSelect01">
                        <option value="0"><?=$lang62?></option>
                        <?php
                        $sql_apply = mysqli_query($db,"select `name` from `muraciet_mektub` where `aktivlik`=1 and `lang_id`='$esas_dil' order by `sira`");
                        while($row_apply=mysqli_fetch_assoc($sql_apply))
                        {
                            ?>
                            <option value="<?=$row_apply['name']?>"><?=$row_apply['name']?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
<!--				<div class="form-group col-4"> -->
<!--					<label for="exampleFormControlInput1">--><?//=$lang43?><!-- *</label>-->
<!--					<input type="text" name="surname" maxlength="30" class="form-control" aria-label="Text input with dropdown button">-->
<!--				</div>-->
<!--				<div class="form-group col-4">-->
<!--					<label for="exampleFormControlInput1">--><?//=$lang44?><!--</label>-->
<!--					<input type="text" name="patronymicname" maxlength="30" class="form-control" aria-label="Text input with dropdown button">-->
<!--				</div>-->
<!--				<div class="form-group col-6"> -->
<!--					<label for="exampleFormControlInput1">--><?//=$lang57?><!-- *</label>-->
<!--					<select class="custom-select" name="socialstatus" id="inputGroupSelect01">-->
<!--                        <option value="0">--><?//=$lang62?><!--</option>-->
<!--                        --><?php
//                            $sql_social = mysqli_query($db,"select `name` from `social_mektub` where `aktivlik`=1 and `lang_id`='$esas_dil' order by `sira`");
//                            while($row_social=mysqli_fetch_assoc($sql_social))
//                            {
//                                ?>
<!--                                <option value="--><?//=$row_social['name']?><!--">--><?//=$row_social['name']?><!--</option>-->
<!--                                --><?php
//                            }
//                        ?>
<!--					</select>-->
<!--				</div>-->
<!--				<div class="form-group col-6"> -->
<!--					<label for="exampleFormControlInput1">--><?//=$lang63?><!-- *</label>-->
<!--					<select class="custom-select" name="country" id="inputGroupSelect01">-->
<!--                        <option value="0">--><?//=$lang62?><!--</option>-->
<!--                        --><?php
//                            $sql_countries = mysqli_query($db,"select `name` from `countries` where `aktivlik`=1 and `lang_id`='$esas_dil' order by `sira`");
//                            while($row_countries=mysqli_fetch_assoc($sql_countries))
//                            {
//                                ?>
<!--                                <option value="--><?//=$row_countries['name']?><!--">--><?//=$row_countries['name']?><!--</option>-->
<!--                                --><?php
//                            }
//                        ?>
<!--					</select>-->
<!--				</div>-->
<!--				<div class="form-group col-6"> -->
<!--					<label for="exampleFormControlInput1">--><?//=$lang64?><!--</label>-->
<!--					<select name="region" class="custom-select" id="inputGroupSelect01">-->
<!--                        <option value="0">--><?//=$lang62?><!--</option>-->
<!--                        --><?php
//                            $sql_regions = mysqli_query($db,"select `name` from `regions` where `aktivlik`=1 and `lang_id`='$esas_dil' order by `sira`");
//                            while($row_regions=mysqli_fetch_assoc($sql_regions))
//                            {
//                                ?>
<!--                                <option value="--><?//=$row_regions['name']?><!--">--><?//=$row_regions['name']?><!--</option>-->
<!--                                --><?php
//                            }
//                        ?>
<!--					</select>-->
<!--				</div>-->
<!--				<div class="form-group col-6">-->
<!--					<label for="exampleFormControlInput1">--><?//=$lang58?><!-- *</label>-->
<!--					<input type="date" name="birthday" class="form-control" aria-label="Text input with dropdown button">-->
<!--				</div>-->
				<div class="form-group col-4">
					<label for="exampleFormControlInput1"><?=$lang48?> *</label>
					<input type="text" name="email" maxlength="30" class="form-control" aria-label="Text input with dropdown button">
				</div>
				<div class="form-group col-4">
					<label for="exampleFormControlInput1"><?=$lang46?> *</label>
					<input type="text" name="address" maxlength="50" class="form-control" aria-label="Text input with dropdown button">
				</div>
				<div class="form-group col-4">
					<label for="exampleFormControlInput1"><?=$lang23?> *</label>
					<input type="number" name="phone" class="form-control" aria-label="Text input with dropdown button">
				</div>
<!--				<div class="form-group col-12">-->
<!--					<label for="exampleFormControlInput1">--><?//=$lang59?><!--</label>-->
<!--					<div class="custom-file">-->
<!--						<input type="file" name="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">-->
<!--						<label class="custom-file-label" for="inputGroupFile01">--><?//=$lang62?><!--</label>-->
<!--					</div>-->
<!--				</div>-->
				<div class="form-group col-12">
					<label for="exampleFormControlTextarea1"><?=$lang60?> *</label>
					<textarea class="form-control" name="description" maxlength="2000" id="exampleFormControlTextarea1" rows="7"></textarea>
				</div>
                <input type="hidden" name="apply_form_letter" value="send_form">
				<div class="col-12">
					<button type="submit" name="submit_apply" class="btn btn-primary float-right"><?=$lang52?></button>
				</div>
				<div class="col-12 font-weight-bold">
					<p>
						<?=$lang61?>
					</p>
				</div>
			</div>
		</div>
	</form>   
</div>