<div class="row">
	<?php include "includes/left.php"; ?>
	<div class="col-8">
		<div class="news-mig"> 
			<div class="in-mig news">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=SITE_PATH."/".$parent_menu['link']?>"><?=$parent_menu['name']?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><span><?=$menyu['name']?></span></li>
					</ol>
				</nav>
                <?php
                    while($row_news=mysqli_fetch_assoc($sql_news))
                    {
                        ?>
                        <div class="card">
                            <div class="row">
                                <div class="col-4">
                                    <a href="<?= SITE_PATH . '/news-inner/' . slugGenerator($row_news['name']) . '-' . $row_news['auto_id'] ?>" title="<?=$row_news['name']?>">
                                        <img class="news_title_img" src="<?=SITE_PATH?>/images/blog/<?=$row_news['auto_id'].".".$row_news['tip']?>" title="<?=$row_news['name']?>" alt="<?=$row_news['name']?>">
                                    </a>
                                </div>
                                <div class="col-8">
                                    <div>
                                        <a href="<?= SITE_PATH . '/news-inner/' . slugGenerator($row_news['name']) . '-' . $row_news['auto_id'] ?>" title="<?=$row_news['name']?>">
                                            <?=more_string($row_news['qisa_metn'],150)?>
                                        </a>
                                    </div>
                                    <div class="float-left">
                                        <?=date("H:i",$row_news['created_at'])?>
                                    </div>
                                    <div class="float-right">
                                        <?php
                                            $news_month = getMonth(date("m",$row_news['created_at']),$lang_short);

                                            echo date("d",$row_news['created_at'])." ".$news_month." ".date('Y',$row_news['created_at']);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                ?>

                <?php
                    if($count_rows > $limit)
                    {
                        $show= 5;
                        ?>
                        <nav aria-label="Page navigation example" class="pagination-mig">
                            <ul class="pagination">
                                <?php
                                    if($page>1)
                                    {
                                        ?>
                                        <li class="page-item">
                                            <a class="page-link" href="<?= SITE_PATH . '/news/' . ($page - 1)?>" aria-label="Previous">
                                                <span aria-hidden="true">«</span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </li>
                                        <?php
                                    }

                                    for ($i = $page - $show; $i <= $page + $show; $i++)
                                    {
                                        if ($i > 0 && $i <= $max_page)
                                        {
                                            if ($i == $page)
                                            {
                                                ?>
                                                <li class="page-item active">
                                                    <a class="page-link" href="javascript:void(0)"><?= $i ?></a>
                                                </li>
                                                <?php
                                            }
                                            else
                                            {
                                            ?>
                                                <li class="page-item">
                                                    <a class="page-link" href="<?= SITE_PATH . '/news/' . $i ?>" title="<?=$i?>"><?= $i ?></a>
                                                </li>
                                                <?php
                                            }
                                        }
                                    }
                                    if ($page < $max_page)
                                    {
                                        ?>
                                        <li class="page-item">
                                            <a class="page-link" aria-label="Next" href="<?= SITE_PATH . '/news/' . ($page + 1) ?>" title="Next">
                                                <span aria-hidden="true">»</span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                ?>
<!--                                <li class="page-item active"><a class="page-link" href="#">1</a></li>-->
<!--                                <li class="page-item"><a class="page-link" href="#">2</a></li>-->
<!--                                <li class="page-item"><a class="page-link" href="#">3</a></li>-->
<!--                                <li class="page-item"><a class="page-link" href="#">4</a></li>-->
<!--                                <li class="page-item"><a class="page-link" href="#">5</a></li>-->
<!--                                <li class="page-item"><a class="page-link" href="#">6</a></li>-->
<!--                                <li class="page-item"><a class="page-link" href="#">7</a></li>-->
<!--                                <li class="page-item">-->
<!--                                    <a class="page-link" href="#" aria-label="Next">-->
<!--                                        <span aria-hidden="true">»</span>-->
<!--                                        <span class="sr-only">Next</span>-->
<!--                                    </a>-->
<!--                                </li>-->
                            </ul>
                        </nav>
                        <?php
                    }
                ?>

			</div>  
		</div>
	</div>
</div>