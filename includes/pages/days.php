<div class="row">
	<?php include "includes/left.php"; ?>
	<div class="col-8">
		<div class="news-mig"> 
			<div class="in-mig">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=SITE_PATH."/".$parent_menu['link']?>"><?=$parent_menu['name']?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span><?=$menyu['name']?></span></li>
					</ol>
				</nav>
				<div class="qebul">
					<?=html_entity_decode($sql_days['text'])?>
				</div>
				<div class="alert alert-info" style="font-size: 14px;" align="justify">
					<?=$sql_days['qisa_metn']?>
				</div>  
			</div>   
		</div>  
	</div>
</div>