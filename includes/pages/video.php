<div class="row">
	<?php include "includes/left.php"; ?>
	<div class="col-8">
		<div class="news-mig"> 
			<div class="in-mig foto">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=SITE_PATH."/".$parent_menu['link']?>"><?=$parent_menu['name']?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><span><?=$menyu['name']?></span></li>
					</ol>
				</nav>  
				<div class="row">
					<?php
						while($row_video=mysqli_fetch_assoc($sql_video))
						{
							?>
							<div class="col-4">
								<a href="<?=$row_video['video_link']?>" class="popup-youtube" title="<?=$row_video['basliq']?>">
									<img src="<?=SITE_PATH?>/images/video/<?=$row_video['auto_id'].".".$row_video['tip']?>" title="<?=$row_video['basliq']?>" alt="<?=$row_video['basliq']?>">
									<hr/>
									<div>
										<span><?=date("d-m-Y",$row_video['tarix'])?></span>
										<p><?=more_string($row_video['basliq'],80)?></p>
									</div>
								</a>
							</div>
							<?php
						}
					?>
				</div>
				<?php
                    if($count_rows > $limit)
                    {
                        $show= 5;
                        ?>
                        <nav aria-label="Page navigation example" class="pagination-mig">
                            <ul class="pagination">
                                <?php
                                    if($page>1)
                                    {
                                        ?>
                                        <li class="page-item">
                                            <a class="page-link" href="<?= SITE_PATH . '/video/' . ($page - 1)?>" aria-label="Previous">
                                                <span aria-hidden="true">«</span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </li>
                                        <?php
                                    }

                                    for ($i = $page - $show; $i <= $page + $show; $i++)
                                    {
                                        if ($i > 0 && $i <= $max_page)
                                        {
                                            if ($i == $page)
                                            {
                                                ?>
                                                <li class="page-item active">
                                                    <a class="page-link" href="javascript:void(0)"><?= $i ?></a>
                                                </li>
                                                <?php
                                            }
                                            else
                                            {
                                            ?>
                                                <li class="page-item">
                                                    <a class="page-link" href="<?= SITE_PATH . '/video/' . $i ?>" title="<?=$i?>"><?= $i ?></a>
                                                </li>
                                                <?php
                                            }
                                        }
                                    }
                                    if ($page < $max_page)
                                    {
                                        ?>
                                        <li class="page-item">
                                            <a class="page-link" aria-label="Next" href="<?= SITE_PATH . '/video/' . ($page + 1) ?>" title="Next">
                                                <span aria-hidden="true">»</span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                ?>
                            </ul>
                        </nav>
                        <?php
                    }
                ?>
			</div>  
		</div>
	</div>
</div>