<div class="row">
	<?php include "includes/left.php"; ?>
	<div class="col-8">
		<div class="news-mig"> 
			<div class="in-mig">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=SITE_PATH."/".$parent_menu['link']?>"><?=$parent_menu['name']?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span><?=$menyu['name']?></span></li>
					</ol>
				</nav> 
				<strong><?=$sql_audio_inner['basliq']?></strong>
                <?=html_entity_decode($sql_audio_inner['tam_metn'])?>
                <div>
                    <?php
                        if($sql_audio_inner['tip']!='' && file_exists("images/audio/".$sql_audio_inner['auto_id'].".".$sql_audio_inner['tip']))
                        {
                            ?>
                            <audio style="width: 100%;" controls>
                                <source src="<?=SITE_PATH?>/images/audio/<?=$sql_audio_inner['auto_id'].".".$sql_audio_inner['tip']?>" type="audio/ogg">
                                <source src="<?=SITE_PATH?>/images/audio/<?=$sql_audio_inner['auto_id'].".".$sql_audio_inner['tip']?>" type="audio/mpeg">
                                Your browser does not support the audio element.
                            </audio>
                            <?php
                        }
                    ?>
                </div>
			</div>   
		</div>
	</div>
</div>