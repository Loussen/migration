<div class="row">
    <?php include "includes/left.php"; ?>
    <div class="col-8">
        <?php
            $sql_news_manset = mysqli_query($db,"SELECT `auto_id`,`tip`,`name`,`qisa_metn` FROM `blog` WHERE `lang_id`='$esas_dil' and `aktivlik`=1 and `manset`=1 and `text`!='' LIMIT 5");
        ?>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <?php
                    for($i=1;$i<mysqli_num_rows($sql_news_manset);$i++)
                    {
                        $class_active = ($i==1) ? 'active' : '';
                        ?>
                        <li data-target="#carouselExampleIndicators" data-slide-to="<?=$i?>" class="<?=$class_active?>"></li>
                        <?php
                    }
                ?>
            </ol>
            <div class="carousel-inner">
                <?php
                    $i=1;
                    while($row_news_manset=mysqli_fetch_assoc($sql_news_manset))
                    {
                        $class_active = ($i==1) ? 'active' : '';
                        ?>
                        <div class="carousel-item <?=$class_active?>">
                            <a href="<?=SITE_PATH.'/news-inner/'.slugGenerator($row_news_manset['name']).'-'.$row_news_manset['auto_id']?>">
                                <img class="d-block w-100" src="<?=SITE_PATH?>/images/blog/<?=$row_news_manset['auto_id'].".".$row_news_manset['tip']?>" alt="<?=$row_news_manset['name']?>">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5><?=$row_news_manset['name']?></h5>
                                    <p><?=more_string($row_news_manset['qisa_metn'],120)?></p>
                                </div>
                            </a>
                        </div>
                        <?php
                        $i++;
                    }
                ?>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <div class="foregin-mig">
                <div class="row">
                    <div class="col-4">
                        <div class="card">
                            <a href="<?=SITE_PATH."/calculator"?>" title="<?=$lang26?>">
                                <img src="<?=SITE_PATH?>/assets/img/qeydiyat.png" title="<?=$lang26?>" alt="<?=$lang26?>">
                                <h6><?=$lang26?></h6>
                            </a>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="card">
                            <a href="<?=SITE_PATH."/booklet"?>" title="<?=$lang27?>">
                                <img src="<?=SITE_PATH?>/assets/img/beledci.png" title="<?=$lang27?>" alt="<?=$lang27?>">
                                <h6><?=$lang27?></h6>
                            </a>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="card">
                            <a href="<?=SITE_PATH."/duties"?>" title="<?=$lang28?>">
                                <img src="<?=SITE_PATH?>/assets/img/rusumlar.png" title="<?=$lang28?>" alt="<?=$lang28?>">
                                <h6><?=$lang28?></h6>
                            </a>
                        </div>
                    </div>
                    <div class="dropdown-divider">
                        
                    </div>
                    <div class="col-4">
                        <div class="card">
                            <a href="<?=SITE_PATH."/questions"?>" title="<?=$lang29?>">
                                <img src="<?=SITE_PATH?>/assets/img/suallar.png" title="<?=$lang29?>" alt="<?=$lang29?>">
                                <h6><?=$lang29?></h6>
                            </a>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="card">
                            <a href="<?=SITE_PATH."/documents"?>" title="<?=$lang30?>">
                                <img src="<?=SITE_PATH?>/assets/img/senedler.png" title="<?=$lang30?>" alt="<?=$lang30?>">
                                <h6><?=$lang30?></h6>
                            </a>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="card">
                            <a href="<?=SITE_PATH."/days"?>" title="<?=$lang31?>">
                                <img src="<?=SITE_PATH?>/assets/img/qebul.png" title="<?=$lang31?>" alt="<?=$lang31?>">
                                <h6><?=$lang31?></h6>
                            </a>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="news-mig">
                <div class="row">
                    <div class="col-8">
                        <div class="title-mig">
                            <h6>
                                <?=$lang32?>
                            </h6>
                        </div>
                        <?php
                            $sql_last_news = mysqli_query($db,"select `created_at`,`tip`,`name`,`auto_id`,`qisa_metn` from `blog` where `aktivlik`=1 and `lang_id`='$esas_dil' and `text`!='' order by `created_at` desc limit 4");
                            while($row_last_news=mysqli_fetch_assoc($sql_last_news))
                            {
                                ?>
                                <div class="card">
                                    <div class="row">
                                        <div class="col-4">
                                            <a href="<?= SITE_PATH . '/news-inner/' . slugGenerator($row_last_news['name']) . '-' . $row_last_news['auto_id'] ?>" title="<?=$row_last_news['name']?>">
                                                <img style="height: 90px;" src="<?=SITE_PATH?>/images/blog/<?=$row_last_news['auto_id'].".".$row_last_news['tip']?>" title="<?=$row_last_news['name']?>">
                                            </a>
                                        </div>
                                        <div class="col-8">
                                            <div>
                                                <a href="<?= SITE_PATH . '/news-inner/' . slugGenerator($row_last_news['name']) . '-' . $row_last_news['auto_id'] ?>" title="<?=$row_last_news['name']?>">
                                                    <?=$row_last_news['name']?>
                                                </a>
                                            </div>
                                            <?php
                                                if($lang==2)
                                                    $lang_short = 'ru';
                                                elseif($lang==3)
                                                    $lang_short = 'en';
                                                else
                                                    $lang_short = 'az';

                                                $news_month = getMonth(date("m",$row_last_news['created_at']),$lang_short);
                                            ?>
                                            <div class="float-left">
                                                <?=date("H:i")?>
                                            </div>
                                            <div class="float-right">
                                                <?= date("d",$row_last_news['created_at'])." ".$news_month." ".date('Y',$row_last_news['created_at']);?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        ?>
        <nav aria-label="Page navigation example" class="pagination-mig" style="padding:10px;">
            <a href="<?=SITE_PATH?>/news"><?=$lang33?></a>
        </nav>
    </div>
    <div class="col-4 link-mig">
        <?php
            $sql_rightlinks = mysqli_query($db, "SELECT `auto_id`,`basliq`,`tip`,`link` FROM `rightlinks` WHERE `lang_id`='$esas_dil' and `aktivlik`=1 order by `sira`");

            while($row_rightlinks=mysqli_fetch_assoc($sql_rightlinks))
            {
                ?>
                <a target="_blank" href="<?=$row_rightlinks['link']?>" title="<?=$row_rightlinks['basliq']?>">
                    <div style="background-image: url(<?=SITE_PATH?>/images/rightlinks/<?=$row_rightlinks['auto_id'].".".$row_rightlinks['tip']?>);">
                <span>
                    <?=$row_rightlinks['basliq']?>
                </span>
                    </div>
                </a>
                <?php
            }
        ?>
    </div>
</div>