<div class="row">
	<?php include "includes/left.php"; ?>
	<div class="col-8">
		<div class="news-mig"> 
			<div class="in-mig">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=$main_menu['link']?>"><?=$main_menu['name']?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span><?=$lang28?></span></li>
					</ol>
					<div class="rusum"> 
						<div class="alert alert-info" role="alert">
							<?=$sql_duties['qisa_metn']?>
						</div>
						<?=html_entity_decode($sql_duties['text'])?>
					</div>
				</nav>  
			</div>   
		</div>
	</div>
</div>