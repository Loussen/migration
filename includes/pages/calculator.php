<div class="row">
	<?php include "includes/left.php"; ?>
	<div class="col-8" id="loading">
		<div class="news-mig"> 
			<div class="in-mig hesabla">
                <img align="center" id="loading-image" src="<?=SITE_PATH?>/assets/img/load.gif   " alt="Loading..." style="display: none; position: fixed; z-index: 1; margin-left: 250px; ">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=SITE_PATH?>"><?=$main_menu['name']?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><span><?=$lang26?></span></li>
					</ol>
				</nav>
				<div class="alert alert-info" role="alert">
					<?=html_entity_decode($sql_calculator['text'])?>
				</div> 
				<div class="row">
					<div class="input-group mb-3 col-4"> 
						<select class="custom-select" id="month">
							<option value="0"><?=getMonth('01',$lang_short)?></option>
							<option value="1"><?=getMonth('02',$lang_short)?></option>
							<option value="2"><?=getMonth('03',$lang_short)?></option>
							<option value="3"><?=getMonth('04',$lang_short)?></option>
							<option value="4"><?=getMonth('05',$lang_short)?></option>
							<option value="5"><?=getMonth('06',$lang_short)?></option>
							<option value="6"><?=getMonth('07',$lang_short)?></option>
							<option value="7"><?=getMonth('08',$lang_short)?></option>
							<option value="8"><?=getMonth('09',$lang_short)?></option>
							<option value="9"><?=getMonth('10',$lang_short)?></option>
							<option value="10"><?=getMonth('11',$lang_short)?></option>
							<option value="11"><?=getMonth('12',$lang_short)?></option>
						</select>
					</div>
					<div class="input-group mb-3 col-4"> 
						<select class="custom-select" id="day">

						</select>
					</div> 
					<div class="col-4">
						<input type="text" class="form-control" placeholder="<?=date('Y')?>" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" id="year" value="<?=date('Y')?>" readonly>
					</div>
					<div class="col-6">
						<input type="text" class="form-control" placeholder="<?=$lang37?>" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" id="resCalc" disabled>
					</div>
					<div class="col-6">
						<button onclick="btnclear();" type="button" class="btn btn-info btn-sm"><?=$lang35?></button>
						<button onclick="btncalc();" type="button" class="btn btn-success btn-sm"><?=$lang36?></button>
					</div>
				</div>
			</div>   
		</div>
	</div>
</div>

<script type="text/javascript">
    selOnchange();

    function btncalc() {
        $('#loading-image').show();
        document.querySelector("#loading").style.opacity = 0.3;

        var selMonth = document.getElementById("month");
        var selDay = 15 + parseInt($("#day").val());
        var d = new Date($("#year").val(), $("#month").val(), selDay);

        if (d.getDay() == 6) {
            selDay = 2 + parseInt(d.getDate());
            d = new Date(d.getFullYear(), d.getMonth(), selDay);
        }
        ;
        if (d.getDay() == 0) {
            selDay = 1 + parseInt(d.getDate());
            d = new Date(d.getFullYear(), d.getMonth(), selDay);
        }
        ;

        var xDate = getHolidays(d.getDate(), d.getMonth(), d.getFullYear());

        setTimeout(function () {
            if (xDate.getFullYear() == 2018) {
                $("#resCalc").show().val("<?=$lang38?>: " + xDate.getDate() + "-" + selMonth.options[xDate.getMonth()].text + "-" + xDate.getFullYear()).css({
                    "background-color": "#5BC0DE", "color": "white"
                });
            } else {
                $("#resCalc").val("-").css({"background-color": "red", "color": "white"});
            }
            $('#loading-image').hide();
            document.querySelector("#loading").style.opacity = 1;
        }, 1000);
    }

    function selOnchange() {
        var m = document.getElementById("month").value;
        var y = document.getElementById("year").value;

        getDaysInMonth(parseInt(m), parseInt(y));
    }

    function getDaysInMonth(month, year) {
        // Since no month has fewer than 28 days
        var d = document.getElementById("day");
        // d.innerHTML = "";
        var date = new Date(year, month, 1);
        while (date.getMonth() === month) {
            var opt = document.createElement('option');
            opt.value = date.getDate();
            opt.innerHTML = date.getDate();
            d.appendChild(opt);
            date.setDate(date.getDate() + 1);
        }
    }

    function getHolidays(day, month, year) {
        var date = new Date(year, month, day);

        var arrJan = [1, 2, 3, 20];
        var arrFeb = [];
        var arrMar = [8, 20, 21, 22, 23, 24, 26];
        var arrApr = [];
        var arrMay = [9, 28];
        var arrJun = [15, 16, 18, 19, 26];
        var arrJul = [];
        var arrAug = [22, 23];
        var arrSep = [];
        var arrOct = [];
        var arrNov = [9];
        var arrDec = [31];
        if (date.getMonth() == 0) {
            arrJan.forEach(function (item) {
                if (date.getDate() == item && date.getMonth() == 0) {
                    date.setDate(item + 1);
                    if (date.getDay() == 0) {
                        date.setDate(parseInt(date.getDate()) + 1);
                    } else if (date.getDay() == 6) {
                        date.setDate(parseInt(date.getDate()) + 2);
                    }
                }
            });
        }
        if (date.getMonth() == 1) {
            arrFeb.forEach(function (item) {
                if (date.getDate() == item && date.getMonth() == 1) {
                    date.setDate(item + 1);
                    if (date.getDay() == 0) {
                        date.setDate(parseInt(date.getDate()) + 1);
                    } else if (date.getDay() == 6) {
                        date.setDate(parseInt(date.getDate()) + 2);
                    }
                }
            });
        }
        if (date.getMonth() == 2) {
            arrMar.forEach(function (item) {
                if (date.getDate() == item && date.getMonth() == 2) {
                    date.setDate(item + 1);
                    if (date.getDay() == 0) {
                        date.setDate(parseInt(date.getDate()) + 1);
                    } else if (date.getDay() == 6) {
                        date.setDate(parseInt(date.getDate()) + 2);
                    }
                }
            });
        }
        if (date.getMonth() == 3) {
            arrApr.forEach(function (item) {
                if (date.getDate() == item && date.getMonth() == 3) {
                    date.setDate(item + 1);
                    if (date.getDay() == 0) {
                        date.setDate(parseInt(date.getDate()) + 1);
                    } else if (date.getDay() == 6) {
                        date.setDate(parseInt(date.getDate()) + 2);
                    }
                }
            });
        }
        if (date.getMonth() == 4) {
            arrMay.forEach(function (item) {
                if (date.getDate() == item && date.getMonth() == 4) {
                    date.setDate(item + 1);
                    if (date.getDay() == 0) {
                        date.setDate(parseInt(date.getDate()) + 1);
                    } else if (date.getDay() == 6) {
                        date.setDate(parseInt(date.getDate()) + 2);
                    }
                }
            });
        }
        if (date.getMonth() == 5) {
            arrJun.forEach(function (item) {
                if (date.getDate() == item && date.getMonth() == 5) {
                    date.setDate(item + 1);
                    if (date.getDay() == 0) {
                        date.setDate(parseInt(date.getDate()) + 1);
                    } else if (date.getDay() == 6) {
                        date.setDate(parseInt(date.getDate()) + 2);
                    }
                }
            });
        }
        if (date.getMonth() == 6) {
            arrJul.forEach(function (item) {
                if (date.getDate() == item && date.getMonth() == 6) {
                    date.setDate(item + 1);
                    if (date.getDay() == 0) {
                        date.setDate(parseInt(date.getDate()) + 1);
                    } else if (date.getDay() == 6) {
                        date.setDate(parseInt(date.getDate()) + 2);
                    }
                }
            });
        }
        if (date.getMonth() == 7) {
            arrAug.forEach(function (item) {
                if (date.getDate() == item && date.getMonth() == 7) {
                    date.setDate(item + 1);
                    if (date.getDay() == 0) {
                        date.setDate(parseInt(date.getDate()) + 1);
                    } else if (date.getDay() == 6) {
                        date.setDate(parseInt(date.getDate()) + 2);
                    }
                }
            });
        }
        if (date.getMonth() == 8) {
            arrSep.forEach(function (item) {
                if (date.getDate() == item && date.getMonth() == 8) {
                    date.setDate(item + 1);
                    if (date.getDay() == 0) {
                        date.setDate(parseInt(date.getDate()) + 1);
                    } else if (date.getDay() == 6) {
                        date.setDate(parseInt(date.getDate()) + 2);
                    }
                }
            });
        }
        if (date.getMonth() == 9) {
            arrOct.forEach(function (item) {
                if (date.getDate() == item && date.getMonth() == 9) {
                    date.setDate(item + 1);
                    if (date.getDay() == 0) {
                        date.setDate(parseInt(date.getDate()) + 1);
                    } else if (date.getDay() == 6) {
                        date.setDate(parseInt(date.getDate()) + 2);
                    }
                }
            });
        }
        if (date.getMonth() == 10) {
            arrNov.forEach(function (item) {
                if (date.getDate() == item && date.getMonth() == 10) {
                    date.setDate(item + 1);
                    if (date.getDay() == 0) {
                        date.setDate(parseInt(date.getDate()) + 1);
                    } else if (date.getDay() == 6) {
                        date.setDate(parseInt(date.getDate()) + 2);
                    }
                }
            });
        }
        if (date.getMonth() == 11) {
            arrDec.forEach(function (item) {
                if (date.getDate() == item && date.getMonth() == 11) {
                    date.setDate(item + 1);
                    if (date.getDay() == 0) {
                        date.setDate(parseInt(date.getDate()) + 1);
                    } else if (date.getDay() == 6) {
                        date.setDate(parseInt(date.getDate()) + 2);
                    }
                }
            });
        }

        return date;
    }

    function btnclear() {
        $("#month").val("0");
        $("#year").val("2018");
        $("#day").val("1");
        $("#resCalc").val("");
    }
</script>
