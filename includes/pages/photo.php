<div class="row">
	<?php include "includes/left.php"; ?>
	<div class="col-8">
		<div class="photo-mig"> 
			<div class="in-mig foto">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=SITE_PATH."/".$parent_menu['link']?>"><?=$parent_menu['name']?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><span><?=$menyu['name']?></span></li>
					</ol>
				</nav>  
				<div class="row">
					<?php
						while($row_photo=mysqli_fetch_assoc($sql_photo))
                    	{
                    		?>
                    		<div class="col-4">
								<a href="<?= SITE_PATH . '/photo-inner/' . slugGenerator($row_photo['basliq']) . '-' . $row_photo['auto_id'] ?>" title="<?=$row_photo['basliq']?>">
									<img style="height: 144px;" src="<?=SITE_PATH?>/images/albom/<?=$row_photo['auto_id'].".".$row_photo['tip']?>" title="<?=$row_photo['basliq']?>" alt="<?=$row_photo['basliq']?>">
									<hr/>
									<div>
										<span>
											<?php
	                                            if($lang==2)
	                                                $lang_short = 'ru';
	                                            elseif($lang==3)
	                                                $lang_short = 'en';
	                                            else
	                                                $lang_short = 'az';

	                                            $photo_month = getMonth(date("m",$row_photo['tarix']),$lang_short);

	                                            echo date("d",$row_photo['tarix'])." ".$photo_month." ".date('Y',$row_photo['tarix']);
	                                        ?>
                                        </span>
										<p><?=more_string($row_photo['basliq'],75)?></p>
									</div>
								</a>
							</div>
                			<?php
                    	}
					?>
				</div>
				<?php
                    if($count_rows > $limit)
                    {
                        $show= 5;
                        ?>
                        <nav aria-label="Page navigation example" class="pagination-mig">
                            <ul class="pagination">
                                <?php
                                    if($page>1)
                                    {
                                        ?>
                                        <li class="page-item">
                                            <a class="page-link" href="<?= SITE_PATH . '/photo/' . ($page - 1)?>" aria-label="Previous">
                                                <span aria-hidden="true">«</span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </li>
                                        <?php
                                    }

                                    for ($i = $page - $show; $i <= $page + $show; $i++)
                                    {
                                        if ($i > 0 && $i <= $max_page)
                                        {
                                            if ($i == $page)
                                            {
                                                ?>
                                                <li class="page-item active">
                                                    <a class="page-link" href="javascript:void(0)"><?= $i ?></a>
                                                </li>
                                                <?php
                                            }
                                            else
                                            {
                                            ?>
                                                <li class="page-item">
                                                    <a class="page-link" href="<?= SITE_PATH . '/photo/' . $i ?>" title="<?=$i?>"><?= $i ?></a>
                                                </li>
                                                <?php
                                            }
                                        }
                                    }
                                    if ($page < $max_page)
                                    {
                                        ?>
                                        <li class="page-item">
                                            <a class="page-link" aria-label="Next" href="<?= SITE_PATH . '/photo/' . ($page + 1) ?>" title="Next">
                                                <span aria-hidden="true">»</span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                ?>
<!--                                <li class="page-item active"><a class="page-link" href="#">1</a></li>-->
<!--                                <li class="page-item"><a class="page-link" href="#">2</a></li>-->
<!--                                <li class="page-item"><a class="page-link" href="#">3</a></li>-->
<!--                                <li class="page-item"><a class="page-link" href="#">4</a></li>-->
<!--                                <li class="page-item"><a class="page-link" href="#">5</a></li>-->
<!--                                <li class="page-item"><a class="page-link" href="#">6</a></li>-->
<!--                                <li class="page-item"><a class="page-link" href="#">7</a></li>-->
<!--                                <li class="page-item">-->
<!--                                    <a class="page-link" href="#" aria-label="Next">-->
<!--                                        <span aria-hidden="true">»</span>-->
<!--                                        <span class="sr-only">Next</span>-->
<!--                                    </a>-->
<!--                                </li>-->
                            </ul>
                        </nav>
                        <?php
                    }
                ?>
			</div>  
		</div>
	</div>
</div>