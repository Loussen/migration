<div class="row">
	<?php include "includes/left.php"; ?>
	<div class="col-8">
		<div class="news-mig"> 
			<div class="in-mig sual">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?=$main_menu['link']?>"><?=$main_menu['name']?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><span><?=$lang29?></span></li>
					</ol>
				</nav> 
				<div class="accordion" id="accordionExample">
                    <?php
                        $i = 1;
                        while($row_questions=mysqli_fetch_assoc($sql_questions))
                        {
                            ?>
                            <div class="card">
                                <div class="card-header" id="heading<?=$i?>">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?=$i?>" aria-expanded="true" aria-controls="collapse<?=$i?>">
                                            <?=$row_questions['name']?>
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapse<?=$i?>" class="collapse" aria-labelledby="heading<?=$i?>" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <?=html_entity_decode($row_questions['text'])?>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $i++;
                        }
                    ?>
				</div>
			</div>    
		</div>
	</div>
</div>