<div class="row">
	<?php include "includes/left.php"; ?>
	<div class="col-8">
		<div class="news-mig"> 
			<div class="in-mig">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=SITE_PATH."/".$parent_menu['link']?>"><?=$parent_menu['name']?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span><?=$menyu['name']?></span></li>
					</ol>
				</nav>
				<div class="tap">
					<iframe width="100%" src="<?=$sql_contact['google_map']?>" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					<div class="alert alert-info">
						<?=html_entity_decode($sql_contact['text'])?>
					</div>
				</div> 
			</div>   
		</div>  
	</div>
</div>