<div class="row">
	<?php include "includes/left.php"; ?>
	<div class="col-8">
		<div class="news-mig"> 
			<div class="in-mig">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?=$main_menu['link']?>"><?=$main_menu['name']?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><span><?=$lang9?></span></li>
					</ol>
				</nav>
				<div class="card beledci">
                    <?php
                        while($row_faydali_alt=mysqli_fetch_assoc($sql_faydali_alt))
                        {
                            ?>
                            <a href="<?= SITE_PATH . '/helpful-inner/' . slugGenerator($row_faydali_alt['basliq']) . '-' . $row_faydali_alt['auto_id'] ?>" title="<?=$row_faydali_alt['basliq']?>">
                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                <?=$row_faydali_alt['basliq']?>
                                <span class="pull-right btn btn-primary btn-xs"><?=$lang34?></span>
                            </a>
                            <?php
                        }
                    ?>
				</div> 
			</div>  
		</div>
	</div>
</div>