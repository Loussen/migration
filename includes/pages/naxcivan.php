<div class="in-mig">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?=SITE_PATH?>"><?=$main_menu['name']?></a></li>
            <li class="breadcrumb-item active" aria-current="page"><span><?=$lang3?></span></li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-4">
            <div class="list-group" id="list-tab" role="tablist">
                <?php
                    $i=1;
                    while($row_naxcivan=mysqli_fetch_assoc($sql_naxcivan))
                    {
                        $class_active = ($i==1) ? "active show" : "";
                        ?>
                        <a class="list-group-item list-group-item-action <?=$class_active?>" id="list-<?=slugGenerator($row_naxcivan['name'])?>-list" data-toggle="list" href="#list-<?=slugGenerator($row_naxcivan['name'])?>" role="tab" aria-controls="home"><?=$row_naxcivan['name']?></a>
                        <?php
                        $i++;
                    }
                ?>
            </div>
        </div>
        <div class="col-8">
            <div class="tab-content" id="nav-tabContent">
                <?php
                    $j=1;
                    while($row_naxcivan_in=mysqli_fetch_assoc($sql_naxcivan_in))
                    {
                        $class_active_in = ($j==1) ? "show active" : "";
                        ?>
                        <div class="tab-pane fade <?=$class_active_in?>" id="list-<?=slugGenerator($row_naxcivan_in['name'])?>" role="tabpanel" aria-labelledby="list-<?=slugGenerator($row_naxcivan_in['name'])?>-list">
                            <?=html_entity_decode($row_naxcivan_in['text'])?>
                        </div>
                        <?php
                        $j++;
                    }
                ?>
            </div>
        </div>
    </div>
</div>