<div class="row">
    <?php include "includes/left.php"; ?>
    <div class="col-8">
        <div class="news-mig">
            <div class="in-mig cagri">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=$main_menu['link']?>"><?=$main_menu['name']?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span><?=$lang7?></span></li>
                    </ol>
                </nav>
                <?=html_entity_decode($row_callcenter['text'])?>
            </div>
        </div>
    </div>
</div>