<div class="row">
	<?php include "includes/left.php"; ?>
	<div class="col-8">
		<div class="news-mig"> 
			<div class="in-mig qanun">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?=SITE_PATH?>"><?=$main_menu['name']?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><span><?=$lang72?></span></li>
					</ol>
				</nav> 
				<div class="card">
					<table class="table table-hover"> 
						<tbody>
                            <?php
                                while($row_sub_acts=mysqli_fetch_assoc($sql_acts))
                                {
                                    if($row_sub_acts['act_id']==2)
                                        $type_act = 'nax';
                                    elseif($row_sub_acts['act_id']==3)
                                        $type_act = 'intern';
                                    else
                                        $type_act = 'az';

                                    ?>
                                    <tr>
                                        <td><a href="<?=SITE_PATH."/images/acts/".$type_act."/".$row_sub_acts['auto_id'].".".$row_sub_acts['tip']?>"><?=$row_sub_acts['basliq']?></a></td>
                                        <td><a href="<?=SITE_PATH."/images/acts/".$type_act."/".$row_sub_acts['auto_id'].".".$row_sub_acts['tip']?>"><i class="fa fa-external-link pull-right" aria-hidden="true"></i></a></td>
                                    </tr>
                                    <?php
                                }
                            ?>
						</tbody>
					</table>
				</div>
			</div>   
		</div>
	</div>
</div>