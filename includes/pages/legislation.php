<div class="row">
	<?php include "includes/left.php"; ?>
	<div class="col-8">
		<div class="news-mig"> 
			<div class="in-mig qanun">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?=SITE_PATH?>"><?=$main_menu['name']?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><span><?=$menyu['name']?></span></li>
					</ol>
				</nav> 
				<div class="card">
					<table class="table table-hover"> 
						<tbody>
                            <?php
                                $query = mysqli_query($db,"select * from `acts_sub` where `act_id`='$act_id' and `aktivlik`=1 and `lang_id`='$esas_dil'");

                                if(mysqli_num_rows($query)==0)
                                {
                                    while($row_acts=mysqli_fetch_assoc($sql_acts))
                                    {
                                        if($row_acts['act_id']==2)
                                            $type_act = 'nax';
                                        elseif($row_acts['act_id']==3)
                                            $type_act = 'intern';
                                        else
                                            $type_act = 'az';

                                        ?>
                                        <tr>
                                            <td><a target="_blank" href="<?=SITE_PATH."/images/acts/".$type_act."/".$row_acts['auto_id'].".".$row_acts['tip']?>"><?=$row_acts['basliq']?></a></td>
                                            <td><a target="_blank" href="<?=SITE_PATH."/images/acts/".$type_act."/".$row_acts['auto_id'].".".$row_acts['tip']?>"><i class="fa fa-external-link pull-right" aria-hidden="true"></i></a></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                else
                                {
                                    while($row_sub_acts=mysqli_fetch_assoc($query))
                                    {
                                        ?>
                                        <tr>
                                            <td><a href="<?= SITE_PATH . '/legislation-inner/' . slugGenerator($row_sub_acts['basliq']) . '-' . $row_sub_acts['auto_id'] ?>"><?=$row_sub_acts['basliq']?></a></td>
                                            <td><a href="<?= SITE_PATH . '/legislation-inner/' . slugGenerator($row_sub_acts['basliq']) . '-' . $row_sub_acts['auto_id'] ?>"><i class="fa fa-external-link pull-right" aria-hidden="true"></i></a></td>
                                        </tr>
                                        <?php
                                    }
                                }
                            ?>
						</tbody>
					</table>
				</div>
			</div>   
		</div>
	</div>
</div>