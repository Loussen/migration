<div class="row">
    <?php include "includes/left.php"; ?>
    <div class="col-8">
        <div class="news-mig">
            <div class="in-mig karabag">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=$main_menu['link']?>"><?=$main_menu['name']?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span><?=$lang41?></span></li>
                    </ol>
                </nav>
                <div class="card">
                    <img width="100%" class="img-responsive" src="<?=SITE_PATH?>/images/karabag/<?=$row_karabag['auto_id'].".".$row_karabag['tip']?>"> <br/>
                    <div class="row">
                        <div class="col-12">
                            <?=html_entity_decode($row_karabag['text'])?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>