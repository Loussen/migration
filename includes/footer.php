<footer>
    <div class="container">
        <div class="row">
            <div class="col-3">
                <h6>
                    <span><?=$lang14?></span><br/>
                    <?=$lang15?>
                </h6>
                <span>
                    <?=$lang16?>
                </span>
            </div>
            <div class="col-3">
                <ul>
                    <?php
                        $sql_footer_menus = mysqli_query($db,"select `id`,`name`,`link`,`parent_auto_id`,`auto_id` from `menyular` where `lang_id`='$esas_dil' and `aktivlik`=1 and `parent_auto_id`=0 order by `sira`");
                        while($row_footer_menus=mysqli_fetch_assoc($sql_footer_menus))
                        {
                            if($row_footer_menus['link']=='/') continue;

                            $row_footer_submenus = mysqli_fetch_assoc(mysqli_query($db,"select `link` from `menyular` where `lang_id`='$esas_dil' and `aktivlik`=1 and `parent_auto_id`='$row_footer_menus[auto_id]' order by `sira` ASC LIMIT 1"));
                            ?>
                            <li>
                                <a href="<?=SITE_PATH."/".$row_footer_submenus['link']?>" title="<?=$row_footer_menus['name']?>">
                                    <?=$row_footer_menus['name']?>
                                </a>
                            </li>
                            <?php
                        }
                    ?>
                </ul>
            </div>
            <div class="col-2">
                <table class="table"> 
                    <tbody>
                        <tr>
                            <td><?=$lang17?></td>
                            <td>240</td> 
                        </tr>
                        <tr>
                            <td><?=$lang18?></td>
                            <td>1575</td> 
                        </tr>
                        <tr>
                            <td><?=$lang19?></td>
                            <td>10058</td> 
                        </tr>
                        <tr>
                            <td><?=$lang20?></td>
                            <td>10058</td>
                        </tr>
                        <tr>
                            <td><?=$lang21?></td>
                            <td>178326</td> 
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-4">
                <ul>
                    <li>
                        <p>
                            <?=$lang22?>: <?=$sql_contact['address']?>
                        </p>
                    </li>
                    <li>
                        <p>
                            <?=$lang23?>: <?=$sql_contact['phone']?>
                        </p>
                    </li>
                    <li>
                        <p>
                            <?=$lang24?>:    <?=$sql_contact['fax']?>
                        </p>
                    </li>
                    <li>
                        <p>
                            <?=$lang25?>: <a href="mailto:<?=$sql_contact['email']?>"><?=$sql_contact['email']?></a>
                        </p>
                    </li>
                </ul> 
            </div>
        </div>
    </div>
</footer>
<script src="<?=SITE_PATH?>/assets/js/jquery.min.js"></script>
<script src="<?=SITE_PATH?>/assets/js/bootstrap.min.js"></script>
<?php
    if($do=='letter')
    {
        ?>
        <script src="<?=SITE_PATH?>/assets/js/fileinput.min.js"></script>
        <?php
    }
?>
<!-- Magnific Popup core JS file -->
<script src="<?=SITE_PATH?>/assets/js/jquery.magnific-popup.min.js"></script>

<script>
    $(document).ready(function() {
        $('ul.gallerybox').each(function() { // the containers for all your galleries
            $(this).magnificPopup({
                delegate: 'a', // the selector for gallery item
                type: 'image',
                gallery: {
                  enabled:true
                }
            });
        });

        $('.popup-youtube').magnificPopup({
          disableOn: 700,
          type: 'iframe',
          mainClass: 'mfp-fade',
          removalDelay: 160,
          preloader: false,

          fixedContentPos: false
        });
    });
</script>
<script type="text/javascript"> 
    $(function () {
        $("#topx").css({
            position: 'fixed'
        }).hide(); 
        $(window).scroll(function () {
            if ($(this).scrollTop() > 150) {
                $('#topx').fadeIn(700);
            } else {
                $('#topx').fadeOut(700);
            }
        });
    });
</script>