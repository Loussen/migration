-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 04, 2018 at 02:27 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `migration`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(3, 'HAQQIMIZDA', '', '', '', 'jpg', '&lt;p dir=&quot;ltr&quot;&gt;&lt;span&gt;A.SET Kredit &amp;ndash; FinTrend BOKT bazasında yaradılmış universal bir məhsuldur. Həmin məhsuldan istifadə etməklə siz partnyor elektronika və məişət texnikasının ticarət şəbəkələrində, mebel mağazalarında, avtomobillərin servis mərkəzlərinda istənilən məhsulu və ya xidməti Sizə maksimal sərfəli şərtlərlə kreditlə əldə edə bilərsiniz. Bu məhsulun əsas &amp;uuml;st&amp;uuml;nl&amp;uuml;y&amp;uuml; mağazadan ayrılmadan kreditə mal və xidmətlərin alınmasıdır.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;span&gt;Bu g&amp;uuml;n biz, Sizə istehlak krediti bazarında se&amp;ccedil;ilən ilkin &amp;ouml;dənişsiz m&amp;uuml;ddəti 30 aya qədər olan rəqabət şərtləri ilə se&amp;ccedil;ilən kredit təklif edirik.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;span&gt;Siz tərəfdaşlarımız barədə ətraflı məlumatı PARTNYORLAR b&amp;ouml;l&amp;uuml;m&amp;uuml;ndən əldə edə bilərsiz.&lt;/span&gt;&lt;/p&gt;', 0, 2, 1, 1, 2, 0),
(4, 'О НАС', '', '', '', 'jpg', '&lt;p dir=&quot;ltr&quot;&gt;&lt;span&gt;A.SET Kredit &amp;ndash; это универсальный продукт создан на базе BOKT FINtrend, с помощью которого Вы сможете купить товар и/или услугу в кредит, во всех торговых сетях партнерах электроники и бытовой техники, мебели, автозапчастей, автомобильных сервисных центрах с максимально выгодными для Вас условиями. &lt;/span&gt;&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;span&gt;Основное преимущество данного продукта &amp;ndash; это приобретение товара или услуги в кредит прямо в магазине.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;span&gt;На сегодняшний день мы предлагаем конкурентные условия работы на рынке потребительского кредитования со сроком кредитования до 36 месяцев без первого взноса.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;span&gt;Вы можете детально ознакомится со всеми нашими партнерами в разделе ПАРТНЕРЫ.&lt;/span&gt;&lt;/p&gt;', 0, 2, 1, 2, 2, 0),
(5, 'ABOUT COMPANY', '', '', '', '', '&lt;p dir=&quot;ltr&quot;&gt;A.SET Kredit &amp;ndash; это универсальный продукт создан на базе BOKT FINtrend, с помощью которого Вы сможете купить товар и/или услугу в кредит, во всех торговых сетях партнерах электроники и бытовой техники, мебели, автозапчастей, автомобильных сервисных центрах с максимально выгодными для Вас условиями.&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;Основное преимущество данного продукта &amp;ndash; это приобретение товара или услуги в кредит прямо в магазине.&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;На сегодняшний день мы предлагаем конкурентные условия работы на рынке потребительского кредитования со сроком кредитования до 36 месяцев без первого взноса.&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;Вы можете детально ознакомится со всеми нашими партнерами в разделе ПАРТНЕРЫ.&lt;/p&gt;', 0, 2, 1, 3, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `acts`
--

CREATE TABLE `acts` (
  `id` int(11) NOT NULL,
  `basliq` varchar(255) NOT NULL,
  `act_id` int(11) NOT NULL,
  `tip` varchar(5) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acts`
--

INSERT INTO `acts` (`id`, `basliq`, `act_id`, `tip`, `sira`, `aktivlik`, `lang_id`, `auto_id`) VALUES
(1, 'sds adasdas dasdasd asd as', 1, 'pdf', 1, 1, 1, 1),
(2, 'sds adasdas dasdasd asd as', 1, 'pdf', 1, 1, 2, 1),
(3, 'sds adasdas dasdasd asd as', 1, 'pdf', 1, 1, 3, 1),
(4, 'sds adasdas dasdasd asd as656', 2, 'pdf', 2, 1, 1, 2),
(5, 'sds adasdas dasdasd asd as656', 2, 'pdf', 2, 1, 2, 2),
(6, 'sds adasdas dasdasd asd as656', 2, 'pdf', 2, 1, 3, 2),
(7, 'sds adasdas dasdasd asd as intern', 3, 'pdf', 3, 1, 1, 3),
(8, 'sds adasdas dasdasd asd as intern', 3, 'pdf', 3, 1, 2, 3),
(9, 'sds adasdas dasdasd asd as intern', 3, 'pdf', 3, 1, 3, 3),
(10, 'sds adasdas dasdasd asd as trttr', 1, 'pdf', 4, 1, 1, 4),
(11, 'sds adasdas dasdasd asd as trttr', 1, 'pdf', 4, 1, 2, 4),
(12, 'sds adasdas dasdasd asd as trttr', 1, 'pdf', 4, 1, 3, 4);

-- --------------------------------------------------------

--
-- Table structure for table `admin_pass`
--

CREATE TABLE `admin_pass` (
  `id` int(11) NOT NULL,
  `login` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_pass`
--

INSERT INTO `admin_pass` (`id`, `login`, `pass`) VALUES
(1, 'aset', '26a66966faa6d0723d3dc7392ca93aa5');

-- --------------------------------------------------------

--
-- Table structure for table `alboms`
--

CREATE TABLE `alboms` (
  `id` int(10) NOT NULL,
  `tarix` int(11) NOT NULL,
  `basliq` varchar(255) DEFAULT NULL,
  `tam_metn` text,
  `tip` varchar(5) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) DEFAULT '0',
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `alboms`
--

INSERT INTO `alboms` (`id`, `tarix`, `basliq`, `tam_metn`, `tip`, `sira`, `aktivlik`, `lang_id`, `auto_id`) VALUES
(1, 1537369028, 'Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi1', '&lt;p&gt;&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&lt;/p&gt;', 'jpg', 2, 1, 1, 1),
(2, 1537369029, 'Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi 2', '&lt;p&gt;&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi 2&lt;/p&gt;', 'jpg', 2, 1, 2, 1),
(3, 1537369029, 'Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi 3', '&lt;p&gt;&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&amp;nbsp;Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi3&lt;/p&gt;', 'jpg', 2, 1, 3, 1),
(4, 1537370014, 'Galeery', '&lt;p&gt;asad sad sad sad asdsa&lt;/p&gt;', 'jpeg', 1, 1, 1, 2),
(5, 1537370014, 'Galeery', '', 'jpeg', 1, 1, 2, 2),
(6, 1537370014, 'Galeery', '', 'jpeg', 1, 1, 3, 2),
(7, 1538299093, 'Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi', '&lt;p&gt;Azərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual MəcəlləsiAzərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi&lt;/p&gt;', 'jpg', 3, 1, 1, 3),
(8, 1538299093, 'Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi', '', 'jpg', 3, 1, 2, 3),
(9, 1538299093, 'Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi', '', 'jpg', 3, 1, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(3) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` longtext NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, 'Qloballaşan dünyada miqrasiya', '', '', '', '', '&lt;p&gt;&lt;span&gt;&lt;em&gt;19 mart - miqrasiya orqanları iş&amp;ccedil;ilərinin peşə bayramı g&amp;uuml;n&amp;uuml;d&amp;uuml;r. On bir il əvvəl məhz həmin g&amp;uuml;n Azərbaycan Respublikasının Prezidenti İlham Əliyevin imzaladığı 560 saylı Fərmana əsasən, D&amp;ouml;vlət Miqrasiya Xidməti yaradılıb.&lt;/em&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;em&gt;&lt;a href=&quot;http://azertag.az/&quot; target=&quot;_blank&quot;&gt;AZƏRTAC&lt;/a&gt;&amp;nbsp;&amp;ouml;tən 11 il ərzində bu qurumun fəaliyyəti, eləcə də &amp;ouml;lkəmizdə miqrasiya proseslərinin idarə olunması və tənzimlənməsi sahəsində g&amp;ouml;r&amp;uuml;lən işlərlə bağlı D&amp;ouml;vlət Miqrasiya Xidmətinin rəisi Firudin Nəbiyevin məqaləsini təqdim edir.&lt;/em&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Təməli ulu &amp;ouml;ndər Heydər Əliyev tərəfindən qoyulmuş d&amp;ouml;vlətimizin inkişafı kursu &amp;ouml;tən 15 ildə Prezident İlham Əliyevin rəhbərliyi ilə m&amp;uuml;vəffəqiyyətlə həyata ke&amp;ccedil;irilib. D&amp;uuml;nyada baş verən siyasi və iqtisadi təlat&amp;uuml;mlərə, qlobal b&amp;ouml;hranlara baxmayaraq, &amp;ouml;lkəmizdə sabitlik və intensiv inkişaf təmin edilib, bir s&amp;ouml;zlə, &amp;ouml;tən d&amp;ouml;vr Azərbaycan &amp;uuml;&amp;ccedil;&amp;uuml;n uğurlu olub. Bu uğurlar, nailiyyətlər təkcə &amp;ouml;lkənin daxilindən yox, həm də xaricdən də aydın g&amp;ouml;r&amp;uuml;nməkdədir. Azərbaycan a&amp;ccedil;ıq &amp;ouml;lkədir, iqtisadiyyatına kifayət qədər xarici investisiya yatırılıb. Bu g&amp;uuml;n Azərbaycan beynəlxalq tədbirlərin ke&amp;ccedil;irildiyi məkana &amp;ccedil;evrilib.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&amp;Uuml;mumbəşəri dəyişikliklər və b&amp;ouml;y&amp;uuml;k əhəmiyyət kəsb edən hallar fonunda d&amp;uuml;nyada miqrasiya axınının intensiv genişlənməsi m&amp;uuml;şahidə olunur. Miqrant adətən ləyaqətli həyat, təminatlı və rahat iş axtarır. Elə bunun nəticəsidir ki, ləyaqətli həyat axtarışı ilə sabit, inkişaf edən, təhl&amp;uuml;kəsizliyin y&amp;uuml;ksək səviyyədə təmin edildiyi bir &amp;ouml;lkənin vətəndaşı olmaq, Azərbaycanda yaşamaq, təhsil almaq istəyi ilə respublikamıza təşrif buyuranların, turist kimi səyahətə gələnlərin sayı ildən-ilə artmaqdadır.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;S&amp;ouml;z yox ki, bəşəriyyət tarixində həmişə b&amp;ouml;y&amp;uuml;k rol oynayan miqrasiya prosesi m&amp;uuml;asir beynəlxalq m&amp;uuml;nasibətlər sistemində &amp;ouml;z&amp;uuml;n&amp;uuml; qabarıq n&amp;uuml;mayiş etdirən problemlərdən birinə &amp;ccedil;evrilib. Qanuni və qanunsuz, beynəlxalq və &amp;ouml;lkədaxili, k&amp;ouml;n&amp;uuml;ll&amp;uuml; və məcburi, daimi və m&amp;uuml;vəqqəti, qa&amp;ccedil;qın və sığınacaq axtaranlar, ailə birliyi və &amp;ldquo;beyin axını&amp;rdquo; kriteriyaları ilə təsnifatlaşdırılan miqrasiyanın coğrafiyası və həcmi genişlənib, miqrasiyanın yeni tipləri - y&amp;uuml;ksək təsnifatlı kadrların miqrasiyası və &amp;ccedil;ox vaxt qanunsuz miqrasiyaya əsas verən ixtisassız şəxslərin miqrasiyası formalaşıb. Məcburi miqrasiya tendensiyası artıb, miqrasiya d&amp;uuml;nyada sosial dəyişikliklərin bir faktoru kimi diqqət &amp;ccedil;əkməyə başlayıb. S&amp;ouml;z yox ki, miqrasiya demoqrafik proseslərin formalaşmasında m&amp;uuml;h&amp;uuml;m rol oynayır və əhalinin etnik-mədəni tərkibini yaradır. Reallıq budur ki, bu g&amp;uuml;n beynəlxalq m&amp;uuml;nasibətlər sistemində miqrasiya geosiyasəti artıq yeni formalaşan bir istiqamət statusu qazanıb.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Beynəlxalq birliyin bərabərh&amp;uuml;quqlu &amp;uuml;zv&amp;uuml; olan &amp;ouml;lkəmizdə də m&amp;uuml;asir d&amp;uuml;nyanın bu aktual m&amp;ouml;vzusunun b&amp;ouml;y&amp;uuml;k əhəmiyyətinə kifayət qədər ciddi yanaşılmaqdadır.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Hələ d&amp;uuml;nyada miqrasiya məsələsi indiki qədər aktual olmadığı bir vaxtda - 2007-ci il martın 19-da Prezident İlham Əliyev m&amp;uuml;stəqil mərkəzi icra hakimiyyəti orqanı olan D&amp;ouml;vlət Miqrasiya Xidmətinin yaradılması haqqında Fərman imzalayıb. &amp;Ouml;lkə rəhbərinin diqqət və qayğısı nəticəsində indi Xidmətin istər mərkəzi aparatı, istərsə də regional miqrasiya idarələri ən m&amp;uuml;asir xidmət imkanları ilə təchiz edilib, m&amp;uuml;raciət edənlərə peşəkar xidmət g&amp;ouml;stərilməsi &amp;uuml;&amp;ccedil;&amp;uuml;n hər c&amp;uuml;r şərait yaradılıb.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&amp;Ouml;tən d&amp;ouml;vr ərzində &amp;ouml;lkəmizdə miqrasiya idarəetmə mexanizmi təkmilləşdirilib, respublikaya gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin yaşayış yeri &amp;uuml;zrə qeydiyyata alınmaları və onların &amp;ouml;lkə ərazisində qanuni əsaslarla yaşamalarının təmin olunması &amp;uuml;&amp;ccedil;&amp;uuml;n m&amp;uuml;vafiq icazələrin verilməsi prosedurları sadələşdirilib, nəticədə bu sahədə şəffaflığın tam təmin edilməsinə nail olunub.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Azərbaycanın həyata ke&amp;ccedil;irdiyi d&amp;ouml;vlət siyasətində miqrasiya məsələləri prioritet yer tutur. Prezident İlham Əliyevin 2012-ci il 29 dekabr tarixli Fərmanı ilə təsdiq edilmiş &amp;ldquo;Azərbaycan 2020: gələcəyə baxış&amp;rdquo; İnkişaf Konsepsiyası yaxın perspektivlər &amp;uuml;&amp;ccedil;&amp;uuml;n d&amp;ouml;vlətimizin inkişaf hədəflərini və prioritetlərini m&amp;uuml;əyyən edən rəsmi d&amp;ouml;vlət sənədi kimi &amp;ouml;z&amp;uuml;ndə miqrasiya problemi ilə bağlı məsələləri də əks etdirir. 11 b&amp;ouml;lmədən ibarət Konsepsiyada d&amp;ouml;vlətin milli təhl&amp;uuml;kəsizliyinin prioritet istiqaməti kimi miqrasiya ilə bağlı m&amp;uuml;ddəalara m&amp;uuml;h&amp;uuml;m yer ayrılır. Ona g&amp;ouml;rə ki, geniş imkanlarla yanaşı, &amp;ouml;z&amp;uuml; ilə b&amp;ouml;y&amp;uuml;k riskləri də gətirən qloballaşma beynəlxalq miqyasda istehsalın, ticarətin, kapital axınının və əmək miqrasiyasının sərbəstləşməsi tendensiyalarını kəskin artırıb. Bu baxımdan konsepsiyada problemin mahiyyətini a&amp;ccedil;mağa və qarşıya qoyulan məqsədə &amp;ccedil;atmağa nail olmaq &amp;uuml;&amp;ccedil;&amp;uuml;n d&amp;uuml;zg&amp;uuml;n olaraq m&amp;uuml;əyyən edilib ki, əhali və onun strukturu ilə bağlı kəmiyyət və keyfiyyət g&amp;ouml;stəriciləri milli g&amp;uuml;c&amp;uuml;n əsas elementləridir və təbii olaraq, Azərbaycan xalqının genofondunun qorunması, miqrasiya və demoqrafiya kimi məsələlər milli təhl&amp;uuml;kəsizlik baxımından olduqca m&amp;uuml;h&amp;uuml;md&amp;uuml;r. Konsepsiyanın əhatə etdiyi d&amp;ouml;vrdə &amp;ouml;lkə əhalisinin hər il orta hesabla 1,1 faiz artaraq, 2020-ci ildə təxminən 10,2 milyon nəfərə &amp;ccedil;atacağı g&amp;ouml;zlənilir. Konsepsiyada nəzərdə tutulmuş məqsədlərə &amp;ccedil;atmaq &amp;uuml;&amp;ccedil;&amp;uuml;n qanunvericilik bazasının m&amp;uuml;təmadi olaraq təkmilləşdirilməsi və m&amp;uuml;vafiq institusional potensialın g&amp;uuml;cləndirilməsi istiqamətində tədbirlər davam etdirilir. Bu m&amp;uuml;tərəqqi sənəddə xaricdən iş&amp;ccedil;i q&amp;uuml;vvəsinin &amp;ouml;lkəyə qanunsuz axınının qarşısının alınması, xarici &amp;ouml;lkələrdə &amp;ccedil;alışan vətəndaşlarımızın sosial m&amp;uuml;dafiəsinin g&amp;uuml;cləndirilməsi ilə bağlı m&amp;uuml;vafiq işlərin g&amp;ouml;r&amp;uuml;lməsi də prioritet kimi m&amp;uuml;əyyən edilib. Əlbəttə, D&amp;ouml;vlət Miqrasiya Xidməti bu istiqamətdə də &amp;ouml;z &amp;uuml;zərinə d&amp;uuml;şən vəzifələrin layiqincə yerinə yetirilməsi &amp;uuml;&amp;ccedil;&amp;uuml;n səylərini əsirgəmir.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Prezident İlham Əliyevin rəhbərliyi ilə Azərbaycanın miqrasiya siyasəti insan və vətəndaş h&amp;uuml;quqlarına və azadlıqlarına h&amp;ouml;rmət, qanun&amp;ccedil;uluq, qanun qarşısında bərabərlik və ədalətlilik, miqrasiya qanunvericiliyinin hamılıqla tanınan beynəlxalq h&amp;uuml;quq normalarına uyğunluğunun təmin edilməsi, miqrasiya proseslərinin tənzimlənməsində innovativ metodların tətbiqi və şəffaflığın təmin edilməsi prinsiplərinə əsaslanır.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Bu g&amp;uuml;n d&amp;ouml;vlətimizin baş&amp;ccedil;ısının vətəndaşlara qayğı ilə m&amp;uuml;nasibət g&amp;ouml;stərilməsi tələbinin yerinə yetirilməsi, əməkdaşlar tərəfindən etik davranış qaydalarına d&amp;uuml;r&amp;uuml;st əməl edilməsi, xidmət aparmaq mədəniyyətinin y&amp;uuml;ksəldilməsinin təmin olunması, miqrasiya xidməti qulluq&amp;ccedil;usu adına və n&amp;uuml;fuzuna xələl gətirən hallara yol verilməməsi &amp;uuml;&amp;ccedil;&amp;uuml;n əsaslı tədbirlər g&amp;ouml;r&amp;uuml;lməkdədir.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Əlbəttə, bu, bir həqiqətdir ki, tədqiqat&amp;ccedil;ıların birinin dediyi kimi, &amp;ldquo;İnsanların hamısı miqrant kimi doğulmaya bilər, ancaq gec-tez hamı miqrant ola bilər&amp;rdquo;. Həm də ona g&amp;ouml;rə hər kəs hər bir əcnəbiyə x&amp;uuml;susi qayğı ilə yanaşmalıdır. S&amp;ouml;z yox ki, insan h&amp;uuml;quqlarından gen-bol danışan, miqrantlara selektiv yanaşan Avropadan və bir &amp;ccedil;ox Qərb d&amp;ouml;vlətlərindən fərqli olaraq qonağa diqqət və qayğı multikultural dəyərlərin, tolerantlığın həyat tərzi olduğu &amp;ouml;lkəmizdə həmişə prioritet olub. Əcnəbilərin və vətəndaşlığı olmayan şəxslərin h&amp;uuml;quqları &amp;ouml;lkəmizdə y&amp;uuml;ksək səviyyədə qorunur. Fərəhli haldır ki, Azərbaycanda indiyə qədər miqrantlara m&amp;uuml;nasibətdə milli, etnik və dini zəmində he&amp;ccedil; bir h&amp;uuml;quq pozuntusu qeydə alınmayıb.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Nə qədər acı olsa da qeyd etməliyik ki, 2017-ci ildə Aralıq dənizindən ke&amp;ccedil;ərək Avropa sahillərinə &amp;ccedil;ıxmağa cəhd edən &amp;uuml;&amp;ccedil; mindən &amp;ccedil;ox qeyri-leqal miqrant və qa&amp;ccedil;qın həlak olub. Beynəlxalq Miqrasiya Təşkilatının baş direktoru Uilyam Leysi Svinq məsələ ilə əlaqədar bildirib ki: &amp;ldquo;Biz bu barədə illərdir danışırıq və danışmaqda davam edəcəyik: bu faciəvi statistikanı sadəcə olaraq qeydə almaq artıq kifayət etmir. Biz hərəkət etməliyik&amp;rdquo;.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;2017-ci ildə Avropaya təxminən 160 mindən bir qədər &amp;ccedil;ox qa&amp;ccedil;qın və miqrant gəlib. 2016-cı ildə isə bu g&amp;ouml;stərici 348 min nəfər idi. Azərbaycan kimi m&amp;uuml;stəqilliyini yenicə bərpa etmiş bir &amp;ouml;lkə bir milyondan &amp;ccedil;ox qa&amp;ccedil;qın və məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;n&amp;uuml;n y&amp;uuml;k&amp;uuml;n&amp;uuml; demək olar təkbaşına &amp;ccedil;əkməkdə olduğu halda, canlarını g&amp;ouml;t&amp;uuml;r&amp;uuml;b qa&amp;ccedil;an 500-600 min məcburi miqrantı selektiv yanaşmaya məruz qoymaqla yanaşı, &amp;ldquo;biz bu insanları qa&amp;ccedil;qın deyil, m&amp;uuml;səlman işğal&amp;ccedil;ıları hesab edirik&amp;rdquo;, - deyən Avropa liderləri də oldu. Beləliklə, İslam amilinin qabardılaraq onun s&amp;uuml;ni islamofobiya təhdidi qismində geosiyasi təzyiq vasitəsinə &amp;ccedil;evrilməsi y&amp;ouml;n&amp;uuml;ndə səylər miqrasiya b&amp;ouml;hranını daha da kəskinləşdirir. G&amp;ouml;r&amp;uuml;nən odur ki, yaranmış miqrasiya b&amp;ouml;hranının əsas səbəblərindən biri miqrantları qəbul edən cəmiyyətlərə onların inteqrasiyası ilə bağlı problemlər, d&amp;ouml;z&amp;uuml;ms&amp;uuml;zl&amp;uuml;k və birgəyaşayışın ənənəvi qaydalarından imtina edilməsidir.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Prezident İlham Əliyevin rəhbərliyi ilə &amp;ouml;tən d&amp;ouml;vrdə həssas kateqoriyadan olan bu insanlar - qa&amp;ccedil;qın və məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlər &amp;uuml;&amp;ccedil;&amp;uuml;n 100-ə yaxın qəsəbə salınıb. Yanvarın 10-da Azərbaycan Respublikasının Prezidenti İlham Əliyevin sədrliyi ilə Nazirlər Kabinetinin 2017-ci ilin sosial-iqtisadi inkişafının yekunlarına və qarşıda duran vəzifələrə həsr olunan iclasında d&amp;ouml;vlətimizin baş&amp;ccedil;ısı bəyan edib ki, &amp;ldquo;Bu il də yeni k&amp;ouml;&amp;ccedil;k&amp;uuml;n şəhərcikləri, qəsəbələri salınacaq. Bu il yeni mənzillərlə təmin ediləcək k&amp;ouml;&amp;ccedil;k&amp;uuml;nlərin sayı ke&amp;ccedil;ən illə m&amp;uuml;qayisədə təxminən iki dəfə artacaq. Əgər 2017-ci ildə 12 min insanı biz yeni mənzillərə, evlərə k&amp;ouml;&amp;ccedil;&amp;uuml;rm&amp;uuml;ş&amp;uuml;ksə, bu il ən azı 20 min k&amp;ouml;&amp;ccedil;k&amp;uuml;n yeni evlərə, mənzillərə k&amp;ouml;&amp;ccedil;&amp;uuml;r&amp;uuml;lməlidir. Bu rəqəm &amp;ccedil;ox da ola bilər&amp;rdquo;.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;2016-cı ilin Aprel d&amp;ouml;y&amp;uuml;şləri zamanı Azərbaycan Respublikasının Prezidenti, Silahlı Q&amp;uuml;vvələrin Ali Baş Komandanı İlham Əliyevin rəhbərliyi ilə ordumuz tərəfindən Lələtəpə y&amp;uuml;ksəkliyinin azad edilməsindən sonra Cəbrayıl rayonunun işğaldan azad olunmuş Cocuq Mərcanlı kəndində yaşayışın bərpa edilməsi &amp;uuml;&amp;ccedil;&amp;uuml;n imkan yarandı. Cocuq Mərcanlı kəndinin bərpası tarixi nailiyyətdir, indi 150 evdən ibarət g&amp;ouml;zəl qəsəbə yeni həyatını yaşayır. Eyni zamanda, 2017-ci ildə Şıxarx qəsəbəsində 1170 ailə &amp;uuml;&amp;ccedil;&amp;uuml;n yeni şəhərcik salınıb. Bu da əlamətdar hadisədir. M&amp;ouml;htərəm Prezidentin qeyd etdiyi kimi, təkcə ona g&amp;ouml;rə yox ki, b&amp;ouml;y&amp;uuml;k k&amp;ouml;&amp;ccedil;k&amp;uuml;n şəhərciyi yaradılıb. Ona g&amp;ouml;rə ki, Şıxarx qəsəbəsi vaxtilə işğal altında olan ərazidir. Bu ərazi erməni işğal&amp;ccedil;ılarından təmizlənib, azad edilib və o yerdə yeni b&amp;ouml;y&amp;uuml;k şəhərciyin salınması bir daha onu g&amp;ouml;stərir ki, Dağlıq Qarabağ bizim əzəli torpağımızdır. B&amp;uuml;t&amp;uuml;n bunlar B&amp;ouml;y&amp;uuml;k Qayıdışın n&amp;uuml;munəsidir.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Son illər d&amp;uuml;nyanın m&amp;uuml;xtəlif &amp;ouml;lkələrində baş verən xaotik b&amp;ouml;hranlar, ayrı-ayrı &amp;ouml;lkələrin tənəzz&amp;uuml;l&amp;uuml; bir daha s&amp;uuml;but edir ki, ilk n&amp;ouml;vbədə, təhl&amp;uuml;kəsizlik təmin olunmadan cəmiyyətin inkişafı m&amp;uuml;mk&amp;uuml;n deyil. B&amp;uuml;t&amp;uuml;n h&amp;ouml;kumət strukturlarında fəaliyyətin milli maraqlar, qanun&amp;ccedil;uluq, insan h&amp;uuml;quq və azadlıqlarının aliliyi prinsipləri &amp;uuml;zərində qurulmasını əsas məqsəd kimi m&amp;uuml;əyyənləşdirmiş Prezident İlham Əliyevin 2003-c&amp;uuml; ilin oktyabr ayında Azərbaycan Respublikasının Prezidenti se&amp;ccedil;ilməsi ilə &amp;ouml;lkəmiz inkişafının yeni mərhələsinə qədəm qoydu, o, gələcək uğurlara və y&amp;uuml;ksəlişə doğru inamla irəliləyən m&amp;uuml;stəqil d&amp;ouml;vlətimizin simvoluna &amp;ccedil;evrildi. Azərbaycan xarici siyasət sahəsində də b&amp;ouml;y&amp;uuml;k uğurlar əldə etdi. Respublikamız bu g&amp;uuml;n d&amp;uuml;nyada &amp;ccedil;ox b&amp;ouml;y&amp;uuml;k n&amp;uuml;fuza malik olan &amp;ouml;lkə kimi tanınır.&lt;/span&gt;&lt;/p&gt;', 0, 1, 1, 1, 1, 0),
(2, 'Qloballaşan dünyada miqrasiya', '', '', '', '', '&lt;p&gt;&lt;span&gt;&lt;em&gt;19 mart - miqrasiya orqanları iş&amp;ccedil;ilərinin peşə bayramı g&amp;uuml;n&amp;uuml;d&amp;uuml;r. On bir il əvvəl məhz həmin g&amp;uuml;n Azərbaycan Respublikasının Prezidenti İlham Əliyevin imzaladığı 560 saylı Fərmana əsasən, D&amp;ouml;vlət Miqrasiya Xidməti yaradılıb.&lt;/em&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;em&gt;&lt;a href=&quot;http://azertag.az/&quot; target=&quot;_blank&quot;&gt;AZƏRTAC&lt;/a&gt;&amp;nbsp;&amp;ouml;tən 11 il ərzində bu qurumun fəaliyyəti, eləcə də &amp;ouml;lkəmizdə miqrasiya proseslərinin idarə olunması və tənzimlənməsi sahəsində g&amp;ouml;r&amp;uuml;lən işlərlə bağlı D&amp;ouml;vlət Miqrasiya Xidmətinin rəisi Firudin Nəbiyevin məqaləsini təqdim edir.&lt;/em&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Təməli ulu &amp;ouml;ndər Heydər Əliyev tərəfindən qoyulmuş d&amp;ouml;vlətimizin inkişafı kursu &amp;ouml;tən 15 ildə Prezident İlham Əliyevin rəhbərliyi ilə m&amp;uuml;vəffəqiyyətlə həyata ke&amp;ccedil;irilib. D&amp;uuml;nyada baş verən siyasi və iqtisadi təlat&amp;uuml;mlərə, qlobal b&amp;ouml;hranlara baxmayaraq, &amp;ouml;lkəmizdə sabitlik və intensiv inkişaf təmin edilib, bir s&amp;ouml;zlə, &amp;ouml;tən d&amp;ouml;vr Azərbaycan &amp;uuml;&amp;ccedil;&amp;uuml;n uğurlu olub. Bu uğurlar, nailiyyətlər təkcə &amp;ouml;lkənin daxilindən yox, həm də xaricdən də aydın g&amp;ouml;r&amp;uuml;nməkdədir. Azərbaycan a&amp;ccedil;ıq &amp;ouml;lkədir, iqtisadiyyatına kifayət qədər xarici investisiya yatırılıb. Bu g&amp;uuml;n Azərbaycan beynəlxalq tədbirlərin ke&amp;ccedil;irildiyi məkana &amp;ccedil;evrilib.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&amp;Uuml;mumbəşəri dəyişikliklər və b&amp;ouml;y&amp;uuml;k əhəmiyyət kəsb edən hallar fonunda d&amp;uuml;nyada miqrasiya axınının intensiv genişlənməsi m&amp;uuml;şahidə olunur. Miqrant adətən ləyaqətli həyat, təminatlı və rahat iş axtarır. Elə bunun nəticəsidir ki, ləyaqətli həyat axtarışı ilə sabit, inkişaf edən, təhl&amp;uuml;kəsizliyin y&amp;uuml;ksək səviyyədə təmin edildiyi bir &amp;ouml;lkənin vətəndaşı olmaq, Azərbaycanda yaşamaq, təhsil almaq istəyi ilə respublikamıza təşrif buyuranların, turist kimi səyahətə gələnlərin sayı ildən-ilə artmaqdadır.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;S&amp;ouml;z yox ki, bəşəriyyət tarixində həmişə b&amp;ouml;y&amp;uuml;k rol oynayan miqrasiya prosesi m&amp;uuml;asir beynəlxalq m&amp;uuml;nasibətlər sistemində &amp;ouml;z&amp;uuml;n&amp;uuml; qabarıq n&amp;uuml;mayiş etdirən problemlərdən birinə &amp;ccedil;evrilib. Qanuni və qanunsuz, beynəlxalq və &amp;ouml;lkədaxili, k&amp;ouml;n&amp;uuml;ll&amp;uuml; və məcburi, daimi və m&amp;uuml;vəqqəti, qa&amp;ccedil;qın və sığınacaq axtaranlar, ailə birliyi və &amp;ldquo;beyin axını&amp;rdquo; kriteriyaları ilə təsnifatlaşdırılan miqrasiyanın coğrafiyası və həcmi genişlənib, miqrasiyanın yeni tipləri - y&amp;uuml;ksək təsnifatlı kadrların miqrasiyası və &amp;ccedil;ox vaxt qanunsuz miqrasiyaya əsas verən ixtisassız şəxslərin miqrasiyası formalaşıb. Məcburi miqrasiya tendensiyası artıb, miqrasiya d&amp;uuml;nyada sosial dəyişikliklərin bir faktoru kimi diqqət &amp;ccedil;əkməyə başlayıb. S&amp;ouml;z yox ki, miqrasiya demoqrafik proseslərin formalaşmasında m&amp;uuml;h&amp;uuml;m rol oynayır və əhalinin etnik-mədəni tərkibini yaradır. Reallıq budur ki, bu g&amp;uuml;n beynəlxalq m&amp;uuml;nasibətlər sistemində miqrasiya geosiyasəti artıq yeni formalaşan bir istiqamət statusu qazanıb.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Beynəlxalq birliyin bərabərh&amp;uuml;quqlu &amp;uuml;zv&amp;uuml; olan &amp;ouml;lkəmizdə də m&amp;uuml;asir d&amp;uuml;nyanın bu aktual m&amp;ouml;vzusunun b&amp;ouml;y&amp;uuml;k əhəmiyyətinə kifayət qədər ciddi yanaşılmaqdadır.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Hələ d&amp;uuml;nyada miqrasiya məsələsi indiki qədər aktual olmadığı bir vaxtda - 2007-ci il martın 19-da Prezident İlham Əliyev m&amp;uuml;stəqil mərkəzi icra hakimiyyəti orqanı olan D&amp;ouml;vlət Miqrasiya Xidmətinin yaradılması haqqında Fərman imzalayıb. &amp;Ouml;lkə rəhbərinin diqqət və qayğısı nəticəsində indi Xidmətin istər mərkəzi aparatı, istərsə də regional miqrasiya idarələri ən m&amp;uuml;asir xidmət imkanları ilə təchiz edilib, m&amp;uuml;raciət edənlərə peşəkar xidmət g&amp;ouml;stərilməsi &amp;uuml;&amp;ccedil;&amp;uuml;n hər c&amp;uuml;r şərait yaradılıb.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&amp;Ouml;tən d&amp;ouml;vr ərzində &amp;ouml;lkəmizdə miqrasiya idarəetmə mexanizmi təkmilləşdirilib, respublikaya gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin yaşayış yeri &amp;uuml;zrə qeydiyyata alınmaları və onların &amp;ouml;lkə ərazisində qanuni əsaslarla yaşamalarının təmin olunması &amp;uuml;&amp;ccedil;&amp;uuml;n m&amp;uuml;vafiq icazələrin verilməsi prosedurları sadələşdirilib, nəticədə bu sahədə şəffaflığın tam təmin edilməsinə nail olunub.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Azərbaycanın həyata ke&amp;ccedil;irdiyi d&amp;ouml;vlət siyasətində miqrasiya məsələləri prioritet yer tutur. Prezident İlham Əliyevin 2012-ci il 29 dekabr tarixli Fərmanı ilə təsdiq edilmiş &amp;ldquo;Azərbaycan 2020: gələcəyə baxış&amp;rdquo; İnkişaf Konsepsiyası yaxın perspektivlər &amp;uuml;&amp;ccedil;&amp;uuml;n d&amp;ouml;vlətimizin inkişaf hədəflərini və prioritetlərini m&amp;uuml;əyyən edən rəsmi d&amp;ouml;vlət sənədi kimi &amp;ouml;z&amp;uuml;ndə miqrasiya problemi ilə bağlı məsələləri də əks etdirir. 11 b&amp;ouml;lmədən ibarət Konsepsiyada d&amp;ouml;vlətin milli təhl&amp;uuml;kəsizliyinin prioritet istiqaməti kimi miqrasiya ilə bağlı m&amp;uuml;ddəalara m&amp;uuml;h&amp;uuml;m yer ayrılır. Ona g&amp;ouml;rə ki, geniş imkanlarla yanaşı, &amp;ouml;z&amp;uuml; ilə b&amp;ouml;y&amp;uuml;k riskləri də gətirən qloballaşma beynəlxalq miqyasda istehsalın, ticarətin, kapital axınının və əmək miqrasiyasının sərbəstləşməsi tendensiyalarını kəskin artırıb. Bu baxımdan konsepsiyada problemin mahiyyətini a&amp;ccedil;mağa və qarşıya qoyulan məqsədə &amp;ccedil;atmağa nail olmaq &amp;uuml;&amp;ccedil;&amp;uuml;n d&amp;uuml;zg&amp;uuml;n olaraq m&amp;uuml;əyyən edilib ki, əhali və onun strukturu ilə bağlı kəmiyyət və keyfiyyət g&amp;ouml;stəriciləri milli g&amp;uuml;c&amp;uuml;n əsas elementləridir və təbii olaraq, Azərbaycan xalqının genofondunun qorunması, miqrasiya və demoqrafiya kimi məsələlər milli təhl&amp;uuml;kəsizlik baxımından olduqca m&amp;uuml;h&amp;uuml;md&amp;uuml;r. Konsepsiyanın əhatə etdiyi d&amp;ouml;vrdə &amp;ouml;lkə əhalisinin hər il orta hesabla 1,1 faiz artaraq, 2020-ci ildə təxminən 10,2 milyon nəfərə &amp;ccedil;atacağı g&amp;ouml;zlənilir. Konsepsiyada nəzərdə tutulmuş məqsədlərə &amp;ccedil;atmaq &amp;uuml;&amp;ccedil;&amp;uuml;n qanunvericilik bazasının m&amp;uuml;təmadi olaraq təkmilləşdirilməsi və m&amp;uuml;vafiq institusional potensialın g&amp;uuml;cləndirilməsi istiqamətində tədbirlər davam etdirilir. Bu m&amp;uuml;tərəqqi sənəddə xaricdən iş&amp;ccedil;i q&amp;uuml;vvəsinin &amp;ouml;lkəyə qanunsuz axınının qarşısının alınması, xarici &amp;ouml;lkələrdə &amp;ccedil;alışan vətəndaşlarımızın sosial m&amp;uuml;dafiəsinin g&amp;uuml;cləndirilməsi ilə bağlı m&amp;uuml;vafiq işlərin g&amp;ouml;r&amp;uuml;lməsi də prioritet kimi m&amp;uuml;əyyən edilib. Əlbəttə, D&amp;ouml;vlət Miqrasiya Xidməti bu istiqamətdə də &amp;ouml;z &amp;uuml;zərinə d&amp;uuml;şən vəzifələrin layiqincə yerinə yetirilməsi &amp;uuml;&amp;ccedil;&amp;uuml;n səylərini əsirgəmir.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Prezident İlham Əliyevin rəhbərliyi ilə Azərbaycanın miqrasiya siyasəti insan və vətəndaş h&amp;uuml;quqlarına və azadlıqlarına h&amp;ouml;rmət, qanun&amp;ccedil;uluq, qanun qarşısında bərabərlik və ədalətlilik, miqrasiya qanunvericiliyinin hamılıqla tanınan beynəlxalq h&amp;uuml;quq normalarına uyğunluğunun təmin edilməsi, miqrasiya proseslərinin tənzimlənməsində innovativ metodların tətbiqi və şəffaflığın təmin edilməsi prinsiplərinə əsaslanır.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Bu g&amp;uuml;n d&amp;ouml;vlətimizin baş&amp;ccedil;ısının vətəndaşlara qayğı ilə m&amp;uuml;nasibət g&amp;ouml;stərilməsi tələbinin yerinə yetirilməsi, əməkdaşlar tərəfindən etik davranış qaydalarına d&amp;uuml;r&amp;uuml;st əməl edilməsi, xidmət aparmaq mədəniyyətinin y&amp;uuml;ksəldilməsinin təmin olunması, miqrasiya xidməti qulluq&amp;ccedil;usu adına və n&amp;uuml;fuzuna xələl gətirən hallara yol verilməməsi &amp;uuml;&amp;ccedil;&amp;uuml;n əsaslı tədbirlər g&amp;ouml;r&amp;uuml;lməkdədir.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Əlbəttə, bu, bir həqiqətdir ki, tədqiqat&amp;ccedil;ıların birinin dediyi kimi, &amp;ldquo;İnsanların hamısı miqrant kimi doğulmaya bilər, ancaq gec-tez hamı miqrant ola bilər&amp;rdquo;. Həm də ona g&amp;ouml;rə hər kəs hər bir əcnəbiyə x&amp;uuml;susi qayğı ilə yanaşmalıdır. S&amp;ouml;z yox ki, insan h&amp;uuml;quqlarından gen-bol danışan, miqrantlara selektiv yanaşan Avropadan və bir &amp;ccedil;ox Qərb d&amp;ouml;vlətlərindən fərqli olaraq qonağa diqqət və qayğı multikultural dəyərlərin, tolerantlığın həyat tərzi olduğu &amp;ouml;lkəmizdə həmişə prioritet olub. Əcnəbilərin və vətəndaşlığı olmayan şəxslərin h&amp;uuml;quqları &amp;ouml;lkəmizdə y&amp;uuml;ksək səviyyədə qorunur. Fərəhli haldır ki, Azərbaycanda indiyə qədər miqrantlara m&amp;uuml;nasibətdə milli, etnik və dini zəmində he&amp;ccedil; bir h&amp;uuml;quq pozuntusu qeydə alınmayıb.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Nə qədər acı olsa da qeyd etməliyik ki, 2017-ci ildə Aralıq dənizindən ke&amp;ccedil;ərək Avropa sahillərinə &amp;ccedil;ıxmağa cəhd edən &amp;uuml;&amp;ccedil; mindən &amp;ccedil;ox qeyri-leqal miqrant və qa&amp;ccedil;qın həlak olub. Beynəlxalq Miqrasiya Təşkilatının baş direktoru Uilyam Leysi Svinq məsələ ilə əlaqədar bildirib ki: &amp;ldquo;Biz bu barədə illərdir danışırıq və danışmaqda davam edəcəyik: bu faciəvi statistikanı sadəcə olaraq qeydə almaq artıq kifayət etmir. Biz hərəkət etməliyik&amp;rdquo;.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;2017-ci ildə Avropaya təxminən 160 mindən bir qədər &amp;ccedil;ox qa&amp;ccedil;qın və miqrant gəlib. 2016-cı ildə isə bu g&amp;ouml;stərici 348 min nəfər idi. Azərbaycan kimi m&amp;uuml;stəqilliyini yenicə bərpa etmiş bir &amp;ouml;lkə bir milyondan &amp;ccedil;ox qa&amp;ccedil;qın və məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;n&amp;uuml;n y&amp;uuml;k&amp;uuml;n&amp;uuml; demək olar təkbaşına &amp;ccedil;əkməkdə olduğu halda, canlarını g&amp;ouml;t&amp;uuml;r&amp;uuml;b qa&amp;ccedil;an 500-600 min məcburi miqrantı selektiv yanaşmaya məruz qoymaqla yanaşı, &amp;ldquo;biz bu insanları qa&amp;ccedil;qın deyil, m&amp;uuml;səlman işğal&amp;ccedil;ıları hesab edirik&amp;rdquo;, - deyən Avropa liderləri də oldu. Beləliklə, İslam amilinin qabardılaraq onun s&amp;uuml;ni islamofobiya təhdidi qismində geosiyasi təzyiq vasitəsinə &amp;ccedil;evrilməsi y&amp;ouml;n&amp;uuml;ndə səylər miqrasiya b&amp;ouml;hranını daha da kəskinləşdirir. G&amp;ouml;r&amp;uuml;nən odur ki, yaranmış miqrasiya b&amp;ouml;hranının əsas səbəblərindən biri miqrantları qəbul edən cəmiyyətlərə onların inteqrasiyası ilə bağlı problemlər, d&amp;ouml;z&amp;uuml;ms&amp;uuml;zl&amp;uuml;k və birgəyaşayışın ənənəvi qaydalarından imtina edilməsidir.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Prezident İlham Əliyevin rəhbərliyi ilə &amp;ouml;tən d&amp;ouml;vrdə həssas kateqoriyadan olan bu insanlar - qa&amp;ccedil;qın və məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlər &amp;uuml;&amp;ccedil;&amp;uuml;n 100-ə yaxın qəsəbə salınıb. Yanvarın 10-da Azərbaycan Respublikasının Prezidenti İlham Əliyevin sədrliyi ilə Nazirlər Kabinetinin 2017-ci ilin sosial-iqtisadi inkişafının yekunlarına və qarşıda duran vəzifələrə həsr olunan iclasında d&amp;ouml;vlətimizin baş&amp;ccedil;ısı bəyan edib ki, &amp;ldquo;Bu il də yeni k&amp;ouml;&amp;ccedil;k&amp;uuml;n şəhərcikləri, qəsəbələri salınacaq. Bu il yeni mənzillərlə təmin ediləcək k&amp;ouml;&amp;ccedil;k&amp;uuml;nlərin sayı ke&amp;ccedil;ən illə m&amp;uuml;qayisədə təxminən iki dəfə artacaq. Əgər 2017-ci ildə 12 min insanı biz yeni mənzillərə, evlərə k&amp;ouml;&amp;ccedil;&amp;uuml;rm&amp;uuml;ş&amp;uuml;ksə, bu il ən azı 20 min k&amp;ouml;&amp;ccedil;k&amp;uuml;n yeni evlərə, mənzillərə k&amp;ouml;&amp;ccedil;&amp;uuml;r&amp;uuml;lməlidir. Bu rəqəm &amp;ccedil;ox da ola bilər&amp;rdquo;.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;2016-cı ilin Aprel d&amp;ouml;y&amp;uuml;şləri zamanı Azərbaycan Respublikasının Prezidenti, Silahlı Q&amp;uuml;vvələrin Ali Baş Komandanı İlham Əliyevin rəhbərliyi ilə ordumuz tərəfindən Lələtəpə y&amp;uuml;ksəkliyinin azad edilməsindən sonra Cəbrayıl rayonunun işğaldan azad olunmuş Cocuq Mərcanlı kəndində yaşayışın bərpa edilməsi &amp;uuml;&amp;ccedil;&amp;uuml;n imkan yarandı. Cocuq Mərcanlı kəndinin bərpası tarixi nailiyyətdir, indi 150 evdən ibarət g&amp;ouml;zəl qəsəbə yeni həyatını yaşayır. Eyni zamanda, 2017-ci ildə Şıxarx qəsəbəsində 1170 ailə &amp;uuml;&amp;ccedil;&amp;uuml;n yeni şəhərcik salınıb. Bu da əlamətdar hadisədir. M&amp;ouml;htərəm Prezidentin qeyd etdiyi kimi, təkcə ona g&amp;ouml;rə yox ki, b&amp;ouml;y&amp;uuml;k k&amp;ouml;&amp;ccedil;k&amp;uuml;n şəhərciyi yaradılıb. Ona g&amp;ouml;rə ki, Şıxarx qəsəbəsi vaxtilə işğal altında olan ərazidir. Bu ərazi erməni işğal&amp;ccedil;ılarından təmizlənib, azad edilib və o yerdə yeni b&amp;ouml;y&amp;uuml;k şəhərciyin salınması bir daha onu g&amp;ouml;stərir ki, Dağlıq Qarabağ bizim əzəli torpağımızdır. B&amp;uuml;t&amp;uuml;n bunlar B&amp;ouml;y&amp;uuml;k Qayıdışın n&amp;uuml;munəsidir.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Son illər d&amp;uuml;nyanın m&amp;uuml;xtəlif &amp;ouml;lkələrində baş verən xaotik b&amp;ouml;hranlar, ayrı-ayrı &amp;ouml;lkələrin tənəzz&amp;uuml;l&amp;uuml; bir daha s&amp;uuml;but edir ki, ilk n&amp;ouml;vbədə, təhl&amp;uuml;kəsizlik təmin olunmadan cəmiyyətin inkişafı m&amp;uuml;mk&amp;uuml;n deyil. B&amp;uuml;t&amp;uuml;n h&amp;ouml;kumət strukturlarında fəaliyyətin milli maraqlar, qanun&amp;ccedil;uluq, insan h&amp;uuml;quq və azadlıqlarının aliliyi prinsipləri &amp;uuml;zərində qurulmasını əsas məqsəd kimi m&amp;uuml;əyyənləşdirmiş Prezident İlham Əliyevin 2003-c&amp;uuml; ilin oktyabr ayında Azərbaycan Respublikasının Prezidenti se&amp;ccedil;ilməsi ilə &amp;ouml;lkəmiz inkişafının yeni mərhələsinə qədəm qoydu, o, gələcək uğurlara və y&amp;uuml;ksəlişə doğru inamla irəliləyən m&amp;uuml;stəqil d&amp;ouml;vlətimizin simvoluna &amp;ccedil;evrildi. Azərbaycan xarici siyasət sahəsində də b&amp;ouml;y&amp;uuml;k uğurlar əldə etdi. Respublikamız bu g&amp;uuml;n d&amp;uuml;nyada &amp;ccedil;ox b&amp;ouml;y&amp;uuml;k n&amp;uuml;fuza malik olan &amp;ouml;lkə kimi tanınır.&lt;/span&gt;&lt;/p&gt;', 0, 1, 1, 2, 1, 0),
(3, 'Qloballaşan dünyada miqrasiya', '', '', '', '', '&lt;p&gt;&lt;span&gt;&lt;em&gt;19 mart - miqrasiya orqanları iş&amp;ccedil;ilərinin peşə bayramı g&amp;uuml;n&amp;uuml;d&amp;uuml;r. On bir il əvvəl məhz həmin g&amp;uuml;n Azərbaycan Respublikasının Prezidenti İlham Əliyevin imzaladığı 560 saylı Fərmana əsasən, D&amp;ouml;vlət Miqrasiya Xidməti yaradılıb.&lt;/em&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;em&gt;&lt;a href=&quot;http://azertag.az/&quot; target=&quot;_blank&quot;&gt;AZƏRTAC&lt;/a&gt;&amp;nbsp;&amp;ouml;tən 11 il ərzində bu qurumun fəaliyyəti, eləcə də &amp;ouml;lkəmizdə miqrasiya proseslərinin idarə olunması və tənzimlənməsi sahəsində g&amp;ouml;r&amp;uuml;lən işlərlə bağlı D&amp;ouml;vlət Miqrasiya Xidmətinin rəisi Firudin Nəbiyevin məqaləsini təqdim edir.&lt;/em&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Təməli ulu &amp;ouml;ndər Heydər Əliyev tərəfindən qoyulmuş d&amp;ouml;vlətimizin inkişafı kursu &amp;ouml;tən 15 ildə Prezident İlham Əliyevin rəhbərliyi ilə m&amp;uuml;vəffəqiyyətlə həyata ke&amp;ccedil;irilib. D&amp;uuml;nyada baş verən siyasi və iqtisadi təlat&amp;uuml;mlərə, qlobal b&amp;ouml;hranlara baxmayaraq, &amp;ouml;lkəmizdə sabitlik və intensiv inkişaf təmin edilib, bir s&amp;ouml;zlə, &amp;ouml;tən d&amp;ouml;vr Azərbaycan &amp;uuml;&amp;ccedil;&amp;uuml;n uğurlu olub. Bu uğurlar, nailiyyətlər təkcə &amp;ouml;lkənin daxilindən yox, həm də xaricdən də aydın g&amp;ouml;r&amp;uuml;nməkdədir. Azərbaycan a&amp;ccedil;ıq &amp;ouml;lkədir, iqtisadiyyatına kifayət qədər xarici investisiya yatırılıb. Bu g&amp;uuml;n Azərbaycan beynəlxalq tədbirlərin ke&amp;ccedil;irildiyi məkana &amp;ccedil;evrilib.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&amp;Uuml;mumbəşəri dəyişikliklər və b&amp;ouml;y&amp;uuml;k əhəmiyyət kəsb edən hallar fonunda d&amp;uuml;nyada miqrasiya axınının intensiv genişlənməsi m&amp;uuml;şahidə olunur. Miqrant adətən ləyaqətli həyat, təminatlı və rahat iş axtarır. Elə bunun nəticəsidir ki, ləyaqətli həyat axtarışı ilə sabit, inkişaf edən, təhl&amp;uuml;kəsizliyin y&amp;uuml;ksək səviyyədə təmin edildiyi bir &amp;ouml;lkənin vətəndaşı olmaq, Azərbaycanda yaşamaq, təhsil almaq istəyi ilə respublikamıza təşrif buyuranların, turist kimi səyahətə gələnlərin sayı ildən-ilə artmaqdadır.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;S&amp;ouml;z yox ki, bəşəriyyət tarixində həmişə b&amp;ouml;y&amp;uuml;k rol oynayan miqrasiya prosesi m&amp;uuml;asir beynəlxalq m&amp;uuml;nasibətlər sistemində &amp;ouml;z&amp;uuml;n&amp;uuml; qabarıq n&amp;uuml;mayiş etdirən problemlərdən birinə &amp;ccedil;evrilib. Qanuni və qanunsuz, beynəlxalq və &amp;ouml;lkədaxili, k&amp;ouml;n&amp;uuml;ll&amp;uuml; və məcburi, daimi və m&amp;uuml;vəqqəti, qa&amp;ccedil;qın və sığınacaq axtaranlar, ailə birliyi və &amp;ldquo;beyin axını&amp;rdquo; kriteriyaları ilə təsnifatlaşdırılan miqrasiyanın coğrafiyası və həcmi genişlənib, miqrasiyanın yeni tipləri - y&amp;uuml;ksək təsnifatlı kadrların miqrasiyası və &amp;ccedil;ox vaxt qanunsuz miqrasiyaya əsas verən ixtisassız şəxslərin miqrasiyası formalaşıb. Məcburi miqrasiya tendensiyası artıb, miqrasiya d&amp;uuml;nyada sosial dəyişikliklərin bir faktoru kimi diqqət &amp;ccedil;əkməyə başlayıb. S&amp;ouml;z yox ki, miqrasiya demoqrafik proseslərin formalaşmasında m&amp;uuml;h&amp;uuml;m rol oynayır və əhalinin etnik-mədəni tərkibini yaradır. Reallıq budur ki, bu g&amp;uuml;n beynəlxalq m&amp;uuml;nasibətlər sistemində miqrasiya geosiyasəti artıq yeni formalaşan bir istiqamət statusu qazanıb.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Beynəlxalq birliyin bərabərh&amp;uuml;quqlu &amp;uuml;zv&amp;uuml; olan &amp;ouml;lkəmizdə də m&amp;uuml;asir d&amp;uuml;nyanın bu aktual m&amp;ouml;vzusunun b&amp;ouml;y&amp;uuml;k əhəmiyyətinə kifayət qədər ciddi yanaşılmaqdadır.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Hələ d&amp;uuml;nyada miqrasiya məsələsi indiki qədər aktual olmadığı bir vaxtda - 2007-ci il martın 19-da Prezident İlham Əliyev m&amp;uuml;stəqil mərkəzi icra hakimiyyəti orqanı olan D&amp;ouml;vlət Miqrasiya Xidmətinin yaradılması haqqında Fərman imzalayıb. &amp;Ouml;lkə rəhbərinin diqqət və qayğısı nəticəsində indi Xidmətin istər mərkəzi aparatı, istərsə də regional miqrasiya idarələri ən m&amp;uuml;asir xidmət imkanları ilə təchiz edilib, m&amp;uuml;raciət edənlərə peşəkar xidmət g&amp;ouml;stərilməsi &amp;uuml;&amp;ccedil;&amp;uuml;n hər c&amp;uuml;r şərait yaradılıb.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&amp;Ouml;tən d&amp;ouml;vr ərzində &amp;ouml;lkəmizdə miqrasiya idarəetmə mexanizmi təkmilləşdirilib, respublikaya gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin yaşayış yeri &amp;uuml;zrə qeydiyyata alınmaları və onların &amp;ouml;lkə ərazisində qanuni əsaslarla yaşamalarının təmin olunması &amp;uuml;&amp;ccedil;&amp;uuml;n m&amp;uuml;vafiq icazələrin verilməsi prosedurları sadələşdirilib, nəticədə bu sahədə şəffaflığın tam təmin edilməsinə nail olunub.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Azərbaycanın həyata ke&amp;ccedil;irdiyi d&amp;ouml;vlət siyasətində miqrasiya məsələləri prioritet yer tutur. Prezident İlham Əliyevin 2012-ci il 29 dekabr tarixli Fərmanı ilə təsdiq edilmiş &amp;ldquo;Azərbaycan 2020: gələcəyə baxış&amp;rdquo; İnkişaf Konsepsiyası yaxın perspektivlər &amp;uuml;&amp;ccedil;&amp;uuml;n d&amp;ouml;vlətimizin inkişaf hədəflərini və prioritetlərini m&amp;uuml;əyyən edən rəsmi d&amp;ouml;vlət sənədi kimi &amp;ouml;z&amp;uuml;ndə miqrasiya problemi ilə bağlı məsələləri də əks etdirir. 11 b&amp;ouml;lmədən ibarət Konsepsiyada d&amp;ouml;vlətin milli təhl&amp;uuml;kəsizliyinin prioritet istiqaməti kimi miqrasiya ilə bağlı m&amp;uuml;ddəalara m&amp;uuml;h&amp;uuml;m yer ayrılır. Ona g&amp;ouml;rə ki, geniş imkanlarla yanaşı, &amp;ouml;z&amp;uuml; ilə b&amp;ouml;y&amp;uuml;k riskləri də gətirən qloballaşma beynəlxalq miqyasda istehsalın, ticarətin, kapital axınının və əmək miqrasiyasının sərbəstləşməsi tendensiyalarını kəskin artırıb. Bu baxımdan konsepsiyada problemin mahiyyətini a&amp;ccedil;mağa və qarşıya qoyulan məqsədə &amp;ccedil;atmağa nail olmaq &amp;uuml;&amp;ccedil;&amp;uuml;n d&amp;uuml;zg&amp;uuml;n olaraq m&amp;uuml;əyyən edilib ki, əhali və onun strukturu ilə bağlı kəmiyyət və keyfiyyət g&amp;ouml;stəriciləri milli g&amp;uuml;c&amp;uuml;n əsas elementləridir və təbii olaraq, Azərbaycan xalqının genofondunun qorunması, miqrasiya və demoqrafiya kimi məsələlər milli təhl&amp;uuml;kəsizlik baxımından olduqca m&amp;uuml;h&amp;uuml;md&amp;uuml;r. Konsepsiyanın əhatə etdiyi d&amp;ouml;vrdə &amp;ouml;lkə əhalisinin hər il orta hesabla 1,1 faiz artaraq, 2020-ci ildə təxminən 10,2 milyon nəfərə &amp;ccedil;atacağı g&amp;ouml;zlənilir. Konsepsiyada nəzərdə tutulmuş məqsədlərə &amp;ccedil;atmaq &amp;uuml;&amp;ccedil;&amp;uuml;n qanunvericilik bazasının m&amp;uuml;təmadi olaraq təkmilləşdirilməsi və m&amp;uuml;vafiq institusional potensialın g&amp;uuml;cləndirilməsi istiqamətində tədbirlər davam etdirilir. Bu m&amp;uuml;tərəqqi sənəddə xaricdən iş&amp;ccedil;i q&amp;uuml;vvəsinin &amp;ouml;lkəyə qanunsuz axınının qarşısının alınması, xarici &amp;ouml;lkələrdə &amp;ccedil;alışan vətəndaşlarımızın sosial m&amp;uuml;dafiəsinin g&amp;uuml;cləndirilməsi ilə bağlı m&amp;uuml;vafiq işlərin g&amp;ouml;r&amp;uuml;lməsi də prioritet kimi m&amp;uuml;əyyən edilib. Əlbəttə, D&amp;ouml;vlət Miqrasiya Xidməti bu istiqamətdə də &amp;ouml;z &amp;uuml;zərinə d&amp;uuml;şən vəzifələrin layiqincə yerinə yetirilməsi &amp;uuml;&amp;ccedil;&amp;uuml;n səylərini əsirgəmir.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Prezident İlham Əliyevin rəhbərliyi ilə Azərbaycanın miqrasiya siyasəti insan və vətəndaş h&amp;uuml;quqlarına və azadlıqlarına h&amp;ouml;rmət, qanun&amp;ccedil;uluq, qanun qarşısında bərabərlik və ədalətlilik, miqrasiya qanunvericiliyinin hamılıqla tanınan beynəlxalq h&amp;uuml;quq normalarına uyğunluğunun təmin edilməsi, miqrasiya proseslərinin tənzimlənməsində innovativ metodların tətbiqi və şəffaflığın təmin edilməsi prinsiplərinə əsaslanır.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Bu g&amp;uuml;n d&amp;ouml;vlətimizin baş&amp;ccedil;ısının vətəndaşlara qayğı ilə m&amp;uuml;nasibət g&amp;ouml;stərilməsi tələbinin yerinə yetirilməsi, əməkdaşlar tərəfindən etik davranış qaydalarına d&amp;uuml;r&amp;uuml;st əməl edilməsi, xidmət aparmaq mədəniyyətinin y&amp;uuml;ksəldilməsinin təmin olunması, miqrasiya xidməti qulluq&amp;ccedil;usu adına və n&amp;uuml;fuzuna xələl gətirən hallara yol verilməməsi &amp;uuml;&amp;ccedil;&amp;uuml;n əsaslı tədbirlər g&amp;ouml;r&amp;uuml;lməkdədir.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Əlbəttə, bu, bir həqiqətdir ki, tədqiqat&amp;ccedil;ıların birinin dediyi kimi, &amp;ldquo;İnsanların hamısı miqrant kimi doğulmaya bilər, ancaq gec-tez hamı miqrant ola bilər&amp;rdquo;. Həm də ona g&amp;ouml;rə hər kəs hər bir əcnəbiyə x&amp;uuml;susi qayğı ilə yanaşmalıdır. S&amp;ouml;z yox ki, insan h&amp;uuml;quqlarından gen-bol danışan, miqrantlara selektiv yanaşan Avropadan və bir &amp;ccedil;ox Qərb d&amp;ouml;vlətlərindən fərqli olaraq qonağa diqqət və qayğı multikultural dəyərlərin, tolerantlığın həyat tərzi olduğu &amp;ouml;lkəmizdə həmişə prioritet olub. Əcnəbilərin və vətəndaşlığı olmayan şəxslərin h&amp;uuml;quqları &amp;ouml;lkəmizdə y&amp;uuml;ksək səviyyədə qorunur. Fərəhli haldır ki, Azərbaycanda indiyə qədər miqrantlara m&amp;uuml;nasibətdə milli, etnik və dini zəmində he&amp;ccedil; bir h&amp;uuml;quq pozuntusu qeydə alınmayıb.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Nə qədər acı olsa da qeyd etməliyik ki, 2017-ci ildə Aralıq dənizindən ke&amp;ccedil;ərək Avropa sahillərinə &amp;ccedil;ıxmağa cəhd edən &amp;uuml;&amp;ccedil; mindən &amp;ccedil;ox qeyri-leqal miqrant və qa&amp;ccedil;qın həlak olub. Beynəlxalq Miqrasiya Təşkilatının baş direktoru Uilyam Leysi Svinq məsələ ilə əlaqədar bildirib ki: &amp;ldquo;Biz bu barədə illərdir danışırıq və danışmaqda davam edəcəyik: bu faciəvi statistikanı sadəcə olaraq qeydə almaq artıq kifayət etmir. Biz hərəkət etməliyik&amp;rdquo;.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;2017-ci ildə Avropaya təxminən 160 mindən bir qədər &amp;ccedil;ox qa&amp;ccedil;qın və miqrant gəlib. 2016-cı ildə isə bu g&amp;ouml;stərici 348 min nəfər idi. Azərbaycan kimi m&amp;uuml;stəqilliyini yenicə bərpa etmiş bir &amp;ouml;lkə bir milyondan &amp;ccedil;ox qa&amp;ccedil;qın və məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;n&amp;uuml;n y&amp;uuml;k&amp;uuml;n&amp;uuml; demək olar təkbaşına &amp;ccedil;əkməkdə olduğu halda, canlarını g&amp;ouml;t&amp;uuml;r&amp;uuml;b qa&amp;ccedil;an 500-600 min məcburi miqrantı selektiv yanaşmaya məruz qoymaqla yanaşı, &amp;ldquo;biz bu insanları qa&amp;ccedil;qın deyil, m&amp;uuml;səlman işğal&amp;ccedil;ıları hesab edirik&amp;rdquo;, - deyən Avropa liderləri də oldu. Beləliklə, İslam amilinin qabardılaraq onun s&amp;uuml;ni islamofobiya təhdidi qismində geosiyasi təzyiq vasitəsinə &amp;ccedil;evrilməsi y&amp;ouml;n&amp;uuml;ndə səylər miqrasiya b&amp;ouml;hranını daha da kəskinləşdirir. G&amp;ouml;r&amp;uuml;nən odur ki, yaranmış miqrasiya b&amp;ouml;hranının əsas səbəblərindən biri miqrantları qəbul edən cəmiyyətlərə onların inteqrasiyası ilə bağlı problemlər, d&amp;ouml;z&amp;uuml;ms&amp;uuml;zl&amp;uuml;k və birgəyaşayışın ənənəvi qaydalarından imtina edilməsidir.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Prezident İlham Əliyevin rəhbərliyi ilə &amp;ouml;tən d&amp;ouml;vrdə həssas kateqoriyadan olan bu insanlar - qa&amp;ccedil;qın və məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlər &amp;uuml;&amp;ccedil;&amp;uuml;n 100-ə yaxın qəsəbə salınıb. Yanvarın 10-da Azərbaycan Respublikasının Prezidenti İlham Əliyevin sədrliyi ilə Nazirlər Kabinetinin 2017-ci ilin sosial-iqtisadi inkişafının yekunlarına və qarşıda duran vəzifələrə həsr olunan iclasında d&amp;ouml;vlətimizin baş&amp;ccedil;ısı bəyan edib ki, &amp;ldquo;Bu il də yeni k&amp;ouml;&amp;ccedil;k&amp;uuml;n şəhərcikləri, qəsəbələri salınacaq. Bu il yeni mənzillərlə təmin ediləcək k&amp;ouml;&amp;ccedil;k&amp;uuml;nlərin sayı ke&amp;ccedil;ən illə m&amp;uuml;qayisədə təxminən iki dəfə artacaq. Əgər 2017-ci ildə 12 min insanı biz yeni mənzillərə, evlərə k&amp;ouml;&amp;ccedil;&amp;uuml;rm&amp;uuml;ş&amp;uuml;ksə, bu il ən azı 20 min k&amp;ouml;&amp;ccedil;k&amp;uuml;n yeni evlərə, mənzillərə k&amp;ouml;&amp;ccedil;&amp;uuml;r&amp;uuml;lməlidir. Bu rəqəm &amp;ccedil;ox da ola bilər&amp;rdquo;.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;2016-cı ilin Aprel d&amp;ouml;y&amp;uuml;şləri zamanı Azərbaycan Respublikasının Prezidenti, Silahlı Q&amp;uuml;vvələrin Ali Baş Komandanı İlham Əliyevin rəhbərliyi ilə ordumuz tərəfindən Lələtəpə y&amp;uuml;ksəkliyinin azad edilməsindən sonra Cəbrayıl rayonunun işğaldan azad olunmuş Cocuq Mərcanlı kəndində yaşayışın bərpa edilməsi &amp;uuml;&amp;ccedil;&amp;uuml;n imkan yarandı. Cocuq Mərcanlı kəndinin bərpası tarixi nailiyyətdir, indi 150 evdən ibarət g&amp;ouml;zəl qəsəbə yeni həyatını yaşayır. Eyni zamanda, 2017-ci ildə Şıxarx qəsəbəsində 1170 ailə &amp;uuml;&amp;ccedil;&amp;uuml;n yeni şəhərcik salınıb. Bu da əlamətdar hadisədir. M&amp;ouml;htərəm Prezidentin qeyd etdiyi kimi, təkcə ona g&amp;ouml;rə yox ki, b&amp;ouml;y&amp;uuml;k k&amp;ouml;&amp;ccedil;k&amp;uuml;n şəhərciyi yaradılıb. Ona g&amp;ouml;rə ki, Şıxarx qəsəbəsi vaxtilə işğal altında olan ərazidir. Bu ərazi erməni işğal&amp;ccedil;ılarından təmizlənib, azad edilib və o yerdə yeni b&amp;ouml;y&amp;uuml;k şəhərciyin salınması bir daha onu g&amp;ouml;stərir ki, Dağlıq Qarabağ bizim əzəli torpağımızdır. B&amp;uuml;t&amp;uuml;n bunlar B&amp;ouml;y&amp;uuml;k Qayıdışın n&amp;uuml;munəsidir.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Son illər d&amp;uuml;nyanın m&amp;uuml;xtəlif &amp;ouml;lkələrində baş verən xaotik b&amp;ouml;hranlar, ayrı-ayrı &amp;ouml;lkələrin tənəzz&amp;uuml;l&amp;uuml; bir daha s&amp;uuml;but edir ki, ilk n&amp;ouml;vbədə, təhl&amp;uuml;kəsizlik təmin olunmadan cəmiyyətin inkişafı m&amp;uuml;mk&amp;uuml;n deyil. B&amp;uuml;t&amp;uuml;n h&amp;ouml;kumət strukturlarında fəaliyyətin milli maraqlar, qanun&amp;ccedil;uluq, insan h&amp;uuml;quq və azadlıqlarının aliliyi prinsipləri &amp;uuml;zərində qurulmasını əsas məqsəd kimi m&amp;uuml;əyyənləşdirmiş Prezident İlham Əliyevin 2003-c&amp;uuml; ilin oktyabr ayında Azərbaycan Respublikasının Prezidenti se&amp;ccedil;ilməsi ilə &amp;ouml;lkəmiz inkişafının yeni mərhələsinə qədəm qoydu, o, gələcək uğurlara və y&amp;uuml;ksəlişə doğru inamla irəliləyən m&amp;uuml;stəqil d&amp;ouml;vlətimizin simvoluna &amp;ccedil;evrildi. Azərbaycan xarici siyasət sahəsində də b&amp;ouml;y&amp;uuml;k uğurlar əldə etdi. Respublikamız bu g&amp;uuml;n d&amp;uuml;nyada &amp;ccedil;ox b&amp;ouml;y&amp;uuml;k n&amp;uuml;fuza malik olan &amp;ouml;lkə kimi tanınır.&lt;/span&gt;&lt;/p&gt;', 0, 1, 1, 3, 1, 0),
(4, 'Müasir dünyada miqrasiya prosesləri: Azərbaycan reallığı kontekstində', '', '', '', '', '&lt;p align=&quot;center&quot;&gt;&lt;strong&gt;18 dekabr - Beynəlxalq Miqrant G&amp;uuml;n&amp;uuml;d&amp;uuml;r&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Hər il dekabrın 18-i d&amp;uuml;nyada Beynəlxalq Miqrant G&amp;uuml;n&amp;uuml; kimi qeyd olunur.&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;&lt;a href=&quot;https://azertag.az/xeber/1121849&quot; target=&quot;_blank&quot;&gt;AZƏRTAC&lt;/a&gt;&lt;/strong&gt;&lt;strong&gt;&amp;nbsp;&lt;/strong&gt;&lt;strong&gt;bu g&amp;uuml;nlə əlaqədar D&amp;ouml;vlət Miqrasiya Xidmətinin rəisi Firudin Nəbiyevin məqaləsini təqdim edir.&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;Bu g&amp;uuml;n tarixinin ən m&amp;ouml;htəşəm inkişaf və q&amp;uuml;drətli d&amp;ouml;vlət&amp;ccedil;ilik d&amp;ouml;vr&amp;uuml;n&amp;uuml; yaşayan Azərbaycanın əldə etdiyi nailiyyətlər &amp;uuml;mummilli lider Heydər Əliyevin əsasını qoyduğu, Prezident İlham Əliyevin isə uğurla davam etdirdiyi milli intibah siyasətinə və ictimai sabitliyə s&amp;ouml;ykənir.&lt;/p&gt;\r\n&lt;p&gt;Məlum olduğu kimi, bu ilin 9 ayında Azərbaycanın valyuta ehtiyatları artıb və hazırda bu rəqəm 42 milyard dollar səviyyəsindədir. İlin əvvəlindən valyuta ehtiyatlarımız 4,5 milyard dollar artıb. Qeyri-neft iqtisadiyyatımız 2,5 faiz artıb. B&amp;uuml;t&amp;uuml;n bunlar Prezident İlham Əliyevin rəhbərliyi ilə &amp;ouml;lkəmizdə həyata ke&amp;ccedil;irilən siyasətin və g&amp;ouml;r&amp;uuml;lən işlərin nəticələridir.&lt;/p&gt;\r\n&lt;p&gt;Davos D&amp;uuml;nya İqtisadi Forumu Azərbaycan iqtisadiyyatını rəqabətqabiliyyətliliyinə g&amp;ouml;rə d&amp;uuml;nyada 35-ci yerə (MDB &amp;ouml;lkələri sırasında birinci yer) layiq g&amp;ouml;r&amp;uuml;b. D&amp;uuml;nyada iqtisadi-maliyyə b&amp;ouml;hranının davam etdiyi bir şəraitdə &amp;ouml;lkəmizin iqtisadiyyatı y&amp;uuml;ksək qiymət alıb.&lt;/p&gt;\r\n&lt;p&gt;Azərbaycanın inkişafının və d&amp;uuml;nyada etiraf edilən sabit &amp;ouml;lkə imicinin nəticəsi olaraq, artıq uzun illərdir ki, respublikamız ən m&amp;ouml;təbər beynəlxalq tədbirlərə ev sahibliyi edir. Hər bir tədbirdə d&amp;ouml;vlətimizin, şəxsən Prezident İlham Əliyevin və Birinci vitse-prezident xanım Mehriban Əliyevanın ali diqqəti və təşkilat&amp;ccedil;ılığı sayəsində genişmiqyaslı, m&amp;ouml;htəşəm fəaliyyət ortaya qoyulur.&lt;/p&gt;\r\n&lt;p&gt;&amp;Uuml;mumiyyətlə, bu il ke&amp;ccedil;irilmiş IV İslam Həmrəyliyi Oyunları, Formula-1 Azərbaycan Qran-Prisi, IV &amp;Uuml;mumd&amp;uuml;nya Mədəniyyətlərarası Dialoq Forumu Azərbaycanın yaratdığı n&amp;ouml;vbəti m&amp;uuml;kəmməl təşkilat&amp;ccedil;ılıq modeli idi. Bu tədbirlərin &amp;ouml;lkəmizdə ke&amp;ccedil;irilməsi Azərbaycan Respublikasının tolerantlıq və multikulturalizm prinsiplərinə bağlılığının bariz n&amp;uuml;munəsidir. Əlbəttə, Prezident İlham Əliyevin Sərəncamı ilə 2017-ci ilin &amp;ldquo;İslam Həmrəyliyi İli&amp;rdquo; elan edilməsi də təsad&amp;uuml;fi deyildir, bunun &amp;ccedil;ox b&amp;ouml;y&amp;uuml;k ictimai-siyasi, beynəlxalq və ideoloji əhəmiyyəti var. &amp;Ccedil;&amp;uuml;nki bu il &amp;ccedil;ər&amp;ccedil;ivəsində g&amp;ouml;r&amp;uuml;lən işlərdə təkcə din amili nəzərə alınmayıb, bu prosesə daha geniş spektrdən yanaşılıb.&lt;/p&gt;\r\n&lt;p&gt;Daha bir əlamətdar hadisə aparıcı beynəlxalq enerji şirkətlərinin konsorsiumu ilə yeni meqaneft m&amp;uuml;qaviləsinin imzalanmasıdır. Bu m&amp;uuml;qavilə Azərbaycana nəhəng neft yataqlarının 2050-ci ilə qədər işlənməsinə imkan verəcək. Sabit neft istehsalat profilini saxlamaq &amp;uuml;&amp;ccedil;&amp;uuml;n on milyardlarla əlavə investisiya yatırılacaq.&lt;/p&gt;\r\n&lt;p&gt;Oktyabrın 30-da tarixi, strateji əhəmiyyətli layihə olan Bakı-Tbilisi-Qars dəmir yolunun a&amp;ccedil;ılışı isə x&amp;uuml;susilə əlamətdar oldu. Bu m&amp;uuml;nasibətlə Azərbaycan Respublikasının Prezidenti İlham Əliyev, birinci xanım Mehriban Əliyeva, T&amp;uuml;rkiyə Respublikasının Prezidenti Rəcəb Tayyib Ərdoğan və xanımı Əminə Ərdoğan, Qazaxıstan Respublikasının Baş naziri Bakıtjan Saqintayev, G&amp;uuml;rc&amp;uuml;stanın Baş naziri Giorgi Kvirikaşvili, &amp;Ouml;zbəkistan Respublikasının Baş naziri Abdulla Aripov, həm&amp;ccedil;inin Tacikistan və T&amp;uuml;rkmənistan respublikalarından n&amp;uuml;mayəndə heyətlərinin iştirakı ilə Bakı Beynəlxalq Dəniz Ticarət Limanında təntənəli mərasim ke&amp;ccedil;irildi. Bakı-Tbilisi-Qars dəmir yolunun tikintisinə dair Saziş 2007-ci il fevralın 7-də Tbilisidə imzalanıb. Elə həmin ilin noyabrında G&amp;uuml;rc&amp;uuml;stanın Marabda məntəqəsində dəmir yolu xəttinin təməli qoyulub. 2008-ci ilin iyulunda isə Qars şəhərində Qars-G&amp;uuml;rc&amp;uuml;stan sərhədi hissəsinin tikintisinin təməlqoyma mərasimi ke&amp;ccedil;irilib. &amp;Uuml;mumi uzunluğu təxminən 850 kilometr olan Bakı-Tbilisi-Qars dəmir yolu xəttinin 504 kilometrlik hissəsi Azərbaycanın ərazisinə d&amp;uuml;ş&amp;uuml;r. Dəmir yolu xəttinin 263 kilometri G&amp;uuml;rc&amp;uuml;standan ke&amp;ccedil;ir. Yolun 79 kilometri isə T&amp;uuml;rkiyə ərazisindədir.&lt;/p&gt;', 0, 2, 1, 1, 2, 0);
INSERT INTO `articles` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(5, 'Müasir dünyada miqrasiya prosesləri: Azərbaycan reallığı kontekstində', '', '', '', '', '&lt;p align=&quot;center&quot;&gt;&lt;strong&gt;18 dekabr - Beynəlxalq Miqrant G&amp;uuml;n&amp;uuml;d&amp;uuml;r&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Hər il dekabrın 18-i d&amp;uuml;nyada Beynəlxalq Miqrant G&amp;uuml;n&amp;uuml; kimi qeyd olunur.&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;&lt;a href=&quot;https://azertag.az/xeber/1121849&quot; target=&quot;_blank&quot;&gt;AZƏRTAC&lt;/a&gt;&lt;/strong&gt;&lt;strong&gt;&amp;nbsp;&lt;/strong&gt;&lt;strong&gt;bu g&amp;uuml;nlə əlaqədar D&amp;ouml;vlət Miqrasiya Xidmətinin rəisi Firudin Nəbiyevin məqaləsini təqdim edir.&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;Bu g&amp;uuml;n tarixinin ən m&amp;ouml;htəşəm inkişaf və q&amp;uuml;drətli d&amp;ouml;vlət&amp;ccedil;ilik d&amp;ouml;vr&amp;uuml;n&amp;uuml; yaşayan Azərbaycanın əldə etdiyi nailiyyətlər &amp;uuml;mummilli lider Heydər Əliyevin əsasını qoyduğu, Prezident İlham Əliyevin isə uğurla davam etdirdiyi milli intibah siyasətinə və ictimai sabitliyə s&amp;ouml;ykənir.&lt;/p&gt;\r\n&lt;p&gt;Məlum olduğu kimi, bu ilin 9 ayında Azərbaycanın valyuta ehtiyatları artıb və hazırda bu rəqəm 42 milyard dollar səviyyəsindədir. İlin əvvəlindən valyuta ehtiyatlarımız 4,5 milyard dollar artıb. Qeyri-neft iqtisadiyyatımız 2,5 faiz artıb. B&amp;uuml;t&amp;uuml;n bunlar Prezident İlham Əliyevin rəhbərliyi ilə &amp;ouml;lkəmizdə həyata ke&amp;ccedil;irilən siyasətin və g&amp;ouml;r&amp;uuml;lən işlərin nəticələridir.&lt;/p&gt;\r\n&lt;p&gt;Davos D&amp;uuml;nya İqtisadi Forumu Azərbaycan iqtisadiyyatını rəqabətqabiliyyətliliyinə g&amp;ouml;rə d&amp;uuml;nyada 35-ci yerə (MDB &amp;ouml;lkələri sırasında birinci yer) layiq g&amp;ouml;r&amp;uuml;b. D&amp;uuml;nyada iqtisadi-maliyyə b&amp;ouml;hranının davam etdiyi bir şəraitdə &amp;ouml;lkəmizin iqtisadiyyatı y&amp;uuml;ksək qiymət alıb.&lt;/p&gt;\r\n&lt;p&gt;Azərbaycanın inkişafının və d&amp;uuml;nyada etiraf edilən sabit &amp;ouml;lkə imicinin nəticəsi olaraq, artıq uzun illərdir ki, respublikamız ən m&amp;ouml;təbər beynəlxalq tədbirlərə ev sahibliyi edir. Hər bir tədbirdə d&amp;ouml;vlətimizin, şəxsən Prezident İlham Əliyevin və Birinci vitse-prezident xanım Mehriban Əliyevanın ali diqqəti və təşkilat&amp;ccedil;ılığı sayəsində genişmiqyaslı, m&amp;ouml;htəşəm fəaliyyət ortaya qoyulur.&lt;/p&gt;\r\n&lt;p&gt;&amp;Uuml;mumiyyətlə, bu il ke&amp;ccedil;irilmiş IV İslam Həmrəyliyi Oyunları, Formula-1 Azərbaycan Qran-Prisi, IV &amp;Uuml;mumd&amp;uuml;nya Mədəniyyətlərarası Dialoq Forumu Azərbaycanın yaratdığı n&amp;ouml;vbəti m&amp;uuml;kəmməl təşkilat&amp;ccedil;ılıq modeli idi. Bu tədbirlərin &amp;ouml;lkəmizdə ke&amp;ccedil;irilməsi Azərbaycan Respublikasının tolerantlıq və multikulturalizm prinsiplərinə bağlılığının bariz n&amp;uuml;munəsidir. Əlbəttə, Prezident İlham Əliyevin Sərəncamı ilə 2017-ci ilin &amp;ldquo;İslam Həmrəyliyi İli&amp;rdquo; elan edilməsi də təsad&amp;uuml;fi deyildir, bunun &amp;ccedil;ox b&amp;ouml;y&amp;uuml;k ictimai-siyasi, beynəlxalq və ideoloji əhəmiyyəti var. &amp;Ccedil;&amp;uuml;nki bu il &amp;ccedil;ər&amp;ccedil;ivəsində g&amp;ouml;r&amp;uuml;lən işlərdə təkcə din amili nəzərə alınmayıb, bu prosesə daha geniş spektrdən yanaşılıb.&lt;/p&gt;\r\n&lt;p&gt;Daha bir əlamətdar hadisə aparıcı beynəlxalq enerji şirkətlərinin konsorsiumu ilə yeni meqaneft m&amp;uuml;qaviləsinin imzalanmasıdır. Bu m&amp;uuml;qavilə Azərbaycana nəhəng neft yataqlarının 2050-ci ilə qədər işlənməsinə imkan verəcək. Sabit neft istehsalat profilini saxlamaq &amp;uuml;&amp;ccedil;&amp;uuml;n on milyardlarla əlavə investisiya yatırılacaq.&lt;/p&gt;\r\n&lt;p&gt;Oktyabrın 30-da tarixi, strateji əhəmiyyətli layihə olan Bakı-Tbilisi-Qars dəmir yolunun a&amp;ccedil;ılışı isə x&amp;uuml;susilə əlamətdar oldu. Bu m&amp;uuml;nasibətlə Azərbaycan Respublikasının Prezidenti İlham Əliyev, birinci xanım Mehriban Əliyeva, T&amp;uuml;rkiyə Respublikasının Prezidenti Rəcəb Tayyib Ərdoğan və xanımı Əminə Ərdoğan, Qazaxıstan Respublikasının Baş naziri Bakıtjan Saqintayev, G&amp;uuml;rc&amp;uuml;stanın Baş naziri Giorgi Kvirikaşvili, &amp;Ouml;zbəkistan Respublikasının Baş naziri Abdulla Aripov, həm&amp;ccedil;inin Tacikistan və T&amp;uuml;rkmənistan respublikalarından n&amp;uuml;mayəndə heyətlərinin iştirakı ilə Bakı Beynəlxalq Dəniz Ticarət Limanında təntənəli mərasim ke&amp;ccedil;irildi. Bakı-Tbilisi-Qars dəmir yolunun tikintisinə dair Saziş 2007-ci il fevralın 7-də Tbilisidə imzalanıb. Elə həmin ilin noyabrında G&amp;uuml;rc&amp;uuml;stanın Marabda məntəqəsində dəmir yolu xəttinin təməli qoyulub. 2008-ci ilin iyulunda isə Qars şəhərində Qars-G&amp;uuml;rc&amp;uuml;stan sərhədi hissəsinin tikintisinin təməlqoyma mərasimi ke&amp;ccedil;irilib. &amp;Uuml;mumi uzunluğu təxminən 850 kilometr olan Bakı-Tbilisi-Qars dəmir yolu xəttinin 504 kilometrlik hissəsi Azərbaycanın ərazisinə d&amp;uuml;ş&amp;uuml;r. Dəmir yolu xəttinin 263 kilometri G&amp;uuml;rc&amp;uuml;standan ke&amp;ccedil;ir. Yolun 79 kilometri isə T&amp;uuml;rkiyə ərazisindədir.&lt;/p&gt;', 0, 2, 1, 2, 2, 0),
(6, 'Müasir dünyada miqrasiya prosesləri: Azərbaycan reallığı kontekstində', '', '', '', '', '&lt;p align=&quot;center&quot;&gt;&lt;strong&gt;18 dekabr - Beynəlxalq Miqrant G&amp;uuml;n&amp;uuml;d&amp;uuml;r&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Hər il dekabrın 18-i d&amp;uuml;nyada Beynəlxalq Miqrant G&amp;uuml;n&amp;uuml; kimi qeyd olunur.&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;&lt;a href=&quot;https://azertag.az/xeber/1121849&quot; target=&quot;_blank&quot;&gt;AZƏRTAC&lt;/a&gt;&lt;/strong&gt;&lt;strong&gt;&amp;nbsp;&lt;/strong&gt;&lt;strong&gt;bu g&amp;uuml;nlə əlaqədar D&amp;ouml;vlət Miqrasiya Xidmətinin rəisi Firudin Nəbiyevin məqaləsini təqdim edir.&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;Bu g&amp;uuml;n tarixinin ən m&amp;ouml;htəşəm inkişaf və q&amp;uuml;drətli d&amp;ouml;vlət&amp;ccedil;ilik d&amp;ouml;vr&amp;uuml;n&amp;uuml; yaşayan Azərbaycanın əldə etdiyi nailiyyətlər &amp;uuml;mummilli lider Heydər Əliyevin əsasını qoyduğu, Prezident İlham Əliyevin isə uğurla davam etdirdiyi milli intibah siyasətinə və ictimai sabitliyə s&amp;ouml;ykənir.&lt;/p&gt;\r\n&lt;p&gt;Məlum olduğu kimi, bu ilin 9 ayında Azərbaycanın valyuta ehtiyatları artıb və hazırda bu rəqəm 42 milyard dollar səviyyəsindədir. İlin əvvəlindən valyuta ehtiyatlarımız 4,5 milyard dollar artıb. Qeyri-neft iqtisadiyyatımız 2,5 faiz artıb. B&amp;uuml;t&amp;uuml;n bunlar Prezident İlham Əliyevin rəhbərliyi ilə &amp;ouml;lkəmizdə həyata ke&amp;ccedil;irilən siyasətin və g&amp;ouml;r&amp;uuml;lən işlərin nəticələridir.&lt;/p&gt;\r\n&lt;p&gt;Davos D&amp;uuml;nya İqtisadi Forumu Azərbaycan iqtisadiyyatını rəqabətqabiliyyətliliyinə g&amp;ouml;rə d&amp;uuml;nyada 35-ci yerə (MDB &amp;ouml;lkələri sırasında birinci yer) layiq g&amp;ouml;r&amp;uuml;b. D&amp;uuml;nyada iqtisadi-maliyyə b&amp;ouml;hranının davam etdiyi bir şəraitdə &amp;ouml;lkəmizin iqtisadiyyatı y&amp;uuml;ksək qiymət alıb.&lt;/p&gt;\r\n&lt;p&gt;Azərbaycanın inkişafının və d&amp;uuml;nyada etiraf edilən sabit &amp;ouml;lkə imicinin nəticəsi olaraq, artıq uzun illərdir ki, respublikamız ən m&amp;ouml;təbər beynəlxalq tədbirlərə ev sahibliyi edir. Hər bir tədbirdə d&amp;ouml;vlətimizin, şəxsən Prezident İlham Əliyevin və Birinci vitse-prezident xanım Mehriban Əliyevanın ali diqqəti və təşkilat&amp;ccedil;ılığı sayəsində genişmiqyaslı, m&amp;ouml;htəşəm fəaliyyət ortaya qoyulur.&lt;/p&gt;\r\n&lt;p&gt;&amp;Uuml;mumiyyətlə, bu il ke&amp;ccedil;irilmiş IV İslam Həmrəyliyi Oyunları, Formula-1 Azərbaycan Qran-Prisi, IV &amp;Uuml;mumd&amp;uuml;nya Mədəniyyətlərarası Dialoq Forumu Azərbaycanın yaratdığı n&amp;ouml;vbəti m&amp;uuml;kəmməl təşkilat&amp;ccedil;ılıq modeli idi. Bu tədbirlərin &amp;ouml;lkəmizdə ke&amp;ccedil;irilməsi Azərbaycan Respublikasının tolerantlıq və multikulturalizm prinsiplərinə bağlılığının bariz n&amp;uuml;munəsidir. Əlbəttə, Prezident İlham Əliyevin Sərəncamı ilə 2017-ci ilin &amp;ldquo;İslam Həmrəyliyi İli&amp;rdquo; elan edilməsi də təsad&amp;uuml;fi deyildir, bunun &amp;ccedil;ox b&amp;ouml;y&amp;uuml;k ictimai-siyasi, beynəlxalq və ideoloji əhəmiyyəti var. &amp;Ccedil;&amp;uuml;nki bu il &amp;ccedil;ər&amp;ccedil;ivəsində g&amp;ouml;r&amp;uuml;lən işlərdə təkcə din amili nəzərə alınmayıb, bu prosesə daha geniş spektrdən yanaşılıb.&lt;/p&gt;\r\n&lt;p&gt;Daha bir əlamətdar hadisə aparıcı beynəlxalq enerji şirkətlərinin konsorsiumu ilə yeni meqaneft m&amp;uuml;qaviləsinin imzalanmasıdır. Bu m&amp;uuml;qavilə Azərbaycana nəhəng neft yataqlarının 2050-ci ilə qədər işlənməsinə imkan verəcək. Sabit neft istehsalat profilini saxlamaq &amp;uuml;&amp;ccedil;&amp;uuml;n on milyardlarla əlavə investisiya yatırılacaq.&lt;/p&gt;\r\n&lt;p&gt;Oktyabrın 30-da tarixi, strateji əhəmiyyətli layihə olan Bakı-Tbilisi-Qars dəmir yolunun a&amp;ccedil;ılışı isə x&amp;uuml;susilə əlamətdar oldu. Bu m&amp;uuml;nasibətlə Azərbaycan Respublikasının Prezidenti İlham Əliyev, birinci xanım Mehriban Əliyeva, T&amp;uuml;rkiyə Respublikasının Prezidenti Rəcəb Tayyib Ərdoğan və xanımı Əminə Ərdoğan, Qazaxıstan Respublikasının Baş naziri Bakıtjan Saqintayev, G&amp;uuml;rc&amp;uuml;stanın Baş naziri Giorgi Kvirikaşvili, &amp;Ouml;zbəkistan Respublikasının Baş naziri Abdulla Aripov, həm&amp;ccedil;inin Tacikistan və T&amp;uuml;rkmənistan respublikalarından n&amp;uuml;mayəndə heyətlərinin iştirakı ilə Bakı Beynəlxalq Dəniz Ticarət Limanında təntənəli mərasim ke&amp;ccedil;irildi. Bakı-Tbilisi-Qars dəmir yolunun tikintisinə dair Saziş 2007-ci il fevralın 7-də Tbilisidə imzalanıb. Elə həmin ilin noyabrında G&amp;uuml;rc&amp;uuml;stanın Marabda məntəqəsində dəmir yolu xəttinin təməli qoyulub. 2008-ci ilin iyulunda isə Qars şəhərində Qars-G&amp;uuml;rc&amp;uuml;stan sərhədi hissəsinin tikintisinin təməlqoyma mərasimi ke&amp;ccedil;irilib. &amp;Uuml;mumi uzunluğu təxminən 850 kilometr olan Bakı-Tbilisi-Qars dəmir yolu xəttinin 504 kilometrlik hissəsi Azərbaycanın ərazisinə d&amp;uuml;ş&amp;uuml;r. Dəmir yolu xəttinin 263 kilometri G&amp;uuml;rc&amp;uuml;standan ke&amp;ccedil;ir. Yolun 79 kilometri isə T&amp;uuml;rkiyə ərazisindədir.&lt;/p&gt;', 0, 2, 1, 3, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `audio`
--

CREATE TABLE `audio` (
  `id` int(10) NOT NULL,
  `tarix` int(11) NOT NULL,
  `basliq` varchar(255) DEFAULT NULL,
  `tam_metn` text,
  `tip` varchar(5) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) DEFAULT '0',
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `audio`
--

INSERT INTO `audio` (`id`, `tarix`, `basliq`, `tam_metn`, `tip`, `sira`, `aktivlik`, `lang_id`, `auto_id`) VALUES
(1, 1537439720, 'Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi1', '&lt;p&gt;sa as adas das&lt;/p&gt;', 'mp3', 1, 1, 1, 1),
(2, 1537439720, '2Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi', '&lt;p&gt;qweq we&lt;/p&gt;', 'mp3', 1, 1, 2, 1),
(3, 1537439721, 'Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi3', '&lt;p&gt;sadsa&lt;/p&gt;', 'mp3', 1, 1, 3, 1),
(4, 1537440554, 'sad sad sadsa', '&lt;p&gt;&amp;nbsp;asd sadas das&lt;/p&gt;', 'mp3', 2, 1, 1, 2),
(5, 1537440554, 'sad sad sadsa', '', 'mp3', 2, 1, 2, 2),
(6, 1537440555, 'sad sad sadsa', '', 'mp3', 2, 1, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `beledci`
--

CREATE TABLE `beledci` (
  `id` int(3) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `beledci`
--

INSERT INTO `beledci` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, 'Miqrasiya qaydaları', '', '', '', '', '&lt;p&gt;Əcnəbilərin və vətəndaşlığı olmayan şəxslərin &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddəti vizada g&amp;ouml;stərilən qalma m&amp;uuml;ddətindən artıq ola bilməz. Bu m&amp;uuml;ddət bitənədək əcnəbilərin və vətəndaşlığı olmayan şəxslərin &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddəti uzadılmamışdırsa, onlara m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verilməmişdirsə, habelə onlardan m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılması və ya m&amp;uuml;vəqqəti yaşama icazəsinin verilməsi &amp;uuml;&amp;ccedil;&amp;uuml;n ərizə qəbul edilməmişdirsə, onlar Azərbaycan Respublikasının ərazisini tərk etməlidirlər.&lt;/p&gt;\r\n&lt;p&gt;Azərbaycan Respublikasında 15&amp;nbsp;g&amp;uuml;ndən artıq m&amp;uuml;vəqqəti&amp;nbsp; olan (o c&amp;uuml;mlədən &amp;ouml;lkə daxilində olduğu yeri dəyişdikdə) əcnəbilər və ya vətəndaşlığı olmayan şəxslər hər hansı d&amp;ouml;vlət r&amp;uuml;sumu &amp;ouml;dəmədən Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Xidməti tərəfindən olduğu yer &amp;uuml;zrə qeydiyyata alınmaq &amp;uuml;&amp;ccedil;&amp;uuml;n qaldığı yerin (mehmanxana, sanatoriya, istirahət evi, pansionat, kempinq, turist bazası, xəstəxana və ya digər belə ictimai yerlər) m&amp;uuml;diriyyətinə və ya mənzilin, digər yaşayış sahəsinin sahibinə m&amp;uuml;raciət etməlidir.&lt;/p&gt;\r\n&lt;p&gt;Elektron vizada g&amp;ouml;stərilən m&amp;uuml;ddətdən artıq &amp;ouml;lkə ərazisində qalan və ya olduğu yer &amp;uuml;zrə qeydiyyata alınmayan (15&amp;nbsp;g&amp;uuml;ndən artıq qaldıqda) yaxud bir yerdən başqa yerə getdikdə olduğu yer &amp;uuml;zrə qeydiyyat qaydalarına əməl&amp;nbsp; etməyən əcnəbilər və vətəndaşlığı olmayan şəxslər qanunla m&amp;uuml;əyyən olunmuş qaydada məsuliyyətə cəlb edilirlər (Bu qaydaların pozulması Azərbaycan Respublikası İnzibati Xətalar Məcəlləsinin&amp;nbsp;&lt;a href=&quot;https://migration.gov.az/content/pdf/575madde.pdf&quot; target=&quot;_blank&quot;&gt;575-ci maddəsinə&lt;/a&gt;&amp;nbsp;əsasən inzibati məsuliyyətə səbəb olur).&lt;/p&gt;\r\n&lt;p&gt;Elektron viza əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasında haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq h&amp;uuml;ququ vermir.Azərbaycan Respublikasında haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq niyyətində olan əcnəbilər və vətəndaşlığı olmayan şəxslər Azərbaycan Respublikasının Miqrasiya Məcəlləsində m&amp;uuml;əyyən edilmiş qaydada əmək vizası (&amp;ouml;lkədə qalma m&amp;uuml;ddəti 90 g&amp;uuml;nədək m&amp;uuml;əyyən edilir) almalıdırlar.&lt;/p&gt;\r\n&lt;p&gt;Azərbaycan Respublikasının ərazisində həm m&amp;uuml;vəqqəti yaşamaq, həm də haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq istəyən əcnəbilər və vətəndaşlığı olmayan şəxslər Azərbaycan Respublikasının Miqrasiya Məcəlləsində m&amp;uuml;əyyən edilmiş qaydada m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə ilə yanaşı, iş icazəsi almalıdırlar.&lt;/p&gt;', 0, 1, 1, 1, 1, 0),
(2, 'Miqrasiya qaydaları', '', '', '', '', '&lt;p&gt;Əcnəbilərin və vətəndaşlığı olmayan şəxslərin &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddəti vizada g&amp;ouml;stərilən qalma m&amp;uuml;ddətindən artıq ola bilməz. Bu m&amp;uuml;ddət bitənədək əcnəbilərin və vətəndaşlığı olmayan şəxslərin &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddəti uzadılmamışdırsa, onlara m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verilməmişdirsə, habelə onlardan m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılması və ya m&amp;uuml;vəqqəti yaşama icazəsinin verilməsi &amp;uuml;&amp;ccedil;&amp;uuml;n ərizə qəbul edilməmişdirsə, onlar Azərbaycan Respublikasının ərazisini tərk etməlidirlər.&lt;/p&gt;\r\n&lt;p&gt;Azərbaycan Respublikasında 15&amp;nbsp;g&amp;uuml;ndən artıq m&amp;uuml;vəqqəti&amp;nbsp; olan (o c&amp;uuml;mlədən &amp;ouml;lkə daxilində olduğu yeri dəyişdikdə) əcnəbilər və ya vətəndaşlığı olmayan şəxslər hər hansı d&amp;ouml;vlət r&amp;uuml;sumu &amp;ouml;dəmədən Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Xidməti tərəfindən olduğu yer &amp;uuml;zrə qeydiyyata alınmaq &amp;uuml;&amp;ccedil;&amp;uuml;n qaldığı yerin (mehmanxana, sanatoriya, istirahət evi, pansionat, kempinq, turist bazası, xəstəxana və ya digər belə ictimai yerlər) m&amp;uuml;diriyyətinə və ya mənzilin, digər yaşayış sahəsinin sahibinə m&amp;uuml;raciət etməlidir.&lt;/p&gt;\r\n&lt;p&gt;Elektron vizada g&amp;ouml;stərilən m&amp;uuml;ddətdən artıq &amp;ouml;lkə ərazisində qalan və ya olduğu yer &amp;uuml;zrə qeydiyyata alınmayan (15&amp;nbsp;g&amp;uuml;ndən artıq qaldıqda) yaxud bir yerdən başqa yerə getdikdə olduğu yer &amp;uuml;zrə qeydiyyat qaydalarına əməl&amp;nbsp; etməyən əcnəbilər və vətəndaşlığı olmayan şəxslər qanunla m&amp;uuml;əyyən olunmuş qaydada məsuliyyətə cəlb edilirlər (Bu qaydaların pozulması Azərbaycan Respublikası İnzibati Xətalar Məcəlləsinin&amp;nbsp;&lt;a href=&quot;https://migration.gov.az/content/pdf/575madde.pdf&quot; target=&quot;_blank&quot;&gt;575-ci maddəsinə&lt;/a&gt;&amp;nbsp;əsasən inzibati məsuliyyətə səbəb olur).&lt;/p&gt;\r\n&lt;p&gt;Elektron viza əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasında haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq h&amp;uuml;ququ vermir.Azərbaycan Respublikasında haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq niyyətində olan əcnəbilər və vətəndaşlığı olmayan şəxslər Azərbaycan Respublikasının Miqrasiya Məcəlləsində m&amp;uuml;əyyən edilmiş qaydada əmək vizası (&amp;ouml;lkədə qalma m&amp;uuml;ddəti 90 g&amp;uuml;nədək m&amp;uuml;əyyən edilir) almalıdırlar.&lt;/p&gt;\r\n&lt;p&gt;Azərbaycan Respublikasının ərazisində həm m&amp;uuml;vəqqəti yaşamaq, həm də haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq istəyən əcnəbilər və vətəndaşlığı olmayan şəxslər Azərbaycan Respublikasının Miqrasiya Məcəlləsində m&amp;uuml;əyyən edilmiş qaydada m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə ilə yanaşı, iş icazəsi almalıdırlar.&lt;/p&gt;', 0, 1, 1, 2, 1, 0),
(3, 'Miqrasiya qaydaları', '', '', '', '', '&lt;p&gt;Əcnəbilərin və vətəndaşlığı olmayan şəxslərin &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddəti vizada g&amp;ouml;stərilən qalma m&amp;uuml;ddətindən artıq ola bilməz. Bu m&amp;uuml;ddət bitənədək əcnəbilərin və vətəndaşlığı olmayan şəxslərin &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddəti uzadılmamışdırsa, onlara m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verilməmişdirsə, habelə onlardan m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılması və ya m&amp;uuml;vəqqəti yaşama icazəsinin verilməsi &amp;uuml;&amp;ccedil;&amp;uuml;n ərizə qəbul edilməmişdirsə, onlar Azərbaycan Respublikasının ərazisini tərk etməlidirlər.&lt;/p&gt;\r\n&lt;p&gt;Azərbaycan Respublikasında 15&amp;nbsp;g&amp;uuml;ndən artıq m&amp;uuml;vəqqəti&amp;nbsp; olan (o c&amp;uuml;mlədən &amp;ouml;lkə daxilində olduğu yeri dəyişdikdə) əcnəbilər və ya vətəndaşlığı olmayan şəxslər hər hansı d&amp;ouml;vlət r&amp;uuml;sumu &amp;ouml;dəmədən Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Xidməti tərəfindən olduğu yer &amp;uuml;zrə qeydiyyata alınmaq &amp;uuml;&amp;ccedil;&amp;uuml;n qaldığı yerin (mehmanxana, sanatoriya, istirahət evi, pansionat, kempinq, turist bazası, xəstəxana və ya digər belə ictimai yerlər) m&amp;uuml;diriyyətinə və ya mənzilin, digər yaşayış sahəsinin sahibinə m&amp;uuml;raciət etməlidir.&lt;/p&gt;\r\n&lt;p&gt;Elektron vizada g&amp;ouml;stərilən m&amp;uuml;ddətdən artıq &amp;ouml;lkə ərazisində qalan və ya olduğu yer &amp;uuml;zrə qeydiyyata alınmayan (15&amp;nbsp;g&amp;uuml;ndən artıq qaldıqda) yaxud bir yerdən başqa yerə getdikdə olduğu yer &amp;uuml;zrə qeydiyyat qaydalarına əməl&amp;nbsp; etməyən əcnəbilər və vətəndaşlığı olmayan şəxslər qanunla m&amp;uuml;əyyən olunmuş qaydada məsuliyyətə cəlb edilirlər (Bu qaydaların pozulması Azərbaycan Respublikası İnzibati Xətalar Məcəlləsinin&amp;nbsp;&lt;a href=&quot;https://migration.gov.az/content/pdf/575madde.pdf&quot; target=&quot;_blank&quot;&gt;575-ci maddəsinə&lt;/a&gt;&amp;nbsp;əsasən inzibati məsuliyyətə səbəb olur).&lt;/p&gt;\r\n&lt;p&gt;Elektron viza əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasında haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq h&amp;uuml;ququ vermir.Azərbaycan Respublikasında haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq niyyətində olan əcnəbilər və vətəndaşlığı olmayan şəxslər Azərbaycan Respublikasının Miqrasiya Məcəlləsində m&amp;uuml;əyyən edilmiş qaydada əmək vizası (&amp;ouml;lkədə qalma m&amp;uuml;ddəti 90 g&amp;uuml;nədək m&amp;uuml;əyyən edilir) almalıdırlar.&lt;/p&gt;\r\n&lt;p&gt;Azərbaycan Respublikasının ərazisində həm m&amp;uuml;vəqqəti yaşamaq, həm də haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq istəyən əcnəbilər və vətəndaşlığı olmayan şəxslər Azərbaycan Respublikasının Miqrasiya Məcəlləsində m&amp;uuml;əyyən edilmiş qaydada m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə ilə yanaşı, iş icazəsi almalıdırlar.&lt;/p&gt;', 0, 1, 1, 3, 1, 0),
(9, '', '', '', '', '', '', 0, 2, 1, 3, 2, 0),
(8, '', '', '', '', '', '', 0, 2, 1, 2, 2, 0),
(10, 'Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!', '', '', '', '', '&lt;p&gt;Azərbaycan Respublikası ərazisində əcnəbilərin və vətəndaşlığı olmayan şəxslərin h&amp;uuml;quqları və &amp;nbsp;vəzifələri Azərbaycan Respublikası Miqrasiya Məcəlləsi ilə m&amp;uuml;əyyən olunur. Miqrasiya Məcəlləsinin 76.3-c&amp;uuml; maddəsinə (Azərbaycan Respublikasının ərazisində olan əcnəbilər və vətəndaşlığı olmayan şəxslər &amp;ouml;lkəyə gəlişinin bəyan edilmiş məqsədlərinə riayət etməli və m&amp;uuml;əyyən olunmuş m&amp;uuml;ddət bitdikdən sonra &amp;ouml;lkə ərazisini tərk etməlidirlər) əsasən &amp;nbsp;Azərbaycan Respublikasının ərazisində olan əcnəbilər və vətəndaşlığı olmayan şəxslər &amp;ouml;lkəyə gəlişinin bəyan edilmiş məqsədlərinə riayət etməlidirlər.&lt;/p&gt;\r\n&lt;p&gt;Nəzərə &amp;ccedil;atdırırıq ki, viza əsasında&amp;nbsp; Azərbaycan Respublikasına gələn əcnəbilərin gəlişinin bəyan edilmiş məqsədi onlara verilən&amp;nbsp; vizalardakı səfərin&amp;nbsp; məqsədinə uyğun olmalıdır. &amp;nbsp;&amp;Ouml;lkəmizə gələn &amp;nbsp;əcnəbilərə rəsmi, işg&amp;uuml;zar, elm, təhsil, əmək, turizm, mədəniyyət, idman, humanitar, m&amp;uuml;alicə və şəxsi səfər məqsədləri ilə vizalar verilir.&lt;/p&gt;\r\n&lt;p&gt;Bu qaydaların pozulması Azərbaycan Respublikası İnzibati Xətalar Məcəlləsinin 575.1.3-c&amp;uuml; maddəsinə (Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti olduğu, m&amp;uuml;vəqqəti və ya daimi yaşadığı d&amp;ouml;vrdə Azərbaycan Respublikasına gəlişinin bəyan edilmiş məqsədinə uyğun olmayan fəaliyyətlə məşğul olmasına) əsasən inzibati məsuliyyətə səbəb olur.&lt;/p&gt;\r\n&lt;p&gt;Eyni zamanda, əcnəbilər Azərbaycan Respublikası ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə almaq məqsədilə Azərbaycan Respublikası D&amp;ouml;vlət Miqrasiya Xidmətinə m&amp;uuml;raciət edərkən, onların bu qaydalara əməl etmələri&amp;nbsp; t&amp;ouml;vsiyə edilir.&amp;nbsp;&lt;/p&gt;', 0, 3, 1, 1, 3, 0),
(11, 'Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!', '', '', '', '', '&lt;p&gt;Azərbaycan Respublikası ərazisində əcnəbilərin və vətəndaşlığı olmayan şəxslərin h&amp;uuml;quqları və &amp;nbsp;vəzifələri Azərbaycan Respublikası Miqrasiya Məcəlləsi ilə m&amp;uuml;əyyən olunur. Miqrasiya Məcəlləsinin 76.3-c&amp;uuml; maddəsinə (Azərbaycan Respublikasının ərazisində olan əcnəbilər və vətəndaşlığı olmayan şəxslər &amp;ouml;lkəyə gəlişinin bəyan edilmiş məqsədlərinə riayət etməli və m&amp;uuml;əyyən olunmuş m&amp;uuml;ddət bitdikdən sonra &amp;ouml;lkə ərazisini tərk etməlidirlər) əsasən &amp;nbsp;Azərbaycan Respublikasının ərazisində olan əcnəbilər və vətəndaşlığı olmayan şəxslər &amp;ouml;lkəyə gəlişinin bəyan edilmiş məqsədlərinə riayət etməlidirlər.&lt;/p&gt;\r\n&lt;p&gt;Nəzərə &amp;ccedil;atdırırıq ki, viza əsasında&amp;nbsp; Azərbaycan Respublikasına gələn əcnəbilərin gəlişinin bəyan edilmiş məqsədi onlara verilən&amp;nbsp; vizalardakı səfərin&amp;nbsp; məqsədinə uyğun olmalıdır. &amp;nbsp;&amp;Ouml;lkəmizə gələn &amp;nbsp;əcnəbilərə rəsmi, işg&amp;uuml;zar, elm, təhsil, əmək, turizm, mədəniyyət, idman, humanitar, m&amp;uuml;alicə və şəxsi səfər məqsədləri ilə vizalar verilir.&lt;/p&gt;\r\n&lt;p&gt;Bu qaydaların pozulması Azərbaycan Respublikası İnzibati Xətalar Məcəlləsinin 575.1.3-c&amp;uuml; maddəsinə (Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti olduğu, m&amp;uuml;vəqqəti və ya daimi yaşadığı d&amp;ouml;vrdə Azərbaycan Respublikasına gəlişinin bəyan edilmiş məqsədinə uyğun olmayan fəaliyyətlə məşğul olmasına) əsasən inzibati məsuliyyətə səbəb olur.&lt;/p&gt;\r\n&lt;p&gt;Eyni zamanda, əcnəbilər Azərbaycan Respublikası ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə almaq məqsədilə Azərbaycan Respublikası D&amp;ouml;vlət Miqrasiya Xidmətinə m&amp;uuml;raciət edərkən, onların bu qaydalara əməl etmələri&amp;nbsp; t&amp;ouml;vsiyə edilir.&amp;nbsp;&lt;/p&gt;', 0, 3, 1, 2, 3, 0),
(12, 'Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!', '', '', '', '', '&lt;p&gt;Azərbaycan Respublikası ərazisində əcnəbilərin və vətəndaşlığı olmayan şəxslərin h&amp;uuml;quqları və &amp;nbsp;vəzifələri Azərbaycan Respublikası Miqrasiya Məcəlləsi ilə m&amp;uuml;əyyən olunur. Miqrasiya Məcəlləsinin 76.3-c&amp;uuml; maddəsinə (Azərbaycan Respublikasının ərazisində olan əcnəbilər və vətəndaşlığı olmayan şəxslər &amp;ouml;lkəyə gəlişinin bəyan edilmiş məqsədlərinə riayət etməli və m&amp;uuml;əyyən olunmuş m&amp;uuml;ddət bitdikdən sonra &amp;ouml;lkə ərazisini tərk etməlidirlər) əsasən &amp;nbsp;Azərbaycan Respublikasının ərazisində olan əcnəbilər və vətəndaşlığı olmayan şəxslər &amp;ouml;lkəyə gəlişinin bəyan edilmiş məqsədlərinə riayət etməlidirlər.&lt;/p&gt;\r\n&lt;p&gt;Nəzərə &amp;ccedil;atdırırıq ki, viza əsasında&amp;nbsp; Azərbaycan Respublikasına gələn əcnəbilərin gəlişinin bəyan edilmiş məqsədi onlara verilən&amp;nbsp; vizalardakı səfərin&amp;nbsp; məqsədinə uyğun olmalıdır. &amp;nbsp;&amp;Ouml;lkəmizə gələn &amp;nbsp;əcnəbilərə rəsmi, işg&amp;uuml;zar, elm, təhsil, əmək, turizm, mədəniyyət, idman, humanitar, m&amp;uuml;alicə və şəxsi səfər məqsədləri ilə vizalar verilir.&lt;/p&gt;\r\n&lt;p&gt;Bu qaydaların pozulması Azərbaycan Respublikası İnzibati Xətalar Məcəlləsinin 575.1.3-c&amp;uuml; maddəsinə (Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti olduğu, m&amp;uuml;vəqqəti və ya daimi yaşadığı d&amp;ouml;vrdə Azərbaycan Respublikasına gəlişinin bəyan edilmiş məqsədinə uyğun olmayan fəaliyyətlə məşğul olmasına) əsasən inzibati məsuliyyətə səbəb olur.&lt;/p&gt;\r\n&lt;p&gt;Eyni zamanda, əcnəbilər Azərbaycan Respublikası ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə almaq məqsədilə Azərbaycan Respublikası D&amp;ouml;vlət Miqrasiya Xidmətinə m&amp;uuml;raciət edərkən, onların bu qaydalara əməl etmələri&amp;nbsp; t&amp;ouml;vsiyə edilir.&amp;nbsp;&lt;/p&gt;', 0, 3, 1, 3, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `manset` int(2) NOT NULL DEFAULT '0',
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL,
  `created_at` int(13) NOT NULL,
  `updated_at` int(13) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `manset`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`, `created_at`, `updated_at`) VALUES
(6, 'Fuad Hasanli', '', '', 'Naxçıvan Muxtar Respublikasının Dövlət Miqrasiya Xidmətinin kollektivi Naxçıvan Biznes Mərkəzində olub.', 'jpg', '&lt;p&gt;Nax&amp;ccedil;ıvan Muxtar Respublikasının D&amp;ouml;vlət Miqrasiya Xidmətinin kollektivi Nax&amp;ccedil;ıvan Biznes Mərkəzində olub.&amp;nbsp;Nax&amp;ccedil;ıvan Muxtar Respublikasının D&amp;ouml;vlət Miqrasiya Xidmətinin kollektivi Nax&amp;ccedil;ıvan Biznes Mərkəzində olub.&amp;nbsp;Nax&amp;ccedil;ıvan Muxtar Respublikasının D&amp;ouml;vlət Miqrasiya Xidmətinin kollektivi Nax&amp;ccedil;ıvan Biznes Mərkəzində olub.&lt;/p&gt;', 1, 0, 1, 1, 3, 1, 0, 1537301002, 1537772759),
(5, 'Fuad Hasanli', '', '', 'Naxçıvan Muxtar Respublikasının Dövlət Miqrasiya Xidmətinin kollektivi Naxçıvan Biznes Mərkəzində olub.', 'jpg', '&lt;p&gt;Nax&amp;ccedil;ıvan Muxtar Respublikasının D&amp;ouml;vlət Miqrasiya Xidmətinin kollektivi Nax&amp;ccedil;ıvan Biznes Mərkəzində olub.&amp;nbsp;Nax&amp;ccedil;ıvan Muxtar Respublikasının D&amp;ouml;vlət Miqrasiya Xidmətinin kollektivi Nax&amp;ccedil;ıvan Biznes Mərkəzində olub.&amp;nbsp;Nax&amp;ccedil;ıvan Muxtar Respublikasının D&amp;ouml;vlət Miqrasiya Xidmətinin kollektivi Nax&amp;ccedil;ıvan Biznes Mərkəzində olub.&lt;/p&gt;', 1, 0, 1, 1, 2, 1, 0, 1537301002, 1537772759),
(4, 'Fuad Hasanli', '', '', 'Naxçıvan Muxtar Respublikasının Dövlət Miqrasiya Xidmətinin kollektivi Naxçıvan Biznes Mərkəzində olub.', 'jpg', '&lt;p&gt;Nax&amp;ccedil;ıvan Muxtar Respublikasının D&amp;ouml;vlət Miqrasiya Xidmətinin kollektivi Nax&amp;ccedil;ıvan Biznes Mərkəzində olub.&amp;nbsp;Nax&amp;ccedil;ıvan Muxtar Respublikasının D&amp;ouml;vlət Miqrasiya Xidmətinin kollektivi Nax&amp;ccedil;ıvan Biznes Mərkəzində olub.&amp;nbsp;Nax&amp;ccedil;ıvan Muxtar Respublikasının D&amp;ouml;vlət Miqrasiya Xidmətinin kollektivi Nax&amp;ccedil;ıvan Biznes Mərkəzində olub.&lt;/p&gt;', 1, 0, 1, 1, 1, 1, 0, 1537301001, 1537772759),
(8, 'aas dasdasd as ', '', '', 'aas dasdasd as aas dasdasd as aas dasdasd as aas dasdasd as aas dasdasd as aas dasdasd as aas dasdasd as ', 'jpg', '&lt;p&gt;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;&lt;/p&gt;', 1, 0, 2, 1, 1, 2, 0, 1537775815, 0),
(9, 'aas dasdasd as ', '', '', 'aas dasdasd as aas dasdasd as aas dasdasd as aas dasdasd as aas dasdasd as aas dasdasd as aas dasdasd as ', 'jpg', '&lt;p&gt;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;&lt;/p&gt;', 1, 0, 2, 1, 2, 2, 0, 1537775816, 0),
(10, 'aas dasdasd as ', '', '', 'aas dasdasd as aas dasdasd as aas dasdasd as aas dasdasd as aas dasdasd as aas dasdasd as aas dasdasd as ', 'jpg', '&lt;p&gt;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;aas dasdasd as&amp;nbsp;&lt;/p&gt;', 1, 0, 2, 1, 3, 2, 0, 1537775816, 0),
(11, 'qwe qwe qwe qw ', '', '', 'qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw ', 'jpg', '&lt;p&gt;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw v&lt;/p&gt;', 1, 0, 3, 1, 1, 3, 0, 1537776033, 1537776048),
(12, 'qwe qwe qwe qw ', '', '', 'qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw ', 'jpg', '&lt;p&gt;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw v&lt;/p&gt;', 1, 0, 3, 1, 2, 3, 0, 1537776033, 1537776048),
(13, 'qwe qwe qwe qw ', '', '', 'qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw qwe qwe qwe qw ', 'jpg', '&lt;p&gt;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw&amp;nbsp;qwe qwe qwe qw v&lt;/p&gt;', 1, 0, 3, 1, 3, 3, 0, 1537776033, 1537776048);

-- --------------------------------------------------------

--
-- Table structure for table `calculator`
--

CREATE TABLE `calculator` (
  `id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `manset` int(2) NOT NULL DEFAULT '0',
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL,
  `created_at` int(13) NOT NULL,
  `updated_at` int(13) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `calculator`
--

INSERT INTO `calculator` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `manset`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`, `created_at`, `updated_at`) VALUES
(1, '', '', '', '', '', '&lt;p&gt;&lt;span&gt;&lt;img src=&quot;/images/upload/image/info.png&quot; alt=&quot;&quot; width=&quot;15&quot; height=&quot;15&quot; /&gt;&amp;nbsp;Azərbaycan Respublikasında 15 g&amp;uuml;ndən artıq m&amp;uuml;vəqqəti qalmaq istəyən əcnəbilər və ya vətəndaşlığı olmayan şəxslər olduğu yer &amp;uuml;zrə qeydiyyata alınmalıdırlar. Qeydiyyat &amp;uuml;&amp;ccedil;&amp;uuml;n əcnəbi &amp;ouml;lkəyə gəldiyi vaxtdan 15 g&amp;uuml;n ərzində elektron qaydada D&amp;ouml;vlət Miqrasiya Xidmətinin rəsmi saytının&amp;nbsp;&lt;/span&gt;&lt;a href=&quot;http://migration.azz/calculator#&quot; target=&quot;_blank&quot;&gt;elektron xidmətləri&lt;/a&gt;&lt;span&gt;,&lt;/span&gt;&lt;a href=&quot;http://migration.azz/calculator#&quot; target=&quot;_blank&quot;&gt;&amp;nbsp;elektron h&amp;ouml;kumət portalı&lt;/a&gt;&lt;span&gt;&amp;nbsp;və elektron po&amp;ccedil;t (&lt;/span&gt;&lt;a href=&quot;mailto:qeydiyyat@migration.gov.az&quot; target=&quot;_top&quot;&gt;qeydiyyat@migration.gov.az&lt;/a&gt;&lt;span&gt;) vasitəsilə və ya şəxsən&amp;nbsp;&lt;/span&gt;&lt;a href=&quot;http://migration.azz/calculator#&quot; target=&quot;_blank&quot;&gt;regional miqrasiya idarələrinə&lt;/a&gt;&lt;span&gt;, yaxud &quot;&lt;/span&gt;&lt;a href=&quot;http://migration.azz/calculator#&quot; target=&quot;_blank&quot;&gt;ASAN xidmət&lt;/a&gt;&lt;span&gt;&quot; mərkəzlərinə m&amp;uuml;raciət edilməlidir.&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;* Tələb olunan sənədlər:&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;-&amp;nbsp;&lt;/span&gt;&lt;a href=&quot;http://migration.azz/calculator#&quot;&gt;ərizə anket&lt;/a&gt;&lt;span&gt;;&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;- əcnəbinin pasportunun (digər sərhədke&amp;ccedil;mə sənədinin) surəti (viza səhifəsi və &amp;ouml;lkəyə son girişi g&amp;ouml;stərilmiş səhifələr daxil olmaqla).&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;* Əcnəbilərin və vətəndaşlığı olmayan şəxslərin olduğu yer &amp;uuml;zrə qeydiyyata alınması &amp;uuml;&amp;ccedil;&amp;uuml;n d&amp;ouml;vlət r&amp;uuml;sumu &amp;ouml;dənilmir.&lt;/span&gt;&lt;/p&gt;\r\n&lt;hr /&gt;\r\n&lt;p&gt;&lt;img src=&quot;http://migration.azz/assets/img/calculator.png&quot; alt=&quot;&quot; width=&quot;15&quot; height=&quot;15&quot; /&gt;&lt;span&gt;&amp;nbsp;Aşağıdakı kalkulyator vasitəsi ilə m&amp;uuml;vafiq xanalardan &amp;ouml;lkəyə gəldiyiniz ay və g&amp;uuml;n&amp;uuml; se&amp;ccedil;məklə son qeydiyyat tarixinizi &amp;ouml;yrənə bilərsiniz&lt;/span&gt;&lt;/p&gt;', 0, 0, 1, 1, 1, 1, 0, 1537940262, 0),
(2, '', '', '', '', '', '&lt;p&gt;&lt;span&gt;&lt;img src=&quot;/images/upload/image/info.png&quot; alt=&quot;&quot; width=&quot;15&quot; height=&quot;15&quot; /&gt;&amp;nbsp;Azərbaycan Respublikasında 15 g&amp;uuml;ndən artıq m&amp;uuml;vəqqəti qalmaq istəyən əcnəbilər və ya vətəndaşlığı olmayan şəxslər olduğu yer &amp;uuml;zrə qeydiyyata alınmalıdırlar. Qeydiyyat &amp;uuml;&amp;ccedil;&amp;uuml;n əcnəbi &amp;ouml;lkəyə gəldiyi vaxtdan 15 g&amp;uuml;n ərzində elektron qaydada D&amp;ouml;vlət Miqrasiya Xidmətinin rəsmi saytının&amp;nbsp;&lt;/span&gt;&lt;a href=&quot;http://migration.azz/calculator#&quot; target=&quot;_blank&quot;&gt;elektron xidmətləri&lt;/a&gt;&lt;span&gt;,&lt;/span&gt;&lt;a href=&quot;http://migration.azz/calculator#&quot; target=&quot;_blank&quot;&gt;&amp;nbsp;elektron h&amp;ouml;kumət portalı&lt;/a&gt;&lt;span&gt;&amp;nbsp;və elektron po&amp;ccedil;t (&lt;/span&gt;&lt;a href=&quot;mailto:qeydiyyat@migration.gov.az&quot; target=&quot;_top&quot;&gt;qeydiyyat@migration.gov.az&lt;/a&gt;&lt;span&gt;) vasitəsilə və ya şəxsən&amp;nbsp;&lt;/span&gt;&lt;a href=&quot;http://migration.azz/calculator#&quot; target=&quot;_blank&quot;&gt;regional miqrasiya idarələrinə&lt;/a&gt;&lt;span&gt;, yaxud &quot;&lt;/span&gt;&lt;a href=&quot;http://migration.azz/calculator#&quot; target=&quot;_blank&quot;&gt;ASAN xidmət&lt;/a&gt;&lt;span&gt;&quot; mərkəzlərinə m&amp;uuml;raciət edilməlidir.&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;* Tələb olunan sənədlər:&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;-&amp;nbsp;&lt;/span&gt;&lt;a href=&quot;http://migration.azz/calculator#&quot;&gt;ərizə anket&lt;/a&gt;&lt;span&gt;;&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;- əcnəbinin pasportunun (digər sərhədke&amp;ccedil;mə sənədinin) surəti (viza səhifəsi və &amp;ouml;lkəyə son girişi g&amp;ouml;stərilmiş səhifələr daxil olmaqla).&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;* Əcnəbilərin və vətəndaşlığı olmayan şəxslərin olduğu yer &amp;uuml;zrə qeydiyyata alınması &amp;uuml;&amp;ccedil;&amp;uuml;n d&amp;ouml;vlət r&amp;uuml;sumu &amp;ouml;dənilmir.&lt;/span&gt;&lt;/p&gt;\r\n&lt;hr /&gt;\r\n&lt;p&gt;&lt;img src=&quot;http://migration.azz/assets/img/calculator.png&quot; alt=&quot;&quot; width=&quot;15&quot; height=&quot;15&quot; /&gt;&lt;span&gt;&amp;nbsp;Aşağıdakı kalkulyator vasitəsi ilə m&amp;uuml;vafiq xanalardan &amp;ouml;lkəyə gəldiyiniz ay və g&amp;uuml;n&amp;uuml; se&amp;ccedil;məklə son qeydiyyat tarixinizi &amp;ouml;yrənə bilərsiniz&lt;/span&gt;&lt;/p&gt;', 0, 0, 1, 1, 2, 1, 0, 1537940262, 0),
(3, '', '', '', '', '', '&lt;p&gt;&lt;span&gt;&lt;img src=&quot;/images/upload/image/info.png&quot; alt=&quot;&quot; width=&quot;15&quot; height=&quot;15&quot; /&gt;&amp;nbsp;Azərbaycan Respublikasında 15 g&amp;uuml;ndən artıq m&amp;uuml;vəqqəti qalmaq istəyən əcnəbilər və ya vətəndaşlığı olmayan şəxslər olduğu yer &amp;uuml;zrə qeydiyyata alınmalıdırlar. Qeydiyyat &amp;uuml;&amp;ccedil;&amp;uuml;n əcnəbi &amp;ouml;lkəyə gəldiyi vaxtdan 15 g&amp;uuml;n ərzində elektron qaydada D&amp;ouml;vlət Miqrasiya Xidmətinin rəsmi saytının&amp;nbsp;&lt;/span&gt;&lt;a href=&quot;http://migration.azz/calculator#&quot; target=&quot;_blank&quot;&gt;elektron xidmətləri&lt;/a&gt;&lt;span&gt;,&lt;/span&gt;&lt;a href=&quot;http://migration.azz/calculator#&quot; target=&quot;_blank&quot;&gt;&amp;nbsp;elektron h&amp;ouml;kumət portalı&lt;/a&gt;&lt;span&gt;&amp;nbsp;və elektron po&amp;ccedil;t (&lt;/span&gt;&lt;a href=&quot;mailto:qeydiyyat@migration.gov.az&quot; target=&quot;_top&quot;&gt;qeydiyyat@migration.gov.az&lt;/a&gt;&lt;span&gt;) vasitəsilə və ya şəxsən&amp;nbsp;&lt;/span&gt;&lt;a href=&quot;http://migration.azz/calculator#&quot; target=&quot;_blank&quot;&gt;regional miqrasiya idarələrinə&lt;/a&gt;&lt;span&gt;, yaxud &quot;&lt;/span&gt;&lt;a href=&quot;http://migration.azz/calculator#&quot; target=&quot;_blank&quot;&gt;ASAN xidmət&lt;/a&gt;&lt;span&gt;&quot; mərkəzlərinə m&amp;uuml;raciət edilməlidir.&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;* Tələb olunan sənədlər:&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;-&amp;nbsp;&lt;/span&gt;&lt;a href=&quot;http://migration.azz/calculator#&quot;&gt;ərizə anket&lt;/a&gt;&lt;span&gt;;&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;- əcnəbinin pasportunun (digər sərhədke&amp;ccedil;mə sənədinin) surəti (viza səhifəsi və &amp;ouml;lkəyə son girişi g&amp;ouml;stərilmiş səhifələr daxil olmaqla).&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;* Əcnəbilərin və vətəndaşlığı olmayan şəxslərin olduğu yer &amp;uuml;zrə qeydiyyata alınması &amp;uuml;&amp;ccedil;&amp;uuml;n d&amp;ouml;vlət r&amp;uuml;sumu &amp;ouml;dənilmir.&lt;/span&gt;&lt;/p&gt;\r\n&lt;hr /&gt;\r\n&lt;p&gt;&lt;img src=&quot;http://migration.azz/assets/img/calculator.png&quot; alt=&quot;&quot; width=&quot;15&quot; height=&quot;15&quot; /&gt;&lt;span&gt;&amp;nbsp;Aşağıdakı kalkulyator vasitəsi ilə m&amp;uuml;vafiq xanalardan &amp;ouml;lkəyə gəldiyiniz ay və g&amp;uuml;n&amp;uuml; se&amp;ccedil;məklə son qeydiyyat tarixinizi &amp;ouml;yrənə bilərsiniz&lt;/span&gt;&lt;/p&gt;', 0, 0, 1, 1, 3, 1, 0, 1537940262, 0);

-- --------------------------------------------------------

--
-- Table structure for table `callcenter`
--

CREATE TABLE `callcenter` (
  `id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `callcenter`
--

INSERT INTO `callcenter` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, '', '', '', '', '', '&lt;p&gt;&lt;span&gt;Miqrasiya məlumat mərkəzi&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;Respublikanın istənilən regionundan stasionar telefonlardan habelə, b&amp;uuml;t&amp;uuml;n mobil operatorlardan (012) 919 n&amp;ouml;mrəsini yığaraq &amp;Ccedil;ağrı Mərkəzinin xidmətlərindən istifadə edə bilərsiniz. Nəzərinizə &amp;ccedil;atdırırıq ki, b&amp;ouml;lgələrdən (012) 919-a edilən zənglər &amp;ouml;dənişsizdir.&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;(036) 202-a zəng edərkən telefon ton rejimində işləməlidir. Əgər (036) 202-a zəng edib m&amp;uuml;vafiq d&amp;uuml;yməni se&amp;ccedil;dikdən sonra əlaqə kəsilirsə bu sizin telofonunuzun ton rejimində işləməməsi ilə əlaqəlidir. Bu zaman (036) 202-a zəng etdikdən sonra m&amp;uuml;vafiq d&amp;uuml;ymə se&amp;ccedil;ilməzdən &amp;ouml;ncə * (ulduz) d&amp;uuml;yməsini sıxmaqla telefonu ton rejiminə ke&amp;ccedil;irmək olar.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Bazar ertəsi - c&amp;uuml;mə g&amp;uuml;nləri saat 09:00-dan 18:00-dək və şənbə g&amp;uuml;n&amp;uuml; saat 09:00-dan 13:00-dək xidmətdən istifadə qaydası&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;Bazar ertəsi - c&amp;uuml;mə g&amp;uuml;nləri saat 09:00-dan 18:00-dək, şənbə g&amp;uuml;n&amp;uuml; saat 09:00-dan 13:00-dək D&amp;ouml;vlət Miqrasiya Xidmətinin &amp;Ccedil;ağrı Mərkəzinə zəng etmiş şəxslərə 3 dildə m&amp;uuml;raciət etmək imkanı verilir. Belə ki, azərbaycan dili, rus dili və ingilis dilində m&amp;uuml;vafiq olaraq &amp;laquo;1&amp;raquo;, &amp;laquo;2&amp;raquo;, &amp;laquo;3&amp;raquo; d&amp;uuml;ymələrini se&amp;ccedil;dikdən sonra operatora bağlanmaq istəyirsinizsə &amp;laquo;0&amp;raquo; d&amp;uuml;yməsini se&amp;ccedil;irsiniz. Bu zaman &amp;uuml;mumi menyu sxemi azərbaycan dili &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;laquo;202-1-0&amp;raquo;, rus dili &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;laquo;202-2-0&amp;raquo;, ingilis dili &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;laquo;202-3-0&amp;raquo; olacaqdır. Əgər dil se&amp;ccedil;imi etdikdən sonra xidmətlərlə bağlı məlumat almaq istəyirsinizsə &amp;laquo;1&amp;raquo; d&amp;uuml;yməsini se&amp;ccedil;dikdən sonra olduğu yer &amp;uuml;zrə qeydiyyata alınma, m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılması, m&amp;uuml;vəqqəti yaşamaq icazəsi, daimi yaşamaq icazəsi və əcnəbilər və vətəndaşlığı olmayan şəxslərin Azərbaycan Respublikasında əmək fəaliyyəti ilə məşğul olması barədə məlumat almaq &amp;uuml;&amp;ccedil;&amp;uuml;n m&amp;uuml;vafiq olaraq &amp;laquo;1&amp;raquo;, &amp;laquo;2&amp;raquo;, &amp;laquo;3&amp;raquo;, &amp;laquo;4&amp;raquo;, &amp;laquo;5&amp;raquo; d&amp;uuml;ymələrini se&amp;ccedil;irsiniz.&lt;/p&gt;\r\n&lt;p&gt;D&amp;ouml;vlət Miqrasiya Xidmətinin iş rejimi barədə məlumat almaq istəyirsinizsə dil se&amp;ccedil;imi etdikdən sonra &amp;laquo;2&amp;raquo;d&amp;uuml;yməsini se&amp;ccedil;irsiniz. Bu zaman &amp;uuml;mumi menyu sxemi azərbaycan dili &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;laquo;202-1-2&amp;raquo;; rus dili &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;laquo;202-2-2&amp;raquo;; ingilis dili &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;laquo;202-3-2&amp;raquo; olacaqdır.&lt;/p&gt;', 0, 1, 1, 1, 1, 0),
(2, '', '', '', '', '', '&lt;p&gt;&lt;span&gt;Miqrasiya məlumat mərkəzi&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;Respublikanın istənilən regionundan stasionar telefonlardan habelə, b&amp;uuml;t&amp;uuml;n mobil operatorlardan (012) 919 n&amp;ouml;mrəsini yığaraq &amp;Ccedil;ağrı Mərkəzinin xidmətlərindən istifadə edə bilərsiniz. Nəzərinizə &amp;ccedil;atdırırıq ki, b&amp;ouml;lgələrdən (012) 919-a edilən zənglər &amp;ouml;dənişsizdir.&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;(036) 202-a zəng edərkən telefon ton rejimində işləməlidir. Əgər (036) 202-a zəng edib m&amp;uuml;vafiq d&amp;uuml;yməni se&amp;ccedil;dikdən sonra əlaqə kəsilirsə bu sizin telofonunuzun ton rejimində işləməməsi ilə əlaqəlidir. Bu zaman (036) 202-a zəng etdikdən sonra m&amp;uuml;vafiq d&amp;uuml;ymə se&amp;ccedil;ilməzdən &amp;ouml;ncə * (ulduz) d&amp;uuml;yməsini sıxmaqla telefonu ton rejiminə ke&amp;ccedil;irmək olar.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Bazar ertəsi - c&amp;uuml;mə g&amp;uuml;nləri saat 09:00-dan 18:00-dək və şənbə g&amp;uuml;n&amp;uuml; saat 09:00-dan 13:00-dək xidmətdən istifadə qaydası&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;Bazar ertəsi - c&amp;uuml;mə g&amp;uuml;nləri saat 09:00-dan 18:00-dək, şənbə g&amp;uuml;n&amp;uuml; saat 09:00-dan 13:00-dək D&amp;ouml;vlət Miqrasiya Xidmətinin &amp;Ccedil;ağrı Mərkəzinə zəng etmiş şəxslərə 3 dildə m&amp;uuml;raciət etmək imkanı verilir. Belə ki, azərbaycan dili, rus dili və ingilis dilində m&amp;uuml;vafiq olaraq &amp;laquo;1&amp;raquo;, &amp;laquo;2&amp;raquo;, &amp;laquo;3&amp;raquo; d&amp;uuml;ymələrini se&amp;ccedil;dikdən sonra operatora bağlanmaq istəyirsinizsə &amp;laquo;0&amp;raquo; d&amp;uuml;yməsini se&amp;ccedil;irsiniz. Bu zaman &amp;uuml;mumi menyu sxemi azərbaycan dili &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;laquo;202-1-0&amp;raquo;, rus dili &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;laquo;202-2-0&amp;raquo;, ingilis dili &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;laquo;202-3-0&amp;raquo; olacaqdır. Əgər dil se&amp;ccedil;imi etdikdən sonra xidmətlərlə bağlı məlumat almaq istəyirsinizsə &amp;laquo;1&amp;raquo; d&amp;uuml;yməsini se&amp;ccedil;dikdən sonra olduğu yer &amp;uuml;zrə qeydiyyata alınma, m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılması, m&amp;uuml;vəqqəti yaşamaq icazəsi, daimi yaşamaq icazəsi və əcnəbilər və vətəndaşlığı olmayan şəxslərin Azərbaycan Respublikasında əmək fəaliyyəti ilə məşğul olması barədə məlumat almaq &amp;uuml;&amp;ccedil;&amp;uuml;n m&amp;uuml;vafiq olaraq &amp;laquo;1&amp;raquo;, &amp;laquo;2&amp;raquo;, &amp;laquo;3&amp;raquo;, &amp;laquo;4&amp;raquo;, &amp;laquo;5&amp;raquo; d&amp;uuml;ymələrini se&amp;ccedil;irsiniz.&lt;/p&gt;\r\n&lt;p&gt;D&amp;ouml;vlət Miqrasiya Xidmətinin iş rejimi barədə məlumat almaq istəyirsinizsə dil se&amp;ccedil;imi etdikdən sonra &amp;laquo;2&amp;raquo;d&amp;uuml;yməsini se&amp;ccedil;irsiniz. Bu zaman &amp;uuml;mumi menyu sxemi azərbaycan dili &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;laquo;202-1-2&amp;raquo;; rus dili &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;laquo;202-2-2&amp;raquo;; ingilis dili &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;laquo;202-3-2&amp;raquo; olacaqdır.&lt;/p&gt;', 0, 1, 1, 2, 1, 0),
(3, '', '', '', '', '', '&lt;p&gt;&lt;span&gt;Miqrasiya məlumat mərkəzi&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;Respublikanın istənilən regionundan stasionar telefonlardan habelə, b&amp;uuml;t&amp;uuml;n mobil operatorlardan (012) 919 n&amp;ouml;mrəsini yığaraq &amp;Ccedil;ağrı Mərkəzinin xidmətlərindən istifadə edə bilərsiniz. Nəzərinizə &amp;ccedil;atdırırıq ki, b&amp;ouml;lgələrdən (012) 919-a edilən zənglər &amp;ouml;dənişsizdir.&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;(036) 202-a zəng edərkən telefon ton rejimində işləməlidir. Əgər (036) 202-a zəng edib m&amp;uuml;vafiq d&amp;uuml;yməni se&amp;ccedil;dikdən sonra əlaqə kəsilirsə bu sizin telofonunuzun ton rejimində işləməməsi ilə əlaqəlidir. Bu zaman (036) 202-a zəng etdikdən sonra m&amp;uuml;vafiq d&amp;uuml;ymə se&amp;ccedil;ilməzdən &amp;ouml;ncə * (ulduz) d&amp;uuml;yməsini sıxmaqla telefonu ton rejiminə ke&amp;ccedil;irmək olar.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Bazar ertəsi - c&amp;uuml;mə g&amp;uuml;nləri saat 09:00-dan 18:00-dək və şənbə g&amp;uuml;n&amp;uuml; saat 09:00-dan 13:00-dək xidmətdən istifadə qaydası&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;Bazar ertəsi - c&amp;uuml;mə g&amp;uuml;nləri saat 09:00-dan 18:00-dək, şənbə g&amp;uuml;n&amp;uuml; saat 09:00-dan 13:00-dək D&amp;ouml;vlət Miqrasiya Xidmətinin &amp;Ccedil;ağrı Mərkəzinə zəng etmiş şəxslərə 3 dildə m&amp;uuml;raciət etmək imkanı verilir. Belə ki, azərbaycan dili, rus dili və ingilis dilində m&amp;uuml;vafiq olaraq &amp;laquo;1&amp;raquo;, &amp;laquo;2&amp;raquo;, &amp;laquo;3&amp;raquo; d&amp;uuml;ymələrini se&amp;ccedil;dikdən sonra operatora bağlanmaq istəyirsinizsə &amp;laquo;0&amp;raquo; d&amp;uuml;yməsini se&amp;ccedil;irsiniz. Bu zaman &amp;uuml;mumi menyu sxemi azərbaycan dili &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;laquo;202-1-0&amp;raquo;, rus dili &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;laquo;202-2-0&amp;raquo;, ingilis dili &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;laquo;202-3-0&amp;raquo; olacaqdır. Əgər dil se&amp;ccedil;imi etdikdən sonra xidmətlərlə bağlı məlumat almaq istəyirsinizsə &amp;laquo;1&amp;raquo; d&amp;uuml;yməsini se&amp;ccedil;dikdən sonra olduğu yer &amp;uuml;zrə qeydiyyata alınma, m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılması, m&amp;uuml;vəqqəti yaşamaq icazəsi, daimi yaşamaq icazəsi və əcnəbilər və vətəndaşlığı olmayan şəxslərin Azərbaycan Respublikasında əmək fəaliyyəti ilə məşğul olması barədə məlumat almaq &amp;uuml;&amp;ccedil;&amp;uuml;n m&amp;uuml;vafiq olaraq &amp;laquo;1&amp;raquo;, &amp;laquo;2&amp;raquo;, &amp;laquo;3&amp;raquo;, &amp;laquo;4&amp;raquo;, &amp;laquo;5&amp;raquo; d&amp;uuml;ymələrini se&amp;ccedil;irsiniz.&lt;/p&gt;\r\n&lt;p&gt;D&amp;ouml;vlət Miqrasiya Xidmətinin iş rejimi barədə məlumat almaq istəyirsinizsə dil se&amp;ccedil;imi etdikdən sonra &amp;laquo;2&amp;raquo;d&amp;uuml;yməsini se&amp;ccedil;irsiniz. Bu zaman &amp;uuml;mumi menyu sxemi azərbaycan dili &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;laquo;202-1-2&amp;raquo;; rus dili &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;laquo;202-2-2&amp;raquo;; ingilis dili &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;laquo;202-3-2&amp;raquo; olacaqdır.&lt;/p&gt;', 0, 1, 1, 3, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(3) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `parent_auto_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `parent_auto_id`) VALUES
(1, 'Afghanistan', 1, 1, 1, 1, 0),
(2, 'Afghanistan', 1, 1, 2, 1, 0),
(3, 'Afghanistan', 1, 1, 3, 1, 0),
(4, 'Albania', 2, 1, 1, 2, 0),
(5, 'Albania', 2, 1, 2, 2, 0),
(6, 'Albania', 2, 1, 3, 2, 0),
(7, 'Algeria', 3, 1, 1, 3, 0),
(8, 'Algeria', 3, 1, 2, 3, 0),
(9, 'Algeria', 3, 1, 3, 3, 0),
(10, 'American Samoa', 4, 1, 1, 4, 0),
(11, 'American Samoa', 4, 1, 2, 4, 0),
(12, 'American Samoa', 4, 1, 3, 4, 0),
(13, 'Andorra', 5, 1, 1, 5, 0),
(14, 'Andorra', 5, 1, 2, 5, 0),
(15, 'Andorra', 5, 1, 3, 5, 0),
(16, 'Angola', 6, 1, 1, 6, 0),
(17, 'Angola', 6, 1, 2, 6, 0),
(18, 'Angola', 6, 1, 3, 6, 0),
(19, 'Anguilla', 7, 1, 1, 7, 0),
(20, 'Anguilla', 7, 1, 2, 7, 0),
(21, 'Anguilla', 7, 1, 3, 7, 0),
(22, 'Antarctica', 8, 1, 1, 8, 0),
(23, 'Antarctica', 8, 1, 2, 8, 0),
(24, 'Antarctica', 8, 1, 3, 8, 0),
(25, 'Antigua and Barbuda', 9, 1, 1, 9, 0),
(26, 'Antigua and Barbuda', 9, 1, 2, 9, 0),
(27, 'Antigua and Barbuda', 9, 1, 3, 9, 0),
(28, 'Argentina', 10, 1, 1, 10, 0),
(29, 'Argentina', 10, 1, 2, 10, 0),
(30, 'Argentina', 10, 1, 3, 10, 0),
(31, 'Armenia', 11, 1, 1, 11, 0),
(32, 'Armenia', 11, 1, 2, 11, 0),
(33, 'Armenia', 11, 1, 3, 11, 0),
(34, 'Aruba', 12, 1, 1, 12, 0),
(35, 'Aruba', 12, 1, 2, 12, 0),
(36, 'Aruba', 12, 1, 3, 12, 0),
(37, 'Australia', 13, 1, 1, 13, 0),
(38, 'Australia', 13, 1, 2, 13, 0),
(39, 'Australia', 13, 1, 3, 13, 0),
(40, 'Austria', 14, 1, 1, 14, 0),
(41, 'Austria', 14, 1, 2, 14, 0),
(42, 'Austria', 14, 1, 3, 14, 0),
(43, 'Azerbaijan', 15, 1, 1, 15, 0),
(44, 'Azerbaijan', 15, 1, 2, 15, 0),
(45, 'Azerbaijan', 15, 1, 3, 15, 0),
(46, 'Bahamas', 16, 1, 1, 16, 0),
(47, 'Bahamas', 16, 1, 2, 16, 0),
(48, 'Bahamas', 16, 1, 3, 16, 0),
(49, 'Bahrain', 17, 1, 1, 17, 0),
(50, 'Bahrain', 17, 1, 2, 17, 0),
(51, 'Bahrain', 17, 1, 3, 17, 0),
(52, 'Bangladesh', 18, 1, 1, 18, 0),
(53, 'Bangladesh', 18, 1, 2, 18, 0),
(54, 'Bangladesh', 18, 1, 3, 18, 0),
(55, 'Barbados', 19, 1, 1, 19, 0),
(56, 'Barbados', 19, 1, 2, 19, 0),
(57, 'Barbados', 19, 1, 3, 19, 0),
(58, 'Belarus', 20, 1, 1, 20, 0),
(59, 'Belarus', 20, 1, 2, 20, 0),
(60, 'Belarus', 20, 1, 3, 20, 0),
(61, 'Belgium', 21, 1, 1, 21, 0),
(62, 'Belgium', 21, 1, 2, 21, 0),
(63, 'Belgium', 21, 1, 3, 21, 0),
(64, 'Belize', 22, 1, 1, 22, 0),
(65, 'Belize', 22, 1, 2, 22, 0),
(66, 'Belize', 22, 1, 3, 22, 0),
(67, 'Benin', 23, 1, 1, 23, 0),
(68, 'Benin', 23, 1, 2, 23, 0),
(69, 'Benin', 23, 1, 3, 23, 0),
(70, 'Bermuda', 24, 1, 1, 24, 0),
(71, 'Bermuda', 24, 1, 2, 24, 0),
(72, 'Bermuda', 24, 1, 3, 24, 0),
(73, 'Bhutan', 25, 1, 1, 25, 0),
(74, 'Bhutan', 25, 1, 2, 25, 0),
(75, 'Bhutan', 25, 1, 3, 25, 0),
(76, 'Bolivia', 26, 1, 1, 26, 0),
(77, 'Bolivia', 26, 1, 2, 26, 0),
(78, 'Bolivia', 26, 1, 3, 26, 0),
(79, 'Bosnia and Herzegowina', 27, 1, 1, 27, 0),
(80, 'Bosnia and Herzegowina', 27, 1, 2, 27, 0),
(81, 'Bosnia and Herzegowina', 27, 1, 3, 27, 0),
(82, 'Botswana', 28, 1, 1, 28, 0),
(83, 'Botswana', 28, 1, 2, 28, 0),
(84, 'Botswana', 28, 1, 3, 28, 0),
(85, 'Bouvet Island', 29, 1, 1, 29, 0),
(86, 'Bouvet Island', 29, 1, 2, 29, 0),
(87, 'Bouvet Island', 29, 1, 3, 29, 0),
(88, 'Brazil', 30, 1, 1, 30, 0),
(89, 'Brazil', 30, 1, 2, 30, 0),
(90, 'Brazil', 30, 1, 3, 30, 0),
(91, 'British Indian Ocean Territory', 31, 1, 1, 31, 0),
(92, 'British Indian Ocean Territory', 31, 1, 2, 31, 0),
(93, 'British Indian Ocean Territory', 31, 1, 3, 31, 0),
(94, 'Brunei Darussalam', 32, 1, 1, 32, 0),
(95, 'Brunei Darussalam', 32, 1, 2, 32, 0),
(96, 'Brunei Darussalam', 32, 1, 3, 32, 0),
(97, 'Bulgaria', 33, 1, 1, 33, 0),
(98, 'Bulgaria', 33, 1, 2, 33, 0),
(99, 'Bulgaria', 33, 1, 3, 33, 0),
(100, 'Burkina Faso', 34, 1, 1, 34, 0),
(101, 'Burkina Faso', 34, 1, 2, 34, 0),
(102, 'Burkina Faso', 34, 1, 3, 34, 0),
(103, 'Burundi', 35, 1, 1, 35, 0),
(104, 'Burundi', 35, 1, 2, 35, 0),
(105, 'Burundi', 35, 1, 3, 35, 0),
(106, 'Cambodia', 36, 1, 1, 36, 0),
(107, 'Cambodia', 36, 1, 2, 36, 0),
(108, 'Cambodia', 36, 1, 3, 36, 0),
(109, 'Cameroon', 37, 1, 1, 37, 0),
(110, 'Cameroon', 37, 1, 2, 37, 0),
(111, 'Cameroon', 37, 1, 3, 37, 0),
(112, 'Canada', 38, 1, 1, 38, 0),
(113, 'Canada', 38, 1, 2, 38, 0),
(114, 'Canada', 38, 1, 3, 38, 0),
(115, 'Cape Verde', 39, 1, 1, 39, 0),
(116, 'Cape Verde', 39, 1, 2, 39, 0),
(117, 'Cape Verde', 39, 1, 3, 39, 0),
(118, 'Cayman Islands', 40, 1, 1, 40, 0),
(119, 'Cayman Islands', 40, 1, 2, 40, 0),
(120, 'Cayman Islands', 40, 1, 3, 40, 0),
(121, 'Central African Republic', 41, 1, 1, 41, 0),
(122, 'Central African Republic', 41, 1, 2, 41, 0),
(123, 'Central African Republic', 41, 1, 3, 41, 0),
(124, 'Chad', 42, 1, 1, 42, 0),
(125, 'Chad', 42, 1, 2, 42, 0),
(126, 'Chad', 42, 1, 3, 42, 0),
(127, 'Chile', 43, 1, 1, 43, 0),
(128, 'Chile', 43, 1, 2, 43, 0),
(129, 'Chile', 43, 1, 3, 43, 0),
(130, 'China', 44, 1, 1, 44, 0),
(131, 'China', 44, 1, 2, 44, 0),
(132, 'China', 44, 1, 3, 44, 0),
(133, 'Christmas Island', 45, 1, 1, 45, 0),
(134, 'Christmas Island', 45, 1, 2, 45, 0),
(135, 'Christmas Island', 45, 1, 3, 45, 0),
(136, 'Cocos (Keeling) Islands', 46, 1, 1, 46, 0),
(137, 'Cocos (Keeling) Islands', 46, 1, 2, 46, 0),
(138, 'Cocos (Keeling) Islands', 46, 1, 3, 46, 0),
(139, 'Colombia', 47, 1, 1, 47, 0),
(140, 'Colombia', 47, 1, 2, 47, 0),
(141, 'Colombia', 47, 1, 3, 47, 0),
(142, 'Comoros', 48, 1, 1, 48, 0),
(143, 'Comoros', 48, 1, 2, 48, 0),
(144, 'Comoros', 48, 1, 3, 48, 0),
(145, 'Congo', 49, 1, 1, 49, 0),
(146, 'Congo', 49, 1, 2, 49, 0),
(147, 'Congo', 49, 1, 3, 49, 0),
(148, 'Congo, the Democratic Republic of the', 50, 1, 1, 50, 0),
(149, 'Congo, the Democratic Republic of the', 50, 1, 2, 50, 0),
(150, 'Congo, the Democratic Republic of the', 50, 1, 3, 50, 0),
(151, 'Cook Islands', 51, 1, 1, 51, 0),
(152, 'Cook Islands', 51, 1, 2, 51, 0),
(153, 'Cook Islands', 51, 1, 3, 51, 0),
(154, 'Costa Rica', 52, 1, 1, 52, 0),
(155, 'Costa Rica', 52, 1, 2, 52, 0),
(156, 'Costa Rica', 52, 1, 3, 52, 0),
(157, 'Croatia (Hrvatska)', 53, 1, 1, 53, 0),
(158, 'Croatia (Hrvatska)', 53, 1, 2, 53, 0),
(159, 'Croatia (Hrvatska)', 53, 1, 3, 53, 0),
(160, 'Cuba', 54, 1, 1, 54, 0),
(161, 'Cuba', 54, 1, 2, 54, 0),
(162, 'Cuba', 54, 1, 3, 54, 0),
(163, 'Cyprus', 55, 1, 1, 55, 0),
(164, 'Cyprus', 55, 1, 2, 55, 0),
(165, 'Cyprus', 55, 1, 3, 55, 0),
(166, 'Czech Republic', 56, 1, 1, 56, 0),
(167, 'Czech Republic', 56, 1, 2, 56, 0),
(168, 'Czech Republic', 56, 1, 3, 56, 0),
(169, 'Denmark', 57, 1, 1, 57, 0),
(170, 'Denmark', 57, 1, 2, 57, 0),
(171, 'Denmark', 57, 1, 3, 57, 0),
(172, 'Djibouti', 58, 1, 1, 58, 0),
(173, 'Djibouti', 58, 1, 2, 58, 0),
(174, 'Djibouti', 58, 1, 3, 58, 0),
(175, 'Dominica', 59, 1, 1, 59, 0),
(176, 'Dominica', 59, 1, 2, 59, 0),
(177, 'Dominica', 59, 1, 3, 59, 0),
(178, 'Dominican Republic', 60, 1, 1, 60, 0),
(179, 'Dominican Republic', 60, 1, 2, 60, 0),
(180, 'Dominican Republic', 60, 1, 3, 60, 0),
(181, 'East Timor', 61, 1, 1, 61, 0),
(182, 'East Timor', 61, 1, 2, 61, 0),
(183, 'East Timor', 61, 1, 3, 61, 0),
(184, 'Ecuador', 62, 1, 1, 62, 0),
(185, 'Ecuador', 62, 1, 2, 62, 0),
(186, 'Ecuador', 62, 1, 3, 62, 0),
(187, 'Egypt', 63, 1, 1, 63, 0),
(188, 'Egypt', 63, 1, 2, 63, 0),
(189, 'Egypt', 63, 1, 3, 63, 0),
(190, 'El Salvador', 64, 1, 1, 64, 0),
(191, 'El Salvador', 64, 1, 2, 64, 0),
(192, 'El Salvador', 64, 1, 3, 64, 0),
(193, 'Equatorial Guinea', 65, 1, 1, 65, 0),
(194, 'Equatorial Guinea', 65, 1, 2, 65, 0),
(195, 'Equatorial Guinea', 65, 1, 3, 65, 0),
(196, 'Eritrea', 66, 1, 1, 66, 0),
(197, 'Eritrea', 66, 1, 2, 66, 0),
(198, 'Eritrea', 66, 1, 3, 66, 0),
(199, 'Estonia', 67, 1, 1, 67, 0),
(200, 'Estonia', 67, 1, 2, 67, 0),
(201, 'Estonia', 67, 1, 3, 67, 0),
(202, 'Ethiopia', 68, 1, 1, 68, 0),
(203, 'Ethiopia', 68, 1, 2, 68, 0),
(204, 'Ethiopia', 68, 1, 3, 68, 0),
(205, 'Falkland Islands (Malvinas)', 69, 1, 1, 69, 0),
(206, 'Falkland Islands (Malvinas)', 69, 1, 2, 69, 0),
(207, 'Falkland Islands (Malvinas)', 69, 1, 3, 69, 0),
(208, 'Faroe Islands', 70, 1, 1, 70, 0),
(209, 'Faroe Islands', 70, 1, 2, 70, 0),
(210, 'Faroe Islands', 70, 1, 3, 70, 0),
(211, 'Fiji', 71, 1, 1, 71, 0),
(212, 'Fiji', 71, 1, 2, 71, 0),
(213, 'Fiji', 71, 1, 3, 71, 0),
(214, 'Finland', 72, 1, 1, 72, 0),
(215, 'Finland', 72, 1, 2, 72, 0),
(216, 'Finland', 72, 1, 3, 72, 0),
(217, 'France', 73, 1, 1, 73, 0),
(218, 'France', 73, 1, 2, 73, 0),
(219, 'France', 73, 1, 3, 73, 0),
(220, 'France Metropolitan', 74, 1, 1, 74, 0),
(221, 'France Metropolitan', 74, 1, 2, 74, 0),
(222, 'France Metropolitan', 74, 1, 3, 74, 0),
(223, 'French Guiana', 75, 1, 1, 75, 0),
(224, 'French Guiana', 75, 1, 2, 75, 0),
(225, 'French Guiana', 75, 1, 3, 75, 0),
(226, 'French Polynesia', 76, 1, 1, 76, 0),
(227, 'French Polynesia', 76, 1, 2, 76, 0),
(228, 'French Polynesia', 76, 1, 3, 76, 0),
(229, 'French Southern Territories', 77, 1, 1, 77, 0),
(230, 'French Southern Territories', 77, 1, 2, 77, 0),
(231, 'French Southern Territories', 77, 1, 3, 77, 0),
(232, 'Gabon', 78, 1, 1, 78, 0),
(233, 'Gabon', 78, 1, 2, 78, 0),
(234, 'Gabon', 78, 1, 3, 78, 0),
(235, 'Gambia', 79, 1, 1, 79, 0),
(236, 'Gambia', 79, 1, 2, 79, 0),
(237, 'Gambia', 79, 1, 3, 79, 0),
(238, 'Georgia', 80, 1, 1, 80, 0),
(239, 'Georgia', 80, 1, 2, 80, 0),
(240, 'Georgia', 80, 1, 3, 80, 0),
(241, 'Germany', 81, 1, 1, 81, 0),
(242, 'Germany', 81, 1, 2, 81, 0),
(243, 'Germany', 81, 1, 3, 81, 0),
(244, 'Ghana', 82, 1, 1, 82, 0),
(245, 'Ghana', 82, 1, 2, 82, 0),
(246, 'Ghana', 82, 1, 3, 82, 0),
(247, 'Gibraltar', 83, 1, 1, 83, 0),
(248, 'Gibraltar', 83, 1, 2, 83, 0),
(249, 'Gibraltar', 83, 1, 3, 83, 0),
(250, 'Greece', 84, 1, 1, 84, 0),
(251, 'Greece', 84, 1, 2, 84, 0),
(252, 'Greece', 84, 1, 3, 84, 0),
(253, 'Greenland', 85, 1, 1, 85, 0),
(254, 'Greenland', 85, 1, 2, 85, 0),
(255, 'Greenland', 85, 1, 3, 85, 0),
(256, 'Grenada', 86, 1, 1, 86, 0),
(257, 'Grenada', 86, 1, 2, 86, 0),
(258, 'Grenada', 86, 1, 3, 86, 0),
(259, 'Guadeloupe', 87, 1, 1, 87, 0),
(260, 'Guadeloupe', 87, 1, 2, 87, 0),
(261, 'Guadeloupe', 87, 1, 3, 87, 0),
(262, 'Guam', 88, 1, 1, 88, 0),
(263, 'Guam', 88, 1, 2, 88, 0),
(264, 'Guam', 88, 1, 3, 88, 0),
(265, 'Guatemala', 89, 1, 1, 89, 0),
(266, 'Guatemala', 89, 1, 2, 89, 0),
(267, 'Guatemala', 89, 1, 3, 89, 0),
(268, 'Guinea', 90, 1, 1, 90, 0),
(269, 'Guinea', 90, 1, 2, 90, 0),
(270, 'Guinea', 90, 1, 3, 90, 0),
(271, 'Guinea-Bissau', 91, 1, 1, 91, 0),
(272, 'Guinea-Bissau', 91, 1, 2, 91, 0),
(273, 'Guinea-Bissau', 91, 1, 3, 91, 0),
(274, 'Guyana', 92, 1, 1, 92, 0),
(275, 'Guyana', 92, 1, 2, 92, 0),
(276, 'Guyana', 92, 1, 3, 92, 0),
(277, 'Haiti', 93, 1, 1, 93, 0),
(278, 'Haiti', 93, 1, 2, 93, 0),
(279, 'Haiti', 93, 1, 3, 93, 0),
(280, 'Heard and Mc Donald Islands', 94, 1, 1, 94, 0),
(281, 'Heard and Mc Donald Islands', 94, 1, 2, 94, 0),
(282, 'Heard and Mc Donald Islands', 94, 1, 3, 94, 0),
(283, 'Holy See (Vatican City State)', 95, 1, 1, 95, 0),
(284, 'Holy See (Vatican City State)', 95, 1, 2, 95, 0),
(285, 'Holy See (Vatican City State)', 95, 1, 3, 95, 0),
(286, 'Honduras', 96, 1, 1, 96, 0),
(287, 'Honduras', 96, 1, 2, 96, 0),
(288, 'Honduras', 96, 1, 3, 96, 0),
(289, 'Hong Kong', 97, 1, 1, 97, 0),
(290, 'Hong Kong', 97, 1, 2, 97, 0),
(291, 'Hong Kong', 97, 1, 3, 97, 0),
(292, 'Hungary', 98, 1, 1, 98, 0),
(293, 'Hungary', 98, 1, 2, 98, 0),
(294, 'Hungary', 98, 1, 3, 98, 0),
(295, 'Iceland', 99, 1, 1, 99, 0),
(296, 'Iceland', 99, 1, 2, 99, 0),
(297, 'Iceland', 99, 1, 3, 99, 0),
(298, 'India', 100, 1, 1, 100, 0),
(299, 'India', 100, 1, 2, 100, 0),
(300, 'India', 100, 1, 3, 100, 0),
(301, 'Indonesia', 101, 1, 1, 101, 0),
(302, 'Indonesia', 101, 1, 2, 101, 0),
(303, 'Indonesia', 101, 1, 3, 101, 0),
(304, 'Iran (Islamic Republic of)', 102, 1, 1, 102, 0),
(305, 'Iran (Islamic Republic of)', 102, 1, 2, 102, 0),
(306, 'Iran (Islamic Republic of)', 102, 1, 3, 102, 0),
(307, 'Iraq', 103, 1, 1, 103, 0),
(308, 'Iraq', 103, 1, 2, 103, 0),
(309, 'Iraq', 103, 1, 3, 103, 0),
(310, 'Ireland', 104, 1, 1, 104, 0),
(311, 'Ireland', 104, 1, 2, 104, 0),
(312, 'Ireland', 104, 1, 3, 104, 0),
(313, 'Israel', 105, 1, 1, 105, 0),
(314, 'Israel', 105, 1, 2, 105, 0),
(315, 'Israel', 105, 1, 3, 105, 0),
(316, 'Italy', 106, 1, 1, 106, 0),
(317, 'Italy', 106, 1, 2, 106, 0),
(318, 'Italy', 106, 1, 3, 106, 0),
(319, 'Jamaica', 107, 1, 1, 107, 0),
(320, 'Jamaica', 107, 1, 2, 107, 0),
(321, 'Jamaica', 107, 1, 3, 107, 0),
(322, 'Japan', 108, 1, 1, 108, 0),
(323, 'Japan', 108, 1, 2, 108, 0),
(324, 'Japan', 108, 1, 3, 108, 0),
(325, 'Jordan', 109, 1, 1, 109, 0),
(326, 'Jordan', 109, 1, 2, 109, 0),
(327, 'Jordan', 109, 1, 3, 109, 0),
(328, 'Kazakhstan', 110, 1, 1, 110, 0),
(329, 'Kazakhstan', 110, 1, 2, 110, 0),
(330, 'Kazakhstan', 110, 1, 3, 110, 0),
(331, 'Kenya', 111, 1, 1, 111, 0),
(332, 'Kenya', 111, 1, 2, 111, 0),
(333, 'Kenya', 111, 1, 3, 111, 0),
(334, 'Kiribati', 112, 1, 1, 112, 0),
(335, 'Kiribati', 112, 1, 2, 112, 0),
(336, 'Kiribati', 112, 1, 3, 112, 0),
(337, 'Korea, Republic of', 113, 1, 1, 113, 0),
(338, 'Korea, Republic of', 113, 1, 2, 113, 0),
(339, 'Korea, Republic of', 113, 1, 3, 113, 0),
(340, 'Kuwait', 114, 1, 1, 114, 0),
(341, 'Kuwait', 114, 1, 2, 114, 0),
(342, 'Kuwait', 114, 1, 3, 114, 0),
(343, 'Kyrgyzstan', 115, 1, 1, 115, 0),
(344, 'Kyrgyzstan', 115, 1, 2, 115, 0),
(345, 'Kyrgyzstan', 115, 1, 3, 115, 0),
(346, 'Latvia', 116, 1, 1, 116, 0),
(347, 'Latvia', 116, 1, 2, 116, 0),
(348, 'Latvia', 116, 1, 3, 116, 0),
(349, 'Lebanon', 117, 1, 1, 117, 0),
(350, 'Lebanon', 117, 1, 2, 117, 0),
(351, 'Lebanon', 117, 1, 3, 117, 0),
(352, 'Lesotho', 118, 1, 1, 118, 0),
(353, 'Lesotho', 118, 1, 2, 118, 0),
(354, 'Lesotho', 118, 1, 3, 118, 0),
(355, 'Liberia', 119, 1, 1, 119, 0),
(356, 'Liberia', 119, 1, 2, 119, 0),
(357, 'Liberia', 119, 1, 3, 119, 0),
(358, 'Libyan Arab Jamahiriya', 120, 1, 1, 120, 0),
(359, 'Libyan Arab Jamahiriya', 120, 1, 2, 120, 0),
(360, 'Libyan Arab Jamahiriya', 120, 1, 3, 120, 0),
(361, 'Liechtenstein', 121, 1, 1, 121, 0),
(362, 'Liechtenstein', 121, 1, 2, 121, 0),
(363, 'Liechtenstein', 121, 1, 3, 121, 0),
(364, 'Lithuania', 122, 1, 1, 122, 0),
(365, 'Lithuania', 122, 1, 2, 122, 0),
(366, 'Lithuania', 122, 1, 3, 122, 0),
(367, 'Luxembourg', 123, 1, 1, 123, 0),
(368, 'Luxembourg', 123, 1, 2, 123, 0),
(369, 'Luxembourg', 123, 1, 3, 123, 0),
(370, 'Macau', 124, 1, 1, 124, 0),
(371, 'Macau', 124, 1, 2, 124, 0),
(372, 'Macau', 124, 1, 3, 124, 0),
(373, 'Macedonia, The Former Yugoslav Republic of', 125, 1, 1, 125, 0),
(374, 'Macedonia, The Former Yugoslav Republic of', 125, 1, 2, 125, 0),
(375, 'Macedonia, The Former Yugoslav Republic of', 125, 1, 3, 125, 0),
(376, 'Madagascar', 126, 1, 1, 126, 0),
(377, 'Madagascar', 126, 1, 2, 126, 0),
(378, 'Madagascar', 126, 1, 3, 126, 0),
(379, 'Malawi', 127, 1, 1, 127, 0),
(380, 'Malawi', 127, 1, 2, 127, 0),
(381, 'Malawi', 127, 1, 3, 127, 0),
(382, 'Malaysia', 128, 1, 1, 128, 0),
(383, 'Malaysia', 128, 1, 2, 128, 0),
(384, 'Malaysia', 128, 1, 3, 128, 0),
(385, 'Maldives', 129, 1, 1, 129, 0),
(386, 'Maldives', 129, 1, 2, 129, 0),
(387, 'Maldives', 129, 1, 3, 129, 0),
(388, 'Mali', 130, 1, 1, 130, 0),
(389, 'Mali', 130, 1, 2, 130, 0),
(390, 'Mali', 130, 1, 3, 130, 0),
(391, 'Malta', 131, 1, 1, 131, 0),
(392, 'Malta', 131, 1, 2, 131, 0),
(393, 'Malta', 131, 1, 3, 131, 0),
(394, 'Marshall Islands', 132, 1, 1, 132, 0),
(395, 'Marshall Islands', 132, 1, 2, 132, 0),
(396, 'Marshall Islands', 132, 1, 3, 132, 0),
(397, 'Martinique', 133, 1, 1, 133, 0),
(398, 'Martinique', 133, 1, 2, 133, 0),
(399, 'Martinique', 133, 1, 3, 133, 0),
(400, 'Mauritania', 134, 1, 1, 134, 0),
(401, 'Mauritania', 134, 1, 2, 134, 0),
(402, 'Mauritania', 134, 1, 3, 134, 0),
(403, 'Mauritius', 135, 1, 1, 135, 0),
(404, 'Mauritius', 135, 1, 2, 135, 0),
(405, 'Mauritius', 135, 1, 3, 135, 0),
(406, 'Mayotte', 136, 1, 1, 136, 0),
(407, 'Mayotte', 136, 1, 2, 136, 0),
(408, 'Mayotte', 136, 1, 3, 136, 0),
(409, 'Mexico', 137, 1, 1, 137, 0),
(410, 'Mexico', 137, 1, 2, 137, 0),
(411, 'Mexico', 137, 1, 3, 137, 0),
(412, 'Micronesia, Federated States of', 138, 1, 1, 138, 0),
(413, 'Micronesia, Federated States of', 138, 1, 2, 138, 0),
(414, 'Micronesia, Federated States of', 138, 1, 3, 138, 0),
(415, 'Moldova, Republic of', 139, 1, 1, 139, 0),
(416, 'Moldova, Republic of', 139, 1, 2, 139, 0),
(417, 'Moldova, Republic of', 139, 1, 3, 139, 0),
(418, 'Monaco', 140, 1, 1, 140, 0),
(419, 'Monaco', 140, 1, 2, 140, 0),
(420, 'Monaco', 140, 1, 3, 140, 0),
(421, 'Mongolia', 141, 1, 1, 141, 0),
(422, 'Mongolia', 141, 1, 2, 141, 0),
(423, 'Mongolia', 141, 1, 3, 141, 0),
(424, 'Montserrat', 142, 1, 1, 142, 0),
(425, 'Montserrat', 142, 1, 2, 142, 0),
(426, 'Montserrat', 142, 1, 3, 142, 0),
(427, 'Morocco', 143, 1, 1, 143, 0),
(428, 'Morocco', 143, 1, 2, 143, 0),
(429, 'Morocco', 143, 1, 3, 143, 0),
(430, 'Mozambique', 144, 1, 1, 144, 0),
(431, 'Mozambique', 144, 1, 2, 144, 0),
(432, 'Mozambique', 144, 1, 3, 144, 0),
(433, 'Myanmar', 145, 1, 1, 145, 0),
(434, 'Myanmar', 145, 1, 2, 145, 0),
(435, 'Myanmar', 145, 1, 3, 145, 0),
(436, 'Namibia', 146, 1, 1, 146, 0),
(437, 'Namibia', 146, 1, 2, 146, 0),
(438, 'Namibia', 146, 1, 3, 146, 0),
(439, 'Nauru', 147, 1, 1, 147, 0),
(440, 'Nauru', 147, 1, 2, 147, 0),
(441, 'Nauru', 147, 1, 3, 147, 0),
(442, 'Nepal', 148, 1, 1, 148, 0),
(443, 'Nepal', 148, 1, 2, 148, 0),
(444, 'Nepal', 148, 1, 3, 148, 0),
(445, 'Netherlands', 149, 1, 1, 149, 0),
(446, 'Netherlands', 149, 1, 2, 149, 0),
(447, 'Netherlands', 149, 1, 3, 149, 0),
(448, 'Netherlands Antilles', 150, 1, 1, 150, 0),
(449, 'Netherlands Antilles', 150, 1, 2, 150, 0),
(450, 'Netherlands Antilles', 150, 1, 3, 150, 0),
(451, 'New Caledonia', 151, 1, 1, 151, 0),
(452, 'New Caledonia', 151, 1, 2, 151, 0),
(453, 'New Caledonia', 151, 1, 3, 151, 0),
(454, 'New Zealand', 152, 1, 1, 152, 0),
(455, 'New Zealand', 152, 1, 2, 152, 0),
(456, 'New Zealand', 152, 1, 3, 152, 0),
(457, 'Nicaragua', 153, 1, 1, 153, 0),
(458, 'Nicaragua', 153, 1, 2, 153, 0),
(459, 'Nicaragua', 153, 1, 3, 153, 0),
(460, 'Niger', 154, 1, 1, 154, 0),
(461, 'Niger', 154, 1, 2, 154, 0),
(462, 'Niger', 154, 1, 3, 154, 0),
(463, 'Nigeria', 155, 1, 1, 155, 0),
(464, 'Nigeria', 155, 1, 2, 155, 0),
(465, 'Nigeria', 155, 1, 3, 155, 0),
(466, 'Niue', 156, 1, 1, 156, 0),
(467, 'Niue', 156, 1, 2, 156, 0),
(468, 'Niue', 156, 1, 3, 156, 0),
(469, 'Norfolk Island', 157, 1, 1, 157, 0),
(470, 'Norfolk Island', 157, 1, 2, 157, 0),
(471, 'Norfolk Island', 157, 1, 3, 157, 0),
(472, 'Northern Mariana Islands', 158, 1, 1, 158, 0),
(473, 'Northern Mariana Islands', 158, 1, 2, 158, 0),
(474, 'Northern Mariana Islands', 158, 1, 3, 158, 0),
(475, 'Norway', 159, 1, 1, 159, 0),
(476, 'Norway', 159, 1, 2, 159, 0),
(477, 'Norway', 159, 1, 3, 159, 0),
(478, 'Oman', 160, 1, 1, 160, 0),
(479, 'Oman', 160, 1, 2, 160, 0),
(480, 'Oman', 160, 1, 3, 160, 0),
(481, 'Pakistan', 161, 1, 1, 161, 0),
(482, 'Pakistan', 161, 1, 2, 161, 0),
(483, 'Pakistan', 161, 1, 3, 161, 0),
(484, 'Palau', 162, 1, 1, 162, 0),
(485, 'Palau', 162, 1, 2, 162, 0),
(486, 'Palau', 162, 1, 3, 162, 0),
(487, 'Panama', 163, 1, 1, 163, 0),
(488, 'Panama', 163, 1, 2, 163, 0),
(489, 'Panama', 163, 1, 3, 163, 0),
(490, 'Papua New Guinea', 164, 1, 1, 164, 0),
(491, 'Papua New Guinea', 164, 1, 2, 164, 0),
(492, 'Papua New Guinea', 164, 1, 3, 164, 0),
(493, 'Paraguay', 165, 1, 1, 165, 0),
(494, 'Paraguay', 165, 1, 2, 165, 0),
(495, 'Paraguay', 165, 1, 3, 165, 0),
(496, 'Peru', 166, 1, 1, 166, 0),
(497, 'Peru', 166, 1, 2, 166, 0),
(498, 'Peru', 166, 1, 3, 166, 0),
(499, 'Philippines', 167, 1, 1, 167, 0),
(500, 'Philippines', 167, 1, 2, 167, 0),
(501, 'Philippines', 167, 1, 3, 167, 0),
(502, 'Pitcairn', 168, 1, 1, 168, 0),
(503, 'Pitcairn', 168, 1, 2, 168, 0),
(504, 'Pitcairn', 168, 1, 3, 168, 0),
(505, 'Poland', 169, 1, 1, 169, 0),
(506, 'Poland', 169, 1, 2, 169, 0),
(507, 'Poland', 169, 1, 3, 169, 0),
(508, 'Portugal', 170, 1, 1, 170, 0),
(509, 'Portugal', 170, 1, 2, 170, 0),
(510, 'Portugal', 170, 1, 3, 170, 0),
(511, 'Puerto Rico', 171, 1, 1, 171, 0),
(512, 'Puerto Rico', 171, 1, 2, 171, 0),
(513, 'Puerto Rico', 171, 1, 3, 171, 0),
(514, 'Qatar', 172, 1, 1, 172, 0),
(515, 'Qatar', 172, 1, 2, 172, 0),
(516, 'Qatar', 172, 1, 3, 172, 0),
(517, 'Reunion', 173, 1, 1, 173, 0),
(518, 'Reunion', 173, 1, 2, 173, 0),
(519, 'Reunion', 173, 1, 3, 173, 0),
(520, 'Romania', 174, 1, 1, 174, 0),
(521, 'Romania', 174, 1, 2, 174, 0),
(522, 'Romania', 174, 1, 3, 174, 0),
(523, 'Russian Federation', 175, 1, 1, 175, 0),
(524, 'Russian Federation', 175, 1, 2, 175, 0),
(525, 'Russian Federation', 175, 1, 3, 175, 0),
(526, 'Rwanda', 176, 1, 1, 176, 0),
(527, 'Rwanda', 176, 1, 2, 176, 0),
(528, 'Rwanda', 176, 1, 3, 176, 0),
(529, 'Saint Kitts and Nevis', 177, 1, 1, 177, 0),
(530, 'Saint Kitts and Nevis', 177, 1, 2, 177, 0),
(531, 'Saint Kitts and Nevis', 177, 1, 3, 177, 0),
(532, 'Saint Lucia', 178, 1, 1, 178, 0),
(533, 'Saint Lucia', 178, 1, 2, 178, 0),
(534, 'Saint Lucia', 178, 1, 3, 178, 0),
(535, 'Saint Vincent and the Grenadines', 179, 1, 1, 179, 0),
(536, 'Saint Vincent and the Grenadines', 179, 1, 2, 179, 0),
(537, 'Saint Vincent and the Grenadines', 179, 1, 3, 179, 0),
(538, 'Samoa', 180, 1, 1, 180, 0),
(539, 'Samoa', 180, 1, 2, 180, 0),
(540, 'Samoa', 180, 1, 3, 180, 0),
(541, 'San Marino', 181, 1, 1, 181, 0),
(542, 'San Marino', 181, 1, 2, 181, 0),
(543, 'San Marino', 181, 1, 3, 181, 0),
(544, 'Sao Tome and Principe', 182, 1, 1, 182, 0),
(545, 'Sao Tome and Principe', 182, 1, 2, 182, 0),
(546, 'Sao Tome and Principe', 182, 1, 3, 182, 0),
(547, 'Saudi Arabia', 183, 1, 1, 183, 0),
(548, 'Saudi Arabia', 183, 1, 2, 183, 0),
(549, 'Saudi Arabia', 183, 1, 3, 183, 0),
(550, 'Senegal', 184, 1, 1, 184, 0),
(551, 'Senegal', 184, 1, 2, 184, 0),
(552, 'Senegal', 184, 1, 3, 184, 0),
(553, 'Seychelles', 185, 1, 1, 185, 0),
(554, 'Seychelles', 185, 1, 2, 185, 0),
(555, 'Seychelles', 185, 1, 3, 185, 0),
(556, 'Sierra Leone', 186, 1, 1, 186, 0),
(557, 'Sierra Leone', 186, 1, 2, 186, 0),
(558, 'Sierra Leone', 186, 1, 3, 186, 0),
(559, 'Singapore', 187, 1, 1, 187, 0),
(560, 'Singapore', 187, 1, 2, 187, 0),
(561, 'Singapore', 187, 1, 3, 187, 0),
(562, 'Slovakia (Slovak Republic)', 188, 1, 1, 188, 0),
(563, 'Slovakia (Slovak Republic)', 188, 1, 2, 188, 0),
(564, 'Slovakia (Slovak Republic)', 188, 1, 3, 188, 0),
(565, 'Slovenia', 189, 1, 1, 189, 0),
(566, 'Slovenia', 189, 1, 2, 189, 0),
(567, 'Slovenia', 189, 1, 3, 189, 0),
(568, 'Solomon Islands', 190, 1, 1, 190, 0),
(569, 'Solomon Islands', 190, 1, 2, 190, 0),
(570, 'Solomon Islands', 190, 1, 3, 190, 0),
(571, 'Somalia', 191, 1, 1, 191, 0),
(572, 'Somalia', 191, 1, 2, 191, 0),
(573, 'Somalia', 191, 1, 3, 191, 0),
(574, 'South Africa', 192, 1, 1, 192, 0),
(575, 'South Africa', 192, 1, 2, 192, 0),
(576, 'South Africa', 192, 1, 3, 192, 0),
(577, 'South Georgia and the South Sandwich Islands', 193, 1, 1, 193, 0),
(578, 'South Georgia and the South Sandwich Islands', 193, 1, 2, 193, 0),
(579, 'South Georgia and the South Sandwich Islands', 193, 1, 3, 193, 0),
(580, 'Spain', 194, 1, 1, 194, 0),
(581, 'Spain', 194, 1, 2, 194, 0),
(582, 'Spain', 194, 1, 3, 194, 0),
(583, 'Sri Lanka', 195, 1, 1, 195, 0),
(584, 'Sri Lanka', 195, 1, 2, 195, 0),
(585, 'Sri Lanka', 195, 1, 3, 195, 0),
(586, 'St. Helena', 196, 1, 1, 196, 0),
(587, 'St. Helena', 196, 1, 2, 196, 0),
(588, 'St. Helena', 196, 1, 3, 196, 0),
(589, 'St. Pierre and Miquelon', 197, 1, 1, 197, 0),
(590, 'St. Pierre and Miquelon', 197, 1, 2, 197, 0),
(591, 'St. Pierre and Miquelon', 197, 1, 3, 197, 0),
(592, 'Sudan', 198, 1, 1, 198, 0),
(593, 'Sudan', 198, 1, 2, 198, 0),
(594, 'Sudan', 198, 1, 3, 198, 0),
(595, 'Suriname', 199, 1, 1, 199, 0),
(596, 'Suriname', 199, 1, 2, 199, 0),
(597, 'Suriname', 199, 1, 3, 199, 0),
(598, 'Svalbard and Jan Mayen Islands', 200, 1, 1, 200, 0),
(599, 'Svalbard and Jan Mayen Islands', 200, 1, 2, 200, 0),
(600, 'Svalbard and Jan Mayen Islands', 200, 1, 3, 200, 0),
(601, 'Swaziland', 201, 1, 1, 201, 0),
(602, 'Swaziland', 201, 1, 2, 201, 0),
(603, 'Swaziland', 201, 1, 3, 201, 0),
(604, 'Sweden', 202, 1, 1, 202, 0),
(605, 'Sweden', 202, 1, 2, 202, 0),
(606, 'Sweden', 202, 1, 3, 202, 0),
(607, 'Switzerland', 203, 1, 1, 203, 0),
(608, 'Switzerland', 203, 1, 2, 203, 0),
(609, 'Switzerland', 203, 1, 3, 203, 0),
(610, 'Syrian Arab Republic', 204, 1, 1, 204, 0),
(611, 'Syrian Arab Republic', 204, 1, 2, 204, 0),
(612, 'Syrian Arab Republic', 204, 1, 3, 204, 0),
(613, 'Taiwan, Province of China', 205, 1, 1, 205, 0),
(614, 'Taiwan, Province of China', 205, 1, 2, 205, 0),
(615, 'Taiwan, Province of China', 205, 1, 3, 205, 0),
(616, 'Tajikistan', 206, 1, 1, 206, 0),
(617, 'Tajikistan', 206, 1, 2, 206, 0),
(618, 'Tajikistan', 206, 1, 3, 206, 0),
(619, 'Tanzania, United Republic of', 207, 1, 1, 207, 0),
(620, 'Tanzania, United Republic of', 207, 1, 2, 207, 0),
(621, 'Tanzania, United Republic of', 207, 1, 3, 207, 0),
(622, 'Thailand', 208, 1, 1, 208, 0),
(623, 'Thailand', 208, 1, 2, 208, 0),
(624, 'Thailand', 208, 1, 3, 208, 0),
(625, 'Togo', 209, 1, 1, 209, 0),
(626, 'Togo', 209, 1, 2, 209, 0),
(627, 'Togo', 209, 1, 3, 209, 0),
(628, 'Tokelau', 210, 1, 1, 210, 0),
(629, 'Tokelau', 210, 1, 2, 210, 0),
(630, 'Tokelau', 210, 1, 3, 210, 0),
(631, 'Tonga', 211, 1, 1, 211, 0),
(632, 'Tonga', 211, 1, 2, 211, 0),
(633, 'Tonga', 211, 1, 3, 211, 0),
(634, 'Trinidad and Tobago', 212, 1, 1, 212, 0),
(635, 'Trinidad and Tobago', 212, 1, 2, 212, 0),
(636, 'Trinidad and Tobago', 212, 1, 3, 212, 0),
(637, 'Tunisia', 213, 1, 1, 213, 0),
(638, 'Tunisia', 213, 1, 2, 213, 0),
(639, 'Tunisia', 213, 1, 3, 213, 0),
(640, 'Turkey', 214, 1, 1, 214, 0),
(641, 'Turkey', 214, 1, 2, 214, 0),
(642, 'Turkey', 214, 1, 3, 214, 0),
(643, 'Turkmenistan', 215, 1, 1, 215, 0),
(644, 'Turkmenistan', 215, 1, 2, 215, 0),
(645, 'Turkmenistan', 215, 1, 3, 215, 0),
(646, 'Turks and Caicos Islands', 216, 1, 1, 216, 0),
(647, 'Turks and Caicos Islands', 216, 1, 2, 216, 0),
(648, 'Turks and Caicos Islands', 216, 1, 3, 216, 0),
(649, 'Tuvalu', 217, 1, 1, 217, 0),
(650, 'Tuvalu', 217, 1, 2, 217, 0),
(651, 'Tuvalu', 217, 1, 3, 217, 0),
(652, 'Uganda', 218, 1, 1, 218, 0),
(653, 'Uganda', 218, 1, 2, 218, 0),
(654, 'Uganda', 218, 1, 3, 218, 0),
(655, 'Ukraine', 219, 1, 1, 219, 0),
(656, 'Ukraine', 219, 1, 2, 219, 0),
(657, 'Ukraine', 219, 1, 3, 219, 0),
(658, 'United Arab Emirates', 220, 1, 1, 220, 0),
(659, 'United Arab Emirates', 220, 1, 2, 220, 0),
(660, 'United Arab Emirates', 220, 1, 3, 220, 0),
(661, 'United Kingdom', 221, 1, 1, 221, 0),
(662, 'United Kingdom', 221, 1, 2, 221, 0),
(663, 'United Kingdom', 221, 1, 3, 221, 0),
(664, 'United States', 222, 1, 1, 222, 0),
(665, 'United States', 222, 1, 2, 222, 0),
(666, 'United States', 222, 1, 3, 222, 0),
(667, 'United States Minor Outlying Islands', 223, 1, 1, 223, 0),
(668, 'United States Minor Outlying Islands', 223, 1, 2, 223, 0),
(669, 'United States Minor Outlying Islands', 223, 1, 3, 223, 0),
(670, 'Uruguay', 224, 1, 1, 224, 0),
(671, 'Uruguay', 224, 1, 2, 224, 0),
(672, 'Uruguay', 224, 1, 3, 224, 0),
(673, 'Uzbekistan', 225, 1, 1, 225, 0),
(674, 'Uzbekistan', 225, 1, 2, 225, 0),
(675, 'Uzbekistan', 225, 1, 3, 225, 0),
(676, 'Vanuatu', 226, 1, 1, 226, 0),
(677, 'Vanuatu', 226, 1, 2, 226, 0),
(678, 'Vanuatu', 226, 1, 3, 226, 0),
(679, 'Venezuela', 227, 1, 1, 227, 0),
(680, 'Venezuela', 227, 1, 2, 227, 0),
(681, 'Venezuela', 227, 1, 3, 227, 0),
(682, 'Vietnam', 228, 1, 1, 228, 0),
(683, 'Vietnam', 228, 1, 2, 228, 0),
(684, 'Vietnam', 228, 1, 3, 228, 0),
(685, 'Virgin Islands (British)', 229, 1, 1, 229, 0),
(686, 'Virgin Islands (British)', 229, 1, 2, 229, 0),
(687, 'Virgin Islands (British)', 229, 1, 3, 229, 0),
(688, 'Virgin Islands (U.S.)', 230, 1, 1, 230, 0),
(689, 'Virgin Islands (U.S.)', 230, 1, 2, 230, 0),
(690, 'Virgin Islands (U.S.)', 230, 1, 3, 230, 0),
(691, 'Wallis and Futuna Islands', 231, 1, 1, 231, 0),
(692, 'Wallis and Futuna Islands', 231, 1, 2, 231, 0),
(693, 'Wallis and Futuna Islands', 231, 1, 3, 231, 0),
(694, 'Western Sahara', 232, 1, 1, 232, 0),
(695, 'Western Sahara', 232, 1, 2, 232, 0),
(696, 'Western Sahara', 232, 1, 3, 232, 0),
(697, 'Yemen', 233, 1, 1, 233, 0),
(698, 'Yemen', 233, 1, 2, 233, 0),
(699, 'Yemen', 233, 1, 3, 233, 0),
(700, 'Yugoslavia', 234, 1, 1, 234, 0),
(701, 'Yugoslavia', 234, 1, 2, 234, 0),
(702, 'Yugoslavia', 234, 1, 3, 234, 0),
(703, 'Zambia', 235, 1, 1, 235, 0),
(704, 'Zambia', 235, 1, 2, 235, 0),
(705, 'Zambia', 235, 1, 3, 235, 0),
(706, 'Zimbabwe', 236, 1, 1, 236, 0),
(707, 'Zimbabwe', 236, 1, 2, 236, 0),
(708, 'Zimbabwe', 236, 1, 3, 236, 0);

-- --------------------------------------------------------

--
-- Table structure for table `days`
--

CREATE TABLE `days` (
  `id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `manset` int(2) NOT NULL DEFAULT '0',
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL,
  `created_at` int(13) NOT NULL,
  `updated_at` int(13) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `days`
--

INSERT INTO `days` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `manset`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`, `created_at`, `updated_at`) VALUES
(1, '', '', '', 'Azərbaycan Respublikasının vətəndaşları, həmçinin əcnəbilər və vətəndaşlığı olmayan şəxslər Azərbaycan Respublikası ərazisində olma müddətinin uzadılması, müvəqqəti və daimi yaşamaq, habelə haqqı ödənilən əmək fəaliyyəti ilə məşğul olmaq üçün müvafiq icazələrin verilməsi, qaçqın statusunun müəyyənləşdirilməsi və vətəndaşlıq məsələləri ilə əlaqədar həftənin I-V günləri saat 09:00-18:00-dək Azərbaycan Respublikasının Dövlət Miqrasiya Xidmətinə müraciət edə bilərlər. \r\n\r\nÜnvan: AZ1114, Bakı şəhəri, Binəqədi rayonu, 3123-cü məhəllə, Binəqədi şossesi 202.', '', '&lt;table class=&quot;table table-bordered&quot;&gt;\r\n&lt;thead&gt;\r\n&lt;tr&gt;&lt;th scope=&quot;col&quot;&gt;№&lt;/th&gt;&lt;th scope=&quot;col&quot;&gt;Soyadı, adı, atasının adı&lt;/th&gt;&lt;th scope=&quot;col&quot;&gt;Vəzifəsi&lt;/th&gt;&lt;th scope=&quot;col&quot;&gt;Qəbul g&amp;uuml;nləri&lt;/th&gt;&lt;th scope=&quot;col&quot;&gt;Qəbula yazılma&lt;/th&gt;&lt;th scope=&quot;col&quot;&gt;Qəbul saatları&lt;/th&gt;&lt;/tr&gt;\r\n&lt;/thead&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;&lt;th scope=&quot;row&quot;&gt;1&lt;/th&gt;\r\n&lt;td&gt;Nəbiyev Şahin Hidayət oğlu&lt;/td&gt;\r\n&lt;td&gt;Xidmət rəisi&lt;/td&gt;\r\n&lt;td&gt;Hər ayın sonuncu həftəsinin şənbə g&amp;uuml;n&amp;uuml;&lt;/td&gt;\r\n&lt;td&gt;Bir g&amp;uuml;n əvvəl&lt;/td&gt;\r\n&lt;td&gt;09&lt;span&gt;00&lt;/span&gt;-13&lt;span&gt;00&lt;/span&gt;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;', 0, 0, 1, 1, 1, 1, 0, 1537943847, 0),
(2, '', '', '', 'Azərbaycan Respublikasının vətəndaşları, həmçinin əcnəbilər və vətəndaşlığı olmayan şəxslər Azərbaycan Respublikası ərazisində olma müddətinin uzadılması, müvəqqəti və daimi yaşamaq, habelə haqqı ödənilən əmək fəaliyyəti ilə məşğul olmaq üçün müvafiq icazələrin verilməsi, qaçqın statusunun müəyyənləşdirilməsi və vətəndaşlıq məsələləri ilə əlaqədar həftənin I-V günləri saat 09:00-18:00-dək Azərbaycan Respublikasının Dövlət Miqrasiya Xidmətinə müraciət edə bilərlər. \r\n\r\nÜnvan: AZ1114, Bakı şəhəri, Binəqədi rayonu, 3123-cü məhəllə, Binəqədi şossesi 202.', '', '&lt;table class=&quot;table table-bordered&quot;&gt;\r\n&lt;thead&gt;\r\n&lt;tr&gt;&lt;th scope=&quot;col&quot;&gt;№&lt;/th&gt;&lt;th scope=&quot;col&quot;&gt;Soyadı, adı, atasının adı&lt;/th&gt;&lt;th scope=&quot;col&quot;&gt;Vəzifəsi&lt;/th&gt;&lt;th scope=&quot;col&quot;&gt;Qəbul g&amp;uuml;nləri&lt;/th&gt;&lt;th scope=&quot;col&quot;&gt;Qəbula yazılma&lt;/th&gt;&lt;th scope=&quot;col&quot;&gt;Qəbul saatları&lt;/th&gt;&lt;/tr&gt;\r\n&lt;/thead&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;&lt;th scope=&quot;row&quot;&gt;1&lt;/th&gt;\r\n&lt;td&gt;Nəbiyev Şahin Hidayət oğlu&lt;/td&gt;\r\n&lt;td&gt;Xidmət rəisi&lt;/td&gt;\r\n&lt;td&gt;Hər ayın sonuncu həftəsinin şənbə g&amp;uuml;n&amp;uuml;&lt;/td&gt;\r\n&lt;td&gt;Bir g&amp;uuml;n əvvəl&lt;/td&gt;\r\n&lt;td&gt;09&lt;span&gt;00&lt;/span&gt;-13&lt;span&gt;00&lt;/span&gt;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;', 0, 0, 1, 1, 2, 1, 0, 1537943847, 0),
(3, '', '', '', 'Azərbaycan Respublikasının vətəndaşları, həmçinin əcnəbilər və vətəndaşlığı olmayan şəxslər Azərbaycan Respublikası ərazisində olma müddətinin uzadılması, müvəqqəti və daimi yaşamaq, habelə haqqı ödənilən əmək fəaliyyəti ilə məşğul olmaq üçün müvafiq icazələrin verilməsi, qaçqın statusunun müəyyənləşdirilməsi və vətəndaşlıq məsələləri ilə əlaqədar həftənin I-V günləri saat 09:00-18:00-dək Azərbaycan Respublikasının Dövlət Miqrasiya Xidmətinə müraciət edə bilərlər. \r\n\r\nÜnvan: AZ1114, Bakı şəhəri, Binəqədi rayonu, 3123-cü məhəllə, Binəqədi şossesi 202.', '', '&lt;table class=&quot;table table-bordered&quot;&gt;\r\n&lt;thead&gt;\r\n&lt;tr&gt;&lt;th scope=&quot;col&quot;&gt;№&lt;/th&gt;&lt;th scope=&quot;col&quot;&gt;Soyadı, adı, atasının adı&lt;/th&gt;&lt;th scope=&quot;col&quot;&gt;Vəzifəsi&lt;/th&gt;&lt;th scope=&quot;col&quot;&gt;Qəbul g&amp;uuml;nləri&lt;/th&gt;&lt;th scope=&quot;col&quot;&gt;Qəbula yazılma&lt;/th&gt;&lt;th scope=&quot;col&quot;&gt;Qəbul saatları&lt;/th&gt;&lt;/tr&gt;\r\n&lt;/thead&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;&lt;th scope=&quot;row&quot;&gt;1&lt;/th&gt;\r\n&lt;td&gt;Nəbiyev Şahin Hidayət oğlu&lt;/td&gt;\r\n&lt;td&gt;Xidmət rəisi&lt;/td&gt;\r\n&lt;td&gt;Hər ayın sonuncu həftəsinin şənbə g&amp;uuml;n&amp;uuml;&lt;/td&gt;\r\n&lt;td&gt;Bir g&amp;uuml;n əvvəl&lt;/td&gt;\r\n&lt;td&gt;09&lt;span&gt;00&lt;/span&gt;-13&lt;span&gt;00&lt;/span&gt;&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;', 0, 0, 1, 1, 3, 1, 0, 1537943847, 0);

-- --------------------------------------------------------

--
-- Table structure for table `description`
--

CREATE TABLE `description` (
  `id` int(11) NOT NULL,
  `description_` text NOT NULL,
  `keywords_` text NOT NULL,
  `title_` varchar(255) NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `description`
--

INSERT INTO `description` (`id`, `description_`, `keywords_`, `title_`, `lang_id`) VALUES
(1, 'Naxçıvan Muxtar Respublikası Dövlət Miqrasiya Xidməti', 'kredit, sigorta', 'Naxçıvan Muxtar Respublikası Dövlət Miqrasiya Xidməti', 1),
(2, 'Naxçıvan Muxtar Respublikası Dövlət Miqrasiya Xidməti', 'kredit, sigorta', 'Naxçıvan Muxtar Respublikası Dövlət Miqrasiya Xidməti', 2),
(3, 'Naxçıvan Muxtar Respublikası Dövlət Miqrasiya Xidməti', 'kredit, sigorta', 'Naxçıvan Muxtar Respublikası Dövlət Miqrasiya Xidməti', 3);

-- --------------------------------------------------------

--
-- Table structure for table `diller`
--

CREATE TABLE `diller` (
  `id` int(11) NOT NULL,
  `ad` varchar(255) NOT NULL,
  `tam_adi` varchar(255) NOT NULL,
  `sira` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `flag` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `diller`
--

INSERT INTO `diller` (`id`, `ad`, `tam_adi`, `sira`, `status`, `aktivlik`, `flag`) VALUES
(1, 'AZ', '', 1, 1, 1, 'United-Kingdom-flag-64.png'),
(2, 'RU', '', 2, 0, 1, 'United-Kingdom-flag-64.png'),
(3, 'EN', '', 3, 0, 1, 'United-Kingdom-flag-64.png');

-- --------------------------------------------------------

--
-- Table structure for table `dmx`
--

CREATE TABLE `dmx` (
  `id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dmx`
--

INSERT INTO `dmx` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, 'Azərbaycan Respublikası  DÖVLƏT MİQRASİYA XİDMƏTİ', '', '', '', '', '&lt;center&gt;&lt;span id=&quot;basliq1&quot;&gt;Azərbaycan Respublikası&amp;nbsp;&lt;br /&gt;D&amp;Ouml;VLƏT MİQRASİYA XİDMƏTİ&lt;/span&gt;&lt;/center&gt;\r\n&lt;p&gt;&lt;span&gt;D&amp;ouml;vlət miqrasiya siyasəti&lt;/span&gt;&lt;br /&gt;Azərbaycan Respublikasında miqrasiya sahəsində d&amp;ouml;vlət siyasətinin həyata ke&amp;ccedil;irilməsi, qanunvericiliyin beynəlxalq normalara və m&amp;uuml;asir d&amp;ouml;vr&amp;uuml;n tələblərinə uyğun təkmilləşdirilməsi, qanunların tətbiqində Azərbaycan Respublikasının milli təhl&amp;uuml;kəsizliyinin və sabit sosial-iqtisadi, demoqrafik inkişafının təmin edilməsi, əmək ehtiyatlarından səmərəli istifadə edilməsi, &amp;ouml;lkə ərazisində əhalinin m&amp;uuml;tənasib yerləşdirilməsi, miqrantların intellektual və əmək potensialından istifadə edilməsi, tənzimlənməyən miqrasiya proseslərinin neqativ təsirinin aradan qaldırılması, insan alveri də daxil olmaqla, qeyri-qanuni miqrasiyanın qarşısının alınması məqsədi ilə Azərbaycan Respublikası Prezidentinin 25 iyul 2006-cı il tarixli Sərəncamı ilə &amp;ldquo;Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Proqramı (2006-2008-ci illər)&amp;rdquo; təsdiq edilmişdir. Bu D&amp;ouml;vlət Proqramının əsas məqsədi miqrasiya sahəsində d&amp;ouml;vlət siyasətinin həyata ke&amp;ccedil;irilməsi, miqrasiya idarəetmə sisteminin inkişaf etdirilməsi, miqrasiya proseslərinin tənzimlənməsi və proqnozlaşdırılması, bu sahədə qanunvericiliyin beynəlxalq normalara və m&amp;uuml;asir d&amp;ouml;vr&amp;uuml;n tələblərinə uyğun təkmilləşdirilməsi, qanunların tətbiqinin effektivliyinin təmin edilməsi, miqrasiya sahəsində vahid məlumat bankının, m&amp;uuml;asir avtomatlaşdırılmış nəzarət sisteminin yaradılması, qeyri-qanuni miqrasiyanın qarşısının alınması, beynəlxalq əməkdaşlığın inkişaf etdirilməsi tədbirlərinin həyata ke&amp;ccedil;irilməsindən ibarətdir. Proqramda miqrasiya proseslərinin x&amp;uuml;susiyyətləri, d&amp;ouml;vlət siyasətinin prioritet istiqamətləri &amp;ouml;z əksini tapmışdır. Bununla yanaşı proqramın əhatə etdiyi d&amp;ouml;vr ərzində ayrı-ayrı d&amp;ouml;vlət qurumları ilə əlaqəli şəkildə həyata ke&amp;ccedil;irməli olduqları tədbirlər qeyd edilmişdir&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;D&amp;ouml;vlət Miqrasiya Xidməti haqqında&lt;/span&gt;&amp;nbsp;&lt;br /&gt;Təsdiq edilmiş &amp;ldquo;Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Proqramı&amp;rdquo; əsasında miqrasiya sahəsində vahid d&amp;ouml;vlət siyasətini həyata ke&amp;ccedil;irən x&amp;uuml;susi d&amp;ouml;vlət orqanının yaradılması zərurətini nəzərə alan Azərbaycan Respublikasının Prezidenti cənab İlham Əliyev 19 mart 2007-ci il tarixdə Azərbaycan Respublikası D&amp;ouml;vlət Miqrasiya Xidmətinin yaradılması haqqında 560 n&amp;ouml;mrəli Fərman imzalamışdır. Eyni zamanda, Fərmana əsasən Xidmətin Əsasnaməsi təsdiq edilmişdir. D&amp;ouml;vlət Miqrasiya Xidmətinin yaradıldığı vaxtdan miqrasiya proseslərinə d&amp;ouml;vlət nəzarətinin g&amp;uuml;cləndirilməsi məqsədi ilə zəruri normativ h&amp;uuml;quqi aktlar qəbul edilmiş, bir sıra institusional və təşkilati tədbirlər həyata ke&amp;ccedil;irilmişdir. Miqrasiya proseslərinin vahid və &amp;ccedil;evik prosedurlar əsasında tənzimlənməsi, sənədləşmənin sadələşdirilməsi məqsədilə Azərbaycan Respublikasının Prezidenti cənab İlham Əliyev tərəfindən &quot;Miqrasiya proseslərinin idarəolunmasında &quot;bir pəncərə&quot; prinsipinin tətbiqi haqqında&quot; 4 mart 2009-cu il tarixli 69 n&amp;ouml;mrəli Fərman imzalanmışdır. Adı&amp;ccedil;əkilən Fərmanın tətbiqi &amp;ouml;lkədə miqrasiya proseslərinin daha &amp;ccedil;evik və işlək mexanizmlər əsasında idarə edilməsinə, bu sahədə operativliyin təmin edilməsinə və həllini g&amp;ouml;zləyən problemlərin aradan qaldırılmasına səbəb olmuşdur. Fərmana əsasən, 2009-cu il iyulun 1-dən miqrasiya proseslərinin idarəolunmasında &amp;laquo;bir pəncərə&amp;raquo; prinsipinin tətbiq edilməsinə başlanılmış və bu prinsip &amp;uuml;zrə vahid d&amp;ouml;vlət orqanının səlahiyyətləri Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Xidmətinə həvalə edilmişdir. &amp;ldquo;Bir pəncərə&amp;rdquo; prinsipi &amp;ccedil;ər&amp;ccedil;ivəsində D&amp;ouml;vlət Miqrasiya Xidməti əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti və daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazələrin və m&amp;uuml;vafiq vəsiqələrin verilməsini, onların qeydiyyata alınmasını, Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılmasını, həm&amp;ccedil;inin &amp;ouml;lkə ərazisində haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq &amp;uuml;&amp;ccedil;&amp;uuml;n iş icazələrinin verilməsini həyata ke&amp;ccedil;irir. Bununla belə, Xidmət vətəndaşlıq məsələlərində iştirak edir və qa&amp;ccedil;qın statusunu m&amp;uuml;əyyənləşdirir. Qeyri-qanuni miqrasiyaya qarşı m&amp;uuml;barizə tədbirlərinin g&amp;uuml;cləndirilməsi, Xidmətin fəaliyyətinin təkmilləşdirilməsi məqsədi ilə Prezident cənab İlham Əliyevin 8 aprel 2009-cu il tarixdə imzaladığı 76 n&amp;ouml;mrəli Fərmanla D&amp;ouml;vlət Miqrasiya Xidmətinə h&amp;uuml;quq-m&amp;uuml;hafizə orqanı statusu verilmişdir.&lt;/p&gt;', 0, 1, 1, 1, 1, 0),
(2, 'Azərbaycan Respublikası  DÖVLƏT MİQRASİYA XİDMƏTİ', '', '', '', '', '&lt;p&gt;&lt;strong&gt;D&amp;ouml;vlət miqrasiya siyasəti&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;&lt;/strong&gt;&lt;br /&gt;Azərbaycan Respublikasında miqrasiya sahəsində d&amp;ouml;vlət siyasətinin həyata ke&amp;ccedil;irilməsi, qanunvericiliyin beynəlxalq normalara və m&amp;uuml;asir d&amp;ouml;vr&amp;uuml;n tələblərinə uyğun təkmilləşdirilməsi, qanunların tətbiqində Azərbaycan Respublikasının milli təhl&amp;uuml;kəsizliyinin və sabit sosial-iqtisadi, demoqrafik inkişafının təmin edilməsi, əmək ehtiyatlarından səmərəli istifadə edilməsi, &amp;ouml;lkə ərazisində əhalinin m&amp;uuml;tənasib yerləşdirilməsi, miqrantların intellektual və əmək potensialından istifadə edilməsi, tənzimlənməyən miqrasiya proseslərinin neqativ təsirinin aradan qaldırılması, insan alveri də daxil olmaqla, qeyri-qanuni miqrasiyanın qarşısının alınması məqsədi ilə Azərbaycan Respublikası Prezidentinin 25 iyul 2006-cı il tarixli Sərəncamı ilə &amp;ldquo;Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Proqramı (2006-2008-ci illər)&amp;rdquo; təsdiq edilmişdir. Bu D&amp;ouml;vlət Proqramının əsas məqsədi miqrasiya sahəsində d&amp;ouml;vlət siyasətinin həyata ke&amp;ccedil;irilməsi, miqrasiya idarəetmə sisteminin inkişaf etdirilməsi, miqrasiya proseslərinin tənzimlənməsi və proqnozlaşdırılması, bu sahədə qanunvericiliyin beynəlxalq normalara və m&amp;uuml;asir d&amp;ouml;vr&amp;uuml;n tələblərinə uyğun təkmilləşdirilməsi, qanunların tətbiqinin effektivliyinin təmin edilməsi, miqrasiya sahəsində vahid məlumat bankının, m&amp;uuml;asir avtomatlaşdırılmış nəzarət sisteminin yaradılması, qeyri-qanuni miqrasiyanın qarşısının alınması, beynəlxalq əməkdaşlığın inkişaf etdirilməsi tədbirlərinin həyata ke&amp;ccedil;irilməsindən ibarətdir. Proqramda miqrasiya proseslərinin x&amp;uuml;susiyyətləri, d&amp;ouml;vlət siyasətinin prioritet istiqamətləri &amp;ouml;z əksini tapmışdır. Bununla yanaşı proqramın əhatə etdiyi d&amp;ouml;vr ərzində ayrı-ayrı d&amp;ouml;vlət qurumları ilə əlaqəli şəkildə həyata ke&amp;ccedil;irməli olduqları tədbirlər qeyd edilmişdir&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;D&amp;ouml;vlət Miqrasiya Xidməti haqqında&lt;/span&gt;&amp;nbsp;&lt;br /&gt;Təsdiq edilmiş &amp;ldquo;Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Proqramı&amp;rdquo; əsasında miqrasiya sahəsində vahid d&amp;ouml;vlət siyasətini həyata ke&amp;ccedil;irən x&amp;uuml;susi d&amp;ouml;vlət orqanının yaradılması zərurətini nəzərə alan Azərbaycan Respublikasının Prezidenti cənab İlham Əliyev 19 mart 2007-ci il tarixdə Azərbaycan Respublikası D&amp;ouml;vlət Miqrasiya Xidmətinin yaradılması haqqında 560 n&amp;ouml;mrəli Fərman imzalamışdır. Eyni zamanda, Fərmana əsasən Xidmətin Əsasnaməsi təsdiq edilmişdir. D&amp;ouml;vlət Miqrasiya Xidmətinin yaradıldığı vaxtdan miqrasiya proseslərinə d&amp;ouml;vlət nəzarətinin g&amp;uuml;cləndirilməsi məqsədi ilə zəruri normativ h&amp;uuml;quqi aktlar qəbul edilmiş, bir sıra institusional və təşkilati tədbirlər həyata ke&amp;ccedil;irilmişdir. Miqrasiya proseslərinin vahid və &amp;ccedil;evik prosedurlar əsasında tənzimlənməsi, sənədləşmənin sadələşdirilməsi məqsədilə Azərbaycan Respublikasının Prezidenti cənab İlham Əliyev tərəfindən &quot;Miqrasiya proseslərinin idarəolunmasında &quot;bir pəncərə&quot; prinsipinin tətbiqi haqqında&quot; 4 mart 2009-cu il tarixli 69 n&amp;ouml;mrəli Fərman imzalanmışdır. Adı&amp;ccedil;əkilən Fərmanın tətbiqi &amp;ouml;lkədə miqrasiya proseslərinin daha &amp;ccedil;evik və işlək mexanizmlər əsasında idarə edilməsinə, bu sahədə operativliyin təmin edilməsinə və həllini g&amp;ouml;zləyən problemlərin aradan qaldırılmasına səbəb olmuşdur. Fərmana əsasən, 2009-cu il iyulun 1-dən miqrasiya proseslərinin idarəolunmasında &amp;laquo;bir pəncərə&amp;raquo; prinsipinin tətbiq edilməsinə başlanılmış və bu prinsip &amp;uuml;zrə vahid d&amp;ouml;vlət orqanının səlahiyyətləri Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Xidmətinə həvalə edilmişdir. &amp;ldquo;Bir pəncərə&amp;rdquo; prinsipi &amp;ccedil;ər&amp;ccedil;ivəsində D&amp;ouml;vlət Miqrasiya Xidməti əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti və daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazələrin və m&amp;uuml;vafiq vəsiqələrin verilməsini, onların qeydiyyata alınmasını, Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılmasını, həm&amp;ccedil;inin &amp;ouml;lkə ərazisində haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq &amp;uuml;&amp;ccedil;&amp;uuml;n iş icazələrinin verilməsini həyata ke&amp;ccedil;irir. Bununla belə, Xidmət vətəndaşlıq məsələlərində iştirak edir və qa&amp;ccedil;qın statusunu m&amp;uuml;əyyənləşdirir. Qeyri-qanuni miqrasiyaya qarşı m&amp;uuml;barizə tədbirlərinin g&amp;uuml;cləndirilməsi, Xidmətin fəaliyyətinin təkmilləşdirilməsi məqsədi ilə Prezident cənab İlham Əliyevin 8 aprel 2009-cu il tarixdə imzaladığı 76 n&amp;ouml;mrəli Fərmanla D&amp;ouml;vlət Miqrasiya Xidmətinə h&amp;uuml;quq-m&amp;uuml;hafizə orqanı statusu verilmişdir.&lt;/p&gt;', 0, 1, 1, 2, 1, 0),
(3, 'Azərbaycan Respublikası  DÖVLƏT MİQRASİYA XİDMƏTİ', '', '', '', '', '&lt;p&gt;&lt;strong&gt;D&amp;ouml;vlət miqrasiya siyasəti&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;&lt;/strong&gt;&lt;br /&gt;Azərbaycan Respublikasında miqrasiya sahəsində d&amp;ouml;vlət siyasətinin həyata ke&amp;ccedil;irilməsi, qanunvericiliyin beynəlxalq normalara və m&amp;uuml;asir d&amp;ouml;vr&amp;uuml;n tələblərinə uyğun təkmilləşdirilməsi, qanunların tətbiqində Azərbaycan Respublikasının milli təhl&amp;uuml;kəsizliyinin və sabit sosial-iqtisadi, demoqrafik inkişafının təmin edilməsi, əmək ehtiyatlarından səmərəli istifadə edilməsi, &amp;ouml;lkə ərazisində əhalinin m&amp;uuml;tənasib yerləşdirilməsi, miqrantların intellektual və əmək potensialından istifadə edilməsi, tənzimlənməyən miqrasiya proseslərinin neqativ təsirinin aradan qaldırılması, insan alveri də daxil olmaqla, qeyri-qanuni miqrasiyanın qarşısının alınması məqsədi ilə Azərbaycan Respublikası Prezidentinin 25 iyul 2006-cı il tarixli Sərəncamı ilə &amp;ldquo;Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Proqramı (2006-2008-ci illər)&amp;rdquo; təsdiq edilmişdir. Bu D&amp;ouml;vlət Proqramının əsas məqsədi miqrasiya sahəsində d&amp;ouml;vlət siyasətinin həyata ke&amp;ccedil;irilməsi, miqrasiya idarəetmə sisteminin inkişaf etdirilməsi, miqrasiya proseslərinin tənzimlənməsi və proqnozlaşdırılması, bu sahədə qanunvericiliyin beynəlxalq normalara və m&amp;uuml;asir d&amp;ouml;vr&amp;uuml;n tələblərinə uyğun təkmilləşdirilməsi, qanunların tətbiqinin effektivliyinin təmin edilməsi, miqrasiya sahəsində vahid məlumat bankının, m&amp;uuml;asir avtomatlaşdırılmış nəzarət sisteminin yaradılması, qeyri-qanuni miqrasiyanın qarşısının alınması, beynəlxalq əməkdaşlığın inkişaf etdirilməsi tədbirlərinin həyata ke&amp;ccedil;irilməsindən ibarətdir. Proqramda miqrasiya proseslərinin x&amp;uuml;susiyyətləri, d&amp;ouml;vlət siyasətinin prioritet istiqamətləri &amp;ouml;z əksini tapmışdır. Bununla yanaşı proqramın əhatə etdiyi d&amp;ouml;vr ərzində ayrı-ayrı d&amp;ouml;vlət qurumları ilə əlaqəli şəkildə həyata ke&amp;ccedil;irməli olduqları tədbirlər qeyd edilmişdir&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;D&amp;ouml;vlət Miqrasiya Xidməti haqqında&lt;/span&gt;&amp;nbsp;&lt;br /&gt;Təsdiq edilmiş &amp;ldquo;Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Proqramı&amp;rdquo; əsasında miqrasiya sahəsində vahid d&amp;ouml;vlət siyasətini həyata ke&amp;ccedil;irən x&amp;uuml;susi d&amp;ouml;vlət orqanının yaradılması zərurətini nəzərə alan Azərbaycan Respublikasının Prezidenti cənab İlham Əliyev 19 mart 2007-ci il tarixdə Azərbaycan Respublikası D&amp;ouml;vlət Miqrasiya Xidmətinin yaradılması haqqında 560 n&amp;ouml;mrəli Fərman imzalamışdır. Eyni zamanda, Fərmana əsasən Xidmətin Əsasnaməsi təsdiq edilmişdir. D&amp;ouml;vlət Miqrasiya Xidmətinin yaradıldığı vaxtdan miqrasiya proseslərinə d&amp;ouml;vlət nəzarətinin g&amp;uuml;cləndirilməsi məqsədi ilə zəruri normativ h&amp;uuml;quqi aktlar qəbul edilmiş, bir sıra institusional və təşkilati tədbirlər həyata ke&amp;ccedil;irilmişdir. Miqrasiya proseslərinin vahid və &amp;ccedil;evik prosedurlar əsasında tənzimlənməsi, sənədləşmənin sadələşdirilməsi məqsədilə Azərbaycan Respublikasının Prezidenti cənab İlham Əliyev tərəfindən &quot;Miqrasiya proseslərinin idarəolunmasında &quot;bir pəncərə&quot; prinsipinin tətbiqi haqqında&quot; 4 mart 2009-cu il tarixli 69 n&amp;ouml;mrəli Fərman imzalanmışdır. Adı&amp;ccedil;əkilən Fərmanın tətbiqi &amp;ouml;lkədə miqrasiya proseslərinin daha &amp;ccedil;evik və işlək mexanizmlər əsasında idarə edilməsinə, bu sahədə operativliyin təmin edilməsinə və həllini g&amp;ouml;zləyən problemlərin aradan qaldırılmasına səbəb olmuşdur. Fərmana əsasən, 2009-cu il iyulun 1-dən miqrasiya proseslərinin idarəolunmasında &amp;laquo;bir pəncərə&amp;raquo; prinsipinin tətbiq edilməsinə başlanılmış və bu prinsip &amp;uuml;zrə vahid d&amp;ouml;vlət orqanının səlahiyyətləri Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Xidmətinə həvalə edilmişdir. &amp;ldquo;Bir pəncərə&amp;rdquo; prinsipi &amp;ccedil;ər&amp;ccedil;ivəsində D&amp;ouml;vlət Miqrasiya Xidməti əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti və daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazələrin və m&amp;uuml;vafiq vəsiqələrin verilməsini, onların qeydiyyata alınmasını, Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılmasını, həm&amp;ccedil;inin &amp;ouml;lkə ərazisində haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq &amp;uuml;&amp;ccedil;&amp;uuml;n iş icazələrinin verilməsini həyata ke&amp;ccedil;irir. Bununla belə, Xidmət vətəndaşlıq məsələlərində iştirak edir və qa&amp;ccedil;qın statusunu m&amp;uuml;əyyənləşdirir. Qeyri-qanuni miqrasiyaya qarşı m&amp;uuml;barizə tədbirlərinin g&amp;uuml;cləndirilməsi, Xidmətin fəaliyyətinin təkmilləşdirilməsi məqsədi ilə Prezident cənab İlham Əliyevin 8 aprel 2009-cu il tarixdə imzaladığı 76 n&amp;ouml;mrəli Fərmanla D&amp;ouml;vlət Miqrasiya Xidmətinə h&amp;uuml;quq-m&amp;uuml;hafizə orqanı statusu verilmişdir.&lt;/p&gt;', 0, 1, 1, 3, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `elanlar`
--

CREATE TABLE `elanlar` (
  `id` int(3) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `elanlar`
--

INSERT INTO `elanlar` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, 'Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!', '', '', '', '', '&lt;p&gt;Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!&lt;/p&gt;', 0, 1, 1, 1, 1, 0),
(2, 'Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!', '', '', '', '', '&lt;p&gt;Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!&lt;/p&gt;', 0, 1, 1, 2, 1, 0),
(3, 'Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!', '', '', '', '', '&lt;p&gt;Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!&lt;/p&gt;', 0, 1, 1, 3, 1, 0),
(4, 'Dövlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş hüquq və azadlıqlar', '', '', '', '', '&lt;p&gt;D&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlar&lt;/p&gt;', 0, 2, 1, 1, 2, 0),
(5, 'Dövlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş hüquq və azadlıqlar', '', '', '', '', '&lt;p&gt;D&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlar&lt;/p&gt;', 0, 2, 1, 2, 2, 0),
(6, 'Dövlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş hüquq və azadlıqlar', '', '', '', '', '&lt;p&gt;D&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlar&lt;/p&gt;', 0, 2, 1, 3, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `elaqe`
--

CREATE TABLE `elaqe` (
  `id` int(11) NOT NULL,
  `text` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `vkontakte` varchar(255) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `digg` varchar(255) NOT NULL,
  `flickr` varchar(255) NOT NULL,
  `dribbble` varchar(255) NOT NULL,
  `vimeo` varchar(255) NOT NULL,
  `myspace` varchar(255) NOT NULL,
  `google` varchar(255) NOT NULL,
  `youtube` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `google_map` text NOT NULL,
  `address` varchar(255) NOT NULL,
  `footer` text NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `elaqe`
--

INSERT INTO `elaqe` (`id`, `text`, `email`, `facebook`, `twitter`, `vkontakte`, `linkedin`, `digg`, `flickr`, `dribbble`, `vimeo`, `myspace`, `google`, `youtube`, `instagram`, `phone`, `mobile`, `skype`, `fax`, `google_map`, `address`, `footer`, `lang_id`, `auto_id`) VALUES
(1, '&lt;p&gt;&lt;span&gt;&amp;Uuml;nvan:&lt;/span&gt;&lt;span&gt;&amp;nbsp;AZ1114, Bakı şəhəri, Binəqədi rayonu,3123-c&amp;uuml; məhəllə, Binəqədi şossesi 202.(Metronun &quot;Azadlıq Prospekti&quot; stansiyasının yaxınlığında)&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;&amp;Ccedil;ağrı Mərkəzi:&lt;/span&gt;&lt;span&gt;&amp;nbsp;(+994 12) 919&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;Xaricdən daxil olan zənglər &amp;uuml;&amp;ccedil;&amp;uuml;n:&lt;/span&gt;&lt;span&gt;&amp;nbsp;(+994 12) 565 61 18&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;Tel:&lt;/span&gt;&lt;span&gt;&amp;nbsp;(+994 12) 565 61 18; (+994 12) 565 61 19; (+994 12) 565 61 20; (+994 12) 565 61 21; (+994 12) 565 61 22&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;Faks:&lt;/span&gt;&lt;span&gt;&amp;nbsp;(+994 12) 562 37 02&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;Elektron &amp;uuml;nvan:&lt;/span&gt;&lt;span&gt;&amp;nbsp;&lt;/span&gt;&lt;a class=&quot;text-primary&quot; href=&quot;mailto:info@migration.gov.az&quot;&gt;info@migration.gov.az&lt;/a&gt;&lt;span&gt;&amp;nbsp;&lt;/span&gt;&lt;/p&gt;', 'info@fintrend.az', 'https://www.facebook.com/laennecofficial/', 'http://www.twitter.com', '', '', '', '', '', '', '', '', 'https://www.youtube.com/channel/laennec', 'https://www.instagram.com/laennec_aze', '+994 12 566 86 99', '', '', '+994 12 566 86 99', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3038.4867803087327!2d49.86614501539537!3d40.3980658793673!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40307d471b381f77%3A0x761d3fd7f40b1eb8!2sHasanoghlu%2C+Baku!5e0!3m2!1sen!2s!4v1523950294311', 'Həsənoğlu 13. AZ 1010, Baku, Azerbaijan', '', 1, 1),
(4, '&lt;p&gt;&amp;Uuml;nvan: AZ1114, Bakı şəhəri, Binəqədi rayonu,3123-c&amp;uuml; məhəllə, Binəqədi şossesi 202.(Metronun &quot;Azadlıq Prospekti&quot; stansiyasının yaxınlığında) &amp;Ccedil;ağrı Mərkəzi: (+994 12) 919 Xaricdən daxil olan zənglər &amp;uuml;&amp;ccedil;&amp;uuml;n: (+994 12) 565 61 18 Tel: (+994 12) 565 61 18; (+994 12) 565 61 19; (+994 12) 565 61 20; (+994 12) 565 61 21; (+994 12) 565 61 22 Faks: (+994 12) 562 37 02 Elektron &amp;uuml;nvan: info@migration.gov.az&lt;/p&gt;', 'info@fintrend.az', 'https://www.facebook.com/laennecofficial/', 'http://www.twitter.com', '', '', '', '', '', '', '', '', 'https://www.youtube.com/channel/laennec', 'https://www.instagram.com/laennec_aze', '+994 12 566 86 99', '', '', '', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3038.4867803087327!2d49.86614501539537!3d40.3980658793673!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40307d471b381f77%3A0x761d3fd7f40b1eb8!2sHasanoghlu%2C+Baku!5e0!3m2!1sen!2s!4v1523950294311', 'dsadsad', '', 2, 1),
(5, '&lt;p&gt;&amp;Uuml;nvan: AZ1114, Bakı şəhəri, Binəqədi rayonu,3123-c&amp;uuml; məhəllə, Binəqədi şossesi 202.(Metronun &quot;Azadlıq Prospekti&quot; stansiyasının yaxınlığında) &amp;Ccedil;ağrı Mərkəzi: (+994 12) 919 Xaricdən daxil olan zənglər &amp;uuml;&amp;ccedil;&amp;uuml;n: (+994 12) 565 61 18 Tel: (+994 12) 565 61 18; (+994 12) 565 61 19; (+994 12) 565 61 20; (+994 12) 565 61 21; (+994 12) 565 61 22 Faks: (+994 12) 562 37 02 Elektron &amp;uuml;nvan: info@migration.gov.az&lt;/p&gt;', 'info@fintrend.az', 'https://www.facebook.com/laennecofficial/', 'http://www.twitter.com', '', '', '', '', '', '', '', '', 'https://www.youtube.com/channel/laennec', 'https://www.instagram.com/laennec_aze', '+994 12 566 86 99', '', '', '', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3038.4867803087327!2d49.86614501539537!3d40.3980658793673!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40307d471b381f77%3A0x761d3fd7f40b1eb8!2sHasanoghlu%2C+Baku!5e0!3m2!1sen!2s!4v1523950294311', 'dsadsad', '', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `faydali`
--

CREATE TABLE `faydali` (
  `id` int(3) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faydali`
--

INSERT INTO `faydali` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, 'Arxiv elan', '', '', '', '', '', 0, 1, 1, 1, 1, 0),
(2, 'Arxiv elan', '', '', '', '', '', 0, 1, 1, 2, 1, 0),
(3, 'Arxiv elan', '', '', '', '', '', 0, 1, 1, 3, 1, 0),
(4, 'Cari statistika', '', '', '', '', '', 0, 2, 1, 1, 2, 0),
(5, 'Cari statistika', '', '', '', '', '', 0, 2, 1, 2, 2, 0),
(6, 'Cari statistika', '', '', '', '', '', 0, 2, 1, 3, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `faydali_alt`
--

CREATE TABLE `faydali_alt` (
  `id` int(11) NOT NULL,
  `basliq` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `act_id` int(11) NOT NULL,
  `tip` varchar(5) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faydali_alt`
--

INSERT INTO `faydali_alt` (`id`, `basliq`, `text`, `act_id`, `tip`, `sira`, `aktivlik`, `lang_id`, `auto_id`) VALUES
(1, 'QEYDİYYAT FORMASI', '&lt;p&gt;&lt;strong&gt;Miqrasiya Məktəbi&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;em&gt;1 noyabr 2018-ci il &amp;ndash; 30 may 2019-cu il&lt;/em&gt;&lt;/p&gt;\r\n&lt;table border=&quot;1&quot; cellspacing=&quot;0&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;Ad və soyad&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;Təhsil&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td rowspan=&quot;2&quot;&gt;\r\n&lt;p&gt;Əlaqə vasitələri&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;e-mail&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;telefon&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;Proqramda nə &amp;uuml;&amp;ccedil;&amp;uuml;n iştirak etmək istəyirsiniz? (maksimum &amp;ndash; 400 s&amp;ouml;z)&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;Miqrasiya ilə bağlı təlimlərdə iştirak etmisinizmi? İştirak etmişsinizsə, təlimlər barədə məlumat verin.&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;Digər qeydlər&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;p&gt;Qeydiyyat formasını tələb olunan sənədlərlə birlikdə 17 oktyabr 2018-ci il tarixədək&amp;nbsp;&lt;a href=&quot;mailto:migrationschool@migration.gov.az&quot;&gt;migrationschool@migration.gov.az&lt;/a&gt;&amp;nbsp;elektron po&amp;ccedil;t &amp;uuml;nvanına g&amp;ouml;ndərməyiniz xahiş olunur.&lt;/p&gt;', 1, '', 1, 1, 1, 1),
(2, 'QEYDİYYAT FORMASI', '&lt;p&gt;&lt;strong&gt;Miqrasiya Məktəbi&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;em&gt;1 noyabr 2018-ci il &amp;ndash; 30 may 2019-cu il&lt;/em&gt;&lt;/p&gt;\r\n&lt;table border=&quot;1&quot; cellspacing=&quot;0&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;Ad və soyad&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;Təhsil&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td rowspan=&quot;2&quot;&gt;\r\n&lt;p&gt;Əlaqə vasitələri&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;e-mail&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;telefon&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;Proqramda nə &amp;uuml;&amp;ccedil;&amp;uuml;n iştirak etmək istəyirsiniz? (maksimum &amp;ndash; 400 s&amp;ouml;z)&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;Miqrasiya ilə bağlı təlimlərdə iştirak etmisinizmi? İştirak etmişsinizsə, təlimlər barədə məlumat verin.&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;Digər qeydlər&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;p&gt;Qeydiyyat formasını tələb olunan sənədlərlə birlikdə 17 oktyabr 2018-ci il tarixədək&amp;nbsp;&lt;a href=&quot;mailto:migrationschool@migration.gov.az&quot;&gt;migrationschool@migration.gov.az&lt;/a&gt;&amp;nbsp;elektron po&amp;ccedil;t &amp;uuml;nvanına g&amp;ouml;ndərməyiniz xahiş olunur.&lt;/p&gt;', 1, '', 1, 1, 2, 1),
(3, 'QEYDİYYAT FORMASI', '&lt;p&gt;&lt;strong&gt;Miqrasiya Məktəbi&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;em&gt;1 noyabr 2018-ci il &amp;ndash; 30 may 2019-cu il&lt;/em&gt;&lt;/p&gt;\r\n&lt;table border=&quot;1&quot; cellspacing=&quot;0&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;Ad və soyad&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;Təhsil&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td rowspan=&quot;2&quot;&gt;\r\n&lt;p&gt;Əlaqə vasitələri&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;e-mail&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;telefon&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;Proqramda nə &amp;uuml;&amp;ccedil;&amp;uuml;n iştirak etmək istəyirsiniz? (maksimum &amp;ndash; 400 s&amp;ouml;z)&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;Miqrasiya ilə bağlı təlimlərdə iştirak etmisinizmi? İştirak etmişsinizsə, təlimlər barədə məlumat verin.&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;\r\n&lt;p&gt;Digər qeydlər&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;p&gt;Qeydiyyat formasını tələb olunan sənədlərlə birlikdə 17 oktyabr 2018-ci il tarixədək&amp;nbsp;&lt;a href=&quot;mailto:migrationschool@migration.gov.az&quot;&gt;migrationschool@migration.gov.az&lt;/a&gt;&amp;nbsp;elektron po&amp;ccedil;t &amp;uuml;nvanına g&amp;ouml;ndərməyiniz xahiş olunur.&lt;/p&gt;', 1, '', 1, 1, 3, 1),
(4, 'sds adasdas dasdasd asd as ', '&lt;p&gt;as a ada dasd as&lt;/p&gt;', 2, '', 1, 1, 1, 2),
(5, 'sds adasdas dasdasd asd as ', '&lt;p&gt;qweqwq qwe&lt;/p&gt;', 2, '', 1, 1, 2, 2),
(6, 'sds adasdas dasdasd asd as ', '&lt;p&gt;text_3&lt;/p&gt;', 2, '', 1, 1, 3, 2),
(7, 'sds adasdas dasdasd asd as intern', '&lt;p&gt;sds adasdas dasdasd asd as intern vsds adasdas dasdasd asd as intern&lt;/p&gt;', 2, '', 2, 1, 1, 3),
(8, 'sds adasdas dasdasd asd as intern', '&lt;p&gt;sds adasdas dasdasd asd as intern vsds adasdas dasdasd asd as intern&lt;/p&gt;', 2, '', 2, 1, 2, 3),
(9, 'sds adasdas dasdasd asd as intern', '&lt;p&gt;sds adasdas dasdasd asd as intern vsds adasdas dasdasd asd as intern&lt;/p&gt;', 2, '', 2, 1, 3, 3),
(10, 'sds adasdas dasdasd asd as trttr', '&lt;p&gt;sds adasdas dasdasd asd as trttr&amp;nbsp;sds adasdas dasdasd asd as trttr&lt;/p&gt;', 1, '', 2, 1, 1, 4),
(11, 'sds adasdas dasdasd asd as trttr', '&lt;p&gt;sds adasdas dasdasd asd as trttr&amp;nbsp;sds adasdas dasdasd asd as trttr&lt;/p&gt;', 1, '', 2, 1, 2, 4),
(12, 'sds adasdas dasdasd asd as trttr', '&lt;p&gt;sds adasdas dasdasd asd as trttr&amp;nbsp;sds adasdas dasdasd asd as trttr&lt;/p&gt;', 1, '', 2, 1, 3, 4);

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(10) NOT NULL,
  `tarix` int(11) DEFAULT NULL,
  `tip` varchar(5) NOT NULL,
  `albom_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `tarix`, `tip`, `albom_id`) VALUES
(19, 1523884362, 'jpg', 1),
(20, 1523884362, 'jpg', 1),
(21, 1523884362, 'jpg', 1),
(7, 1523884328, 'jpg', 1),
(8, 1523884328, 'jpg', 1),
(9, 1523884328, 'jpg', 1),
(10, 1523884328, 'jpg', 1),
(11, 1523884328, 'jpg', 1),
(12, 1523884328, 'jpg', 1),
(13, 1523884328, 'jpg', 1),
(14, 1523884328, 'jpg', 1),
(15, 1523884328, 'jpg', 1),
(16, 1523884328, 'jpg', 1),
(17, 1523884328, 'jpg', 1),
(18, 1523884328, 'jpg', 1),
(22, 1523884362, 'jpg', 1),
(23, 1523884362, 'jpg', 1),
(24, 1523884362, 'jpg', 1),
(25, 1523884362, 'jpg', 1),
(26, 1523884362, 'jpg', 1),
(27, 1523884362, 'jpg', 1),
(28, 1523884379, 'jpg', 1),
(29, 1523884379, 'jpg', 1),
(30, 1523884379, 'jpg', 1),
(31, 1523884379, 'jpg', 1),
(32, 1523884379, 'jpg', 1),
(33, 1523884379, 'png', 1),
(34, 1523957456, 'jpg', 1),
(35, 1523957456, 'jpg', 1),
(36, 1523957456, 'jpg', 1),
(37, 1523957456, 'jpg', 1),
(38, 1523957456, 'jpg', 1),
(39, 1523957456, 'jpg', 1),
(40, 1523957456, 'jpg', 1),
(41, 1523957456, 'jpg', 1),
(42, 1523957456, 'jpg', 1),
(43, 1523957473, 'jpg', 1),
(44, 1523957473, 'jpg', 1),
(45, 1523957473, 'jpg', 1),
(46, 1523957473, 'jpg', 1),
(47, 1523957473, 'jpg', 1),
(48, 1523957473, 'jpg', 1),
(49, 1523957473, 'jpg', 1),
(50, 1523957473, 'jpg', 1),
(51, 1523957473, 'jpg', 1),
(52, 1523957473, 'jpg', 1),
(53, 1523957473, 'jpg', 1),
(55, 1523957473, 'jpg', 1),
(56, 1523957473, 'jpg', 1),
(57, 1523957473, 'jpg', 1),
(58, 1523957473, 'jpg', 1),
(59, 1523957473, 'jpg', 1),
(60, 1537370581, 'jpeg', 2),
(61, 1537370581, 'png', 2),
(62, 1538299110, 'jpg', 3),
(63, 1538299110, 'jpg', 3),
(64, 1538299110, 'jpg', 3),
(65, 1538299110, 'png', 3);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_footer`
--

CREATE TABLE `gallery_footer` (
  `id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery_footer`
--

INSERT INTO `gallery_footer` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, '', '', '', '', 'jpg', '', 0, 1, 1, 1, 1, 0),
(2, '', '', '', '', 'jpg', '', 0, 1, 1, 2, 1, 0),
(3, '', '', '', '', 'jpg', '', 0, 1, 1, 3, 1, 0),
(4, '', '', '', '', 'jpg', '', 0, 2, 1, 1, 2, 0),
(5, '', '', '', '', 'jpg', '', 0, 2, 1, 2, 2, 0),
(6, '', '', '', '', 'jpg', '', 0, 2, 1, 3, 2, 0),
(7, '', '', '', '', 'jpg', '', 0, 3, 1, 1, 3, 0),
(8, '', '', '', '', 'jpg', '', 0, 3, 1, 2, 3, 0),
(9, '', '', '', '', 'jpg', '', 0, 3, 1, 3, 3, 0),
(10, '', '', '', '', 'jpg', '', 0, 4, 1, 1, 4, 0),
(11, '', '', '', '', 'jpg', '', 0, 4, 1, 2, 4, 0),
(12, '', '', '', '', 'jpg', '', 0, 4, 1, 3, 4, 0),
(13, '', '', '', '', 'jpg', '', 0, 5, 1, 1, 5, 0),
(14, '', '', '', '', 'jpg', '', 0, 5, 1, 2, 5, 0),
(15, '', '', '', '', 'jpg', '', 0, 5, 1, 3, 5, 0),
(16, '', '', '', '', 'jpg', '', 0, 6, 1, 1, 6, 0),
(17, '', '', '', '', 'jpg', '', 0, 6, 1, 2, 6, 0),
(18, '', '', '', '', 'jpg', '', 0, 6, 1, 3, 6, 0),
(19, '', '', '', '', 'jpg', '', 0, 7, 1, 1, 7, 0),
(20, '', '', '', '', 'jpg', '', 0, 7, 1, 2, 7, 0),
(21, '', '', '', '', 'jpg', '', 0, 7, 1, 3, 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `interviews`
--

CREATE TABLE `interviews` (
  `id` int(3) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` longtext NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `interviews`
--

INSERT INTO `interviews` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, 'Dövlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş hüquq və azadlıqlar', '', '', '', '', '&lt;p&gt;D&amp;ouml;vlət Miqrasiya Xidmətinin rəisi, II dərəcəli d&amp;ouml;vlət miqrasiya xidməti m&amp;uuml;şaviri Firudin Nəbiyevin Yeni Azərbaycan Partiyasının saytına (&lt;a href=&quot;http://yap.org.az/az/view/interview/349/dovlet-miqrasiya-xidmeti-fealiyyetini-insan-ve-vetendash-huquq-ve-azadliqlarina-hormet-qanunchuluq-ve-humanizm-prinsipleri-esasinda-heyata-kechirir&quot;&gt;www.yap.org.az&lt;/a&gt;) m&amp;uuml;sahibəsi&lt;/p&gt;\r\n&lt;p&gt;- Firudin m&amp;uuml;əllim, bir ne&amp;ccedil;ə g&amp;uuml;n əvvəl BMT Baş Assambleyasının &amp;ldquo;Qa&amp;ccedil;qın və miqrantların b&amp;ouml;y&amp;uuml;k axınının aradan qaldırılması&amp;rdquo; m&amp;ouml;vzusunda y&amp;uuml;ksək səviyyəli g&amp;ouml;r&amp;uuml;ş&amp;uuml;ndə iştirak etmisiniz. Bu barədə bir qədər ətraflı məlumat verməyinizi xahiş edirəm.&lt;/p&gt;\r\n&lt;p&gt;-&amp;nbsp;Plenar iclas və altı interaktiv dəyirmi masa formatında ke&amp;ccedil;irilən&amp;nbsp; y&amp;uuml;ksək səviyyəli g&amp;ouml;r&amp;uuml;şdə bir sıra d&amp;ouml;vlət və h&amp;ouml;kumət baş&amp;ccedil;ıları, nazirlər, elmi-tədqiqat mərkəzlərinin və vətəndaş cəmiyyətinin n&amp;uuml;mayəndələri iştirak edirdi. Tədbirdə b&amp;ouml;y&amp;uuml;k qa&amp;ccedil;qın axınlarının aradan qaldırılması, miqrasiya, x&amp;uuml;susilə b&amp;ouml;y&amp;uuml;k miqrant axınlarının səbəblərinin aradan qaldırılması və miqrantların verdiyi m&amp;uuml;sbət t&amp;ouml;hfələrin vurğulanması kimi vacib məsələlər m&amp;uuml;zakirə olundu.&lt;/p&gt;\r\n&lt;p&gt;BMT-nin Baş Assambleyası prezidentinin sədrlik etdiyi g&amp;ouml;r&amp;uuml;ş&amp;uuml;n ke&amp;ccedil;irilməsində məqsəd qeyd edilən sahədə daha humanist və əlaqələndirilmiş yanaşmanın formalaşdırılması &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;ouml;lkələrin bir araya gətirilməsi idi. &amp;ldquo;Qa&amp;ccedil;qın və miqrantların b&amp;ouml;y&amp;uuml;k axınının aradan qaldırılması&amp;rdquo; m&amp;ouml;vzusunda y&amp;uuml;ksək səviyyəli g&amp;ouml;r&amp;uuml;şdə və g&amp;ouml;r&amp;uuml;ş &amp;ccedil;ər&amp;ccedil;ivəsində &amp;ldquo;Qa&amp;ccedil;qın və miqrantlar, habelə k&amp;ouml;&amp;ccedil;&amp;uuml;r&amp;uuml;lmə ilə bağlı məsələlər &amp;uuml;zrə beynəlxalq tədbirlər və əməkdaşlıq: qabaqdakı yol&amp;rdquo; adlı dəyirmi masada tərəfimizdən bir sıra ciddi məsələlər qaldırıldı. Azərbaycanın məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;n məsələsinə verdiyi &amp;ouml;nəm və həssaslığın səbəbləri bu y&amp;uuml;ksək tribunadan bir daha səsləndirildi.&lt;/p&gt;\r\n&lt;p&gt;Məlum olduğu kimi Ermənistan tərəfindən ərazisinin işğalına g&amp;ouml;rə &amp;ouml;lkəmiz genişmiqyaslı məcburi k&amp;ouml;&amp;ccedil;&amp;uuml;r&amp;uuml;lmə problemi ilə &amp;uuml;zləşib. 9,7 milyon əhalisi olan Azərbaycan adambaşına d&amp;uuml;şən məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlərin sayına g&amp;ouml;rə d&amp;uuml;nyada ən b&amp;ouml;y&amp;uuml;k k&amp;ouml;&amp;ccedil;&amp;uuml;r&amp;uuml;lmə y&amp;uuml;k&amp;uuml;nə sahib &amp;ouml;lkələrdən biridir. 25 ildən artıqdır ki, Ermənistan beynəlxalq h&amp;uuml;ququn norma və prinsiplərini kobud şəkildə pozaraq &amp;ouml;z&amp;uuml;n&amp;uuml;n işğal&amp;ccedil;ılıq siyasətini, hazırkı status-kvonun m&amp;ouml;hkəmlənməsinə y&amp;ouml;nəlmiş səylərini davam etdirir və y&amp;uuml;z minlərlə azərbaycanlı məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;n&amp;uuml;n &amp;ouml;z doğma yurdlarına qayıdışını əngəlləyir. BMT Təhl&amp;uuml;kəsizlik Şurası tərəfindən işğal olunmuş ərazilərin Ermənistan silahlı q&amp;uuml;vvələrindən azad edilməsi ilə bağlı 1993-c&amp;uuml; ildə qəbul edilmiş 4 qətnamə təəss&amp;uuml;f ki, icra olunmur. Qeyd etmək lazımdır ki, azərbaycanlı qa&amp;ccedil;qın və məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlərin &amp;ouml;z torpaqlarına qayıtmaq imkanı verilməsinin zəruriliyi ilə bağlı beynəlxalq birliyin tələbinin yerinə yetirilməsi onların h&amp;uuml;quqlarının hərtərəfli təminatı &amp;uuml;&amp;ccedil;&amp;uuml;n olduqca zəruridir. Bundan başqa, təəss&amp;uuml;flə qeyd etməliyik ki, Ermənistan hazırkı qa&amp;ccedil;qın və miqrant b&amp;ouml;hranından sui-istifadə edərək, Suriyadan olan erməniləri Azərbaycanın işğal olunmuş ərazilərində məskunlaşdırır. Diqqətə &amp;ccedil;atdırıldı ki, bu beynəlxalq humanitar h&amp;uuml;ququn, ilk n&amp;ouml;vbədə 1949-cu il d&amp;ouml;rd&amp;uuml;nc&amp;uuml; Cenevrə Konvensiyasının və Əlavə Protokollarının kobud surətdə pozulmasıdır.&lt;/p&gt;\r\n&lt;p&gt;Bilirsiniz ki, &amp;Uuml;mummilli lider Heydər Əliyevin və hazırda m&amp;ouml;htərəm Prezidentimiz İlham Əliyevin rəhbərliyi ilə Azərbaycan &amp;ouml;z doğma yerlərindən qovulmuş məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlərin b&amp;uuml;t&amp;uuml;n problemlərini &amp;ouml;z &amp;uuml;zərinə g&amp;ouml;t&amp;uuml;r&amp;uuml;b. Azərbaycan h&amp;ouml;kuməti tərəfindən bu insanların məşğulluq, təhsil, yaşayış və tibbi, sosial təminatı ilə əlaqədar tədbirlər davamlı olaraq həyata ke&amp;ccedil;irilir. Belə ki, &amp;ouml;tən d&amp;ouml;vr ərzində Azərbaycan h&amp;ouml;kuməti tәrәfindәn məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlәr &amp;uuml;&amp;ccedil;&amp;uuml;n y&amp;uuml;zə yaxın m&amp;uuml;asir qәsәbә salınıb, minlərlə qa&amp;ccedil;qın və məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;n ailəsinin mənzil-məişət şəraiti yaxşılaşdırılıb. Son 20 il ərzində Azərbaycanda məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlərin sosial problemlərinin həllinə təxminən 6 milyard ABŞ dolları həcmində vəsait xərclənib. Azərbaycan h&amp;ouml;kumətinin məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlərin problemlərinin həll olunması sahəsində səyləri BMT-nin Qa&amp;ccedil;qınlar &amp;uuml;zrə Ali Komissarlığı, Beynəlxalq Miqrasiya Təşkilatı və digər təşkilatlar tərəfindən y&amp;uuml;ksək dəyərləndirilib. Bu fikirlər həmin tribunadan iştirak&amp;ccedil;ıların nəzərinə &amp;ccedil;atdırıldı.&lt;/p&gt;\r\n&lt;p&gt;-&amp;nbsp;Bəs&amp;nbsp;Azərbaycanda&amp;nbsp;miqrantlar və onların ailə &amp;uuml;zvlərinin h&amp;uuml;quqlarının qorunması ilə bağlı hansı işlər g&amp;ouml;r&amp;uuml;l&amp;uuml;r, hansı &amp;ouml;lkələrlə sazişlər imzalanıb və yaxud belə sazişlərin imzalanması g&amp;ouml;zlənilir?&lt;/p&gt;\r\n&lt;p&gt;- Qeyd edim ki, D&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarına h&amp;ouml;rmət, qanun&amp;ccedil;uluq və humanizm prinsipləri əsasında həyata ke&amp;ccedil;irir və hər zaman miqrantların h&amp;uuml;quqlarının qorunmasına x&amp;uuml;susi &amp;ouml;nəm verir. H&amp;uuml;quqlarını bilməyən, &amp;ouml;lkədə olmasını leqallaşdırmayan miqrantlar hər an insan alveri qurbanı olmaq təhl&amp;uuml;kəsi ilə qarşılaşa bilərlər. Bu zaman qanunun tələblərinə riayət etməyən miqrantların h&amp;uuml;quqlarını qorumaq m&amp;uuml;şk&amp;uuml;l bir məsələyə &amp;ccedil;evirilir. Məhz bu kimi xoşagəlməz hadisələrin baş verməməsi &amp;uuml;&amp;ccedil;&amp;uuml;n əcnəbilər D&amp;ouml;vlət Miqrasiya Xidmətinə, Xidmətin regional idarələrinə m&amp;uuml;raciət edib &amp;ouml;lkə ərazisində m&amp;uuml;vəqqəti olmalarını, yaşamalarını leqallaşdırmalı, fəaliyyətlərini qanuni əsaslarla həyata ke&amp;ccedil;irməli, bu sahədə m&amp;ouml;vcud olan qanunlarımızı bilməli və ona riayət etməlidirlər.&lt;/p&gt;\r\n&lt;p&gt;D&amp;ouml;vlət Miqrasiya Xidməti bununla əlaqədar əcnəbi və vətəndaşlığı olmayan şəxsləri, ictimaiyyəti maarifləndirmək və məlumatlandırmaq məqsədilə bir sıra əhəmiyyətli tədbirlər həyata ke&amp;ccedil;irir.&lt;/p&gt;\r\n&lt;p&gt;Miqrasiya Məcəlləsinin 54-c&amp;uuml; maddəsinə əsasən əcnəbilərin və vətəndaşlığı olmayan şəxslərin Azərbaycan Respublikasında daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verilməsi ilə bağlı m&amp;uuml;raciətlərinə baxılarkən, onların Azərbaycan Respublikasının qanunları ilə nəzərdə tutulmuş h&amp;uuml;quq və vəzifələri, habelə d&amp;ouml;vlət dili ilə bağlı biliyi D&amp;ouml;vlət Miqrasiya Xidməti, Ədliyyə Nazirliyi və Təhsil Nazirliyi n&amp;uuml;mayəndələrindən ibarət komissiya tərəfindən yoxlanılır. Azərbaycanın d&amp;ouml;vlət dilini bilmək miqrantların yerli əhali ilə &amp;uuml;nsiyyətinin asanlaşdırılması və cəmiyyətimizə inteqrasiyası baxımından əlbəttə &amp;ccedil;ox m&amp;uuml;h&amp;uuml;m amildir. Azərbaycan Respublikasında m&amp;uuml;vəqqəti və ya daimi yaşayan əcnəbilər və vətəndaşlığı olmayan şəxslər &amp;ouml;z arzuları ilə Azərbaycan dilini, tarixini, mədəniyyətini, &amp;ouml;z h&amp;uuml;quq və vəzifələri ilə bağlı qanunvericiliyi &amp;ouml;yrənmək &amp;uuml;&amp;ccedil;&amp;uuml;n D&amp;ouml;vlət Miqrasiya Xidmətinin təlim-tədris mərkəzinə m&amp;uuml;raciət edə bilərlər. Onlar &amp;uuml;&amp;ccedil;&amp;uuml;n m&amp;uuml;vafiq yer ayrılıb, x&amp;uuml;susi proqramlar hazırlanıb və kurslar təşkil edilib.&lt;/p&gt;\r\n&lt;p&gt;Bununla yanaşı, Azərbaycan miqrantların h&amp;uuml;quqlarının səmərəli m&amp;uuml;dafiəsinin təmin edilməsi, qanunsuz miqrasiyanın qarşısının alınması, miqrasiya proseslərinin idarəolunması və proqnozlaşdırılması məqsədilə qabaqcıl təcr&amp;uuml;bənin &amp;ouml;yrənilməsi və tətbiqi sahəsində beynəlxalq əməkdaşlığa x&amp;uuml;susi &amp;ouml;nəm verir.&lt;/p&gt;\r\n&lt;p&gt;Miqrantların və onların ailə &amp;uuml;zvlərinin h&amp;uuml;quqlarının m&amp;uuml;dafiəsinin təmin edilməsi məqsədilə Azərbaycan Respublikası bir sıra &amp;ouml;lkələrlə ikitərəfli və &amp;ccedil;oxtərəfli sazişlər imzalayıb. Belə ki, miqrasiya sahəsində əməkdaşlığa dair T&amp;uuml;rkiyə, Moldova, Qazaxıstan, Qırğızıstan, Ukrayna və Belarus ilə ikitərəfli sazişlər m&amp;ouml;vcuddur. Hazırda bir ne&amp;ccedil;ə &amp;ouml;lkə ilə də belə sazişlərin imzalanması g&amp;ouml;zlənilir. Miqrantların, o c&amp;uuml;mlədən digər &amp;ouml;lkələrdə miqrant həyatı yaşayan Azərbaycan Respublikası vətəndaşlarının h&amp;uuml;quqlarının m&amp;uuml;dafiəsi, sosial m&amp;uuml;dafiəsi, işə d&amp;uuml;zəlmə prosedurlarının asanlaşdırılması bu sazişlərin əsasını təşkil edir.&lt;/p&gt;\r\n&lt;p&gt;Bundan əlavə, &amp;ouml;lkəmiz Avropa İttifaqı və Norve&amp;ccedil; ilə imzaladığı readmissiya sazişləri &amp;ccedil;ər&amp;ccedil;ivəsində qanunsuz vəziyyətdə olan miqrantların geri qaytarılmasını həyata ke&amp;ccedil;irməklə, onların insan h&amp;uuml;quqlarının m&amp;uuml;dafiəsini də təmin edir. Bir sıra digər &amp;ouml;lkələrlə də belə sazişlərin imzalanması istiqamətində işlər davam etdirilir.&lt;/p&gt;\r\n&lt;p&gt;- Əcnəbilərin və vətəndaşlığı olmayan şəxslərin dəqiq u&amp;ccedil;otu necə aparılır?&lt;/p&gt;\r\n&lt;p&gt;- Әsası ulu &amp;ouml;ndәr Heydәr Әliyev tәrәfindәn qoyulmuş inkişaf strategiyasına uyğun olaraq iqtisadi tərəqqi və demokratikləşmə xəttini milli inkişafın dəyişməz formulu kimi &amp;ouml;nə &amp;ccedil;əkən Prezident İlham Əliyev respublikamızın qlobal siyasi arenada m&amp;uuml;sbət imiclə tanınmasını, regional və beynəlxalq iqtisadi əməkdaşlıq mərkəzinə &amp;ccedil;evrilməsini təmin edib. Azərbaycan Respublikasında m&amp;uuml;şahidə olunan s&amp;uuml;rətli sosial-iqtisadi inkişaf, miqrantların h&amp;uuml;quq və azadlıqlarının qorunması sahəsində g&amp;ouml;r&amp;uuml;lən işlər, &amp;ouml;lkəmizdəki siyasi sabitlik, həm&amp;ccedil;inin &amp;ouml;lkəmizin geosiyasi m&amp;ouml;vqeyi d&amp;uuml;nyanın m&amp;uuml;xtəlif yerlərindən Azərbaycana miqrasiya edənlərin sayının artmasına səbəb olub. Azərbaycan Respublikasında yaşayan, işləyən və m&amp;uuml;vəqqəti olan əcnəbilərin və vətəndaşlığı olmayan şəxslərin dəqiq u&amp;ccedil;otunun aparılması, miqrasiya proseslərinin idarə olunmasında iştirak edən d&amp;ouml;vlət orqanlarının zəruri informasiya ilə təmin edilməsi, miqrasiya ilə bağlı sənədləşmə, yoxlama, sorğu və təhlil işlərinin avtomatlaşdırılması və bu sahədə g&amp;ouml;stərilən elektron xidmətlərin təkmilləşdirilməsi məqsədilə D&amp;ouml;vlət Miqrasiya Xidmətinin nəzdində Vahid Miqrasiya Məlumat Sistemi (VMMS) yaradılıb. VMMS &amp;ouml;lkədə baş verən miqrasiya proseslərinin dinamikası barədə tam təsəvv&amp;uuml;r&amp;uuml;n yaradılmasına imkan verərək, qeyri-qanuni miqrasiya ilə m&amp;uuml;barizə və təhl&amp;uuml;kəsizliyin təmini sahəsində m&amp;uuml;vafiq tədbirlərin g&amp;ouml;r&amp;uuml;lməsi &amp;uuml;&amp;ccedil;&amp;uuml;n əlverişli şərait yaradıb.&lt;/p&gt;\r\n&lt;p&gt;- Son d&amp;ouml;vrlər D&amp;ouml;vlət Miqrasiya Xidməti tərəfindən vətəndaşsızlıq hallarının aradan qaldırılması ilə əlaqədar genişmiqyaslı tədbirlərin həyata ke&amp;ccedil;irildiyi ilə bağlı məlumatlar verilir. Nə kimi tədbirlər g&amp;ouml;r&amp;uuml;l&amp;uuml;r?&lt;/p&gt;\r\n&lt;p&gt;-&amp;nbsp;Vətəndaşsızlıq halları ilə m&amp;uuml;barizə sahəsində məlumatlılığın artırılması və bu şəxslərin h&amp;uuml;quqi statusunun m&amp;uuml;əyyənləşdirilməsi zamanı yaranan problemlərin, o c&amp;uuml;mlədən onların h&amp;uuml;quqlarının səmərəli m&amp;uuml;dafiəsi naminə əməkdaşlığın g&amp;uuml;cləndirilməsi məqsədi ilə Azərbaycan Respublikası BMT-nin Qa&amp;ccedil;qınlar &amp;uuml;zrə Ali Komissarlığının &amp;ldquo;10 ilə vətəndaşsızlığa son&amp;rdquo; kampaniyasına qoşulub.&amp;nbsp; Vətəndaşsızlıq hallarının aradan qaldırılması istiqamətində konkret işlər g&amp;ouml;r&amp;uuml;l&amp;uuml;r. D&amp;ouml;vlət Miqrasiya Xidməti tərəfindən respublikanın ayrı-ayrı b&amp;ouml;lgələrində vətəndaşsızlıqla bağlı ke&amp;ccedil;irilən &amp;ccedil;oxsaylı maarifləndirmə tədbirləri, g&amp;ouml;r&amp;uuml;şlər də &amp;ouml;z bəhrəsini verir. Belə ki, istər vətəndaşlıq mənsubiyyətinin m&amp;uuml;əyyən olunması, istərsə də vətəndaşlığa qəbul və bərpa ilə bağlı m&amp;uuml;raciətlərin sayı bunu deməyə əsas verir.&lt;/p&gt;\r\n&lt;p&gt;Azәrbaycan Prezidentinin m&amp;uuml;vafiq sәrәncamları ilә 2008&amp;shy;-ci ildәn indiyәdәk 1682 nәfәr Azәrbaycan vәtәndaşlığına qәbul və bərpa edilib. Bununla belә, bu m&amp;uuml;ddәt әrzindә 61 mindən &amp;ccedil;ox şәxsin Azәrbaycan Respublikası vәtәndaşlığına mәnsubiyyәti tanınıb vә onlar m&amp;uuml;vafiq d&amp;ouml;vlәt qurumu tәrәfindәn Azәrbaycan Respublikası vәtәndaşının şәxsiyyәtini tәsdiq edәn sәnәdlәrlә tәmin edilib.&lt;/p&gt;\r\n&lt;p&gt;- Hansı hallarda şəxsin Azərbaycan Respublikasının vətəndaşlığına mənsubiyyəti tanınır?&lt;/p&gt;\r\n&lt;p&gt;- Azərbaycan Respublikası Nazirlər Kabinetinin 2015-ci il 18 mart tarixli&amp;nbsp; 84 n&amp;ouml;mrəli qərarı ilə təsdiq edilmiş &amp;ldquo;Şəxsin Azərbaycan Respublikasının vətəndaşlığına mənsubiyyətinin m&amp;uuml;əyyənləşdirilməsi Qaydası&amp;rdquo;na əsasən aşağıdakı hallardan hər hansı biri olduqda, şəxsin Azərbaycan Respublikasının vətəndaşlığına mənsubiyyəti tanınır:&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&amp;middot;&amp;nbsp;&amp;nbsp; &amp;ldquo;Azərbaycan Respublikasının vətəndaşlığı haqqında&amp;rdquo; Azərbaycan Respublikası Qanununun q&amp;uuml;vvəyə mindiyi g&amp;uuml;nədək (1998-ci il oktyabrın 7-dək) Azərbaycan Respublikasının (və ya Azərbaycan SSR-in) vətəndaşlığında olmuş şəxslər həmin tarixədək Azərbaycan Respublikasında yaşayış yeri &amp;uuml;zrə daimi qeydiyyatda olduqda;&lt;/p&gt;\r\n&lt;p&gt;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; 1988-ci il yanvarın 1-dən 1992-ci il yanvarın 1-dək Azərbaycan Respublikasının ərazisində məskunlaşmış qa&amp;ccedil;qın olduqda;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Azərbaycan Respublikasının ərazisində olan, hər iki valideyni naməlum olan uşaq olduqda;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Azərbaycan Respublikasının ərazisində doğulmuş aşağıdakı şəxslərdən biri olduqda:&lt;/p&gt;\r\n&lt;p&gt;1) doğulduğu anda valideynlərindən biri və ya hər ikisi Azərbaycan Respublikasının vətəndaşı olmuşdursa;&lt;/p&gt;\r\n&lt;p&gt;2)&amp;nbsp;&amp;nbsp; doğulduğu anda hər iki valideyni vətəndaşlığı olmayan şəxs olmuşdursa;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&amp;middot; Azərbaycan Respublikasının h&amp;uuml;dudlarından kənarda Azərbaycan Respublikasının vətəndaşından (vətəndaşlarından) doğulmuş şəxs;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp; &amp;middot;&amp;nbsp;&amp;nbsp; Əcnəbi və ya vətəndaşlığı olmayan uşağı &amp;ouml;vladlığa g&amp;ouml;t&amp;uuml;rən ər-arvadın hər ikisi Azərbaycan Respublikasının vətəndaşıdırsa, həmin uşaq;&lt;/p&gt;\r\n&lt;p&gt;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Əcnəbi və ya vətəndaşlığı olmayan uşağı &amp;ouml;vladlığa g&amp;ouml;t&amp;uuml;rən ər-arvaddan biri Azərbaycan Respublikasının vətəndaşıdırsa, digəri isə vətəndaşlığı olmayan şəxsdirsə, həmin uşaq;&lt;/p&gt;\r\n&lt;p&gt;&amp;middot;&amp;nbsp;&amp;nbsp; Valideynlərin vətəndaşlığı dəyişdikdə və bunun nəticəsində hər ikisi Azərbaycan Respublikasının vətəndaşlığını əldə etdikdə, onların uşağı;&lt;/p&gt;\r\n&lt;p&gt;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Uşağın valideynlərindən biri məlumdursa, həmin valideyn Azərbaycan Respublikasının vətəndaşlığını əldə etdikdə, onun uşağı;&lt;/p&gt;\r\n&lt;p&gt;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Azərbaycan Respublikasının ərazisində yaşayan uşağın valideynlərindən biri Azərbaycan Respublikasının vətəndaşlığını əldə etmiş şəxs, digəri isə vətəndaşlığı olmayan şəxs olduqda;&lt;/p&gt;\r\n&lt;p&gt;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Valideynlərdən birinin Azərbaycan Respublikasının vətəndaşlığına xitam verilərsə, digəri isə Azərbaycan Respublikasının vətəndaşlığında qalarsa, onların uşağı.&lt;/p&gt;\r\n&lt;p&gt;Bundan başqa, &amp;ldquo;Azərbaycan Respublikasının vətəndaşlığı haqqında&amp;rdquo; Azərbaycan Respublikası Qanununun q&amp;uuml;vvəyə mindiyi g&amp;uuml;nədək (1998-ci il oktyabrın 7-dək) Azərbaycan Respublikasının (və ya Azərbaycan SSR-in) vətəndaşlığında olmuş və Azərbaycan Respublikasının ərazisini tərk etməyən şəxslərin bu Qanun q&amp;uuml;vvəyə mindiyi g&amp;uuml;nədək Azərbaycan Respublikasında yaşayış yeri &amp;uuml;zrə qeydiyyatda olmamaları onların vətəndaşsızlığına səbəb olduqda, həmin şəxslərin Azərbaycan vətəndaşlığına mənsubiyyətləri tanınır.&lt;/p&gt;\r\n&lt;p&gt;- Bəs əcnəbilər və vətəndaşlığı olmayan şəxslər Azərbaycan Respublikasının vətəndaşlığına hansı hallarda qəbul edilə bilərlər?&lt;/p&gt;\r\n&lt;p&gt;-&amp;nbsp;&amp;ldquo;Azərbaycan Respublikasının vətəndaşlığı haqqında&amp;rdquo; Azərbaycan Respublikasının Qanununa əsasən Azərbaycan Respublikasının ərazisində son beş il ərzində fasiləsiz olaraq qanuni əsaslarla daimi yaşayan, qanuni gəlir mənbəyi olan, Azərbaycan Respublikasının Konstitusiyasına və qanunlarına riayət olunması barədə &amp;ouml;hdəlik g&amp;ouml;t&amp;uuml;rən, habelə Azərbaycan Respublikasının d&amp;ouml;vlət dilini bilməsi haqqında sənəd təqdim edən əcnəbi və vətəndaşlığı olmayan şəxs mənşəyindən, irqi və milli mənsubiyyətindən, cinsindən, təhsilindən, dinə m&amp;uuml;nasibətindən, siyasi və başqa əqidələrindən asılı olmayaraq bu qanuna m&amp;uuml;vafiq surətdə &amp;ouml;z vəsatəti ilə Azərbaycan Respublikasının vətəndaşlığına qəbul edilə bilər.&lt;/p&gt;\r\n&lt;p&gt;Həm&amp;ccedil;inin aşağıdakı hallarda əcnəbilər və vətəndaşlığı olmayan şəxslər qeyd olunan m&amp;uuml;ddət nəzərə alınmadan Azərbaycan Respublikasının vətəndaşlığına qəbul edilə bilərlər:&lt;/p&gt;\r\n&lt;p&gt;1) şəxsin elm, texnika, mədəniyyət və ya idman sahələrində y&amp;uuml;ksək nailiyyətləri olduqda;&lt;/p&gt;\r\n&lt;p&gt;2) şəxs Azərbaycan Respublikası &amp;uuml;&amp;ccedil;&amp;uuml;n x&amp;uuml;susi maraq kəsb etdikdə və digər m&amp;uuml;stəsna hallarda.&lt;/p&gt;\r\n&lt;p&gt;Eləcə də, şəxsin Azərbaycan Respublikasının qarşısında x&amp;uuml;susi xidmətləri olduqda o, qeyd olunan şərtlər nəzərə alınmadan Azərbaycan Respublikasının vətəndaşlığına qəbul edilə bilər.&lt;/p&gt;\r\n&lt;p&gt;Bundan başqa, əvvəllər Azərbaycan Respublikasının vətəndaşı olmuş və ya Azərbaycan Respublikası vətəndaşlığına xitam verilmiş şəxs &amp;ouml;z vəsatəti əsasında Azərbaycan Respublikası vətəndaşlığına bərpa edilə bilər.&lt;/p&gt;\r\n&lt;p&gt;Bir məsələni də qeyd edim ki, &amp;ldquo;Azərbaycan Respublikasının vətəndaşlığı haqqında&amp;rdquo; qanuna 2014-c&amp;uuml; il 30 may tarixli dəyişikliklər əsasında yeni redaksiyada verilmiş qanunun 10-cu maddəsində g&amp;ouml;stərilir ki, Azərbaycan Respublikasının vətəndaşı olan şəxsin ikili vətəndaşlığı olduqda həmin şəxsin xarici d&amp;ouml;vlətin vətəndaşlığına mənsubiyyəti, Azərbaycan Respublikasının beynəlxalq m&amp;uuml;qavilələrində nəzərdə tutulmuş və ya Konstitusiyanın 109-cu maddəsinin 32-ci bəndinə m&amp;uuml;vafiq surətdə həll edilmiş hallar istisna olmaqla tanınmır. Və xarici d&amp;ouml;vlətin vətəndaşlığını qəbul etmiş Azərbaycan Respublikasının vətəndaşı bir ay m&amp;uuml;ddətində bu barədə Azərbaycan ərazisində olduqda D&amp;ouml;vlət Miqrasiya Xidmətinə, &amp;ouml;lkə h&amp;uuml;dudlarından kənarda olduqda isə Azərbaycan Respublikasının diplomatik n&amp;uuml;mayəndəlikləri və ya konsulluq idarələri vasitəsilə Xarici İşlər Nazirliyinə yazılı məlumat verməlidir. Əks halda Cinayət Məcəlləsinin 318-2-ci&amp;nbsp; maddəsinə&amp;nbsp; əsasən, həmin vətəndaşlar&amp;nbsp; 3 min manatdan 5 min manata qədər cərimə və ya 360 saatdan 480 saatadək ictimai işlər ilə&amp;nbsp;cəzalandırılırlar.&lt;/p&gt;\r\n&lt;p&gt;- Miqrasiaya prosesinin idarə olunmasını, tənzimlənməsini həyata ke&amp;ccedil;irən bir orqan kimi Sizin rəhbərlik etdiyiniz Xidmət həm də prosesə d&amp;ouml;vlət nəzarətini də həyata ke&amp;ccedil;irir.&amp;nbsp; D&amp;ouml;vlət nəzarəti işinin təşkili haqqında nə deyə bilərsiniz?&lt;/p&gt;\r\n&lt;p&gt;-&amp;nbsp;Azərbaycanda miqrantların rahatlıqlarını təmin etmək &amp;uuml;&amp;ccedil;&amp;uuml;n Prezident İlham Əliyevin imzaladığı fərmana əsasən, 2009-cu il iyulun 1-dən miqrasiya proseslərinin idarəolunmasında &amp;laquo;bir pəncərə&amp;raquo; prinsipi &amp;uuml;zrə vahid d&amp;ouml;vlət orqanının səlahiyyətlərini D&amp;ouml;vlət Miqrasiya Xidməti həyata ke&amp;ccedil;irir. Respublikada əcnəbilərin və vətəndaşlığı olmayan şəxslərin olduğu yer &amp;uuml;zrə qeydiyyatı, m&amp;uuml;vəqqəti olma, m&amp;uuml;vəqqəti və daimi yaşama, iş icazələrinin verilməsi və m&amp;uuml;ddətlərinin uzadılması, qa&amp;ccedil;qın statusunun m&amp;uuml;əyyənləşdirilməsi, vətəndaşlığa qəbul, xitam, bərpa, vətəndaşlıq mənsubiyyətinin m&amp;uuml;əyyən olunması, eləcə də vətəndaşlıq məsələlərinin həlli ilə məşğul olan h&amp;uuml;quq-m&amp;uuml;hafizə orqanı status&amp;shy;unu daşıyan Xidmət həm də miqrasiya qanunvericiliyinin tətbiqində yol verilə biləcək n&amp;ouml;q&amp;shy;san və sui-istifadə&amp;shy;lə&amp;shy;rin, miq&amp;shy;ra&amp;shy;siya proses&amp;shy;lərinin neqa&amp;shy;tiv təsirinin aradan qal&amp;shy;dı&amp;shy;rılmasını, qeyri-qanuni miqrasiyaya qarşı effektiv m&amp;uuml;barizə tədbir&amp;shy;ləri&amp;shy;ni həyata ke&amp;ccedil;irir.&lt;/p&gt;\r\n&lt;p&gt;Belə ki, əcnəbi və vətəndaşlığı olmayan şəxs əvvəllər Azərbaycan Respublikasında olarkən &amp;ouml;lkəyə gəlişinin bəyan edilmiş məqsədlərini pozduqda, Azərbaycan Respublikasına gəlməsi haqqında vəsatət qaldırarkən, &amp;ouml;z&amp;uuml; və ya səfərinin məqsədi haqqında yalan məlumat verdikdə, yaxud miqrasiya qanunvericiliyini pozduğuna g&amp;ouml;rə son &amp;uuml;&amp;ccedil; il ərzində iki dəfə və ya daha &amp;ccedil;ox inzibati məsuliyyətə cəlb olunduqda onların Azərbaycan Respublikasına gəlişinə 5 il m&amp;uuml;ddətinə qadağa qoyulur.&lt;/p&gt;\r\n&lt;p&gt;Xatırladım ki, Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti yaşamağa icazə alınması &amp;uuml;&amp;ccedil;&amp;uuml;n əsas olmuş hal aradan qalxdıqda və icazə alınması ilə bağlı digər əsaslar olmadıqda əcnəbilərə və vətəndaşlığı olmayan şəxslərə &amp;ouml;lkəmizin ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verilmir, əvvəl verilmiş icazə isə ləğv edilir. Həm&amp;ccedil;inin Miqrasiya Məcəlləsinin tələbinə g&amp;ouml;rə&amp;nbsp; əcnəbi 180 g&amp;uuml;n ərzində 90 g&amp;uuml;ndən &amp;ccedil;ox Azərbaycan Respublikasının ərazisindən kənarda olduqda, yaxud &amp;ouml;lkəmizdə m&amp;uuml;vəqqəti yaşadığı d&amp;ouml;vrdə&amp;nbsp; gəlişinin bəyan edilmiş məqsədini pozduqda da ona m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verilmir, yaxud əvvəl verilmiş icazə ləğv edilir.&lt;/p&gt;\r\n&lt;p&gt;Bundan əlavə &amp;ouml;lkə ərazisində qanunsuz yaşayan, eləcə də qa&amp;ccedil;qın statusu almaq niyyətində olan şəxslər h&amp;uuml;quqi statusları m&amp;uuml;əyyənləşənədək qanunda g&amp;ouml;stərilən m&amp;uuml;ddətə kimi D&amp;ouml;vlət Miqrasiya Xidmətinin Bakı və Yevlax şəhərlərində beynəlxalq standartlara uyğun olan Qanunsuz Miqrantların Saxlanılması Mərkəzlərində yerləşdirilir, qanunam&amp;uuml;vafiq tədbirlər g&amp;ouml;r&amp;uuml;l&amp;uuml;r, m&amp;uuml;vafiq qərarlar qəbul olunur.&lt;/p&gt;\r\n&lt;p&gt;Miqrasiya Məcəlləsində Azərbaycan Respublikasının ərazisində olmalarının arzuolunmaz hesab edilməsi proseduru m&amp;uuml;əyyən edilmişdir. Bir daha onu da xatırladım ki, əcnəbilərin və vətəndaşlığı olmayan şəxslərin arzuolunmaz hesab edilməsinin 5 il m&amp;uuml;ddətinə m&amp;uuml;əyyən edilməsi, m&amp;uuml;vafiq əsaslar aradan qalxmadıqda eyni m&amp;uuml;ddətə uzadıla bilməsi və həmin şəxslər barəsində Azərbaycan Respublikasının h&amp;uuml;dudlarından kənara &amp;ccedil;ıxarılması haqqında qərar qəbul edilməsi miqrasiya orqanları tərəfindən həyata ke&amp;ccedil;irilir. Əlbəttə, deyə bilmərik ki, bu g&amp;uuml;n respublikamızda m&amp;uuml;vəqqəti olma və yaxud &amp;ouml;lkədə m&amp;uuml;vəqqəti və daimi yaşama qaydalarını pozan, etibarsız sənədlərlə yaşayan, qanunsuz əmək fəaliyyəti ilə məşğul olan qeyri-leqal miqrantlar yoxdur. D&amp;uuml;nyanın b&amp;uuml;t&amp;uuml;n &amp;ouml;lkələrində qanunsuz miqrasiya halları var. Əsas məsələ bu arzuolunmaz halın qarşısını almaq və qanunsuz miqrasiyaya qarşı ciddi m&amp;uuml;barizə aparmaqdır. D&amp;ouml;vlət Miqrasiya Xidməti bu işin &amp;ouml;hdəsindən uğurla gəlir və bu barədə məlumatlar m&amp;uuml;təmadi olaraq ictimaiyyətə a&amp;ccedil;ıqlanır.&lt;/p&gt;\r\n&lt;p&gt;F&amp;uuml;rsətdən istifadə edib bir daha &amp;ouml;lkəmizə təşrif buyurmuş əcnəbilərə və vətəndaşlığı olmayan şəxslərə m&amp;uuml;raciət edirəm ki, onlar qanunvericiliyin tələbinə uyğun olaraq &amp;ouml;lkədə olmaq və yaşamaqlarını leqallaşdırsınlar m&amp;uuml;əyyən olunmuş m&amp;uuml;ddətdə qeydiyyata d&amp;uuml;şs&amp;uuml;nlər. &amp;Uuml;mumiyyətlə əcnəbi və ya vətəndaşlığı olmayan şəxs olduğu yeri dəyişdikdə, o, DMX-nə bu barədə m&amp;uuml;tləq məlumat verməli və yeni olduğu yer &amp;uuml;zrə qeydiyyata alınmalıdır.&amp;nbsp;&lt;/p&gt;', 0, 1, 1, 1, 1, 0);
INSERT INTO `interviews` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(2, 'Dövlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş hüquq və azadlıqlar', '', '', '', '', '&lt;p&gt;D&amp;ouml;vlət Miqrasiya Xidmətinin rəisi, II dərəcəli d&amp;ouml;vlət miqrasiya xidməti m&amp;uuml;şaviri Firudin Nəbiyevin Yeni Azərbaycan Partiyasının saytına (&lt;a href=&quot;http://yap.org.az/az/view/interview/349/dovlet-miqrasiya-xidmeti-fealiyyetini-insan-ve-vetendash-huquq-ve-azadliqlarina-hormet-qanunchuluq-ve-humanizm-prinsipleri-esasinda-heyata-kechirir&quot;&gt;www.yap.org.az&lt;/a&gt;) m&amp;uuml;sahibəsi&lt;/p&gt;\r\n&lt;p&gt;- Firudin m&amp;uuml;əllim, bir ne&amp;ccedil;ə g&amp;uuml;n əvvəl BMT Baş Assambleyasının &amp;ldquo;Qa&amp;ccedil;qın və miqrantların b&amp;ouml;y&amp;uuml;k axınının aradan qaldırılması&amp;rdquo; m&amp;ouml;vzusunda y&amp;uuml;ksək səviyyəli g&amp;ouml;r&amp;uuml;ş&amp;uuml;ndə iştirak etmisiniz. Bu barədə bir qədər ətraflı məlumat verməyinizi xahiş edirəm.&lt;/p&gt;\r\n&lt;p&gt;-&amp;nbsp;Plenar iclas və altı interaktiv dəyirmi masa formatında ke&amp;ccedil;irilən&amp;nbsp; y&amp;uuml;ksək səviyyəli g&amp;ouml;r&amp;uuml;şdə bir sıra d&amp;ouml;vlət və h&amp;ouml;kumət baş&amp;ccedil;ıları, nazirlər, elmi-tədqiqat mərkəzlərinin və vətəndaş cəmiyyətinin n&amp;uuml;mayəndələri iştirak edirdi. Tədbirdə b&amp;ouml;y&amp;uuml;k qa&amp;ccedil;qın axınlarının aradan qaldırılması, miqrasiya, x&amp;uuml;susilə b&amp;ouml;y&amp;uuml;k miqrant axınlarının səbəblərinin aradan qaldırılması və miqrantların verdiyi m&amp;uuml;sbət t&amp;ouml;hfələrin vurğulanması kimi vacib məsələlər m&amp;uuml;zakirə olundu.&lt;/p&gt;\r\n&lt;p&gt;BMT-nin Baş Assambleyası prezidentinin sədrlik etdiyi g&amp;ouml;r&amp;uuml;ş&amp;uuml;n ke&amp;ccedil;irilməsində məqsəd qeyd edilən sahədə daha humanist və əlaqələndirilmiş yanaşmanın formalaşdırılması &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;ouml;lkələrin bir araya gətirilməsi idi. &amp;ldquo;Qa&amp;ccedil;qın və miqrantların b&amp;ouml;y&amp;uuml;k axınının aradan qaldırılması&amp;rdquo; m&amp;ouml;vzusunda y&amp;uuml;ksək səviyyəli g&amp;ouml;r&amp;uuml;şdə və g&amp;ouml;r&amp;uuml;ş &amp;ccedil;ər&amp;ccedil;ivəsində &amp;ldquo;Qa&amp;ccedil;qın və miqrantlar, habelə k&amp;ouml;&amp;ccedil;&amp;uuml;r&amp;uuml;lmə ilə bağlı məsələlər &amp;uuml;zrə beynəlxalq tədbirlər və əməkdaşlıq: qabaqdakı yol&amp;rdquo; adlı dəyirmi masada tərəfimizdən bir sıra ciddi məsələlər qaldırıldı. Azərbaycanın məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;n məsələsinə verdiyi &amp;ouml;nəm və həssaslığın səbəbləri bu y&amp;uuml;ksək tribunadan bir daha səsləndirildi.&lt;/p&gt;\r\n&lt;p&gt;Məlum olduğu kimi Ermənistan tərəfindən ərazisinin işğalına g&amp;ouml;rə &amp;ouml;lkəmiz genişmiqyaslı məcburi k&amp;ouml;&amp;ccedil;&amp;uuml;r&amp;uuml;lmə problemi ilə &amp;uuml;zləşib. 9,7 milyon əhalisi olan Azərbaycan adambaşına d&amp;uuml;şən məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlərin sayına g&amp;ouml;rə d&amp;uuml;nyada ən b&amp;ouml;y&amp;uuml;k k&amp;ouml;&amp;ccedil;&amp;uuml;r&amp;uuml;lmə y&amp;uuml;k&amp;uuml;nə sahib &amp;ouml;lkələrdən biridir. 25 ildən artıqdır ki, Ermənistan beynəlxalq h&amp;uuml;ququn norma və prinsiplərini kobud şəkildə pozaraq &amp;ouml;z&amp;uuml;n&amp;uuml;n işğal&amp;ccedil;ılıq siyasətini, hazırkı status-kvonun m&amp;ouml;hkəmlənməsinə y&amp;ouml;nəlmiş səylərini davam etdirir və y&amp;uuml;z minlərlə azərbaycanlı məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;n&amp;uuml;n &amp;ouml;z doğma yurdlarına qayıdışını əngəlləyir. BMT Təhl&amp;uuml;kəsizlik Şurası tərəfindən işğal olunmuş ərazilərin Ermənistan silahlı q&amp;uuml;vvələrindən azad edilməsi ilə bağlı 1993-c&amp;uuml; ildə qəbul edilmiş 4 qətnamə təəss&amp;uuml;f ki, icra olunmur. Qeyd etmək lazımdır ki, azərbaycanlı qa&amp;ccedil;qın və məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlərin &amp;ouml;z torpaqlarına qayıtmaq imkanı verilməsinin zəruriliyi ilə bağlı beynəlxalq birliyin tələbinin yerinə yetirilməsi onların h&amp;uuml;quqlarının hərtərəfli təminatı &amp;uuml;&amp;ccedil;&amp;uuml;n olduqca zəruridir. Bundan başqa, təəss&amp;uuml;flə qeyd etməliyik ki, Ermənistan hazırkı qa&amp;ccedil;qın və miqrant b&amp;ouml;hranından sui-istifadə edərək, Suriyadan olan erməniləri Azərbaycanın işğal olunmuş ərazilərində məskunlaşdırır. Diqqətə &amp;ccedil;atdırıldı ki, bu beynəlxalq humanitar h&amp;uuml;ququn, ilk n&amp;ouml;vbədə 1949-cu il d&amp;ouml;rd&amp;uuml;nc&amp;uuml; Cenevrə Konvensiyasının və Əlavə Protokollarının kobud surətdə pozulmasıdır.&lt;/p&gt;\r\n&lt;p&gt;Bilirsiniz ki, &amp;Uuml;mummilli lider Heydər Əliyevin və hazırda m&amp;ouml;htərəm Prezidentimiz İlham Əliyevin rəhbərliyi ilə Azərbaycan &amp;ouml;z doğma yerlərindən qovulmuş məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlərin b&amp;uuml;t&amp;uuml;n problemlərini &amp;ouml;z &amp;uuml;zərinə g&amp;ouml;t&amp;uuml;r&amp;uuml;b. Azərbaycan h&amp;ouml;kuməti tərəfindən bu insanların məşğulluq, təhsil, yaşayış və tibbi, sosial təminatı ilə əlaqədar tədbirlər davamlı olaraq həyata ke&amp;ccedil;irilir. Belə ki, &amp;ouml;tən d&amp;ouml;vr ərzində Azərbaycan h&amp;ouml;kuməti tәrәfindәn məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlәr &amp;uuml;&amp;ccedil;&amp;uuml;n y&amp;uuml;zə yaxın m&amp;uuml;asir qәsәbә salınıb, minlərlə qa&amp;ccedil;qın və məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;n ailəsinin mənzil-məişət şəraiti yaxşılaşdırılıb. Son 20 il ərzində Azərbaycanda məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlərin sosial problemlərinin həllinə təxminən 6 milyard ABŞ dolları həcmində vəsait xərclənib. Azərbaycan h&amp;ouml;kumətinin məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlərin problemlərinin həll olunması sahəsində səyləri BMT-nin Qa&amp;ccedil;qınlar &amp;uuml;zrə Ali Komissarlığı, Beynəlxalq Miqrasiya Təşkilatı və digər təşkilatlar tərəfindən y&amp;uuml;ksək dəyərləndirilib. Bu fikirlər həmin tribunadan iştirak&amp;ccedil;ıların nəzərinə &amp;ccedil;atdırıldı.&lt;/p&gt;\r\n&lt;p&gt;-&amp;nbsp;Bəs&amp;nbsp;Azərbaycanda&amp;nbsp;miqrantlar və onların ailə &amp;uuml;zvlərinin h&amp;uuml;quqlarının qorunması ilə bağlı hansı işlər g&amp;ouml;r&amp;uuml;l&amp;uuml;r, hansı &amp;ouml;lkələrlə sazişlər imzalanıb və yaxud belə sazişlərin imzalanması g&amp;ouml;zlənilir?&lt;/p&gt;\r\n&lt;p&gt;- Qeyd edim ki, D&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarına h&amp;ouml;rmət, qanun&amp;ccedil;uluq və humanizm prinsipləri əsasında həyata ke&amp;ccedil;irir və hər zaman miqrantların h&amp;uuml;quqlarının qorunmasına x&amp;uuml;susi &amp;ouml;nəm verir. H&amp;uuml;quqlarını bilməyən, &amp;ouml;lkədə olmasını leqallaşdırmayan miqrantlar hər an insan alveri qurbanı olmaq təhl&amp;uuml;kəsi ilə qarşılaşa bilərlər. Bu zaman qanunun tələblərinə riayət etməyən miqrantların h&amp;uuml;quqlarını qorumaq m&amp;uuml;şk&amp;uuml;l bir məsələyə &amp;ccedil;evirilir. Məhz bu kimi xoşagəlməz hadisələrin baş verməməsi &amp;uuml;&amp;ccedil;&amp;uuml;n əcnəbilər D&amp;ouml;vlət Miqrasiya Xidmətinə, Xidmətin regional idarələrinə m&amp;uuml;raciət edib &amp;ouml;lkə ərazisində m&amp;uuml;vəqqəti olmalarını, yaşamalarını leqallaşdırmalı, fəaliyyətlərini qanuni əsaslarla həyata ke&amp;ccedil;irməli, bu sahədə m&amp;ouml;vcud olan qanunlarımızı bilməli və ona riayət etməlidirlər.&lt;/p&gt;\r\n&lt;p&gt;D&amp;ouml;vlət Miqrasiya Xidməti bununla əlaqədar əcnəbi və vətəndaşlığı olmayan şəxsləri, ictimaiyyəti maarifləndirmək və məlumatlandırmaq məqsədilə bir sıra əhəmiyyətli tədbirlər həyata ke&amp;ccedil;irir.&lt;/p&gt;\r\n&lt;p&gt;Miqrasiya Məcəlləsinin 54-c&amp;uuml; maddəsinə əsasən əcnəbilərin və vətəndaşlığı olmayan şəxslərin Azərbaycan Respublikasında daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verilməsi ilə bağlı m&amp;uuml;raciətlərinə baxılarkən, onların Azərbaycan Respublikasının qanunları ilə nəzərdə tutulmuş h&amp;uuml;quq və vəzifələri, habelə d&amp;ouml;vlət dili ilə bağlı biliyi D&amp;ouml;vlət Miqrasiya Xidməti, Ədliyyə Nazirliyi və Təhsil Nazirliyi n&amp;uuml;mayəndələrindən ibarət komissiya tərəfindən yoxlanılır. Azərbaycanın d&amp;ouml;vlət dilini bilmək miqrantların yerli əhali ilə &amp;uuml;nsiyyətinin asanlaşdırılması və cəmiyyətimizə inteqrasiyası baxımından əlbəttə &amp;ccedil;ox m&amp;uuml;h&amp;uuml;m amildir. Azərbaycan Respublikasında m&amp;uuml;vəqqəti və ya daimi yaşayan əcnəbilər və vətəndaşlığı olmayan şəxslər &amp;ouml;z arzuları ilə Azərbaycan dilini, tarixini, mədəniyyətini, &amp;ouml;z h&amp;uuml;quq və vəzifələri ilə bağlı qanunvericiliyi &amp;ouml;yrənmək &amp;uuml;&amp;ccedil;&amp;uuml;n D&amp;ouml;vlət Miqrasiya Xidmətinin təlim-tədris mərkəzinə m&amp;uuml;raciət edə bilərlər. Onlar &amp;uuml;&amp;ccedil;&amp;uuml;n m&amp;uuml;vafiq yer ayrılıb, x&amp;uuml;susi proqramlar hazırlanıb və kurslar təşkil edilib.&lt;/p&gt;\r\n&lt;p&gt;Bununla yanaşı, Azərbaycan miqrantların h&amp;uuml;quqlarının səmərəli m&amp;uuml;dafiəsinin təmin edilməsi, qanunsuz miqrasiyanın qarşısının alınması, miqrasiya proseslərinin idarəolunması və proqnozlaşdırılması məqsədilə qabaqcıl təcr&amp;uuml;bənin &amp;ouml;yrənilməsi və tətbiqi sahəsində beynəlxalq əməkdaşlığa x&amp;uuml;susi &amp;ouml;nəm verir.&lt;/p&gt;\r\n&lt;p&gt;Miqrantların və onların ailə &amp;uuml;zvlərinin h&amp;uuml;quqlarının m&amp;uuml;dafiəsinin təmin edilməsi məqsədilə Azərbaycan Respublikası bir sıra &amp;ouml;lkələrlə ikitərəfli və &amp;ccedil;oxtərəfli sazişlər imzalayıb. Belə ki, miqrasiya sahəsində əməkdaşlığa dair T&amp;uuml;rkiyə, Moldova, Qazaxıstan, Qırğızıstan, Ukrayna və Belarus ilə ikitərəfli sazişlər m&amp;ouml;vcuddur. Hazırda bir ne&amp;ccedil;ə &amp;ouml;lkə ilə də belə sazişlərin imzalanması g&amp;ouml;zlənilir. Miqrantların, o c&amp;uuml;mlədən digər &amp;ouml;lkələrdə miqrant həyatı yaşayan Azərbaycan Respublikası vətəndaşlarının h&amp;uuml;quqlarının m&amp;uuml;dafiəsi, sosial m&amp;uuml;dafiəsi, işə d&amp;uuml;zəlmə prosedurlarının asanlaşdırılması bu sazişlərin əsasını təşkil edir.&lt;/p&gt;\r\n&lt;p&gt;Bundan əlavə, &amp;ouml;lkəmiz Avropa İttifaqı və Norve&amp;ccedil; ilə imzaladığı readmissiya sazişləri &amp;ccedil;ər&amp;ccedil;ivəsində qanunsuz vəziyyətdə olan miqrantların geri qaytarılmasını həyata ke&amp;ccedil;irməklə, onların insan h&amp;uuml;quqlarının m&amp;uuml;dafiəsini də təmin edir. Bir sıra digər &amp;ouml;lkələrlə də belə sazişlərin imzalanması istiqamətində işlər davam etdirilir.&lt;/p&gt;\r\n&lt;p&gt;- Əcnəbilərin və vətəndaşlığı olmayan şəxslərin dəqiq u&amp;ccedil;otu necə aparılır?&lt;/p&gt;\r\n&lt;p&gt;- Әsası ulu &amp;ouml;ndәr Heydәr Әliyev tәrәfindәn qoyulmuş inkişaf strategiyasına uyğun olaraq iqtisadi tərəqqi və demokratikləşmə xəttini milli inkişafın dəyişməz formulu kimi &amp;ouml;nə &amp;ccedil;əkən Prezident İlham Əliyev respublikamızın qlobal siyasi arenada m&amp;uuml;sbət imiclə tanınmasını, regional və beynəlxalq iqtisadi əməkdaşlıq mərkəzinə &amp;ccedil;evrilməsini təmin edib. Azərbaycan Respublikasında m&amp;uuml;şahidə olunan s&amp;uuml;rətli sosial-iqtisadi inkişaf, miqrantların h&amp;uuml;quq və azadlıqlarının qorunması sahəsində g&amp;ouml;r&amp;uuml;lən işlər, &amp;ouml;lkəmizdəki siyasi sabitlik, həm&amp;ccedil;inin &amp;ouml;lkəmizin geosiyasi m&amp;ouml;vqeyi d&amp;uuml;nyanın m&amp;uuml;xtəlif yerlərindən Azərbaycana miqrasiya edənlərin sayının artmasına səbəb olub. Azərbaycan Respublikasında yaşayan, işləyən və m&amp;uuml;vəqqəti olan əcnəbilərin və vətəndaşlığı olmayan şəxslərin dəqiq u&amp;ccedil;otunun aparılması, miqrasiya proseslərinin idarə olunmasında iştirak edən d&amp;ouml;vlət orqanlarının zəruri informasiya ilə təmin edilməsi, miqrasiya ilə bağlı sənədləşmə, yoxlama, sorğu və təhlil işlərinin avtomatlaşdırılması və bu sahədə g&amp;ouml;stərilən elektron xidmətlərin təkmilləşdirilməsi məqsədilə D&amp;ouml;vlət Miqrasiya Xidmətinin nəzdində Vahid Miqrasiya Məlumat Sistemi (VMMS) yaradılıb. VMMS &amp;ouml;lkədə baş verən miqrasiya proseslərinin dinamikası barədə tam təsəvv&amp;uuml;r&amp;uuml;n yaradılmasına imkan verərək, qeyri-qanuni miqrasiya ilə m&amp;uuml;barizə və təhl&amp;uuml;kəsizliyin təmini sahəsində m&amp;uuml;vafiq tədbirlərin g&amp;ouml;r&amp;uuml;lməsi &amp;uuml;&amp;ccedil;&amp;uuml;n əlverişli şərait yaradıb.&lt;/p&gt;\r\n&lt;p&gt;- Son d&amp;ouml;vrlər D&amp;ouml;vlət Miqrasiya Xidməti tərəfindən vətəndaşsızlıq hallarının aradan qaldırılması ilə əlaqədar genişmiqyaslı tədbirlərin həyata ke&amp;ccedil;irildiyi ilə bağlı məlumatlar verilir. Nə kimi tədbirlər g&amp;ouml;r&amp;uuml;l&amp;uuml;r?&lt;/p&gt;\r\n&lt;p&gt;-&amp;nbsp;Vətəndaşsızlıq halları ilə m&amp;uuml;barizə sahəsində məlumatlılığın artırılması və bu şəxslərin h&amp;uuml;quqi statusunun m&amp;uuml;əyyənləşdirilməsi zamanı yaranan problemlərin, o c&amp;uuml;mlədən onların h&amp;uuml;quqlarının səmərəli m&amp;uuml;dafiəsi naminə əməkdaşlığın g&amp;uuml;cləndirilməsi məqsədi ilə Azərbaycan Respublikası BMT-nin Qa&amp;ccedil;qınlar &amp;uuml;zrə Ali Komissarlığının &amp;ldquo;10 ilə vətəndaşsızlığa son&amp;rdquo; kampaniyasına qoşulub.&amp;nbsp; Vətəndaşsızlıq hallarının aradan qaldırılması istiqamətində konkret işlər g&amp;ouml;r&amp;uuml;l&amp;uuml;r. D&amp;ouml;vlət Miqrasiya Xidməti tərəfindən respublikanın ayrı-ayrı b&amp;ouml;lgələrində vətəndaşsızlıqla bağlı ke&amp;ccedil;irilən &amp;ccedil;oxsaylı maarifləndirmə tədbirləri, g&amp;ouml;r&amp;uuml;şlər də &amp;ouml;z bəhrəsini verir. Belə ki, istər vətəndaşlıq mənsubiyyətinin m&amp;uuml;əyyən olunması, istərsə də vətəndaşlığa qəbul və bərpa ilə bağlı m&amp;uuml;raciətlərin sayı bunu deməyə əsas verir.&lt;/p&gt;\r\n&lt;p&gt;Azәrbaycan Prezidentinin m&amp;uuml;vafiq sәrәncamları ilә 2008&amp;shy;-ci ildәn indiyәdәk 1682 nәfәr Azәrbaycan vәtәndaşlığına qәbul və bərpa edilib. Bununla belә, bu m&amp;uuml;ddәt әrzindә 61 mindən &amp;ccedil;ox şәxsin Azәrbaycan Respublikası vәtәndaşlığına mәnsubiyyәti tanınıb vә onlar m&amp;uuml;vafiq d&amp;ouml;vlәt qurumu tәrәfindәn Azәrbaycan Respublikası vәtәndaşının şәxsiyyәtini tәsdiq edәn sәnәdlәrlә tәmin edilib.&lt;/p&gt;\r\n&lt;p&gt;- Hansı hallarda şəxsin Azərbaycan Respublikasının vətəndaşlığına mənsubiyyəti tanınır?&lt;/p&gt;\r\n&lt;p&gt;- Azərbaycan Respublikası Nazirlər Kabinetinin 2015-ci il 18 mart tarixli&amp;nbsp; 84 n&amp;ouml;mrəli qərarı ilə təsdiq edilmiş &amp;ldquo;Şəxsin Azərbaycan Respublikasının vətəndaşlığına mənsubiyyətinin m&amp;uuml;əyyənləşdirilməsi Qaydası&amp;rdquo;na əsasən aşağıdakı hallardan hər hansı biri olduqda, şəxsin Azərbaycan Respublikasının vətəndaşlığına mənsubiyyəti tanınır:&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&amp;middot;&amp;nbsp;&amp;nbsp; &amp;ldquo;Azərbaycan Respublikasının vətəndaşlığı haqqında&amp;rdquo; Azərbaycan Respublikası Qanununun q&amp;uuml;vvəyə mindiyi g&amp;uuml;nədək (1998-ci il oktyabrın 7-dək) Azərbaycan Respublikasının (və ya Azərbaycan SSR-in) vətəndaşlığında olmuş şəxslər həmin tarixədək Azərbaycan Respublikasında yaşayış yeri &amp;uuml;zrə daimi qeydiyyatda olduqda;&lt;/p&gt;\r\n&lt;p&gt;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; 1988-ci il yanvarın 1-dən 1992-ci il yanvarın 1-dək Azərbaycan Respublikasının ərazisində məskunlaşmış qa&amp;ccedil;qın olduqda;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Azərbaycan Respublikasının ərazisində olan, hər iki valideyni naməlum olan uşaq olduqda;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Azərbaycan Respublikasının ərazisində doğulmuş aşağıdakı şəxslərdən biri olduqda:&lt;/p&gt;\r\n&lt;p&gt;1) doğulduğu anda valideynlərindən biri və ya hər ikisi Azərbaycan Respublikasının vətəndaşı olmuşdursa;&lt;/p&gt;\r\n&lt;p&gt;2)&amp;nbsp;&amp;nbsp; doğulduğu anda hər iki valideyni vətəndaşlığı olmayan şəxs olmuşdursa;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&amp;middot; Azərbaycan Respublikasının h&amp;uuml;dudlarından kənarda Azərbaycan Respublikasının vətəndaşından (vətəndaşlarından) doğulmuş şəxs;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp; &amp;middot;&amp;nbsp;&amp;nbsp; Əcnəbi və ya vətəndaşlığı olmayan uşağı &amp;ouml;vladlığa g&amp;ouml;t&amp;uuml;rən ər-arvadın hər ikisi Azərbaycan Respublikasının vətəndaşıdırsa, həmin uşaq;&lt;/p&gt;\r\n&lt;p&gt;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Əcnəbi və ya vətəndaşlığı olmayan uşağı &amp;ouml;vladlığa g&amp;ouml;t&amp;uuml;rən ər-arvaddan biri Azərbaycan Respublikasının vətəndaşıdırsa, digəri isə vətəndaşlığı olmayan şəxsdirsə, həmin uşaq;&lt;/p&gt;\r\n&lt;p&gt;&amp;middot;&amp;nbsp;&amp;nbsp; Valideynlərin vətəndaşlığı dəyişdikdə və bunun nəticəsində hər ikisi Azərbaycan Respublikasının vətəndaşlığını əldə etdikdə, onların uşağı;&lt;/p&gt;\r\n&lt;p&gt;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Uşağın valideynlərindən biri məlumdursa, həmin valideyn Azərbaycan Respublikasının vətəndaşlığını əldə etdikdə, onun uşağı;&lt;/p&gt;\r\n&lt;p&gt;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Azərbaycan Respublikasının ərazisində yaşayan uşağın valideynlərindən biri Azərbaycan Respublikasının vətəndaşlığını əldə etmiş şəxs, digəri isə vətəndaşlığı olmayan şəxs olduqda;&lt;/p&gt;\r\n&lt;p&gt;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Valideynlərdən birinin Azərbaycan Respublikasının vətəndaşlığına xitam verilərsə, digəri isə Azərbaycan Respublikasının vətəndaşlığında qalarsa, onların uşağı.&lt;/p&gt;\r\n&lt;p&gt;Bundan başqa, &amp;ldquo;Azərbaycan Respublikasının vətəndaşlığı haqqında&amp;rdquo; Azərbaycan Respublikası Qanununun q&amp;uuml;vvəyə mindiyi g&amp;uuml;nədək (1998-ci il oktyabrın 7-dək) Azərbaycan Respublikasının (və ya Azərbaycan SSR-in) vətəndaşlığında olmuş və Azərbaycan Respublikasının ərazisini tərk etməyən şəxslərin bu Qanun q&amp;uuml;vvəyə mindiyi g&amp;uuml;nədək Azərbaycan Respublikasında yaşayış yeri &amp;uuml;zrə qeydiyyatda olmamaları onların vətəndaşsızlığına səbəb olduqda, həmin şəxslərin Azərbaycan vətəndaşlığına mənsubiyyətləri tanınır.&lt;/p&gt;\r\n&lt;p&gt;- Bəs əcnəbilər və vətəndaşlığı olmayan şəxslər Azərbaycan Respublikasının vətəndaşlığına hansı hallarda qəbul edilə bilərlər?&lt;/p&gt;\r\n&lt;p&gt;-&amp;nbsp;&amp;ldquo;Azərbaycan Respublikasının vətəndaşlığı haqqında&amp;rdquo; Azərbaycan Respublikasının Qanununa əsasən Azərbaycan Respublikasının ərazisində son beş il ərzində fasiləsiz olaraq qanuni əsaslarla daimi yaşayan, qanuni gəlir mənbəyi olan, Azərbaycan Respublikasının Konstitusiyasına və qanunlarına riayət olunması barədə &amp;ouml;hdəlik g&amp;ouml;t&amp;uuml;rən, habelə Azərbaycan Respublikasının d&amp;ouml;vlət dilini bilməsi haqqında sənəd təqdim edən əcnəbi və vətəndaşlığı olmayan şəxs mənşəyindən, irqi və milli mənsubiyyətindən, cinsindən, təhsilindən, dinə m&amp;uuml;nasibətindən, siyasi və başqa əqidələrindən asılı olmayaraq bu qanuna m&amp;uuml;vafiq surətdə &amp;ouml;z vəsatəti ilə Azərbaycan Respublikasının vətəndaşlığına qəbul edilə bilər.&lt;/p&gt;\r\n&lt;p&gt;Həm&amp;ccedil;inin aşağıdakı hallarda əcnəbilər və vətəndaşlığı olmayan şəxslər qeyd olunan m&amp;uuml;ddət nəzərə alınmadan Azərbaycan Respublikasının vətəndaşlığına qəbul edilə bilərlər:&lt;/p&gt;\r\n&lt;p&gt;1) şəxsin elm, texnika, mədəniyyət və ya idman sahələrində y&amp;uuml;ksək nailiyyətləri olduqda;&lt;/p&gt;\r\n&lt;p&gt;2) şəxs Azərbaycan Respublikası &amp;uuml;&amp;ccedil;&amp;uuml;n x&amp;uuml;susi maraq kəsb etdikdə və digər m&amp;uuml;stəsna hallarda.&lt;/p&gt;\r\n&lt;p&gt;Eləcə də, şəxsin Azərbaycan Respublikasının qarşısında x&amp;uuml;susi xidmətləri olduqda o, qeyd olunan şərtlər nəzərə alınmadan Azərbaycan Respublikasının vətəndaşlığına qəbul edilə bilər.&lt;/p&gt;\r\n&lt;p&gt;Bundan başqa, əvvəllər Azərbaycan Respublikasının vətəndaşı olmuş və ya Azərbaycan Respublikası vətəndaşlığına xitam verilmiş şəxs &amp;ouml;z vəsatəti əsasında Azərbaycan Respublikası vətəndaşlığına bərpa edilə bilər.&lt;/p&gt;\r\n&lt;p&gt;Bir məsələni də qeyd edim ki, &amp;ldquo;Azərbaycan Respublikasının vətəndaşlığı haqqında&amp;rdquo; qanuna 2014-c&amp;uuml; il 30 may tarixli dəyişikliklər əsasında yeni redaksiyada verilmiş qanunun 10-cu maddəsində g&amp;ouml;stərilir ki, Azərbaycan Respublikasının vətəndaşı olan şəxsin ikili vətəndaşlığı olduqda həmin şəxsin xarici d&amp;ouml;vlətin vətəndaşlığına mənsubiyyəti, Azərbaycan Respublikasının beynəlxalq m&amp;uuml;qavilələrində nəzərdə tutulmuş və ya Konstitusiyanın 109-cu maddəsinin 32-ci bəndinə m&amp;uuml;vafiq surətdə həll edilmiş hallar istisna olmaqla tanınmır. Və xarici d&amp;ouml;vlətin vətəndaşlığını qəbul etmiş Azərbaycan Respublikasının vətəndaşı bir ay m&amp;uuml;ddətində bu barədə Azərbaycan ərazisində olduqda D&amp;ouml;vlət Miqrasiya Xidmətinə, &amp;ouml;lkə h&amp;uuml;dudlarından kənarda olduqda isə Azərbaycan Respublikasının diplomatik n&amp;uuml;mayəndəlikləri və ya konsulluq idarələri vasitəsilə Xarici İşlər Nazirliyinə yazılı məlumat verməlidir. Əks halda Cinayət Məcəlləsinin 318-2-ci&amp;nbsp; maddəsinə&amp;nbsp; əsasən, həmin vətəndaşlar&amp;nbsp; 3 min manatdan 5 min manata qədər cərimə və ya 360 saatdan 480 saatadək ictimai işlər ilə&amp;nbsp;cəzalandırılırlar.&lt;/p&gt;\r\n&lt;p&gt;- Miqrasiaya prosesinin idarə olunmasını, tənzimlənməsini həyata ke&amp;ccedil;irən bir orqan kimi Sizin rəhbərlik etdiyiniz Xidmət həm də prosesə d&amp;ouml;vlət nəzarətini də həyata ke&amp;ccedil;irir.&amp;nbsp; D&amp;ouml;vlət nəzarəti işinin təşkili haqqında nə deyə bilərsiniz?&lt;/p&gt;\r\n&lt;p&gt;-&amp;nbsp;Azərbaycanda miqrantların rahatlıqlarını təmin etmək &amp;uuml;&amp;ccedil;&amp;uuml;n Prezident İlham Əliyevin imzaladığı fərmana əsasən, 2009-cu il iyulun 1-dən miqrasiya proseslərinin idarəolunmasında &amp;laquo;bir pəncərə&amp;raquo; prinsipi &amp;uuml;zrə vahid d&amp;ouml;vlət orqanının səlahiyyətlərini D&amp;ouml;vlət Miqrasiya Xidməti həyata ke&amp;ccedil;irir. Respublikada əcnəbilərin və vətəndaşlığı olmayan şəxslərin olduğu yer &amp;uuml;zrə qeydiyyatı, m&amp;uuml;vəqqəti olma, m&amp;uuml;vəqqəti və daimi yaşama, iş icazələrinin verilməsi və m&amp;uuml;ddətlərinin uzadılması, qa&amp;ccedil;qın statusunun m&amp;uuml;əyyənləşdirilməsi, vətəndaşlığa qəbul, xitam, bərpa, vətəndaşlıq mənsubiyyətinin m&amp;uuml;əyyən olunması, eləcə də vətəndaşlıq məsələlərinin həlli ilə məşğul olan h&amp;uuml;quq-m&amp;uuml;hafizə orqanı status&amp;shy;unu daşıyan Xidmət həm də miqrasiya qanunvericiliyinin tətbiqində yol verilə biləcək n&amp;ouml;q&amp;shy;san və sui-istifadə&amp;shy;lə&amp;shy;rin, miq&amp;shy;ra&amp;shy;siya proses&amp;shy;lərinin neqa&amp;shy;tiv təsirinin aradan qal&amp;shy;dı&amp;shy;rılmasını, qeyri-qanuni miqrasiyaya qarşı effektiv m&amp;uuml;barizə tədbir&amp;shy;ləri&amp;shy;ni həyata ke&amp;ccedil;irir.&lt;/p&gt;\r\n&lt;p&gt;Belə ki, əcnəbi və vətəndaşlığı olmayan şəxs əvvəllər Azərbaycan Respublikasında olarkən &amp;ouml;lkəyə gəlişinin bəyan edilmiş məqsədlərini pozduqda, Azərbaycan Respublikasına gəlməsi haqqında vəsatət qaldırarkən, &amp;ouml;z&amp;uuml; və ya səfərinin məqsədi haqqında yalan məlumat verdikdə, yaxud miqrasiya qanunvericiliyini pozduğuna g&amp;ouml;rə son &amp;uuml;&amp;ccedil; il ərzində iki dəfə və ya daha &amp;ccedil;ox inzibati məsuliyyətə cəlb olunduqda onların Azərbaycan Respublikasına gəlişinə 5 il m&amp;uuml;ddətinə qadağa qoyulur.&lt;/p&gt;\r\n&lt;p&gt;Xatırladım ki, Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti yaşamağa icazə alınması &amp;uuml;&amp;ccedil;&amp;uuml;n əsas olmuş hal aradan qalxdıqda və icazə alınması ilə bağlı digər əsaslar olmadıqda əcnəbilərə və vətəndaşlığı olmayan şəxslərə &amp;ouml;lkəmizin ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verilmir, əvvəl verilmiş icazə isə ləğv edilir. Həm&amp;ccedil;inin Miqrasiya Məcəlləsinin tələbinə g&amp;ouml;rə&amp;nbsp; əcnəbi 180 g&amp;uuml;n ərzində 90 g&amp;uuml;ndən &amp;ccedil;ox Azərbaycan Respublikasının ərazisindən kənarda olduqda, yaxud &amp;ouml;lkəmizdə m&amp;uuml;vəqqəti yaşadığı d&amp;ouml;vrdə&amp;nbsp; gəlişinin bəyan edilmiş məqsədini pozduqda da ona m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verilmir, yaxud əvvəl verilmiş icazə ləğv edilir.&lt;/p&gt;\r\n&lt;p&gt;Bundan əlavə &amp;ouml;lkə ərazisində qanunsuz yaşayan, eləcə də qa&amp;ccedil;qın statusu almaq niyyətində olan şəxslər h&amp;uuml;quqi statusları m&amp;uuml;əyyənləşənədək qanunda g&amp;ouml;stərilən m&amp;uuml;ddətə kimi D&amp;ouml;vlət Miqrasiya Xidmətinin Bakı və Yevlax şəhərlərində beynəlxalq standartlara uyğun olan Qanunsuz Miqrantların Saxlanılması Mərkəzlərində yerləşdirilir, qanunam&amp;uuml;vafiq tədbirlər g&amp;ouml;r&amp;uuml;l&amp;uuml;r, m&amp;uuml;vafiq qərarlar qəbul olunur.&lt;/p&gt;\r\n&lt;p&gt;Miqrasiya Məcəlləsində Azərbaycan Respublikasının ərazisində olmalarının arzuolunmaz hesab edilməsi proseduru m&amp;uuml;əyyən edilmişdir. Bir daha onu da xatırladım ki, əcnəbilərin və vətəndaşlığı olmayan şəxslərin arzuolunmaz hesab edilməsinin 5 il m&amp;uuml;ddətinə m&amp;uuml;əyyən edilməsi, m&amp;uuml;vafiq əsaslar aradan qalxmadıqda eyni m&amp;uuml;ddətə uzadıla bilməsi və həmin şəxslər barəsində Azərbaycan Respublikasının h&amp;uuml;dudlarından kənara &amp;ccedil;ıxarılması haqqında qərar qəbul edilməsi miqrasiya orqanları tərəfindən həyata ke&amp;ccedil;irilir. Əlbəttə, deyə bilmərik ki, bu g&amp;uuml;n respublikamızda m&amp;uuml;vəqqəti olma və yaxud &amp;ouml;lkədə m&amp;uuml;vəqqəti və daimi yaşama qaydalarını pozan, etibarsız sənədlərlə yaşayan, qanunsuz əmək fəaliyyəti ilə məşğul olan qeyri-leqal miqrantlar yoxdur. D&amp;uuml;nyanın b&amp;uuml;t&amp;uuml;n &amp;ouml;lkələrində qanunsuz miqrasiya halları var. Əsas məsələ bu arzuolunmaz halın qarşısını almaq və qanunsuz miqrasiyaya qarşı ciddi m&amp;uuml;barizə aparmaqdır. D&amp;ouml;vlət Miqrasiya Xidməti bu işin &amp;ouml;hdəsindən uğurla gəlir və bu barədə məlumatlar m&amp;uuml;təmadi olaraq ictimaiyyətə a&amp;ccedil;ıqlanır.&lt;/p&gt;\r\n&lt;p&gt;F&amp;uuml;rsətdən istifadə edib bir daha &amp;ouml;lkəmizə təşrif buyurmuş əcnəbilərə və vətəndaşlığı olmayan şəxslərə m&amp;uuml;raciət edirəm ki, onlar qanunvericiliyin tələbinə uyğun olaraq &amp;ouml;lkədə olmaq və yaşamaqlarını leqallaşdırsınlar m&amp;uuml;əyyən olunmuş m&amp;uuml;ddətdə qeydiyyata d&amp;uuml;şs&amp;uuml;nlər. &amp;Uuml;mumiyyətlə əcnəbi və ya vətəndaşlığı olmayan şəxs olduğu yeri dəyişdikdə, o, DMX-nə bu barədə m&amp;uuml;tləq məlumat verməli və yeni olduğu yer &amp;uuml;zrə qeydiyyata alınmalıdır.&amp;nbsp;&lt;/p&gt;', 0, 1, 1, 2, 1, 0);
INSERT INTO `interviews` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(3, 'Dövlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş hüquq və azadlıqlar', '', '', '', '', '&lt;p&gt;D&amp;ouml;vlət Miqrasiya Xidmətinin rəisi, II dərəcəli d&amp;ouml;vlət miqrasiya xidməti m&amp;uuml;şaviri Firudin Nəbiyevin Yeni Azərbaycan Partiyasının saytına (&lt;a href=&quot;http://yap.org.az/az/view/interview/349/dovlet-miqrasiya-xidmeti-fealiyyetini-insan-ve-vetendash-huquq-ve-azadliqlarina-hormet-qanunchuluq-ve-humanizm-prinsipleri-esasinda-heyata-kechirir&quot;&gt;www.yap.org.az&lt;/a&gt;) m&amp;uuml;sahibəsi&lt;/p&gt;\r\n&lt;p&gt;- Firudin m&amp;uuml;əllim, bir ne&amp;ccedil;ə g&amp;uuml;n əvvəl BMT Baş Assambleyasının &amp;ldquo;Qa&amp;ccedil;qın və miqrantların b&amp;ouml;y&amp;uuml;k axınının aradan qaldırılması&amp;rdquo; m&amp;ouml;vzusunda y&amp;uuml;ksək səviyyəli g&amp;ouml;r&amp;uuml;ş&amp;uuml;ndə iştirak etmisiniz. Bu barədə bir qədər ətraflı məlumat verməyinizi xahiş edirəm.&lt;/p&gt;\r\n&lt;p&gt;-&amp;nbsp;Plenar iclas və altı interaktiv dəyirmi masa formatında ke&amp;ccedil;irilən&amp;nbsp; y&amp;uuml;ksək səviyyəli g&amp;ouml;r&amp;uuml;şdə bir sıra d&amp;ouml;vlət və h&amp;ouml;kumət baş&amp;ccedil;ıları, nazirlər, elmi-tədqiqat mərkəzlərinin və vətəndaş cəmiyyətinin n&amp;uuml;mayəndələri iştirak edirdi. Tədbirdə b&amp;ouml;y&amp;uuml;k qa&amp;ccedil;qın axınlarının aradan qaldırılması, miqrasiya, x&amp;uuml;susilə b&amp;ouml;y&amp;uuml;k miqrant axınlarının səbəblərinin aradan qaldırılması və miqrantların verdiyi m&amp;uuml;sbət t&amp;ouml;hfələrin vurğulanması kimi vacib məsələlər m&amp;uuml;zakirə olundu.&lt;/p&gt;\r\n&lt;p&gt;BMT-nin Baş Assambleyası prezidentinin sədrlik etdiyi g&amp;ouml;r&amp;uuml;ş&amp;uuml;n ke&amp;ccedil;irilməsində məqsəd qeyd edilən sahədə daha humanist və əlaqələndirilmiş yanaşmanın formalaşdırılması &amp;uuml;&amp;ccedil;&amp;uuml;n &amp;ouml;lkələrin bir araya gətirilməsi idi. &amp;ldquo;Qa&amp;ccedil;qın və miqrantların b&amp;ouml;y&amp;uuml;k axınının aradan qaldırılması&amp;rdquo; m&amp;ouml;vzusunda y&amp;uuml;ksək səviyyəli g&amp;ouml;r&amp;uuml;şdə və g&amp;ouml;r&amp;uuml;ş &amp;ccedil;ər&amp;ccedil;ivəsində &amp;ldquo;Qa&amp;ccedil;qın və miqrantlar, habelə k&amp;ouml;&amp;ccedil;&amp;uuml;r&amp;uuml;lmə ilə bağlı məsələlər &amp;uuml;zrə beynəlxalq tədbirlər və əməkdaşlıq: qabaqdakı yol&amp;rdquo; adlı dəyirmi masada tərəfimizdən bir sıra ciddi məsələlər qaldırıldı. Azərbaycanın məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;n məsələsinə verdiyi &amp;ouml;nəm və həssaslığın səbəbləri bu y&amp;uuml;ksək tribunadan bir daha səsləndirildi.&lt;/p&gt;\r\n&lt;p&gt;Məlum olduğu kimi Ermənistan tərəfindən ərazisinin işğalına g&amp;ouml;rə &amp;ouml;lkəmiz genişmiqyaslı məcburi k&amp;ouml;&amp;ccedil;&amp;uuml;r&amp;uuml;lmə problemi ilə &amp;uuml;zləşib. 9,7 milyon əhalisi olan Azərbaycan adambaşına d&amp;uuml;şən məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlərin sayına g&amp;ouml;rə d&amp;uuml;nyada ən b&amp;ouml;y&amp;uuml;k k&amp;ouml;&amp;ccedil;&amp;uuml;r&amp;uuml;lmə y&amp;uuml;k&amp;uuml;nə sahib &amp;ouml;lkələrdən biridir. 25 ildən artıqdır ki, Ermənistan beynəlxalq h&amp;uuml;ququn norma və prinsiplərini kobud şəkildə pozaraq &amp;ouml;z&amp;uuml;n&amp;uuml;n işğal&amp;ccedil;ılıq siyasətini, hazırkı status-kvonun m&amp;ouml;hkəmlənməsinə y&amp;ouml;nəlmiş səylərini davam etdirir və y&amp;uuml;z minlərlə azərbaycanlı məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;n&amp;uuml;n &amp;ouml;z doğma yurdlarına qayıdışını əngəlləyir. BMT Təhl&amp;uuml;kəsizlik Şurası tərəfindən işğal olunmuş ərazilərin Ermənistan silahlı q&amp;uuml;vvələrindən azad edilməsi ilə bağlı 1993-c&amp;uuml; ildə qəbul edilmiş 4 qətnamə təəss&amp;uuml;f ki, icra olunmur. Qeyd etmək lazımdır ki, azərbaycanlı qa&amp;ccedil;qın və məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlərin &amp;ouml;z torpaqlarına qayıtmaq imkanı verilməsinin zəruriliyi ilə bağlı beynəlxalq birliyin tələbinin yerinə yetirilməsi onların h&amp;uuml;quqlarının hərtərəfli təminatı &amp;uuml;&amp;ccedil;&amp;uuml;n olduqca zəruridir. Bundan başqa, təəss&amp;uuml;flə qeyd etməliyik ki, Ermənistan hazırkı qa&amp;ccedil;qın və miqrant b&amp;ouml;hranından sui-istifadə edərək, Suriyadan olan erməniləri Azərbaycanın işğal olunmuş ərazilərində məskunlaşdırır. Diqqətə &amp;ccedil;atdırıldı ki, bu beynəlxalq humanitar h&amp;uuml;ququn, ilk n&amp;ouml;vbədə 1949-cu il d&amp;ouml;rd&amp;uuml;nc&amp;uuml; Cenevrə Konvensiyasının və Əlavə Protokollarının kobud surətdə pozulmasıdır.&lt;/p&gt;\r\n&lt;p&gt;Bilirsiniz ki, &amp;Uuml;mummilli lider Heydər Əliyevin və hazırda m&amp;ouml;htərəm Prezidentimiz İlham Əliyevin rəhbərliyi ilə Azərbaycan &amp;ouml;z doğma yerlərindən qovulmuş məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlərin b&amp;uuml;t&amp;uuml;n problemlərini &amp;ouml;z &amp;uuml;zərinə g&amp;ouml;t&amp;uuml;r&amp;uuml;b. Azərbaycan h&amp;ouml;kuməti tərəfindən bu insanların məşğulluq, təhsil, yaşayış və tibbi, sosial təminatı ilə əlaqədar tədbirlər davamlı olaraq həyata ke&amp;ccedil;irilir. Belə ki, &amp;ouml;tən d&amp;ouml;vr ərzində Azərbaycan h&amp;ouml;kuməti tәrәfindәn məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlәr &amp;uuml;&amp;ccedil;&amp;uuml;n y&amp;uuml;zə yaxın m&amp;uuml;asir qәsәbә salınıb, minlərlə qa&amp;ccedil;qın və məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;n ailəsinin mənzil-məişət şəraiti yaxşılaşdırılıb. Son 20 il ərzində Azərbaycanda məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlərin sosial problemlərinin həllinə təxminən 6 milyard ABŞ dolları həcmində vəsait xərclənib. Azərbaycan h&amp;ouml;kumətinin məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlərin problemlərinin həll olunması sahəsində səyləri BMT-nin Qa&amp;ccedil;qınlar &amp;uuml;zrə Ali Komissarlığı, Beynəlxalq Miqrasiya Təşkilatı və digər təşkilatlar tərəfindən y&amp;uuml;ksək dəyərləndirilib. Bu fikirlər həmin tribunadan iştirak&amp;ccedil;ıların nəzərinə &amp;ccedil;atdırıldı.&lt;/p&gt;\r\n&lt;p&gt;-&amp;nbsp;Bəs&amp;nbsp;Azərbaycanda&amp;nbsp;miqrantlar və onların ailə &amp;uuml;zvlərinin h&amp;uuml;quqlarının qorunması ilə bağlı hansı işlər g&amp;ouml;r&amp;uuml;l&amp;uuml;r, hansı &amp;ouml;lkələrlə sazişlər imzalanıb və yaxud belə sazişlərin imzalanması g&amp;ouml;zlənilir?&lt;/p&gt;\r\n&lt;p&gt;- Qeyd edim ki, D&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarına h&amp;ouml;rmət, qanun&amp;ccedil;uluq və humanizm prinsipləri əsasında həyata ke&amp;ccedil;irir və hər zaman miqrantların h&amp;uuml;quqlarının qorunmasına x&amp;uuml;susi &amp;ouml;nəm verir. H&amp;uuml;quqlarını bilməyən, &amp;ouml;lkədə olmasını leqallaşdırmayan miqrantlar hər an insan alveri qurbanı olmaq təhl&amp;uuml;kəsi ilə qarşılaşa bilərlər. Bu zaman qanunun tələblərinə riayət etməyən miqrantların h&amp;uuml;quqlarını qorumaq m&amp;uuml;şk&amp;uuml;l bir məsələyə &amp;ccedil;evirilir. Məhz bu kimi xoşagəlməz hadisələrin baş verməməsi &amp;uuml;&amp;ccedil;&amp;uuml;n əcnəbilər D&amp;ouml;vlət Miqrasiya Xidmətinə, Xidmətin regional idarələrinə m&amp;uuml;raciət edib &amp;ouml;lkə ərazisində m&amp;uuml;vəqqəti olmalarını, yaşamalarını leqallaşdırmalı, fəaliyyətlərini qanuni əsaslarla həyata ke&amp;ccedil;irməli, bu sahədə m&amp;ouml;vcud olan qanunlarımızı bilməli və ona riayət etməlidirlər.&lt;/p&gt;\r\n&lt;p&gt;D&amp;ouml;vlət Miqrasiya Xidməti bununla əlaqədar əcnəbi və vətəndaşlığı olmayan şəxsləri, ictimaiyyəti maarifləndirmək və məlumatlandırmaq məqsədilə bir sıra əhəmiyyətli tədbirlər həyata ke&amp;ccedil;irir.&lt;/p&gt;\r\n&lt;p&gt;Miqrasiya Məcəlləsinin 54-c&amp;uuml; maddəsinə əsasən əcnəbilərin və vətəndaşlığı olmayan şəxslərin Azərbaycan Respublikasında daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verilməsi ilə bağlı m&amp;uuml;raciətlərinə baxılarkən, onların Azərbaycan Respublikasının qanunları ilə nəzərdə tutulmuş h&amp;uuml;quq və vəzifələri, habelə d&amp;ouml;vlət dili ilə bağlı biliyi D&amp;ouml;vlət Miqrasiya Xidməti, Ədliyyə Nazirliyi və Təhsil Nazirliyi n&amp;uuml;mayəndələrindən ibarət komissiya tərəfindən yoxlanılır. Azərbaycanın d&amp;ouml;vlət dilini bilmək miqrantların yerli əhali ilə &amp;uuml;nsiyyətinin asanlaşdırılması və cəmiyyətimizə inteqrasiyası baxımından əlbəttə &amp;ccedil;ox m&amp;uuml;h&amp;uuml;m amildir. Azərbaycan Respublikasında m&amp;uuml;vəqqəti və ya daimi yaşayan əcnəbilər və vətəndaşlığı olmayan şəxslər &amp;ouml;z arzuları ilə Azərbaycan dilini, tarixini, mədəniyyətini, &amp;ouml;z h&amp;uuml;quq və vəzifələri ilə bağlı qanunvericiliyi &amp;ouml;yrənmək &amp;uuml;&amp;ccedil;&amp;uuml;n D&amp;ouml;vlət Miqrasiya Xidmətinin təlim-tədris mərkəzinə m&amp;uuml;raciət edə bilərlər. Onlar &amp;uuml;&amp;ccedil;&amp;uuml;n m&amp;uuml;vafiq yer ayrılıb, x&amp;uuml;susi proqramlar hazırlanıb və kurslar təşkil edilib.&lt;/p&gt;\r\n&lt;p&gt;Bununla yanaşı, Azərbaycan miqrantların h&amp;uuml;quqlarının səmərəli m&amp;uuml;dafiəsinin təmin edilməsi, qanunsuz miqrasiyanın qarşısının alınması, miqrasiya proseslərinin idarəolunması və proqnozlaşdırılması məqsədilə qabaqcıl təcr&amp;uuml;bənin &amp;ouml;yrənilməsi və tətbiqi sahəsində beynəlxalq əməkdaşlığa x&amp;uuml;susi &amp;ouml;nəm verir.&lt;/p&gt;\r\n&lt;p&gt;Miqrantların və onların ailə &amp;uuml;zvlərinin h&amp;uuml;quqlarının m&amp;uuml;dafiəsinin təmin edilməsi məqsədilə Azərbaycan Respublikası bir sıra &amp;ouml;lkələrlə ikitərəfli və &amp;ccedil;oxtərəfli sazişlər imzalayıb. Belə ki, miqrasiya sahəsində əməkdaşlığa dair T&amp;uuml;rkiyə, Moldova, Qazaxıstan, Qırğızıstan, Ukrayna və Belarus ilə ikitərəfli sazişlər m&amp;ouml;vcuddur. Hazırda bir ne&amp;ccedil;ə &amp;ouml;lkə ilə də belə sazişlərin imzalanması g&amp;ouml;zlənilir. Miqrantların, o c&amp;uuml;mlədən digər &amp;ouml;lkələrdə miqrant həyatı yaşayan Azərbaycan Respublikası vətəndaşlarının h&amp;uuml;quqlarının m&amp;uuml;dafiəsi, sosial m&amp;uuml;dafiəsi, işə d&amp;uuml;zəlmə prosedurlarının asanlaşdırılması bu sazişlərin əsasını təşkil edir.&lt;/p&gt;\r\n&lt;p&gt;Bundan əlavə, &amp;ouml;lkəmiz Avropa İttifaqı və Norve&amp;ccedil; ilə imzaladığı readmissiya sazişləri &amp;ccedil;ər&amp;ccedil;ivəsində qanunsuz vəziyyətdə olan miqrantların geri qaytarılmasını həyata ke&amp;ccedil;irməklə, onların insan h&amp;uuml;quqlarının m&amp;uuml;dafiəsini də təmin edir. Bir sıra digər &amp;ouml;lkələrlə də belə sazişlərin imzalanması istiqamətində işlər davam etdirilir.&lt;/p&gt;\r\n&lt;p&gt;- Əcnəbilərin və vətəndaşlığı olmayan şəxslərin dəqiq u&amp;ccedil;otu necə aparılır?&lt;/p&gt;\r\n&lt;p&gt;- Әsası ulu &amp;ouml;ndәr Heydәr Әliyev tәrәfindәn qoyulmuş inkişaf strategiyasına uyğun olaraq iqtisadi tərəqqi və demokratikləşmə xəttini milli inkişafın dəyişməz formulu kimi &amp;ouml;nə &amp;ccedil;əkən Prezident İlham Əliyev respublikamızın qlobal siyasi arenada m&amp;uuml;sbət imiclə tanınmasını, regional və beynəlxalq iqtisadi əməkdaşlıq mərkəzinə &amp;ccedil;evrilməsini təmin edib. Azərbaycan Respublikasında m&amp;uuml;şahidə olunan s&amp;uuml;rətli sosial-iqtisadi inkişaf, miqrantların h&amp;uuml;quq və azadlıqlarının qorunması sahəsində g&amp;ouml;r&amp;uuml;lən işlər, &amp;ouml;lkəmizdəki siyasi sabitlik, həm&amp;ccedil;inin &amp;ouml;lkəmizin geosiyasi m&amp;ouml;vqeyi d&amp;uuml;nyanın m&amp;uuml;xtəlif yerlərindən Azərbaycana miqrasiya edənlərin sayının artmasına səbəb olub. Azərbaycan Respublikasında yaşayan, işləyən və m&amp;uuml;vəqqəti olan əcnəbilərin və vətəndaşlığı olmayan şəxslərin dəqiq u&amp;ccedil;otunun aparılması, miqrasiya proseslərinin idarə olunmasında iştirak edən d&amp;ouml;vlət orqanlarının zəruri informasiya ilə təmin edilməsi, miqrasiya ilə bağlı sənədləşmə, yoxlama, sorğu və təhlil işlərinin avtomatlaşdırılması və bu sahədə g&amp;ouml;stərilən elektron xidmətlərin təkmilləşdirilməsi məqsədilə D&amp;ouml;vlət Miqrasiya Xidmətinin nəzdində Vahid Miqrasiya Məlumat Sistemi (VMMS) yaradılıb. VMMS &amp;ouml;lkədə baş verən miqrasiya proseslərinin dinamikası barədə tam təsəvv&amp;uuml;r&amp;uuml;n yaradılmasına imkan verərək, qeyri-qanuni miqrasiya ilə m&amp;uuml;barizə və təhl&amp;uuml;kəsizliyin təmini sahəsində m&amp;uuml;vafiq tədbirlərin g&amp;ouml;r&amp;uuml;lməsi &amp;uuml;&amp;ccedil;&amp;uuml;n əlverişli şərait yaradıb.&lt;/p&gt;\r\n&lt;p&gt;- Son d&amp;ouml;vrlər D&amp;ouml;vlət Miqrasiya Xidməti tərəfindən vətəndaşsızlıq hallarının aradan qaldırılması ilə əlaqədar genişmiqyaslı tədbirlərin həyata ke&amp;ccedil;irildiyi ilə bağlı məlumatlar verilir. Nə kimi tədbirlər g&amp;ouml;r&amp;uuml;l&amp;uuml;r?&lt;/p&gt;\r\n&lt;p&gt;-&amp;nbsp;Vətəndaşsızlıq halları ilə m&amp;uuml;barizə sahəsində məlumatlılığın artırılması və bu şəxslərin h&amp;uuml;quqi statusunun m&amp;uuml;əyyənləşdirilməsi zamanı yaranan problemlərin, o c&amp;uuml;mlədən onların h&amp;uuml;quqlarının səmərəli m&amp;uuml;dafiəsi naminə əməkdaşlığın g&amp;uuml;cləndirilməsi məqsədi ilə Azərbaycan Respublikası BMT-nin Qa&amp;ccedil;qınlar &amp;uuml;zrə Ali Komissarlığının &amp;ldquo;10 ilə vətəndaşsızlığa son&amp;rdquo; kampaniyasına qoşulub.&amp;nbsp; Vətəndaşsızlıq hallarının aradan qaldırılması istiqamətində konkret işlər g&amp;ouml;r&amp;uuml;l&amp;uuml;r. D&amp;ouml;vlət Miqrasiya Xidməti tərəfindən respublikanın ayrı-ayrı b&amp;ouml;lgələrində vətəndaşsızlıqla bağlı ke&amp;ccedil;irilən &amp;ccedil;oxsaylı maarifləndirmə tədbirləri, g&amp;ouml;r&amp;uuml;şlər də &amp;ouml;z bəhrəsini verir. Belə ki, istər vətəndaşlıq mənsubiyyətinin m&amp;uuml;əyyən olunması, istərsə də vətəndaşlığa qəbul və bərpa ilə bağlı m&amp;uuml;raciətlərin sayı bunu deməyə əsas verir.&lt;/p&gt;\r\n&lt;p&gt;Azәrbaycan Prezidentinin m&amp;uuml;vafiq sәrәncamları ilә 2008&amp;shy;-ci ildәn indiyәdәk 1682 nәfәr Azәrbaycan vәtәndaşlığına qәbul və bərpa edilib. Bununla belә, bu m&amp;uuml;ddәt әrzindә 61 mindən &amp;ccedil;ox şәxsin Azәrbaycan Respublikası vәtәndaşlığına mәnsubiyyәti tanınıb vә onlar m&amp;uuml;vafiq d&amp;ouml;vlәt qurumu tәrәfindәn Azәrbaycan Respublikası vәtәndaşının şәxsiyyәtini tәsdiq edәn sәnәdlәrlә tәmin edilib.&lt;/p&gt;\r\n&lt;p&gt;- Hansı hallarda şəxsin Azərbaycan Respublikasının vətəndaşlığına mənsubiyyəti tanınır?&lt;/p&gt;\r\n&lt;p&gt;- Azərbaycan Respublikası Nazirlər Kabinetinin 2015-ci il 18 mart tarixli&amp;nbsp; 84 n&amp;ouml;mrəli qərarı ilə təsdiq edilmiş &amp;ldquo;Şəxsin Azərbaycan Respublikasının vətəndaşlığına mənsubiyyətinin m&amp;uuml;əyyənləşdirilməsi Qaydası&amp;rdquo;na əsasən aşağıdakı hallardan hər hansı biri olduqda, şəxsin Azərbaycan Respublikasının vətəndaşlığına mənsubiyyəti tanınır:&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&amp;middot;&amp;nbsp;&amp;nbsp; &amp;ldquo;Azərbaycan Respublikasının vətəndaşlığı haqqında&amp;rdquo; Azərbaycan Respublikası Qanununun q&amp;uuml;vvəyə mindiyi g&amp;uuml;nədək (1998-ci il oktyabrın 7-dək) Azərbaycan Respublikasının (və ya Azərbaycan SSR-in) vətəndaşlığında olmuş şəxslər həmin tarixədək Azərbaycan Respublikasında yaşayış yeri &amp;uuml;zrə daimi qeydiyyatda olduqda;&lt;/p&gt;\r\n&lt;p&gt;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; 1988-ci il yanvarın 1-dən 1992-ci il yanvarın 1-dək Azərbaycan Respublikasının ərazisində məskunlaşmış qa&amp;ccedil;qın olduqda;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Azərbaycan Respublikasının ərazisində olan, hər iki valideyni naməlum olan uşaq olduqda;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Azərbaycan Respublikasının ərazisində doğulmuş aşağıdakı şəxslərdən biri olduqda:&lt;/p&gt;\r\n&lt;p&gt;1) doğulduğu anda valideynlərindən biri və ya hər ikisi Azərbaycan Respublikasının vətəndaşı olmuşdursa;&lt;/p&gt;\r\n&lt;p&gt;2)&amp;nbsp;&amp;nbsp; doğulduğu anda hər iki valideyni vətəndaşlığı olmayan şəxs olmuşdursa;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&amp;middot; Azərbaycan Respublikasının h&amp;uuml;dudlarından kənarda Azərbaycan Respublikasının vətəndaşından (vətəndaşlarından) doğulmuş şəxs;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp; &amp;middot;&amp;nbsp;&amp;nbsp; Əcnəbi və ya vətəndaşlığı olmayan uşağı &amp;ouml;vladlığa g&amp;ouml;t&amp;uuml;rən ər-arvadın hər ikisi Azərbaycan Respublikasının vətəndaşıdırsa, həmin uşaq;&lt;/p&gt;\r\n&lt;p&gt;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Əcnəbi və ya vətəndaşlığı olmayan uşağı &amp;ouml;vladlığa g&amp;ouml;t&amp;uuml;rən ər-arvaddan biri Azərbaycan Respublikasının vətəndaşıdırsa, digəri isə vətəndaşlığı olmayan şəxsdirsə, həmin uşaq;&lt;/p&gt;\r\n&lt;p&gt;&amp;middot;&amp;nbsp;&amp;nbsp; Valideynlərin vətəndaşlığı dəyişdikdə və bunun nəticəsində hər ikisi Azərbaycan Respublikasının vətəndaşlığını əldə etdikdə, onların uşağı;&lt;/p&gt;\r\n&lt;p&gt;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Uşağın valideynlərindən biri məlumdursa, həmin valideyn Azərbaycan Respublikasının vətəndaşlığını əldə etdikdə, onun uşağı;&lt;/p&gt;\r\n&lt;p&gt;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Azərbaycan Respublikasının ərazisində yaşayan uşağın valideynlərindən biri Azərbaycan Respublikasının vətəndaşlığını əldə etmiş şəxs, digəri isə vətəndaşlığı olmayan şəxs olduqda;&lt;/p&gt;\r\n&lt;p&gt;&amp;middot;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Valideynlərdən birinin Azərbaycan Respublikasının vətəndaşlığına xitam verilərsə, digəri isə Azərbaycan Respublikasının vətəndaşlığında qalarsa, onların uşağı.&lt;/p&gt;\r\n&lt;p&gt;Bundan başqa, &amp;ldquo;Azərbaycan Respublikasının vətəndaşlığı haqqında&amp;rdquo; Azərbaycan Respublikası Qanununun q&amp;uuml;vvəyə mindiyi g&amp;uuml;nədək (1998-ci il oktyabrın 7-dək) Azərbaycan Respublikasının (və ya Azərbaycan SSR-in) vətəndaşlığında olmuş və Azərbaycan Respublikasının ərazisini tərk etməyən şəxslərin bu Qanun q&amp;uuml;vvəyə mindiyi g&amp;uuml;nədək Azərbaycan Respublikasında yaşayış yeri &amp;uuml;zrə qeydiyyatda olmamaları onların vətəndaşsızlığına səbəb olduqda, həmin şəxslərin Azərbaycan vətəndaşlığına mənsubiyyətləri tanınır.&lt;/p&gt;\r\n&lt;p&gt;- Bəs əcnəbilər və vətəndaşlığı olmayan şəxslər Azərbaycan Respublikasının vətəndaşlığına hansı hallarda qəbul edilə bilərlər?&lt;/p&gt;\r\n&lt;p&gt;-&amp;nbsp;&amp;ldquo;Azərbaycan Respublikasının vətəndaşlığı haqqında&amp;rdquo; Azərbaycan Respublikasının Qanununa əsasən Azərbaycan Respublikasının ərazisində son beş il ərzində fasiləsiz olaraq qanuni əsaslarla daimi yaşayan, qanuni gəlir mənbəyi olan, Azərbaycan Respublikasının Konstitusiyasına və qanunlarına riayət olunması barədə &amp;ouml;hdəlik g&amp;ouml;t&amp;uuml;rən, habelə Azərbaycan Respublikasının d&amp;ouml;vlət dilini bilməsi haqqında sənəd təqdim edən əcnəbi və vətəndaşlığı olmayan şəxs mənşəyindən, irqi və milli mənsubiyyətindən, cinsindən, təhsilindən, dinə m&amp;uuml;nasibətindən, siyasi və başqa əqidələrindən asılı olmayaraq bu qanuna m&amp;uuml;vafiq surətdə &amp;ouml;z vəsatəti ilə Azərbaycan Respublikasının vətəndaşlığına qəbul edilə bilər.&lt;/p&gt;\r\n&lt;p&gt;Həm&amp;ccedil;inin aşağıdakı hallarda əcnəbilər və vətəndaşlığı olmayan şəxslər qeyd olunan m&amp;uuml;ddət nəzərə alınmadan Azərbaycan Respublikasının vətəndaşlığına qəbul edilə bilərlər:&lt;/p&gt;\r\n&lt;p&gt;1) şəxsin elm, texnika, mədəniyyət və ya idman sahələrində y&amp;uuml;ksək nailiyyətləri olduqda;&lt;/p&gt;\r\n&lt;p&gt;2) şəxs Azərbaycan Respublikası &amp;uuml;&amp;ccedil;&amp;uuml;n x&amp;uuml;susi maraq kəsb etdikdə və digər m&amp;uuml;stəsna hallarda.&lt;/p&gt;\r\n&lt;p&gt;Eləcə də, şəxsin Azərbaycan Respublikasının qarşısında x&amp;uuml;susi xidmətləri olduqda o, qeyd olunan şərtlər nəzərə alınmadan Azərbaycan Respublikasının vətəndaşlığına qəbul edilə bilər.&lt;/p&gt;\r\n&lt;p&gt;Bundan başqa, əvvəllər Azərbaycan Respublikasının vətəndaşı olmuş və ya Azərbaycan Respublikası vətəndaşlığına xitam verilmiş şəxs &amp;ouml;z vəsatəti əsasında Azərbaycan Respublikası vətəndaşlığına bərpa edilə bilər.&lt;/p&gt;\r\n&lt;p&gt;Bir məsələni də qeyd edim ki, &amp;ldquo;Azərbaycan Respublikasının vətəndaşlığı haqqında&amp;rdquo; qanuna 2014-c&amp;uuml; il 30 may tarixli dəyişikliklər əsasında yeni redaksiyada verilmiş qanunun 10-cu maddəsində g&amp;ouml;stərilir ki, Azərbaycan Respublikasının vətəndaşı olan şəxsin ikili vətəndaşlığı olduqda həmin şəxsin xarici d&amp;ouml;vlətin vətəndaşlığına mənsubiyyəti, Azərbaycan Respublikasının beynəlxalq m&amp;uuml;qavilələrində nəzərdə tutulmuş və ya Konstitusiyanın 109-cu maddəsinin 32-ci bəndinə m&amp;uuml;vafiq surətdə həll edilmiş hallar istisna olmaqla tanınmır. Və xarici d&amp;ouml;vlətin vətəndaşlığını qəbul etmiş Azərbaycan Respublikasının vətəndaşı bir ay m&amp;uuml;ddətində bu barədə Azərbaycan ərazisində olduqda D&amp;ouml;vlət Miqrasiya Xidmətinə, &amp;ouml;lkə h&amp;uuml;dudlarından kənarda olduqda isə Azərbaycan Respublikasının diplomatik n&amp;uuml;mayəndəlikləri və ya konsulluq idarələri vasitəsilə Xarici İşlər Nazirliyinə yazılı məlumat verməlidir. Əks halda Cinayət Məcəlləsinin 318-2-ci&amp;nbsp; maddəsinə&amp;nbsp; əsasən, həmin vətəndaşlar&amp;nbsp; 3 min manatdan 5 min manata qədər cərimə və ya 360 saatdan 480 saatadək ictimai işlər ilə&amp;nbsp;cəzalandırılırlar.&lt;/p&gt;\r\n&lt;p&gt;- Miqrasiaya prosesinin idarə olunmasını, tənzimlənməsini həyata ke&amp;ccedil;irən bir orqan kimi Sizin rəhbərlik etdiyiniz Xidmət həm də prosesə d&amp;ouml;vlət nəzarətini də həyata ke&amp;ccedil;irir.&amp;nbsp; D&amp;ouml;vlət nəzarəti işinin təşkili haqqında nə deyə bilərsiniz?&lt;/p&gt;\r\n&lt;p&gt;-&amp;nbsp;Azərbaycanda miqrantların rahatlıqlarını təmin etmək &amp;uuml;&amp;ccedil;&amp;uuml;n Prezident İlham Əliyevin imzaladığı fərmana əsasən, 2009-cu il iyulun 1-dən miqrasiya proseslərinin idarəolunmasında &amp;laquo;bir pəncərə&amp;raquo; prinsipi &amp;uuml;zrə vahid d&amp;ouml;vlət orqanının səlahiyyətlərini D&amp;ouml;vlət Miqrasiya Xidməti həyata ke&amp;ccedil;irir. Respublikada əcnəbilərin və vətəndaşlığı olmayan şəxslərin olduğu yer &amp;uuml;zrə qeydiyyatı, m&amp;uuml;vəqqəti olma, m&amp;uuml;vəqqəti və daimi yaşama, iş icazələrinin verilməsi və m&amp;uuml;ddətlərinin uzadılması, qa&amp;ccedil;qın statusunun m&amp;uuml;əyyənləşdirilməsi, vətəndaşlığa qəbul, xitam, bərpa, vətəndaşlıq mənsubiyyətinin m&amp;uuml;əyyən olunması, eləcə də vətəndaşlıq məsələlərinin həlli ilə məşğul olan h&amp;uuml;quq-m&amp;uuml;hafizə orqanı status&amp;shy;unu daşıyan Xidmət həm də miqrasiya qanunvericiliyinin tətbiqində yol verilə biləcək n&amp;ouml;q&amp;shy;san və sui-istifadə&amp;shy;lə&amp;shy;rin, miq&amp;shy;ra&amp;shy;siya proses&amp;shy;lərinin neqa&amp;shy;tiv təsirinin aradan qal&amp;shy;dı&amp;shy;rılmasını, qeyri-qanuni miqrasiyaya qarşı effektiv m&amp;uuml;barizə tədbir&amp;shy;ləri&amp;shy;ni həyata ke&amp;ccedil;irir.&lt;/p&gt;\r\n&lt;p&gt;Belə ki, əcnəbi və vətəndaşlığı olmayan şəxs əvvəllər Azərbaycan Respublikasında olarkən &amp;ouml;lkəyə gəlişinin bəyan edilmiş məqsədlərini pozduqda, Azərbaycan Respublikasına gəlməsi haqqında vəsatət qaldırarkən, &amp;ouml;z&amp;uuml; və ya səfərinin məqsədi haqqında yalan məlumat verdikdə, yaxud miqrasiya qanunvericiliyini pozduğuna g&amp;ouml;rə son &amp;uuml;&amp;ccedil; il ərzində iki dəfə və ya daha &amp;ccedil;ox inzibati məsuliyyətə cəlb olunduqda onların Azərbaycan Respublikasına gəlişinə 5 il m&amp;uuml;ddətinə qadağa qoyulur.&lt;/p&gt;\r\n&lt;p&gt;Xatırladım ki, Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti yaşamağa icazə alınması &amp;uuml;&amp;ccedil;&amp;uuml;n əsas olmuş hal aradan qalxdıqda və icazə alınması ilə bağlı digər əsaslar olmadıqda əcnəbilərə və vətəndaşlığı olmayan şəxslərə &amp;ouml;lkəmizin ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verilmir, əvvəl verilmiş icazə isə ləğv edilir. Həm&amp;ccedil;inin Miqrasiya Məcəlləsinin tələbinə g&amp;ouml;rə&amp;nbsp; əcnəbi 180 g&amp;uuml;n ərzində 90 g&amp;uuml;ndən &amp;ccedil;ox Azərbaycan Respublikasının ərazisindən kənarda olduqda, yaxud &amp;ouml;lkəmizdə m&amp;uuml;vəqqəti yaşadığı d&amp;ouml;vrdə&amp;nbsp; gəlişinin bəyan edilmiş məqsədini pozduqda da ona m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verilmir, yaxud əvvəl verilmiş icazə ləğv edilir.&lt;/p&gt;\r\n&lt;p&gt;Bundan əlavə &amp;ouml;lkə ərazisində qanunsuz yaşayan, eləcə də qa&amp;ccedil;qın statusu almaq niyyətində olan şəxslər h&amp;uuml;quqi statusları m&amp;uuml;əyyənləşənədək qanunda g&amp;ouml;stərilən m&amp;uuml;ddətə kimi D&amp;ouml;vlət Miqrasiya Xidmətinin Bakı və Yevlax şəhərlərində beynəlxalq standartlara uyğun olan Qanunsuz Miqrantların Saxlanılması Mərkəzlərində yerləşdirilir, qanunam&amp;uuml;vafiq tədbirlər g&amp;ouml;r&amp;uuml;l&amp;uuml;r, m&amp;uuml;vafiq qərarlar qəbul olunur.&lt;/p&gt;\r\n&lt;p&gt;Miqrasiya Məcəlləsində Azərbaycan Respublikasının ərazisində olmalarının arzuolunmaz hesab edilməsi proseduru m&amp;uuml;əyyən edilmişdir. Bir daha onu da xatırladım ki, əcnəbilərin və vətəndaşlığı olmayan şəxslərin arzuolunmaz hesab edilməsinin 5 il m&amp;uuml;ddətinə m&amp;uuml;əyyən edilməsi, m&amp;uuml;vafiq əsaslar aradan qalxmadıqda eyni m&amp;uuml;ddətə uzadıla bilməsi və həmin şəxslər barəsində Azərbaycan Respublikasının h&amp;uuml;dudlarından kənara &amp;ccedil;ıxarılması haqqında qərar qəbul edilməsi miqrasiya orqanları tərəfindən həyata ke&amp;ccedil;irilir. Əlbəttə, deyə bilmərik ki, bu g&amp;uuml;n respublikamızda m&amp;uuml;vəqqəti olma və yaxud &amp;ouml;lkədə m&amp;uuml;vəqqəti və daimi yaşama qaydalarını pozan, etibarsız sənədlərlə yaşayan, qanunsuz əmək fəaliyyəti ilə məşğul olan qeyri-leqal miqrantlar yoxdur. D&amp;uuml;nyanın b&amp;uuml;t&amp;uuml;n &amp;ouml;lkələrində qanunsuz miqrasiya halları var. Əsas məsələ bu arzuolunmaz halın qarşısını almaq və qanunsuz miqrasiyaya qarşı ciddi m&amp;uuml;barizə aparmaqdır. D&amp;ouml;vlət Miqrasiya Xidməti bu işin &amp;ouml;hdəsindən uğurla gəlir və bu barədə məlumatlar m&amp;uuml;təmadi olaraq ictimaiyyətə a&amp;ccedil;ıqlanır.&lt;/p&gt;\r\n&lt;p&gt;F&amp;uuml;rsətdən istifadə edib bir daha &amp;ouml;lkəmizə təşrif buyurmuş əcnəbilərə və vətəndaşlığı olmayan şəxslərə m&amp;uuml;raciət edirəm ki, onlar qanunvericiliyin tələbinə uyğun olaraq &amp;ouml;lkədə olmaq və yaşamaqlarını leqallaşdırsınlar m&amp;uuml;əyyən olunmuş m&amp;uuml;ddətdə qeydiyyata d&amp;uuml;şs&amp;uuml;nlər. &amp;Uuml;mumiyyətlə əcnəbi və ya vətəndaşlığı olmayan şəxs olduğu yeri dəyişdikdə, o, DMX-nə bu barədə m&amp;uuml;tləq məlumat verməli və yeni olduğu yer &amp;uuml;zrə qeydiyyata alınmalıdır.&amp;nbsp;&lt;/p&gt;', 0, 1, 1, 3, 1, 0),
(4, 'DMX rəisi: “Azərbaycandakı iqtisadi uğurlar, ictimai-siyasi sabitlik ölkəd', '', '', '', '', '&lt;p&gt;&amp;ldquo;Azərbaycana gələn əmək&amp;ccedil;i miqrantların palitrası zəngindir&amp;rdquo;&lt;/p&gt;\r\n&lt;p&gt;Azərbaycan Respublikası D&amp;ouml;vlət Miqrasiya Xidmətinin (DMX) rəisi, II dərəcəli d&amp;ouml;vlət miqrasiya xidməti m&amp;uuml;şaviri Firudin Nəbiyevin &quot;Report&quot;a m&amp;uuml;sahibəsi&lt;/p&gt;\r\n&lt;p&gt;- Firudin m&amp;uuml;əllim, bir ne&amp;ccedil;ə g&amp;uuml;n &amp;ouml;ncə sahibkarlarla g&amp;ouml;r&amp;uuml;şd&amp;uuml;n&amp;uuml;z, dinlədiniz. Onları təmsil etdiyiniz sahə ilə bağlı daha &amp;ccedil;ox hansı problemlər narahat edir?&lt;/p&gt;\r\n&lt;p&gt;- Bu, Xəzər-Avropa İnteqrasiya İşg&amp;uuml;zar Klubunun D&amp;ouml;vlət Miqrasiya Xidməti rəisinin iştirakı ilə təşkil etdiyi n&amp;ouml;vbəti biznes forum idi. Bundan əvvəl də bizim belə forumlarda sahibkarlarla, biznes sektorunun n&amp;uuml;mayəndələri ilə &amp;ccedil;oxsaylı g&amp;ouml;r&amp;uuml;şlərimiz olub. Deyə bilərəm ki, sonuncu ke&amp;ccedil;irilən biznes-forumda iş adamları, işəg&amp;ouml;t&amp;uuml;rənlərin n&amp;uuml;mayəndələri, sahibkarlar D&amp;ouml;vlət Miqrasiya Xidməti ilə bağlı hər hansı problem yox, daha &amp;ccedil;ox miqrasiya qanunvericiliyi sahəsində yeniliklər, o c&amp;uuml;mlədən &amp;ldquo;Əmək miqrasiyası kvotasının m&amp;uuml;əyyən edilməsi Qaydası&amp;rdquo;na əsasən Məşğulluq Təsnifatı &amp;uuml;zrə peşələr g&amp;ouml;stərilməklə hər il may ayının 1-dək n&amp;ouml;vbəti il &amp;uuml;&amp;ccedil;&amp;uuml;n D&amp;ouml;vlət Miqrasiya Xidmətinə əcnəbi iş&amp;ccedil;i q&amp;uuml;vvəsinə tələbat haqqında onlayn rejimdə təqdim etməli olduqları proqnoz-məlumat barədə suallar səsləndirdilər və bəzi təkliflər irəli s&amp;uuml;rd&amp;uuml;lər. Biz onlayn rejimdə proqnoz-məlumatın təqdim edilməsinin yeni olduğunu nəzərə alaraq həmin m&amp;uuml;ddəti bu il mayın 15-nə kimi artırmışdıq.&lt;/p&gt;\r\n&lt;p&gt;DMX hər zaman &amp;ouml;zəl və biznes sektora diqqət və qayğı g&amp;ouml;stərir, onun problemləri ilə maraqlanır, təhlil edir və m&amp;uuml;vafiq həll yollarını tapır.&lt;/p&gt;\r\n&lt;p&gt;&amp;Ccedil;ox sevindirici haldır ki, artıq biznes sektorunu D&amp;ouml;vlət Miqrasiya Xidməti ilə bağlı narahat edən problem yoxdur. Biz sadəcə onların m&amp;uuml;xtəlif məzmunlu təkliflərini dinləmişik, təhlillər aparılır. Gələcəkdə normativ h&amp;uuml;quqi aktlarda həmin təkliflərin m&amp;uuml;mk&amp;uuml;n qədər əksini tapması &amp;uuml;&amp;ccedil;&amp;uuml;n xidmətin m&amp;uuml;vafiq struktur b&amp;ouml;lmələrinə tapşırıqlar verilib.&lt;/p&gt;\r\n&lt;p&gt;- D&amp;ouml;vlət Miqrasiya Xidmətini sahibkarların yaratdığı hansı problemlər daha &amp;ccedil;ox narahat edir?&lt;/p&gt;\r\n&lt;p&gt;- Miqrasiya Məcəlləsinə g&amp;ouml;rə, Azərbaycan Respublikasının ərazisində sahibkarlıq fəaliyyəti ilə məşğul olan (həmin şəxslər azı 5 nəfər ilə tam iş vaxtı şəraitində və ya 10 nəfər ilə natamam iş vaxtı şəraitində əmək m&amp;uuml;qaviləsi bağlamaqla faktiki fəaliyyət g&amp;ouml;stərdikləri və iş&amp;ccedil;ilərinin ən azı 80 faizi Azərbaycan Respublikasının vətəndaşları olduqları halda), o c&amp;uuml;mlədən xarici h&amp;uuml;quqi şəxsin Azərbaycan Respublikasındakı filial və n&amp;uuml;mayəndəliyinin rəhbərləri və onların m&amp;uuml;avinləri olan əcnəbilər və vətəndaşlığı olmayan şəxslər &amp;uuml;&amp;ccedil;&amp;uuml;n iş icazəsinin alınması tələb olunmur. Həmin forumda da mən bildirdim ki, &amp;ouml;lkədə fəaliyyət g&amp;ouml;stərən xarici bir şirkətdə 50-60 iş&amp;ccedil;i olduğu halda, 12-14 m&amp;uuml;avin vəzifəsi var. Biz xarici h&amp;uuml;quqi şəxsin Azərbaycan Respublikasındakı filial və n&amp;uuml;mayəndəliyinin m&amp;uuml;dir m&amp;uuml;avinləri vəzifələrinin 3-4-ə endirilməsini təklif etmişik.&lt;/p&gt;\r\n&lt;p&gt;Bir n&amp;uuml;munəni deyim ki, bir ne&amp;ccedil;ə il əvvəl BP şirkəti və SOCAR arasında imzalanan m&amp;uuml;qaviləyə əsasən, azərbaycanlı m&amp;uuml;təxəssislər hazırlanır. Hazırda bu sektorda &amp;ccedil;alışan əcnəbilərin sayı 10 faizə enib. B&amp;uuml;t&amp;uuml;n sahələrdə bu istiqamətdə iş aparılmalı, azərbaycanlı m&amp;uuml;təxəssislər hazırlanmalıdır.&lt;/p&gt;\r\n&lt;p&gt;Digər tərəfdən, ixtisara d&amp;uuml;şən Azərbaycan vətəndaşlarının yerinə əcnəbilər təyin olunur ki, bu da bizi narahat edir, belə hallara yol vermək olmaz. Məqsədimiz odur ki, daxili əmək bazarını qoruyaq və ilk n&amp;ouml;vbədə yerli məhsuldar q&amp;uuml;vvələr nəzərə alınsın, Azərbaycan vətəndaşları işlə təmin olunsun. Prezident İlham Əliyevin vurğuladığı kimi respublikanın iqtisadi inkişafından ilk n&amp;ouml;vbədə Azərbaycan vətəndaşları bəhrələnməlidirlər. S&amp;ouml;z yox ki, iş yerinin tələblərinə cavab verən peşə hazırlığına və ixtisasa malik Azərbaycan vətəndaşının iddia etmədiyi boş iş yerlərinin m&amp;ouml;vcudluğu, məşğulluq xidməti orqanlarının işəg&amp;ouml;t&amp;uuml;rənlərin iş&amp;ccedil;i q&amp;uuml;vvəsinə olan ehtiyaclarını yerli əmək ehtiyatları hesabına təmin etmək imkanın olmadığı hallar, habelə y&amp;uuml;ksək ixtisaslı əcnəbi m&amp;uuml;təxəssislərin cəlb edilməsi zəruriliyi nəzərə alınır və əmək miqrasiyası kvotası &amp;ccedil;ər&amp;ccedil;ivəsində qanuna m&amp;uuml;vafiq qaydada, yəni işəg&amp;ouml;t&amp;uuml;rənlər tərəfindən iş icazəsi alınmaqla əcnəbi iş&amp;ccedil;i q&amp;uuml;vvəsindən istifadə edilməsi məsələsi həll edilir. Bilirsiniz ki, Azərbaycan Respublikasında sahibkarlığın inkişafının stimullaşdırılması məqsədi ilə qəbul olunan &amp;ldquo;Sahibkarlıq sahəsində aparılan yoxlamaların dayandırılması haqqında&amp;rdquo; Azərbaycan Respublikasının Qanununa əsasən, 2015-ci il noyabrın 1-dən Azərbaycan Respublikası ərazisində sahibkarlıq sahəsində aparılan yoxlamalar 2 il m&amp;uuml;ddətinə dayandırılsa da, m&amp;uuml;əyyən məlumatlar daxil olduqda şirkətlərlə m&amp;uuml;zakirələr aparırıq. Sahibkarlarla g&amp;ouml;r&amp;uuml;ş&amp;uuml;m&amp;uuml;zdə demişdim ki, yoxlamaların dayandırılması bəzi şirkətlərdə arxayınlıq yaradıb. Lakin sahibkarlar d&amp;ouml;vlət baş&amp;ccedil;ısının onlara diqqət və qayğısına adekvat m&amp;uuml;nasibət g&amp;ouml;stərməli, qanunlara riayət etməlidirlər. Qanuna əməl etmək hər bir vətəndaşın, o c&amp;uuml;mlədən sahibkarların əsas vəzifəsidir.&lt;/p&gt;\r\n&lt;p&gt;- Əmək miqrasiyası kvotası, n&amp;ouml;vbəti il &amp;uuml;&amp;ccedil;&amp;uuml;n tələbatla bağlı işəg&amp;ouml;t&amp;uuml;rənlər sizə məlumat təqdim edibmi?&lt;/p&gt;\r\n&lt;p&gt;- Bəli, işəg&amp;ouml;t&amp;uuml;rənlər əcnəbi iş&amp;ccedil;i q&amp;uuml;vvəsinə tələbat haqqında proqnoz-məlumatı artıq təqdim ediblər. Bir daha xatırladıram ki, işəg&amp;ouml;t&amp;uuml;rənlər &amp;ldquo;Əmək miqrasiyası kvotasının m&amp;uuml;əyyən edilməsi Qaydası&amp;rdquo;na əsasən n&amp;ouml;vbəti il &amp;uuml;&amp;ccedil;&amp;uuml;n əcnəbi iş&amp;ccedil;i q&amp;uuml;vvəsinə tələbat haqqında Məşğulluq Təsnifatı &amp;uuml;zrə peşələr g&amp;ouml;stərilməklə hər il mayın 1-dək D&amp;ouml;vlət Miqrasiya Xidmətinə onlayn rejimdə proqnoz-məlumat təqdim etməlidirlər.&lt;/p&gt;\r\n&lt;p&gt;- Bəzi ekspertlər hesab edirlər ki, əcnəbi vətəndaşlarla dini kəbinlə evlənənlər sonradan ayrılanda ortada he&amp;ccedil; bir h&amp;uuml;quqi əsas olmur. &amp;Uuml;mumiyyətlə, əcnəbilərlə nikah məsələsində hansı problemlər m&amp;ouml;vcuddur?&lt;/p&gt;\r\n&lt;p&gt;- Azərbaycan Respublikasının Konstitusiyasına g&amp;ouml;rə, hər kəsin qanunla nəzərdə tutulmuş yaşa &amp;ccedil;atdıqda ailə qurmaq h&amp;uuml;ququ var. Nikah k&amp;ouml;n&amp;uuml;ll&amp;uuml; razılıq əsasında bağlanır. He&amp;ccedil; kəs zorla evləndirilə, ərə verilə bilməz.&lt;/p&gt;\r\n&lt;p&gt;&amp;ldquo;Nikaha daxil olmağa razılıq, minimal nikah yaşı və nikahların qeydə alınması haqqında&amp;rdquo; Konvensiyaya g&amp;ouml;rə, yetkinlik yaşına &amp;ccedil;atmış kişilər və qadınlar irqi, milli, yaxud dini əlamətlərə g&amp;ouml;rə he&amp;ccedil; bir məhdudiyyət qoyulmadan nikah bağlamaq və ailə qurmaq h&amp;uuml;ququna malikdir. Onlar nikah bağlayarkən, nikahda olarkən və onu pozarkən eyni h&amp;uuml;quqlardan istifadə edirlər. Nikahın hər iki tərəfin qanunam&amp;uuml;vafiq, lazımi qaydada elan edildikdən sonra nikahı rəsmiləşdirmək h&amp;uuml;ququ olan hakimiyyət orqanı n&amp;uuml;mayəndəsinin yanında və şahidlərin iştirakı ilə şəxsən bildirməli olduqları tam və azad razılıq olmadan bağlanmasına yol verilmir.&lt;/p&gt;\r\n&lt;p&gt;Azərbaycan Respublikasının Ailə Məcəlləsinə g&amp;ouml;rə də &amp;ouml;lkəmizdə nikah və ailə m&amp;uuml;nasibətlərinin h&amp;uuml;quqi baxımdan tənzimlənməsi d&amp;ouml;vlət tərəfindən həyata ke&amp;ccedil;irilir və yalnız m&amp;uuml;vafiq icra hakimiyyəti orqanında bağlanan nikah tanınır.&lt;/p&gt;\r\n&lt;p&gt;Miqrasiya Məcəlləsinə əsasən isə əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verildiyi hallardan biri Azərbaycan Respublikasının vətəndaşı ilə yaxın qohumluq m&amp;uuml;nasibətlərində olmasıdır. Məcəllədə yaxın qohumun anlayışı verilib. Ata, ana, ər (arvad), &amp;ouml;vlad, qardaş, bacı və onların &amp;ouml;vladları, baba, nənə, babanın (nənənin) atası və anası, nəvə, qayınata, qayınana, qayın, baldız yaxın qohum hesab olunurlar. Həm&amp;ccedil;inin, adı&amp;ccedil;əkilən Məcəllənin 64-c&amp;uuml; maddəsində iş icazəsinin alınması tələb olunmayan halların dairəsi m&amp;uuml;əyyən edilib. Azərbaycan Respublikasının vətəndaşı ilə nikahda olanlar, bu şərtlə ki, həmin vətəndaş Azərbaycan Respublikasının ərazisində yaşayış yeri &amp;uuml;zrə qeydiyyatda olduqda həmin maddənin təsiri altına d&amp;uuml;ş&amp;uuml;r.&lt;/p&gt;\r\n&lt;p&gt;Miqrasiya Məcəlləsinə əsasən, m&amp;uuml;vafiq icra hakimiyyəti orqanı tərəfindən təsdiq edilən təhl&amp;uuml;kəli yoluxucu xəstəliklərin siyahısında nəzərdə tutulmuş xəstəlik virusu daşıyıcısı olan əcnəbilərə və vətəndaşlığı olmayan şəxslərə, Azərbaycan vətəndaşı ilə nikahda olanlar istisna olmaqla, &amp;ouml;lkəmizin ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verilmr. B&amp;uuml;t&amp;uuml;n bunlar Azərbaycan d&amp;ouml;vlətinin humanist mahiyyətindən, ailə-nikah m&amp;uuml;nasibətlərinə verdiyi dəyərdən irəli gəlir.&lt;/p&gt;\r\n&lt;p&gt;Doğrudur, bəzən qeyd etdiyim bu imtiyazlardan bəhrələnməyə, respublikada m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə almağa g&amp;ouml;rə formal nikah m&amp;uuml;nasibətləri ilə bağlı məlumatlar daxil olur və ş&amp;uuml;bhələr &amp;uuml;&amp;ccedil;&amp;uuml;n kifayət qədər əsas olduqda qanunam&amp;uuml;vafiq araşdırmalar aparılır və tədbirlər g&amp;ouml;r&amp;uuml;l&amp;uuml;r.&lt;/p&gt;\r\n&lt;p&gt;- Azərbaycana iş&amp;ccedil;i q&amp;uuml;vvəsi kimi uşaq, yeniyetmə miqrant gətirənlər olurmu?&lt;/p&gt;\r\n&lt;p&gt;- D&amp;ouml;vlət Miqrasiya Xidməti tərəfindən belə bir hal aşkar edilməyib. Azərbaycanın Əmək Məcəlləsinin &amp;ldquo;Əcnəbilərin və vətəndaşlığı olmayan şəxslərin əmək h&amp;uuml;ququnun tənzimlənməsi&amp;rdquo; maddəsində g&amp;ouml;stərilir ki, qanunla və ya Azərbaycan Respublikasının tərəfdar &amp;ccedil;ıxdığı beynəlxalq m&amp;uuml;qavilələrlə başqa hal nəzərdə tutulmayıbsa, əcnəbilər və vətəndaşlığı olmayan şəxslər Azərbaycanda olarkən Azərbaycan Respublikasının vətəndaşları ilə bərabər b&amp;uuml;t&amp;uuml;n əmək h&amp;uuml;quqlarından istifadə edə bilər və bu h&amp;uuml;quqlara m&amp;uuml;vafiq olan vəzifələr daşıyırlar. Elə həmin məcəllənin 12-ci maddəsində də bəyan olunur ki, iş&amp;ccedil;ilərin h&amp;uuml;quqlarını pozan, əmək m&amp;uuml;qaviləsi &amp;uuml;zrə &amp;ouml;z &amp;ouml;hdəliklərini yerinə yetirməyən,15 yaşına &amp;ccedil;atmamış şəxsləri işə g&amp;ouml;t&amp;uuml;rən, uşaqları onların həyatına, sağlamlığına və ya mənəviyyatına təhl&amp;uuml;kə t&amp;ouml;rədə bilən fəaliyyətə cəlb edən işəg&amp;ouml;t&amp;uuml;rən qanunvericiliklə m&amp;uuml;əyyən olunmuş qaydada m&amp;uuml;vafiq məsuliyyətə cəlb edilir.&lt;/p&gt;\r\n&lt;p&gt;- Azərbaycana daha &amp;ccedil;ox hansı &amp;ouml;lkələrdən iş&amp;ccedil;i q&amp;uuml;vvəsi cəlb olunur?&lt;/p&gt;\r\n&lt;p&gt;- Bu il m&amp;uuml;stəqilliyinin 25 illiyini qeyd edən Azərbaycan qısa zaman kəsiyində &amp;ccedil;ox b&amp;ouml;y&amp;uuml;k iqtisadi uğurlara imza atıb. 2004-2014-c&amp;uuml; illərdə Azərbaycan iqtisadiyyatı d&amp;uuml;nyanın ən s&amp;uuml;rətlə inkişaf edən iqtisadiyyatı olub. Bu illər ərzində işsizlik kəskin şəkildə azalaraq 40 faizdən 5 faizə enib. İqtisadiyyatımız daha da rəqabətqabiliyyətli olub və Davos İqtisadi Forumunun hesablamalarına əsasən, rəqabət qabiliyyətinə g&amp;ouml;rə Azərbaycan iqtisadiyyatı d&amp;uuml;nyada 40-cı yerdədir. Bu g&amp;uuml;n Avropada həyata ke&amp;ccedil;irilən ən iri infrastruktur və enerji layihələrindən biri olan Cənub Qaz Dəhlizi layihəsinin təşəbb&amp;uuml;skarı kimi &amp;ouml;lkəmiz &amp;ccedil;ıxış edir.&lt;/p&gt;\r\n&lt;p&gt;Qeyd olunan iqtisadi uğurlar &amp;ouml;z n&amp;ouml;vbəsində &amp;ouml;lkəmizdə yaradılan ictimai-siyasi sabitliyin və təhl&amp;uuml;kəsizliyin y&amp;uuml;ksək səviyyədə təmin edilməsinin nəticəsidir. Bu da &amp;ouml;z n&amp;ouml;vbəsində &amp;ouml;lkəmizə gələn əcnəbilərin sayına təsirini g&amp;ouml;stərib. Əvvəlki illərdə &amp;ouml;lkədən gedən vətəndaşlarımızın geri d&amp;ouml;nməsi ilə yanaşı, xeyli sayda əmək&amp;ccedil;i miqrantların &amp;ouml;lkəmizə &amp;uuml;z tutmasına səbəb olub. Əmək&amp;ccedil;i miqrantların palitrası zəngindir. D&amp;uuml;nyanın bir &amp;ccedil;ox &amp;ouml;lkələrindən Azərbaycanda haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olanlar var. Onların sırasında T&amp;uuml;rkiyə, G&amp;uuml;rc&amp;uuml;stan, Rusiya, Hindistan, İran, &amp;Ccedil;in, Banqladeş, B&amp;ouml;y&amp;uuml;k Britaniya və Şimali İrlandiya Birləşmiş Krallığı, Pakistan, Ukrayna və s. &amp;ouml;lkələrin vətəndaşları &amp;ccedil;oxluq təşkil edir.&lt;/p&gt;\r\n&lt;p&gt;- Azərbaycana əcnəbi iş&amp;ccedil;i gətirən şirkətlər qanunu pozduğu təqdirdə nə qədər cərimə &amp;ouml;dəməli olurlar?&lt;/p&gt;\r\n&lt;p&gt;- İnzibati Xətalar Məcəlləsinin 578-ci maddəsinə əsasən, işəg&amp;ouml;t&amp;uuml;rən tərəfindən Azərbaycan Respublikası Miqrasiya Məcəlləsinin tələbləri pozulmaqla iş icazəsi alınmadan əcnəbinin və ya vətəndaşlığı olmayan şəxsin işə cəlb edilməsinə g&amp;ouml;rə vəzifəli şəxslər &amp;uuml;&amp;ccedil; min manatdan d&amp;ouml;rd min manatadək məbləğdə, h&amp;uuml;quqi şəxslər on beş min manatdan iyirmi min manatadək məbləğdə cərimə edilir. Eyni xəta &amp;uuml;&amp;ccedil; və ya daha &amp;ccedil;ox əcnəbi və ya vətəndaşlığı olmayan şəxs barəsində t&amp;ouml;rədildikdə vəzifəli şəxslər beş min manatdan yeddi min manatadək məbləğdə, h&amp;uuml;quqi şəxslər isə otuz min manatdan otuz beş min manatadək məbləğdə cərimə edilir. İşəg&amp;ouml;t&amp;uuml;rənin işə cəlb etdiyi əcnəbini və vətəndaşlığı olmayan şəxsi &amp;ouml;z iş yerindən kənarda işlə təmin etməsinə g&amp;ouml;rə vəzifəli şəxslər beş y&amp;uuml;z manat məbləğində cərimə edilir.&lt;/p&gt;\r\n&lt;p&gt;- Təmsil etdiyiniz qurum &quot;ASAN xidmət&quot; sektorunda hansı xidmətlərlə təmsil olunur?&lt;/p&gt;\r\n&lt;p&gt;- D&amp;ouml;vlət Miqrasiya Xidməti tərəfindən Azərbaycan brendi olan &quot;ASAN xidmət&quot; mərkəzlərində əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazənin verilməsi (m&amp;uuml;ddətinin uzadılması), daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazənin verilməsi (m&amp;uuml;ddətinin uzadılması), haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaları &amp;uuml;&amp;ccedil;&amp;uuml;n iş icazələrinin verilməsi, habelə şəxsin Azərbaycan Respublikasının vətəndaşlığına mənsubiyyətinin m&amp;uuml;əyyənləşdirilməsi ilə bağlı sorğuların qəbulu və cavablandırılması xidmətləri təqdim edilir.&lt;/p&gt;\r\n&lt;p&gt;- Şirkətlər tərəfindən vəsiqələrin g&amp;ouml;t&amp;uuml;r&amp;uuml;lməsi sahəsində hansı problemlər var və bu kimi halların aradan qaldırılması &amp;uuml;&amp;ccedil;&amp;uuml;n tədbirlər nəzərdə tutulurmu?&lt;/p&gt;\r\n&lt;p&gt;- Bəli, hazır vəsiqələrin g&amp;ouml;t&amp;uuml;r&amp;uuml;lməməsi sahəsində m&amp;uuml;əyyən problemlər olur. Azərbaycan Respublikasında m&amp;uuml;vəqqəti və ya daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə alan əcnəbilər və vətəndaşlığı olmayan şəxslərə m&amp;uuml;vəqqəti və ya daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqələri verilir. Bəzən hazır vəsiqələri vaxtında g&amp;ouml;t&amp;uuml;rməmək halları qeydə alınır. Xatırladım ki, Azərbaycan ərazisində haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq &amp;uuml;&amp;ccedil;&amp;uuml;n iş icazəsi alan əcnəbi və ya vətəndaşlığı olmayan şəxsə burada m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verilir. İş icazəsi bu icazəni almaq &amp;uuml;&amp;ccedil;&amp;uuml;n d&amp;ouml;vlət r&amp;uuml;sumunun &amp;ouml;dənilməsini təsdiq edən sənəd təqdim olunduqdan sonra işəg&amp;ouml;t&amp;uuml;rənə verilir. İşəg&amp;ouml;t&amp;uuml;rən tərəfindən 30 g&amp;uuml;n m&amp;uuml;ddətində iş icazəsinin alınmasına, eləcə də iş icazəsinin m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə d&amp;ouml;vlət r&amp;uuml;sumu &amp;ouml;dənilmədikdə, habelə əcnəbiyə və vətəndaşlığı olmayan şəxsə Azərbaycanın ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazənin verilməsindən imtina olunduqda iş icazəsi ləğv edilir.&lt;/p&gt;\r\n&lt;p&gt;- İşə cəlb edilən əcnəbinin və ya vətəndaşlığı olmayan şəxsin pasportunun işəg&amp;ouml;t&amp;uuml;rən tərəfindən g&amp;ouml;t&amp;uuml;r&amp;uuml;lərək saxlanması ilə bağlı hansı tədbirlər g&amp;ouml;r&amp;uuml;l&amp;uuml;r?&lt;/p&gt;\r\n&lt;p&gt;- Qeyd etdiyiniz əməl Azərbaycanın Cinayət Məcəlləsinin m&amp;uuml;vafiq maddəsinə əsasən cinayət məsuliyyətinə səbəb olmadıqda İnzibati Xətalar Məcəlləsinə g&amp;ouml;rə, işə cəlb edilən əcnəbinin və vətəndaşlığı olmayan şəxsin pasportunun və ya şəxsiyyətini təsdiq edən digər sənədinin işəg&amp;ouml;t&amp;uuml;rən tərəfindən g&amp;ouml;t&amp;uuml;r&amp;uuml;lərək saxlanmasına g&amp;ouml;rə - vəzifəli şəxslər beş y&amp;uuml;z manat məbləğində cərimə edilirlər.&lt;/p&gt;', 0, 2, 1, 1, 2, 0);
INSERT INTO `interviews` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(5, 'DMX rəisi: “Azərbaycandakı iqtisadi uğurlar, ictimai-siyasi sabitlik ölkəd', '', '', '', '', '&lt;p&gt;&amp;ldquo;Azərbaycana gələn əmək&amp;ccedil;i miqrantların palitrası zəngindir&amp;rdquo;&lt;/p&gt;\r\n&lt;p&gt;Azərbaycan Respublikası D&amp;ouml;vlət Miqrasiya Xidmətinin (DMX) rəisi, II dərəcəli d&amp;ouml;vlət miqrasiya xidməti m&amp;uuml;şaviri Firudin Nəbiyevin &quot;Report&quot;a m&amp;uuml;sahibəsi&lt;/p&gt;\r\n&lt;p&gt;- Firudin m&amp;uuml;əllim, bir ne&amp;ccedil;ə g&amp;uuml;n &amp;ouml;ncə sahibkarlarla g&amp;ouml;r&amp;uuml;şd&amp;uuml;n&amp;uuml;z, dinlədiniz. Onları təmsil etdiyiniz sahə ilə bağlı daha &amp;ccedil;ox hansı problemlər narahat edir?&lt;/p&gt;\r\n&lt;p&gt;- Bu, Xəzər-Avropa İnteqrasiya İşg&amp;uuml;zar Klubunun D&amp;ouml;vlət Miqrasiya Xidməti rəisinin iştirakı ilə təşkil etdiyi n&amp;ouml;vbəti biznes forum idi. Bundan əvvəl də bizim belə forumlarda sahibkarlarla, biznes sektorunun n&amp;uuml;mayəndələri ilə &amp;ccedil;oxsaylı g&amp;ouml;r&amp;uuml;şlərimiz olub. Deyə bilərəm ki, sonuncu ke&amp;ccedil;irilən biznes-forumda iş adamları, işəg&amp;ouml;t&amp;uuml;rənlərin n&amp;uuml;mayəndələri, sahibkarlar D&amp;ouml;vlət Miqrasiya Xidməti ilə bağlı hər hansı problem yox, daha &amp;ccedil;ox miqrasiya qanunvericiliyi sahəsində yeniliklər, o c&amp;uuml;mlədən &amp;ldquo;Əmək miqrasiyası kvotasının m&amp;uuml;əyyən edilməsi Qaydası&amp;rdquo;na əsasən Məşğulluq Təsnifatı &amp;uuml;zrə peşələr g&amp;ouml;stərilməklə hər il may ayının 1-dək n&amp;ouml;vbəti il &amp;uuml;&amp;ccedil;&amp;uuml;n D&amp;ouml;vlət Miqrasiya Xidmətinə əcnəbi iş&amp;ccedil;i q&amp;uuml;vvəsinə tələbat haqqında onlayn rejimdə təqdim etməli olduqları proqnoz-məlumat barədə suallar səsləndirdilər və bəzi təkliflər irəli s&amp;uuml;rd&amp;uuml;lər. Biz onlayn rejimdə proqnoz-məlumatın təqdim edilməsinin yeni olduğunu nəzərə alaraq həmin m&amp;uuml;ddəti bu il mayın 15-nə kimi artırmışdıq.&lt;/p&gt;\r\n&lt;p&gt;DMX hər zaman &amp;ouml;zəl və biznes sektora diqqət və qayğı g&amp;ouml;stərir, onun problemləri ilə maraqlanır, təhlil edir və m&amp;uuml;vafiq həll yollarını tapır.&lt;/p&gt;\r\n&lt;p&gt;&amp;Ccedil;ox sevindirici haldır ki, artıq biznes sektorunu D&amp;ouml;vlət Miqrasiya Xidməti ilə bağlı narahat edən problem yoxdur. Biz sadəcə onların m&amp;uuml;xtəlif məzmunlu təkliflərini dinləmişik, təhlillər aparılır. Gələcəkdə normativ h&amp;uuml;quqi aktlarda həmin təkliflərin m&amp;uuml;mk&amp;uuml;n qədər əksini tapması &amp;uuml;&amp;ccedil;&amp;uuml;n xidmətin m&amp;uuml;vafiq struktur b&amp;ouml;lmələrinə tapşırıqlar verilib.&lt;/p&gt;\r\n&lt;p&gt;- D&amp;ouml;vlət Miqrasiya Xidmətini sahibkarların yaratdığı hansı problemlər daha &amp;ccedil;ox narahat edir?&lt;/p&gt;\r\n&lt;p&gt;- Miqrasiya Məcəlləsinə g&amp;ouml;rə, Azərbaycan Respublikasının ərazisində sahibkarlıq fəaliyyəti ilə məşğul olan (həmin şəxslər azı 5 nəfər ilə tam iş vaxtı şəraitində və ya 10 nəfər ilə natamam iş vaxtı şəraitində əmək m&amp;uuml;qaviləsi bağlamaqla faktiki fəaliyyət g&amp;ouml;stərdikləri və iş&amp;ccedil;ilərinin ən azı 80 faizi Azərbaycan Respublikasının vətəndaşları olduqları halda), o c&amp;uuml;mlədən xarici h&amp;uuml;quqi şəxsin Azərbaycan Respublikasındakı filial və n&amp;uuml;mayəndəliyinin rəhbərləri və onların m&amp;uuml;avinləri olan əcnəbilər və vətəndaşlığı olmayan şəxslər &amp;uuml;&amp;ccedil;&amp;uuml;n iş icazəsinin alınması tələb olunmur. Həmin forumda da mən bildirdim ki, &amp;ouml;lkədə fəaliyyət g&amp;ouml;stərən xarici bir şirkətdə 50-60 iş&amp;ccedil;i olduğu halda, 12-14 m&amp;uuml;avin vəzifəsi var. Biz xarici h&amp;uuml;quqi şəxsin Azərbaycan Respublikasındakı filial və n&amp;uuml;mayəndəliyinin m&amp;uuml;dir m&amp;uuml;avinləri vəzifələrinin 3-4-ə endirilməsini təklif etmişik.&lt;/p&gt;\r\n&lt;p&gt;Bir n&amp;uuml;munəni deyim ki, bir ne&amp;ccedil;ə il əvvəl BP şirkəti və SOCAR arasında imzalanan m&amp;uuml;qaviləyə əsasən, azərbaycanlı m&amp;uuml;təxəssislər hazırlanır. Hazırda bu sektorda &amp;ccedil;alışan əcnəbilərin sayı 10 faizə enib. B&amp;uuml;t&amp;uuml;n sahələrdə bu istiqamətdə iş aparılmalı, azərbaycanlı m&amp;uuml;təxəssislər hazırlanmalıdır.&lt;/p&gt;\r\n&lt;p&gt;Digər tərəfdən, ixtisara d&amp;uuml;şən Azərbaycan vətəndaşlarının yerinə əcnəbilər təyin olunur ki, bu da bizi narahat edir, belə hallara yol vermək olmaz. Məqsədimiz odur ki, daxili əmək bazarını qoruyaq və ilk n&amp;ouml;vbədə yerli məhsuldar q&amp;uuml;vvələr nəzərə alınsın, Azərbaycan vətəndaşları işlə təmin olunsun. Prezident İlham Əliyevin vurğuladığı kimi respublikanın iqtisadi inkişafından ilk n&amp;ouml;vbədə Azərbaycan vətəndaşları bəhrələnməlidirlər. S&amp;ouml;z yox ki, iş yerinin tələblərinə cavab verən peşə hazırlığına və ixtisasa malik Azərbaycan vətəndaşının iddia etmədiyi boş iş yerlərinin m&amp;ouml;vcudluğu, məşğulluq xidməti orqanlarının işəg&amp;ouml;t&amp;uuml;rənlərin iş&amp;ccedil;i q&amp;uuml;vvəsinə olan ehtiyaclarını yerli əmək ehtiyatları hesabına təmin etmək imkanın olmadığı hallar, habelə y&amp;uuml;ksək ixtisaslı əcnəbi m&amp;uuml;təxəssislərin cəlb edilməsi zəruriliyi nəzərə alınır və əmək miqrasiyası kvotası &amp;ccedil;ər&amp;ccedil;ivəsində qanuna m&amp;uuml;vafiq qaydada, yəni işəg&amp;ouml;t&amp;uuml;rənlər tərəfindən iş icazəsi alınmaqla əcnəbi iş&amp;ccedil;i q&amp;uuml;vvəsindən istifadə edilməsi məsələsi həll edilir. Bilirsiniz ki, Azərbaycan Respublikasında sahibkarlığın inkişafının stimullaşdırılması məqsədi ilə qəbul olunan &amp;ldquo;Sahibkarlıq sahəsində aparılan yoxlamaların dayandırılması haqqında&amp;rdquo; Azərbaycan Respublikasının Qanununa əsasən, 2015-ci il noyabrın 1-dən Azərbaycan Respublikası ərazisində sahibkarlıq sahəsində aparılan yoxlamalar 2 il m&amp;uuml;ddətinə dayandırılsa da, m&amp;uuml;əyyən məlumatlar daxil olduqda şirkətlərlə m&amp;uuml;zakirələr aparırıq. Sahibkarlarla g&amp;ouml;r&amp;uuml;ş&amp;uuml;m&amp;uuml;zdə demişdim ki, yoxlamaların dayandırılması bəzi şirkətlərdə arxayınlıq yaradıb. Lakin sahibkarlar d&amp;ouml;vlət baş&amp;ccedil;ısının onlara diqqət və qayğısına adekvat m&amp;uuml;nasibət g&amp;ouml;stərməli, qanunlara riayət etməlidirlər. Qanuna əməl etmək hər bir vətəndaşın, o c&amp;uuml;mlədən sahibkarların əsas vəzifəsidir.&lt;/p&gt;\r\n&lt;p&gt;- Əmək miqrasiyası kvotası, n&amp;ouml;vbəti il &amp;uuml;&amp;ccedil;&amp;uuml;n tələbatla bağlı işəg&amp;ouml;t&amp;uuml;rənlər sizə məlumat təqdim edibmi?&lt;/p&gt;\r\n&lt;p&gt;- Bəli, işəg&amp;ouml;t&amp;uuml;rənlər əcnəbi iş&amp;ccedil;i q&amp;uuml;vvəsinə tələbat haqqında proqnoz-məlumatı artıq təqdim ediblər. Bir daha xatırladıram ki, işəg&amp;ouml;t&amp;uuml;rənlər &amp;ldquo;Əmək miqrasiyası kvotasının m&amp;uuml;əyyən edilməsi Qaydası&amp;rdquo;na əsasən n&amp;ouml;vbəti il &amp;uuml;&amp;ccedil;&amp;uuml;n əcnəbi iş&amp;ccedil;i q&amp;uuml;vvəsinə tələbat haqqında Məşğulluq Təsnifatı &amp;uuml;zrə peşələr g&amp;ouml;stərilməklə hər il mayın 1-dək D&amp;ouml;vlət Miqrasiya Xidmətinə onlayn rejimdə proqnoz-məlumat təqdim etməlidirlər.&lt;/p&gt;\r\n&lt;p&gt;- Bəzi ekspertlər hesab edirlər ki, əcnəbi vətəndaşlarla dini kəbinlə evlənənlər sonradan ayrılanda ortada he&amp;ccedil; bir h&amp;uuml;quqi əsas olmur. &amp;Uuml;mumiyyətlə, əcnəbilərlə nikah məsələsində hansı problemlər m&amp;ouml;vcuddur?&lt;/p&gt;\r\n&lt;p&gt;- Azərbaycan Respublikasının Konstitusiyasına g&amp;ouml;rə, hər kəsin qanunla nəzərdə tutulmuş yaşa &amp;ccedil;atdıqda ailə qurmaq h&amp;uuml;ququ var. Nikah k&amp;ouml;n&amp;uuml;ll&amp;uuml; razılıq əsasında bağlanır. He&amp;ccedil; kəs zorla evləndirilə, ərə verilə bilməz.&lt;/p&gt;\r\n&lt;p&gt;&amp;ldquo;Nikaha daxil olmağa razılıq, minimal nikah yaşı və nikahların qeydə alınması haqqında&amp;rdquo; Konvensiyaya g&amp;ouml;rə, yetkinlik yaşına &amp;ccedil;atmış kişilər və qadınlar irqi, milli, yaxud dini əlamətlərə g&amp;ouml;rə he&amp;ccedil; bir məhdudiyyət qoyulmadan nikah bağlamaq və ailə qurmaq h&amp;uuml;ququna malikdir. Onlar nikah bağlayarkən, nikahda olarkən və onu pozarkən eyni h&amp;uuml;quqlardan istifadə edirlər. Nikahın hər iki tərəfin qanunam&amp;uuml;vafiq, lazımi qaydada elan edildikdən sonra nikahı rəsmiləşdirmək h&amp;uuml;ququ olan hakimiyyət orqanı n&amp;uuml;mayəndəsinin yanında və şahidlərin iştirakı ilə şəxsən bildirməli olduqları tam və azad razılıq olmadan bağlanmasına yol verilmir.&lt;/p&gt;\r\n&lt;p&gt;Azərbaycan Respublikasının Ailə Məcəlləsinə g&amp;ouml;rə də &amp;ouml;lkəmizdə nikah və ailə m&amp;uuml;nasibətlərinin h&amp;uuml;quqi baxımdan tənzimlənməsi d&amp;ouml;vlət tərəfindən həyata ke&amp;ccedil;irilir və yalnız m&amp;uuml;vafiq icra hakimiyyəti orqanında bağlanan nikah tanınır.&lt;/p&gt;\r\n&lt;p&gt;Miqrasiya Məcəlləsinə əsasən isə əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verildiyi hallardan biri Azərbaycan Respublikasının vətəndaşı ilə yaxın qohumluq m&amp;uuml;nasibətlərində olmasıdır. Məcəllədə yaxın qohumun anlayışı verilib. Ata, ana, ər (arvad), &amp;ouml;vlad, qardaş, bacı və onların &amp;ouml;vladları, baba, nənə, babanın (nənənin) atası və anası, nəvə, qayınata, qayınana, qayın, baldız yaxın qohum hesab olunurlar. Həm&amp;ccedil;inin, adı&amp;ccedil;əkilən Məcəllənin 64-c&amp;uuml; maddəsində iş icazəsinin alınması tələb olunmayan halların dairəsi m&amp;uuml;əyyən edilib. Azərbaycan Respublikasının vətəndaşı ilə nikahda olanlar, bu şərtlə ki, həmin vətəndaş Azərbaycan Respublikasının ərazisində yaşayış yeri &amp;uuml;zrə qeydiyyatda olduqda həmin maddənin təsiri altına d&amp;uuml;ş&amp;uuml;r.&lt;/p&gt;\r\n&lt;p&gt;Miqrasiya Məcəlləsinə əsasən, m&amp;uuml;vafiq icra hakimiyyəti orqanı tərəfindən təsdiq edilən təhl&amp;uuml;kəli yoluxucu xəstəliklərin siyahısında nəzərdə tutulmuş xəstəlik virusu daşıyıcısı olan əcnəbilərə və vətəndaşlığı olmayan şəxslərə, Azərbaycan vətəndaşı ilə nikahda olanlar istisna olmaqla, &amp;ouml;lkəmizin ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verilmr. B&amp;uuml;t&amp;uuml;n bunlar Azərbaycan d&amp;ouml;vlətinin humanist mahiyyətindən, ailə-nikah m&amp;uuml;nasibətlərinə verdiyi dəyərdən irəli gəlir.&lt;/p&gt;\r\n&lt;p&gt;Doğrudur, bəzən qeyd etdiyim bu imtiyazlardan bəhrələnməyə, respublikada m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə almağa g&amp;ouml;rə formal nikah m&amp;uuml;nasibətləri ilə bağlı məlumatlar daxil olur və ş&amp;uuml;bhələr &amp;uuml;&amp;ccedil;&amp;uuml;n kifayət qədər əsas olduqda qanunam&amp;uuml;vafiq araşdırmalar aparılır və tədbirlər g&amp;ouml;r&amp;uuml;l&amp;uuml;r.&lt;/p&gt;\r\n&lt;p&gt;- Azərbaycana iş&amp;ccedil;i q&amp;uuml;vvəsi kimi uşaq, yeniyetmə miqrant gətirənlər olurmu?&lt;/p&gt;\r\n&lt;p&gt;- D&amp;ouml;vlət Miqrasiya Xidməti tərəfindən belə bir hal aşkar edilməyib. Azərbaycanın Əmək Məcəlləsinin &amp;ldquo;Əcnəbilərin və vətəndaşlığı olmayan şəxslərin əmək h&amp;uuml;ququnun tənzimlənməsi&amp;rdquo; maddəsində g&amp;ouml;stərilir ki, qanunla və ya Azərbaycan Respublikasının tərəfdar &amp;ccedil;ıxdığı beynəlxalq m&amp;uuml;qavilələrlə başqa hal nəzərdə tutulmayıbsa, əcnəbilər və vətəndaşlığı olmayan şəxslər Azərbaycanda olarkən Azərbaycan Respublikasının vətəndaşları ilə bərabər b&amp;uuml;t&amp;uuml;n əmək h&amp;uuml;quqlarından istifadə edə bilər və bu h&amp;uuml;quqlara m&amp;uuml;vafiq olan vəzifələr daşıyırlar. Elə həmin məcəllənin 12-ci maddəsində də bəyan olunur ki, iş&amp;ccedil;ilərin h&amp;uuml;quqlarını pozan, əmək m&amp;uuml;qaviləsi &amp;uuml;zrə &amp;ouml;z &amp;ouml;hdəliklərini yerinə yetirməyən,15 yaşına &amp;ccedil;atmamış şəxsləri işə g&amp;ouml;t&amp;uuml;rən, uşaqları onların həyatına, sağlamlığına və ya mənəviyyatına təhl&amp;uuml;kə t&amp;ouml;rədə bilən fəaliyyətə cəlb edən işəg&amp;ouml;t&amp;uuml;rən qanunvericiliklə m&amp;uuml;əyyən olunmuş qaydada m&amp;uuml;vafiq məsuliyyətə cəlb edilir.&lt;/p&gt;\r\n&lt;p&gt;- Azərbaycana daha &amp;ccedil;ox hansı &amp;ouml;lkələrdən iş&amp;ccedil;i q&amp;uuml;vvəsi cəlb olunur?&lt;/p&gt;\r\n&lt;p&gt;- Bu il m&amp;uuml;stəqilliyinin 25 illiyini qeyd edən Azərbaycan qısa zaman kəsiyində &amp;ccedil;ox b&amp;ouml;y&amp;uuml;k iqtisadi uğurlara imza atıb. 2004-2014-c&amp;uuml; illərdə Azərbaycan iqtisadiyyatı d&amp;uuml;nyanın ən s&amp;uuml;rətlə inkişaf edən iqtisadiyyatı olub. Bu illər ərzində işsizlik kəskin şəkildə azalaraq 40 faizdən 5 faizə enib. İqtisadiyyatımız daha da rəqabətqabiliyyətli olub və Davos İqtisadi Forumunun hesablamalarına əsasən, rəqabət qabiliyyətinə g&amp;ouml;rə Azərbaycan iqtisadiyyatı d&amp;uuml;nyada 40-cı yerdədir. Bu g&amp;uuml;n Avropada həyata ke&amp;ccedil;irilən ən iri infrastruktur və enerji layihələrindən biri olan Cənub Qaz Dəhlizi layihəsinin təşəbb&amp;uuml;skarı kimi &amp;ouml;lkəmiz &amp;ccedil;ıxış edir.&lt;/p&gt;\r\n&lt;p&gt;Qeyd olunan iqtisadi uğurlar &amp;ouml;z n&amp;ouml;vbəsində &amp;ouml;lkəmizdə yaradılan ictimai-siyasi sabitliyin və təhl&amp;uuml;kəsizliyin y&amp;uuml;ksək səviyyədə təmin edilməsinin nəticəsidir. Bu da &amp;ouml;z n&amp;ouml;vbəsində &amp;ouml;lkəmizə gələn əcnəbilərin sayına təsirini g&amp;ouml;stərib. Əvvəlki illərdə &amp;ouml;lkədən gedən vətəndaşlarımızın geri d&amp;ouml;nməsi ilə yanaşı, xeyli sayda əmək&amp;ccedil;i miqrantların &amp;ouml;lkəmizə &amp;uuml;z tutmasına səbəb olub. Əmək&amp;ccedil;i miqrantların palitrası zəngindir. D&amp;uuml;nyanın bir &amp;ccedil;ox &amp;ouml;lkələrindən Azərbaycanda haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olanlar var. Onların sırasında T&amp;uuml;rkiyə, G&amp;uuml;rc&amp;uuml;stan, Rusiya, Hindistan, İran, &amp;Ccedil;in, Banqladeş, B&amp;ouml;y&amp;uuml;k Britaniya və Şimali İrlandiya Birləşmiş Krallığı, Pakistan, Ukrayna və s. &amp;ouml;lkələrin vətəndaşları &amp;ccedil;oxluq təşkil edir.&lt;/p&gt;\r\n&lt;p&gt;- Azərbaycana əcnəbi iş&amp;ccedil;i gətirən şirkətlər qanunu pozduğu təqdirdə nə qədər cərimə &amp;ouml;dəməli olurlar?&lt;/p&gt;\r\n&lt;p&gt;- İnzibati Xətalar Məcəlləsinin 578-ci maddəsinə əsasən, işəg&amp;ouml;t&amp;uuml;rən tərəfindən Azərbaycan Respublikası Miqrasiya Məcəlləsinin tələbləri pozulmaqla iş icazəsi alınmadan əcnəbinin və ya vətəndaşlığı olmayan şəxsin işə cəlb edilməsinə g&amp;ouml;rə vəzifəli şəxslər &amp;uuml;&amp;ccedil; min manatdan d&amp;ouml;rd min manatadək məbləğdə, h&amp;uuml;quqi şəxslər on beş min manatdan iyirmi min manatadək məbləğdə cərimə edilir. Eyni xəta &amp;uuml;&amp;ccedil; və ya daha &amp;ccedil;ox əcnəbi və ya vətəndaşlığı olmayan şəxs barəsində t&amp;ouml;rədildikdə vəzifəli şəxslər beş min manatdan yeddi min manatadək məbləğdə, h&amp;uuml;quqi şəxslər isə otuz min manatdan otuz beş min manatadək məbləğdə cərimə edilir. İşəg&amp;ouml;t&amp;uuml;rənin işə cəlb etdiyi əcnəbini və vətəndaşlığı olmayan şəxsi &amp;ouml;z iş yerindən kənarda işlə təmin etməsinə g&amp;ouml;rə vəzifəli şəxslər beş y&amp;uuml;z manat məbləğində cərimə edilir.&lt;/p&gt;\r\n&lt;p&gt;- Təmsil etdiyiniz qurum &quot;ASAN xidmət&quot; sektorunda hansı xidmətlərlə təmsil olunur?&lt;/p&gt;\r\n&lt;p&gt;- D&amp;ouml;vlət Miqrasiya Xidməti tərəfindən Azərbaycan brendi olan &quot;ASAN xidmət&quot; mərkəzlərində əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazənin verilməsi (m&amp;uuml;ddətinin uzadılması), daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazənin verilməsi (m&amp;uuml;ddətinin uzadılması), haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaları &amp;uuml;&amp;ccedil;&amp;uuml;n iş icazələrinin verilməsi, habelə şəxsin Azərbaycan Respublikasının vətəndaşlığına mənsubiyyətinin m&amp;uuml;əyyənləşdirilməsi ilə bağlı sorğuların qəbulu və cavablandırılması xidmətləri təqdim edilir.&lt;/p&gt;\r\n&lt;p&gt;- Şirkətlər tərəfindən vəsiqələrin g&amp;ouml;t&amp;uuml;r&amp;uuml;lməsi sahəsində hansı problemlər var və bu kimi halların aradan qaldırılması &amp;uuml;&amp;ccedil;&amp;uuml;n tədbirlər nəzərdə tutulurmu?&lt;/p&gt;\r\n&lt;p&gt;- Bəli, hazır vəsiqələrin g&amp;ouml;t&amp;uuml;r&amp;uuml;lməməsi sahəsində m&amp;uuml;əyyən problemlər olur. Azərbaycan Respublikasında m&amp;uuml;vəqqəti və ya daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə alan əcnəbilər və vətəndaşlığı olmayan şəxslərə m&amp;uuml;vəqqəti və ya daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqələri verilir. Bəzən hazır vəsiqələri vaxtında g&amp;ouml;t&amp;uuml;rməmək halları qeydə alınır. Xatırladım ki, Azərbaycan ərazisində haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq &amp;uuml;&amp;ccedil;&amp;uuml;n iş icazəsi alan əcnəbi və ya vətəndaşlığı olmayan şəxsə burada m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verilir. İş icazəsi bu icazəni almaq &amp;uuml;&amp;ccedil;&amp;uuml;n d&amp;ouml;vlət r&amp;uuml;sumunun &amp;ouml;dənilməsini təsdiq edən sənəd təqdim olunduqdan sonra işəg&amp;ouml;t&amp;uuml;rənə verilir. İşəg&amp;ouml;t&amp;uuml;rən tərəfindən 30 g&amp;uuml;n m&amp;uuml;ddətində iş icazəsinin alınmasına, eləcə də iş icazəsinin m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə d&amp;ouml;vlət r&amp;uuml;sumu &amp;ouml;dənilmədikdə, habelə əcnəbiyə və vətəndaşlığı olmayan şəxsə Azərbaycanın ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazənin verilməsindən imtina olunduqda iş icazəsi ləğv edilir.&lt;/p&gt;\r\n&lt;p&gt;- İşə cəlb edilən əcnəbinin və ya vətəndaşlığı olmayan şəxsin pasportunun işəg&amp;ouml;t&amp;uuml;rən tərəfindən g&amp;ouml;t&amp;uuml;r&amp;uuml;lərək saxlanması ilə bağlı hansı tədbirlər g&amp;ouml;r&amp;uuml;l&amp;uuml;r?&lt;/p&gt;\r\n&lt;p&gt;- Qeyd etdiyiniz əməl Azərbaycanın Cinayət Məcəlləsinin m&amp;uuml;vafiq maddəsinə əsasən cinayət məsuliyyətinə səbəb olmadıqda İnzibati Xətalar Məcəlləsinə g&amp;ouml;rə, işə cəlb edilən əcnəbinin və vətəndaşlığı olmayan şəxsin pasportunun və ya şəxsiyyətini təsdiq edən digər sənədinin işəg&amp;ouml;t&amp;uuml;rən tərəfindən g&amp;ouml;t&amp;uuml;r&amp;uuml;lərək saxlanmasına g&amp;ouml;rə - vəzifəli şəxslər beş y&amp;uuml;z manat məbləğində cərimə edilirlər.&lt;/p&gt;', 0, 2, 1, 2, 2, 0),
(6, 'DMX rəisi: “Azərbaycandakı iqtisadi uğurlar, ictimai-siyasi sabitlik ölkəd', '', '', '', '', '&lt;p&gt;&amp;ldquo;Azərbaycana gələn əmək&amp;ccedil;i miqrantların palitrası zəngindir&amp;rdquo;&lt;/p&gt;\r\n&lt;p&gt;Azərbaycan Respublikası D&amp;ouml;vlət Miqrasiya Xidmətinin (DMX) rəisi, II dərəcəli d&amp;ouml;vlət miqrasiya xidməti m&amp;uuml;şaviri Firudin Nəbiyevin &quot;Report&quot;a m&amp;uuml;sahibəsi&lt;/p&gt;\r\n&lt;p&gt;- Firudin m&amp;uuml;əllim, bir ne&amp;ccedil;ə g&amp;uuml;n &amp;ouml;ncə sahibkarlarla g&amp;ouml;r&amp;uuml;şd&amp;uuml;n&amp;uuml;z, dinlədiniz. Onları təmsil etdiyiniz sahə ilə bağlı daha &amp;ccedil;ox hansı problemlər narahat edir?&lt;/p&gt;\r\n&lt;p&gt;- Bu, Xəzər-Avropa İnteqrasiya İşg&amp;uuml;zar Klubunun D&amp;ouml;vlət Miqrasiya Xidməti rəisinin iştirakı ilə təşkil etdiyi n&amp;ouml;vbəti biznes forum idi. Bundan əvvəl də bizim belə forumlarda sahibkarlarla, biznes sektorunun n&amp;uuml;mayəndələri ilə &amp;ccedil;oxsaylı g&amp;ouml;r&amp;uuml;şlərimiz olub. Deyə bilərəm ki, sonuncu ke&amp;ccedil;irilən biznes-forumda iş adamları, işəg&amp;ouml;t&amp;uuml;rənlərin n&amp;uuml;mayəndələri, sahibkarlar D&amp;ouml;vlət Miqrasiya Xidməti ilə bağlı hər hansı problem yox, daha &amp;ccedil;ox miqrasiya qanunvericiliyi sahəsində yeniliklər, o c&amp;uuml;mlədən &amp;ldquo;Əmək miqrasiyası kvotasının m&amp;uuml;əyyən edilməsi Qaydası&amp;rdquo;na əsasən Məşğulluq Təsnifatı &amp;uuml;zrə peşələr g&amp;ouml;stərilməklə hər il may ayının 1-dək n&amp;ouml;vbəti il &amp;uuml;&amp;ccedil;&amp;uuml;n D&amp;ouml;vlət Miqrasiya Xidmətinə əcnəbi iş&amp;ccedil;i q&amp;uuml;vvəsinə tələbat haqqında onlayn rejimdə təqdim etməli olduqları proqnoz-məlumat barədə suallar səsləndirdilər və bəzi təkliflər irəli s&amp;uuml;rd&amp;uuml;lər. Biz onlayn rejimdə proqnoz-məlumatın təqdim edilməsinin yeni olduğunu nəzərə alaraq həmin m&amp;uuml;ddəti bu il mayın 15-nə kimi artırmışdıq.&lt;/p&gt;\r\n&lt;p&gt;DMX hər zaman &amp;ouml;zəl və biznes sektora diqqət və qayğı g&amp;ouml;stərir, onun problemləri ilə maraqlanır, təhlil edir və m&amp;uuml;vafiq həll yollarını tapır.&lt;/p&gt;\r\n&lt;p&gt;&amp;Ccedil;ox sevindirici haldır ki, artıq biznes sektorunu D&amp;ouml;vlət Miqrasiya Xidməti ilə bağlı narahat edən problem yoxdur. Biz sadəcə onların m&amp;uuml;xtəlif məzmunlu təkliflərini dinləmişik, təhlillər aparılır. Gələcəkdə normativ h&amp;uuml;quqi aktlarda həmin təkliflərin m&amp;uuml;mk&amp;uuml;n qədər əksini tapması &amp;uuml;&amp;ccedil;&amp;uuml;n xidmətin m&amp;uuml;vafiq struktur b&amp;ouml;lmələrinə tapşırıqlar verilib.&lt;/p&gt;\r\n&lt;p&gt;- D&amp;ouml;vlət Miqrasiya Xidmətini sahibkarların yaratdığı hansı problemlər daha &amp;ccedil;ox narahat edir?&lt;/p&gt;\r\n&lt;p&gt;- Miqrasiya Məcəlləsinə g&amp;ouml;rə, Azərbaycan Respublikasının ərazisində sahibkarlıq fəaliyyəti ilə məşğul olan (həmin şəxslər azı 5 nəfər ilə tam iş vaxtı şəraitində və ya 10 nəfər ilə natamam iş vaxtı şəraitində əmək m&amp;uuml;qaviləsi bağlamaqla faktiki fəaliyyət g&amp;ouml;stərdikləri və iş&amp;ccedil;ilərinin ən azı 80 faizi Azərbaycan Respublikasının vətəndaşları olduqları halda), o c&amp;uuml;mlədən xarici h&amp;uuml;quqi şəxsin Azərbaycan Respublikasındakı filial və n&amp;uuml;mayəndəliyinin rəhbərləri və onların m&amp;uuml;avinləri olan əcnəbilər və vətəndaşlığı olmayan şəxslər &amp;uuml;&amp;ccedil;&amp;uuml;n iş icazəsinin alınması tələb olunmur. Həmin forumda da mən bildirdim ki, &amp;ouml;lkədə fəaliyyət g&amp;ouml;stərən xarici bir şirkətdə 50-60 iş&amp;ccedil;i olduğu halda, 12-14 m&amp;uuml;avin vəzifəsi var. Biz xarici h&amp;uuml;quqi şəxsin Azərbaycan Respublikasındakı filial və n&amp;uuml;mayəndəliyinin m&amp;uuml;dir m&amp;uuml;avinləri vəzifələrinin 3-4-ə endirilməsini təklif etmişik.&lt;/p&gt;\r\n&lt;p&gt;Bir n&amp;uuml;munəni deyim ki, bir ne&amp;ccedil;ə il əvvəl BP şirkəti və SOCAR arasında imzalanan m&amp;uuml;qaviləyə əsasən, azərbaycanlı m&amp;uuml;təxəssislər hazırlanır. Hazırda bu sektorda &amp;ccedil;alışan əcnəbilərin sayı 10 faizə enib. B&amp;uuml;t&amp;uuml;n sahələrdə bu istiqamətdə iş aparılmalı, azərbaycanlı m&amp;uuml;təxəssislər hazırlanmalıdır.&lt;/p&gt;\r\n&lt;p&gt;Digər tərəfdən, ixtisara d&amp;uuml;şən Azərbaycan vətəndaşlarının yerinə əcnəbilər təyin olunur ki, bu da bizi narahat edir, belə hallara yol vermək olmaz. Məqsədimiz odur ki, daxili əmək bazarını qoruyaq və ilk n&amp;ouml;vbədə yerli məhsuldar q&amp;uuml;vvələr nəzərə alınsın, Azərbaycan vətəndaşları işlə təmin olunsun. Prezident İlham Əliyevin vurğuladığı kimi respublikanın iqtisadi inkişafından ilk n&amp;ouml;vbədə Azərbaycan vətəndaşları bəhrələnməlidirlər. S&amp;ouml;z yox ki, iş yerinin tələblərinə cavab verən peşə hazırlığına və ixtisasa malik Azərbaycan vətəndaşının iddia etmədiyi boş iş yerlərinin m&amp;ouml;vcudluğu, məşğulluq xidməti orqanlarının işəg&amp;ouml;t&amp;uuml;rənlərin iş&amp;ccedil;i q&amp;uuml;vvəsinə olan ehtiyaclarını yerli əmək ehtiyatları hesabına təmin etmək imkanın olmadığı hallar, habelə y&amp;uuml;ksək ixtisaslı əcnəbi m&amp;uuml;təxəssislərin cəlb edilməsi zəruriliyi nəzərə alınır və əmək miqrasiyası kvotası &amp;ccedil;ər&amp;ccedil;ivəsində qanuna m&amp;uuml;vafiq qaydada, yəni işəg&amp;ouml;t&amp;uuml;rənlər tərəfindən iş icazəsi alınmaqla əcnəbi iş&amp;ccedil;i q&amp;uuml;vvəsindən istifadə edilməsi məsələsi həll edilir. Bilirsiniz ki, Azərbaycan Respublikasında sahibkarlığın inkişafının stimullaşdırılması məqsədi ilə qəbul olunan &amp;ldquo;Sahibkarlıq sahəsində aparılan yoxlamaların dayandırılması haqqında&amp;rdquo; Azərbaycan Respublikasının Qanununa əsasən, 2015-ci il noyabrın 1-dən Azərbaycan Respublikası ərazisində sahibkarlıq sahəsində aparılan yoxlamalar 2 il m&amp;uuml;ddətinə dayandırılsa da, m&amp;uuml;əyyən məlumatlar daxil olduqda şirkətlərlə m&amp;uuml;zakirələr aparırıq. Sahibkarlarla g&amp;ouml;r&amp;uuml;ş&amp;uuml;m&amp;uuml;zdə demişdim ki, yoxlamaların dayandırılması bəzi şirkətlərdə arxayınlıq yaradıb. Lakin sahibkarlar d&amp;ouml;vlət baş&amp;ccedil;ısının onlara diqqət və qayğısına adekvat m&amp;uuml;nasibət g&amp;ouml;stərməli, qanunlara riayət etməlidirlər. Qanuna əməl etmək hər bir vətəndaşın, o c&amp;uuml;mlədən sahibkarların əsas vəzifəsidir.&lt;/p&gt;\r\n&lt;p&gt;- Əmək miqrasiyası kvotası, n&amp;ouml;vbəti il &amp;uuml;&amp;ccedil;&amp;uuml;n tələbatla bağlı işəg&amp;ouml;t&amp;uuml;rənlər sizə məlumat təqdim edibmi?&lt;/p&gt;\r\n&lt;p&gt;- Bəli, işəg&amp;ouml;t&amp;uuml;rənlər əcnəbi iş&amp;ccedil;i q&amp;uuml;vvəsinə tələbat haqqında proqnoz-məlumatı artıq təqdim ediblər. Bir daha xatırladıram ki, işəg&amp;ouml;t&amp;uuml;rənlər &amp;ldquo;Əmək miqrasiyası kvotasının m&amp;uuml;əyyən edilməsi Qaydası&amp;rdquo;na əsasən n&amp;ouml;vbəti il &amp;uuml;&amp;ccedil;&amp;uuml;n əcnəbi iş&amp;ccedil;i q&amp;uuml;vvəsinə tələbat haqqında Məşğulluq Təsnifatı &amp;uuml;zrə peşələr g&amp;ouml;stərilməklə hər il mayın 1-dək D&amp;ouml;vlət Miqrasiya Xidmətinə onlayn rejimdə proqnoz-məlumat təqdim etməlidirlər.&lt;/p&gt;\r\n&lt;p&gt;- Bəzi ekspertlər hesab edirlər ki, əcnəbi vətəndaşlarla dini kəbinlə evlənənlər sonradan ayrılanda ortada he&amp;ccedil; bir h&amp;uuml;quqi əsas olmur. &amp;Uuml;mumiyyətlə, əcnəbilərlə nikah məsələsində hansı problemlər m&amp;ouml;vcuddur?&lt;/p&gt;\r\n&lt;p&gt;- Azərbaycan Respublikasının Konstitusiyasına g&amp;ouml;rə, hər kəsin qanunla nəzərdə tutulmuş yaşa &amp;ccedil;atdıqda ailə qurmaq h&amp;uuml;ququ var. Nikah k&amp;ouml;n&amp;uuml;ll&amp;uuml; razılıq əsasında bağlanır. He&amp;ccedil; kəs zorla evləndirilə, ərə verilə bilməz.&lt;/p&gt;\r\n&lt;p&gt;&amp;ldquo;Nikaha daxil olmağa razılıq, minimal nikah yaşı və nikahların qeydə alınması haqqında&amp;rdquo; Konvensiyaya g&amp;ouml;rə, yetkinlik yaşına &amp;ccedil;atmış kişilər və qadınlar irqi, milli, yaxud dini əlamətlərə g&amp;ouml;rə he&amp;ccedil; bir məhdudiyyət qoyulmadan nikah bağlamaq və ailə qurmaq h&amp;uuml;ququna malikdir. Onlar nikah bağlayarkən, nikahda olarkən və onu pozarkən eyni h&amp;uuml;quqlardan istifadə edirlər. Nikahın hər iki tərəfin qanunam&amp;uuml;vafiq, lazımi qaydada elan edildikdən sonra nikahı rəsmiləşdirmək h&amp;uuml;ququ olan hakimiyyət orqanı n&amp;uuml;mayəndəsinin yanında və şahidlərin iştirakı ilə şəxsən bildirməli olduqları tam və azad razılıq olmadan bağlanmasına yol verilmir.&lt;/p&gt;\r\n&lt;p&gt;Azərbaycan Respublikasının Ailə Məcəlləsinə g&amp;ouml;rə də &amp;ouml;lkəmizdə nikah və ailə m&amp;uuml;nasibətlərinin h&amp;uuml;quqi baxımdan tənzimlənməsi d&amp;ouml;vlət tərəfindən həyata ke&amp;ccedil;irilir və yalnız m&amp;uuml;vafiq icra hakimiyyəti orqanında bağlanan nikah tanınır.&lt;/p&gt;\r\n&lt;p&gt;Miqrasiya Məcəlləsinə əsasən isə əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verildiyi hallardan biri Azərbaycan Respublikasının vətəndaşı ilə yaxın qohumluq m&amp;uuml;nasibətlərində olmasıdır. Məcəllədə yaxın qohumun anlayışı verilib. Ata, ana, ər (arvad), &amp;ouml;vlad, qardaş, bacı və onların &amp;ouml;vladları, baba, nənə, babanın (nənənin) atası və anası, nəvə, qayınata, qayınana, qayın, baldız yaxın qohum hesab olunurlar. Həm&amp;ccedil;inin, adı&amp;ccedil;əkilən Məcəllənin 64-c&amp;uuml; maddəsində iş icazəsinin alınması tələb olunmayan halların dairəsi m&amp;uuml;əyyən edilib. Azərbaycan Respublikasının vətəndaşı ilə nikahda olanlar, bu şərtlə ki, həmin vətəndaş Azərbaycan Respublikasının ərazisində yaşayış yeri &amp;uuml;zrə qeydiyyatda olduqda həmin maddənin təsiri altına d&amp;uuml;ş&amp;uuml;r.&lt;/p&gt;\r\n&lt;p&gt;Miqrasiya Məcəlləsinə əsasən, m&amp;uuml;vafiq icra hakimiyyəti orqanı tərəfindən təsdiq edilən təhl&amp;uuml;kəli yoluxucu xəstəliklərin siyahısında nəzərdə tutulmuş xəstəlik virusu daşıyıcısı olan əcnəbilərə və vətəndaşlığı olmayan şəxslərə, Azərbaycan vətəndaşı ilə nikahda olanlar istisna olmaqla, &amp;ouml;lkəmizin ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verilmr. B&amp;uuml;t&amp;uuml;n bunlar Azərbaycan d&amp;ouml;vlətinin humanist mahiyyətindən, ailə-nikah m&amp;uuml;nasibətlərinə verdiyi dəyərdən irəli gəlir.&lt;/p&gt;\r\n&lt;p&gt;Doğrudur, bəzən qeyd etdiyim bu imtiyazlardan bəhrələnməyə, respublikada m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə almağa g&amp;ouml;rə formal nikah m&amp;uuml;nasibətləri ilə bağlı məlumatlar daxil olur və ş&amp;uuml;bhələr &amp;uuml;&amp;ccedil;&amp;uuml;n kifayət qədər əsas olduqda qanunam&amp;uuml;vafiq araşdırmalar aparılır və tədbirlər g&amp;ouml;r&amp;uuml;l&amp;uuml;r.&lt;/p&gt;\r\n&lt;p&gt;- Azərbaycana iş&amp;ccedil;i q&amp;uuml;vvəsi kimi uşaq, yeniyetmə miqrant gətirənlər olurmu?&lt;/p&gt;\r\n&lt;p&gt;- D&amp;ouml;vlət Miqrasiya Xidməti tərəfindən belə bir hal aşkar edilməyib. Azərbaycanın Əmək Məcəlləsinin &amp;ldquo;Əcnəbilərin və vətəndaşlığı olmayan şəxslərin əmək h&amp;uuml;ququnun tənzimlənməsi&amp;rdquo; maddəsində g&amp;ouml;stərilir ki, qanunla və ya Azərbaycan Respublikasının tərəfdar &amp;ccedil;ıxdığı beynəlxalq m&amp;uuml;qavilələrlə başqa hal nəzərdə tutulmayıbsa, əcnəbilər və vətəndaşlığı olmayan şəxslər Azərbaycanda olarkən Azərbaycan Respublikasının vətəndaşları ilə bərabər b&amp;uuml;t&amp;uuml;n əmək h&amp;uuml;quqlarından istifadə edə bilər və bu h&amp;uuml;quqlara m&amp;uuml;vafiq olan vəzifələr daşıyırlar. Elə həmin məcəllənin 12-ci maddəsində də bəyan olunur ki, iş&amp;ccedil;ilərin h&amp;uuml;quqlarını pozan, əmək m&amp;uuml;qaviləsi &amp;uuml;zrə &amp;ouml;z &amp;ouml;hdəliklərini yerinə yetirməyən,15 yaşına &amp;ccedil;atmamış şəxsləri işə g&amp;ouml;t&amp;uuml;rən, uşaqları onların həyatına, sağlamlığına və ya mənəviyyatına təhl&amp;uuml;kə t&amp;ouml;rədə bilən fəaliyyətə cəlb edən işəg&amp;ouml;t&amp;uuml;rən qanunvericiliklə m&amp;uuml;əyyən olunmuş qaydada m&amp;uuml;vafiq məsuliyyətə cəlb edilir.&lt;/p&gt;\r\n&lt;p&gt;- Azərbaycana daha &amp;ccedil;ox hansı &amp;ouml;lkələrdən iş&amp;ccedil;i q&amp;uuml;vvəsi cəlb olunur?&lt;/p&gt;\r\n&lt;p&gt;- Bu il m&amp;uuml;stəqilliyinin 25 illiyini qeyd edən Azərbaycan qısa zaman kəsiyində &amp;ccedil;ox b&amp;ouml;y&amp;uuml;k iqtisadi uğurlara imza atıb. 2004-2014-c&amp;uuml; illərdə Azərbaycan iqtisadiyyatı d&amp;uuml;nyanın ən s&amp;uuml;rətlə inkişaf edən iqtisadiyyatı olub. Bu illər ərzində işsizlik kəskin şəkildə azalaraq 40 faizdən 5 faizə enib. İqtisadiyyatımız daha da rəqabətqabiliyyətli olub və Davos İqtisadi Forumunun hesablamalarına əsasən, rəqabət qabiliyyətinə g&amp;ouml;rə Azərbaycan iqtisadiyyatı d&amp;uuml;nyada 40-cı yerdədir. Bu g&amp;uuml;n Avropada həyata ke&amp;ccedil;irilən ən iri infrastruktur və enerji layihələrindən biri olan Cənub Qaz Dəhlizi layihəsinin təşəbb&amp;uuml;skarı kimi &amp;ouml;lkəmiz &amp;ccedil;ıxış edir.&lt;/p&gt;\r\n&lt;p&gt;Qeyd olunan iqtisadi uğurlar &amp;ouml;z n&amp;ouml;vbəsində &amp;ouml;lkəmizdə yaradılan ictimai-siyasi sabitliyin və təhl&amp;uuml;kəsizliyin y&amp;uuml;ksək səviyyədə təmin edilməsinin nəticəsidir. Bu da &amp;ouml;z n&amp;ouml;vbəsində &amp;ouml;lkəmizə gələn əcnəbilərin sayına təsirini g&amp;ouml;stərib. Əvvəlki illərdə &amp;ouml;lkədən gedən vətəndaşlarımızın geri d&amp;ouml;nməsi ilə yanaşı, xeyli sayda əmək&amp;ccedil;i miqrantların &amp;ouml;lkəmizə &amp;uuml;z tutmasına səbəb olub. Əmək&amp;ccedil;i miqrantların palitrası zəngindir. D&amp;uuml;nyanın bir &amp;ccedil;ox &amp;ouml;lkələrindən Azərbaycanda haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olanlar var. Onların sırasında T&amp;uuml;rkiyə, G&amp;uuml;rc&amp;uuml;stan, Rusiya, Hindistan, İran, &amp;Ccedil;in, Banqladeş, B&amp;ouml;y&amp;uuml;k Britaniya və Şimali İrlandiya Birləşmiş Krallığı, Pakistan, Ukrayna və s. &amp;ouml;lkələrin vətəndaşları &amp;ccedil;oxluq təşkil edir.&lt;/p&gt;\r\n&lt;p&gt;- Azərbaycana əcnəbi iş&amp;ccedil;i gətirən şirkətlər qanunu pozduğu təqdirdə nə qədər cərimə &amp;ouml;dəməli olurlar?&lt;/p&gt;\r\n&lt;p&gt;- İnzibati Xətalar Məcəlləsinin 578-ci maddəsinə əsasən, işəg&amp;ouml;t&amp;uuml;rən tərəfindən Azərbaycan Respublikası Miqrasiya Məcəlləsinin tələbləri pozulmaqla iş icazəsi alınmadan əcnəbinin və ya vətəndaşlığı olmayan şəxsin işə cəlb edilməsinə g&amp;ouml;rə vəzifəli şəxslər &amp;uuml;&amp;ccedil; min manatdan d&amp;ouml;rd min manatadək məbləğdə, h&amp;uuml;quqi şəxslər on beş min manatdan iyirmi min manatadək məbləğdə cərimə edilir. Eyni xəta &amp;uuml;&amp;ccedil; və ya daha &amp;ccedil;ox əcnəbi və ya vətəndaşlığı olmayan şəxs barəsində t&amp;ouml;rədildikdə vəzifəli şəxslər beş min manatdan yeddi min manatadək məbləğdə, h&amp;uuml;quqi şəxslər isə otuz min manatdan otuz beş min manatadək məbləğdə cərimə edilir. İşəg&amp;ouml;t&amp;uuml;rənin işə cəlb etdiyi əcnəbini və vətəndaşlığı olmayan şəxsi &amp;ouml;z iş yerindən kənarda işlə təmin etməsinə g&amp;ouml;rə vəzifəli şəxslər beş y&amp;uuml;z manat məbləğində cərimə edilir.&lt;/p&gt;\r\n&lt;p&gt;- Təmsil etdiyiniz qurum &quot;ASAN xidmət&quot; sektorunda hansı xidmətlərlə təmsil olunur?&lt;/p&gt;\r\n&lt;p&gt;- D&amp;ouml;vlət Miqrasiya Xidməti tərəfindən Azərbaycan brendi olan &quot;ASAN xidmət&quot; mərkəzlərində əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazənin verilməsi (m&amp;uuml;ddətinin uzadılması), daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazənin verilməsi (m&amp;uuml;ddətinin uzadılması), haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaları &amp;uuml;&amp;ccedil;&amp;uuml;n iş icazələrinin verilməsi, habelə şəxsin Azərbaycan Respublikasının vətəndaşlığına mənsubiyyətinin m&amp;uuml;əyyənləşdirilməsi ilə bağlı sorğuların qəbulu və cavablandırılması xidmətləri təqdim edilir.&lt;/p&gt;\r\n&lt;p&gt;- Şirkətlər tərəfindən vəsiqələrin g&amp;ouml;t&amp;uuml;r&amp;uuml;lməsi sahəsində hansı problemlər var və bu kimi halların aradan qaldırılması &amp;uuml;&amp;ccedil;&amp;uuml;n tədbirlər nəzərdə tutulurmu?&lt;/p&gt;\r\n&lt;p&gt;- Bəli, hazır vəsiqələrin g&amp;ouml;t&amp;uuml;r&amp;uuml;lməməsi sahəsində m&amp;uuml;əyyən problemlər olur. Azərbaycan Respublikasında m&amp;uuml;vəqqəti və ya daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə alan əcnəbilər və vətəndaşlığı olmayan şəxslərə m&amp;uuml;vəqqəti və ya daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqələri verilir. Bəzən hazır vəsiqələri vaxtında g&amp;ouml;t&amp;uuml;rməmək halları qeydə alınır. Xatırladım ki, Azərbaycan ərazisində haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq &amp;uuml;&amp;ccedil;&amp;uuml;n iş icazəsi alan əcnəbi və ya vətəndaşlığı olmayan şəxsə burada m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə verilir. İş icazəsi bu icazəni almaq &amp;uuml;&amp;ccedil;&amp;uuml;n d&amp;ouml;vlət r&amp;uuml;sumunun &amp;ouml;dənilməsini təsdiq edən sənəd təqdim olunduqdan sonra işəg&amp;ouml;t&amp;uuml;rənə verilir. İşəg&amp;ouml;t&amp;uuml;rən tərəfindən 30 g&amp;uuml;n m&amp;uuml;ddətində iş icazəsinin alınmasına, eləcə də iş icazəsinin m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə d&amp;ouml;vlət r&amp;uuml;sumu &amp;ouml;dənilmədikdə, habelə əcnəbiyə və vətəndaşlığı olmayan şəxsə Azərbaycanın ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazənin verilməsindən imtina olunduqda iş icazəsi ləğv edilir.&lt;/p&gt;\r\n&lt;p&gt;- İşə cəlb edilən əcnəbinin və ya vətəndaşlığı olmayan şəxsin pasportunun işəg&amp;ouml;t&amp;uuml;rən tərəfindən g&amp;ouml;t&amp;uuml;r&amp;uuml;lərək saxlanması ilə bağlı hansı tədbirlər g&amp;ouml;r&amp;uuml;l&amp;uuml;r?&lt;/p&gt;\r\n&lt;p&gt;- Qeyd etdiyiniz əməl Azərbaycanın Cinayət Məcəlləsinin m&amp;uuml;vafiq maddəsinə əsasən cinayət məsuliyyətinə səbəb olmadıqda İnzibati Xətalar Məcəlləsinə g&amp;ouml;rə, işə cəlb edilən əcnəbinin və vətəndaşlığı olmayan şəxsin pasportunun və ya şəxsiyyətini təsdiq edən digər sənədinin işəg&amp;ouml;t&amp;uuml;rən tərəfindən g&amp;ouml;t&amp;uuml;r&amp;uuml;lərək saxlanmasına g&amp;ouml;rə - vəzifəli şəxslər beş y&amp;uuml;z manat məbləğində cərimə edilirlər.&lt;/p&gt;', 0, 2, 1, 3, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `karabag`
--

CREATE TABLE `karabag` (
  `id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `karabag`
--

INSERT INTO `karabag` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(4, '', '', '', '', 'png', '&lt;p&gt;ERMƏNİSTAN RESPUBLİKASININ HƏRBİ TƏCAV&amp;Uuml;Z&amp;Uuml;N&amp;Uuml;N NƏTİCƏLƏRİ&lt;br /&gt;Cəbhə xəttinin uzunluğu - 246 km&lt;br /&gt;Dağlıq Qarabağ (1989)&lt;br /&gt;Ərazi &amp;ndash; 4 388 kv.km&lt;br /&gt;Əhali - 189 085&lt;br /&gt;Ermənilər - 145 450 (76.9 %)&lt;br /&gt;Azərbaycanlılar &amp;ndash; 40 688 (21.5 %)&lt;br /&gt;Sair - 2 947 (1.6 %)&lt;br /&gt;Sərhədin uzunluğu &amp;ndash; 485 km&lt;/p&gt;\r\n&lt;p&gt;O c&amp;uuml;mlədən: Şuşa rayonu&lt;br /&gt;Ərazi &amp;ndash; 312 kv.km&lt;br /&gt;Əhali - 20 579&lt;br /&gt;Azərbaycanlılar &amp;ndash; 19 036 (92.5 %)&lt;br /&gt;Ermənilər &amp;ndash; 1 377 (6.7 % )&lt;br /&gt;İşğal olunmuşdur - 1992-ci il, 8 may&lt;/p&gt;\r\n&lt;p&gt;DAĞLIQ QARABAĞIN ƏTRAFINDAKI RAYONLAR&lt;br /&gt;İşğal olunmuşdur Əhalisi - 2015&lt;/p&gt;\r\n&lt;p&gt;La&amp;ccedil;ın rayonu 1992-ci il, 18 may 74 100&lt;br /&gt;Kəlbəcər rayonu 1993-ci il, 2 aprel 88 300&lt;br /&gt;Ağdam rayonu 1993-c&amp;uuml; il, 23 iyul 191 700&lt;br /&gt;F&amp;uuml;zuli rayonu 1993- c&amp;uuml; il, 23 avqust 125 400&lt;br /&gt;Cəbrayıl rayonu 1993-c&amp;uuml; il, 23 avqust 76 600&lt;br /&gt;Qubadlı rayonu 1993- c&amp;uuml; il, 31 avqust 38 900&lt;br /&gt;Zəngilan rayonu 1993-c&amp;uuml; il, 29 oktyabr 42 700&lt;/p&gt;\r\n&lt;p&gt;TƏCAV&amp;Uuml;Z&amp;Uuml;N QURBANLARI&lt;br /&gt;Həlak olmuşdur &amp;ndash; 20 000&lt;br /&gt;Əlil olmuşdur - 50 000&lt;br /&gt;İtkin d&amp;uuml;şm&amp;uuml;şd&amp;uuml;r (2015) &amp;ndash; 4 011&lt;br /&gt;Qeyd: M&amp;uuml;naqişə m&amp;uuml;ddətində 6000-dən &amp;ccedil;ox Azərbaycan vətəndaşı itkin d&amp;uuml;şm&amp;uuml;ş,&lt;br /&gt;əsir-girov g&amp;ouml;t&amp;uuml;r&amp;uuml;lm&amp;uuml;şd&amp;uuml;r.&lt;/p&gt;\r\n&lt;p&gt;DAĞINTILAR VƏ ZİYANLAR (1988-1993-c&amp;uuml; illər)&lt;br /&gt;Yaşayış məntəqələri 900&lt;br /&gt;Evlər 150 000&lt;br /&gt;İctimai binalar 7000&lt;br /&gt;Məktəblər 693&lt;br /&gt;Uşaq bağ&amp;ccedil;aları 855&lt;br /&gt;Tibb m&amp;uuml;əssisələri 695&lt;br /&gt;Kitabxanalar 927&lt;br /&gt;Məbədlər 44&lt;br /&gt;Məscidlər 9&lt;br /&gt;Tarixi abidələr, saraylar və muzeylər 473&lt;br /&gt;Muzey eksponatları 40 000&lt;br /&gt;Sənaye və kənd təsərr&amp;uuml;fatı m&amp;uuml;əssisələri 6000&lt;br /&gt;Avtomobil yolları 2 670&lt;br /&gt;K&amp;ouml;rp&amp;uuml;lər 160&lt;br /&gt;Su kommunikasiyaları 2 300 km&lt;br /&gt;Qaz kommunikasiyaları 2 000 km&lt;br /&gt;Elektrik xətləri 15 000 km&lt;br /&gt;Meşələr 280 000 ha&lt;br /&gt;Kənd təsərr&amp;uuml;fatı &amp;uuml;&amp;ccedil;&amp;uuml;n yararlı torpaqlar 1 000 000 ha&lt;/p&gt;\r\n&lt;p&gt;İrriqasiya sistemləri 1 200 km&lt;/p&gt;\r\n&lt;p&gt;QA&amp;Ccedil;QIN, MƏCBURİ K&amp;Ouml;&amp;Ccedil;K&amp;Uuml;N VƏ SIĞINACAQ AXTARAN ŞƏXSLƏR (2015)&lt;br /&gt;Ermənistandan qa&amp;ccedil;qınlar 350 000&lt;br /&gt;Azərbaycanın işğal olunmuş ərazilərindən məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlər 789 000&lt;/p&gt;\r\n&lt;p&gt;Orta Asiyadan qa&amp;ccedil;qınlar (Ahısxa t&amp;uuml;rkləri) 60 000&lt;br /&gt;Digər &amp;ouml;lkələrdən olan qa&amp;ccedil;qın və sığınacaq axtaran şəxslər 1 500&lt;br /&gt;(2003-c&amp;uuml; ilə qədər olanların sayı 11 000 nəfər olmuşdur)&lt;/p&gt;\r\n&lt;p&gt;Cəmi: 1 200 500&lt;/p&gt;\r\n&lt;p&gt;İşğal edilmiş ərazilərdən olan Ermənistan hərbi q&amp;uuml;vvələri (2015)&lt;br /&gt;Tank - 350&lt;br /&gt;Artilleriya - 425&lt;br /&gt;ACV - 398&lt;br /&gt;Şəxsi heyət - 45 000 nəfər&lt;/p&gt;\r\n&lt;p&gt;İşğal edilmiş ərazilərə qanunsuz k&amp;ouml;&amp;ccedil;&amp;uuml;r&amp;uuml;lən əhali (2015)&lt;br /&gt;Dağlıq Qarabağ - 8 500&lt;br /&gt;La&amp;ccedil;ın - 13 000&lt;br /&gt;Kəlbəcər - 700&lt;br /&gt;Zəngilan - 520&lt;br /&gt;Cəbrayıl - 280&lt;/p&gt;\r\n&lt;p&gt;Cəmi: 23 000 nəfər&lt;/p&gt;\r\n&lt;p&gt;Qeyd:&lt;br /&gt;1. Qa&amp;ccedil;qın və məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlər m&amp;uuml;vəqqəti olaraq 62 şəhər və rayon ərazisində əsasən 1600-dən &amp;ccedil;ox obyektdə məskunlaşmışdır.&lt;br /&gt;2. Ermənistanla və işğal olunmuş ərazilərlə həmsərhəd yaşayış məntəqələrindən olan məcburi k&amp;ouml;&amp;ccedil;k&amp;uuml;nlərin sayı (2015) &amp;ndash; 110 612 nəfərdir.&lt;/p&gt;', 0, 1, 1, 1, 2, 0),
(5, '', '', '', '', 'png', '&lt;p&gt;&lt;span&gt;D&amp;ouml;vlət miqrasiya siyasəti&lt;/span&gt;&lt;br /&gt;Azərbaycan Respublikasında miqrasiya sahəsində d&amp;ouml;vlət siyasətinin həyata ke&amp;ccedil;irilməsi, qanunvericiliyin beynəlxalq normalara və m&amp;uuml;asir d&amp;ouml;vr&amp;uuml;n tələblərinə uyğun təkmilləşdirilməsi, qanunların tətbiqində Azərbaycan Respublikasının milli təhl&amp;uuml;kəsizliyinin və sabit sosial-iqtisadi, demoqrafik inkişafının təmin edilməsi, əmək ehtiyatlarından səmərəli istifadə edilməsi, &amp;ouml;lkə ərazisində əhalinin m&amp;uuml;tənasib yerləşdirilməsi, miqrantların intellektual və əmək potensialından istifadə edilməsi, tənzimlənməyən miqrasiya proseslərinin neqativ təsirinin aradan qaldırılması, insan alveri də daxil olmaqla, qeyri-qanuni miqrasiyanın qarşısının alınması məqsədi ilə Azərbaycan Respublikası Prezidentinin 25 iyul 2006-cı il tarixli Sərəncamı ilə &amp;ldquo;Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Proqramı (2006-2008-ci illər)&amp;rdquo; təsdiq edilmişdir. Bu D&amp;ouml;vlət Proqramının əsas məqsədi miqrasiya sahəsində d&amp;ouml;vlət siyasətinin həyata ke&amp;ccedil;irilməsi, miqrasiya idarəetmə sisteminin inkişaf etdirilməsi, miqrasiya proseslərinin tənzimlənməsi və proqnozlaşdırılması, bu sahədə qanunvericiliyin beynəlxalq normalara və m&amp;uuml;asir d&amp;ouml;vr&amp;uuml;n tələblərinə uyğun təkmilləşdirilməsi, qanunların tətbiqinin effektivliyinin təmin edilməsi, miqrasiya sahəsində vahid məlumat bankının, m&amp;uuml;asir avtomatlaşdırılmış nəzarət sisteminin yaradılması, qeyri-qanuni miqrasiyanın qarşısının alınması, beynəlxalq əməkdaşlığın inkişaf etdirilməsi tədbirlərinin həyata ke&amp;ccedil;irilməsindən ibarətdir. Proqramda miqrasiya proseslərinin x&amp;uuml;susiyyətləri, d&amp;ouml;vlət siyasətinin prioritet istiqamətləri &amp;ouml;z əksini tapmışdır. Bununla yanaşı proqramın əhatə etdiyi d&amp;ouml;vr ərzində ayrı-ayrı d&amp;ouml;vlət qurumları ilə əlaqəli şəkildə həyata ke&amp;ccedil;irməli olduqları tədbirlər qeyd edilmişdir&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;D&amp;ouml;vlət Miqrasiya Xidməti haqqında&lt;/span&gt;&amp;nbsp;&lt;br /&gt;Təsdiq edilmiş &amp;ldquo;Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Proqramı&amp;rdquo; əsasında miqrasiya sahəsində vahid d&amp;ouml;vlət siyasətini həyata ke&amp;ccedil;irən x&amp;uuml;susi d&amp;ouml;vlət orqanının yaradılması zərurətini nəzərə alan Azərbaycan Respublikasının Prezidenti cənab İlham Əliyev 19 mart 2007-ci il tarixdə Azərbaycan Respublikası D&amp;ouml;vlət Miqrasiya Xidmətinin yaradılması haqqında 560 n&amp;ouml;mrəli Fərman imzalamışdır. Eyni zamanda, Fərmana əsasən Xidmətin Əsasnaməsi təsdiq edilmişdir. D&amp;ouml;vlət Miqrasiya Xidmətinin yaradıldığı vaxtdan miqrasiya proseslərinə d&amp;ouml;vlət nəzarətinin g&amp;uuml;cləndirilməsi məqsədi ilə zəruri normativ h&amp;uuml;quqi aktlar qəbul edilmiş, bir sıra institusional və təşkilati tədbirlər həyata ke&amp;ccedil;irilmişdir. Miqrasiya proseslərinin vahid və &amp;ccedil;evik prosedurlar əsasında tənzimlənməsi, sənədləşmənin sadələşdirilməsi məqsədilə Azərbaycan Respublikasının Prezidenti cənab İlham Əliyev tərəfindən &quot;Miqrasiya proseslərinin idarəolunmasında &quot;bir pəncərə&quot; prinsipinin tətbiqi haqqında&quot; 4 mart 2009-cu il tarixli 69 n&amp;ouml;mrəli Fərman imzalanmışdır. Adı&amp;ccedil;əkilən Fərmanın tətbiqi &amp;ouml;lkədə miqrasiya proseslərinin daha &amp;ccedil;evik və işlək mexanizmlər əsasında idarə edilməsinə, bu sahədə operativliyin təmin edilməsinə və həllini g&amp;ouml;zləyən problemlərin aradan qaldırılmasına səbəb olmuşdur. Fərmana əsasən, 2009-cu il iyulun 1-dən miqrasiya proseslərinin idarəolunmasında &amp;laquo;bir pəncərə&amp;raquo; prinsipinin tətbiq edilməsinə başlanılmış və bu prinsip &amp;uuml;zrə vahid d&amp;ouml;vlət orqanının səlahiyyətləri Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Xidmətinə həvalə edilmişdir. &amp;ldquo;Bir pəncərə&amp;rdquo; prinsipi &amp;ccedil;ər&amp;ccedil;ivəsində D&amp;ouml;vlət Miqrasiya Xidməti əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti və daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazələrin və m&amp;uuml;vafiq vəsiqələrin verilməsini, onların qeydiyyata alınmasını, Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılmasını, həm&amp;ccedil;inin &amp;ouml;lkə ərazisində haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq &amp;uuml;&amp;ccedil;&amp;uuml;n iş icazələrinin verilməsini həyata ke&amp;ccedil;irir. Bununla belə, Xidmət vətəndaşlıq məsələlərində iştirak edir və qa&amp;ccedil;qın statusunu m&amp;uuml;əyyənləşdirir. Qeyri-qanuni miqrasiyaya qarşı m&amp;uuml;barizə tədbirlərinin g&amp;uuml;cləndirilməsi, Xidmətin fəaliyyətinin təkmilləşdirilməsi məqsədi ilə Prezident cənab İlham Əliyevin 8 aprel 2009-cu il tarixdə imzaladığı 76 n&amp;ouml;mrəli Fərmanla D&amp;ouml;vlət Miqrasiya Xidmətinə h&amp;uuml;quq-m&amp;uuml;hafizə orqanı statusu verilmişdir.&lt;/p&gt;', 0, 1, 1, 2, 2, 0),
(6, '', '', '', '', 'png', '&lt;p&gt;&lt;span&gt;D&amp;ouml;vlət miqrasiya siyasəti&lt;/span&gt;&lt;br /&gt;Azərbaycan Respublikasında miqrasiya sahəsində d&amp;ouml;vlət siyasətinin həyata ke&amp;ccedil;irilməsi, qanunvericiliyin beynəlxalq normalara və m&amp;uuml;asir d&amp;ouml;vr&amp;uuml;n tələblərinə uyğun təkmilləşdirilməsi, qanunların tətbiqində Azərbaycan Respublikasının milli təhl&amp;uuml;kəsizliyinin və sabit sosial-iqtisadi, demoqrafik inkişafının təmin edilməsi, əmək ehtiyatlarından səmərəli istifadə edilməsi, &amp;ouml;lkə ərazisində əhalinin m&amp;uuml;tənasib yerləşdirilməsi, miqrantların intellektual və əmək potensialından istifadə edilməsi, tənzimlənməyən miqrasiya proseslərinin neqativ təsirinin aradan qaldırılması, insan alveri də daxil olmaqla, qeyri-qanuni miqrasiyanın qarşısının alınması məqsədi ilə Azərbaycan Respublikası Prezidentinin 25 iyul 2006-cı il tarixli Sərəncamı ilə &amp;ldquo;Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Proqramı (2006-2008-ci illər)&amp;rdquo; təsdiq edilmişdir. Bu D&amp;ouml;vlət Proqramının əsas məqsədi miqrasiya sahəsində d&amp;ouml;vlət siyasətinin həyata ke&amp;ccedil;irilməsi, miqrasiya idarəetmə sisteminin inkişaf etdirilməsi, miqrasiya proseslərinin tənzimlənməsi və proqnozlaşdırılması, bu sahədə qanunvericiliyin beynəlxalq normalara və m&amp;uuml;asir d&amp;ouml;vr&amp;uuml;n tələblərinə uyğun təkmilləşdirilməsi, qanunların tətbiqinin effektivliyinin təmin edilməsi, miqrasiya sahəsində vahid məlumat bankının, m&amp;uuml;asir avtomatlaşdırılmış nəzarət sisteminin yaradılması, qeyri-qanuni miqrasiyanın qarşısının alınması, beynəlxalq əməkdaşlığın inkişaf etdirilməsi tədbirlərinin həyata ke&amp;ccedil;irilməsindən ibarətdir. Proqramda miqrasiya proseslərinin x&amp;uuml;susiyyətləri, d&amp;ouml;vlət siyasətinin prioritet istiqamətləri &amp;ouml;z əksini tapmışdır. Bununla yanaşı proqramın əhatə etdiyi d&amp;ouml;vr ərzində ayrı-ayrı d&amp;ouml;vlət qurumları ilə əlaqəli şəkildə həyata ke&amp;ccedil;irməli olduqları tədbirlər qeyd edilmişdir&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;D&amp;ouml;vlət Miqrasiya Xidməti haqqında&lt;/span&gt;&amp;nbsp;&lt;br /&gt;Təsdiq edilmiş &amp;ldquo;Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Proqramı&amp;rdquo; əsasında miqrasiya sahəsində vahid d&amp;ouml;vlət siyasətini həyata ke&amp;ccedil;irən x&amp;uuml;susi d&amp;ouml;vlət orqanının yaradılması zərurətini nəzərə alan Azərbaycan Respublikasının Prezidenti cənab İlham Əliyev 19 mart 2007-ci il tarixdə Azərbaycan Respublikası D&amp;ouml;vlət Miqrasiya Xidmətinin yaradılması haqqında 560 n&amp;ouml;mrəli Fərman imzalamışdır. Eyni zamanda, Fərmana əsasən Xidmətin Əsasnaməsi təsdiq edilmişdir. D&amp;ouml;vlət Miqrasiya Xidmətinin yaradıldığı vaxtdan miqrasiya proseslərinə d&amp;ouml;vlət nəzarətinin g&amp;uuml;cləndirilməsi məqsədi ilə zəruri normativ h&amp;uuml;quqi aktlar qəbul edilmiş, bir sıra institusional və təşkilati tədbirlər həyata ke&amp;ccedil;irilmişdir. Miqrasiya proseslərinin vahid və &amp;ccedil;evik prosedurlar əsasında tənzimlənməsi, sənədləşmənin sadələşdirilməsi məqsədilə Azərbaycan Respublikasının Prezidenti cənab İlham Əliyev tərəfindən &quot;Miqrasiya proseslərinin idarəolunmasında &quot;bir pəncərə&quot; prinsipinin tətbiqi haqqında&quot; 4 mart 2009-cu il tarixli 69 n&amp;ouml;mrəli Fərman imzalanmışdır. Adı&amp;ccedil;əkilən Fərmanın tətbiqi &amp;ouml;lkədə miqrasiya proseslərinin daha &amp;ccedil;evik və işlək mexanizmlər əsasında idarə edilməsinə, bu sahədə operativliyin təmin edilməsinə və həllini g&amp;ouml;zləyən problemlərin aradan qaldırılmasına səbəb olmuşdur. Fərmana əsasən, 2009-cu il iyulun 1-dən miqrasiya proseslərinin idarəolunmasında &amp;laquo;bir pəncərə&amp;raquo; prinsipinin tətbiq edilməsinə başlanılmış və bu prinsip &amp;uuml;zrə vahid d&amp;ouml;vlət orqanının səlahiyyətləri Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Xidmətinə həvalə edilmişdir. &amp;ldquo;Bir pəncərə&amp;rdquo; prinsipi &amp;ccedil;ər&amp;ccedil;ivəsində D&amp;ouml;vlət Miqrasiya Xidməti əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti və daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazələrin və m&amp;uuml;vafiq vəsiqələrin verilməsini, onların qeydiyyata alınmasını, Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılmasını, həm&amp;ccedil;inin &amp;ouml;lkə ərazisində haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq &amp;uuml;&amp;ccedil;&amp;uuml;n iş icazələrinin verilməsini həyata ke&amp;ccedil;irir. Bununla belə, Xidmət vətəndaşlıq məsələlərində iştirak edir və qa&amp;ccedil;qın statusunu m&amp;uuml;əyyənləşdirir. Qeyri-qanuni miqrasiyaya qarşı m&amp;uuml;barizə tədbirlərinin g&amp;uuml;cləndirilməsi, Xidmətin fəaliyyətinin təkmilləşdirilməsi məqsədi ilə Prezident cənab İlham Əliyevin 8 aprel 2009-cu il tarixdə imzaladığı 76 n&amp;ouml;mrəli Fərmanla D&amp;ouml;vlət Miqrasiya Xidmətinə h&amp;uuml;quq-m&amp;uuml;hafizə orqanı statusu verilmişdir.&lt;/p&gt;', 0, 1, 1, 3, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kime_mektub`
--

CREATE TABLE `kime_mektub` (
  `id` int(3) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kime_mektub`
--

INSERT INTO `kime_mektub` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, 'Vüsal Hüseynov – Xidmət rəisi', '', '', '', '', '', 0, 1, 1, 1, 1, 0),
(2, 'Vüsal Hüseynov – Xidmət rəisi', '', '', '', '', '', 0, 1, 1, 2, 1, 0),
(3, 'Vüsal Hüseynov – Xidmət rəisi', '', '', '', '', '', 0, 1, 1, 3, 1, 0),
(4, 'Vəli Nağıyev – Xidmət rəisinin birinci müavini', '', '', '', '', '', 0, 2, 1, 1, 2, 0),
(5, 'Vəli Nağıyev – Xidmət rəisinin birinci müavini', '', '', '', '', '', 0, 2, 1, 2, 2, 0),
(6, 'Vəli Nağıyev – Xidmət rəisinin birinci müavini', '', '', '', '', '', 0, 2, 1, 3, 2, 0),
(7, 'Pərviz Musayev – Xidmət rəisinin müavini', '', '', '', '', '', 0, 3, 1, 1, 3, 0),
(8, 'Pərviz Musayev – Xidmət rəisinin müavini', '', '', '', '', '', 0, 3, 1, 2, 3, 0),
(9, 'Pərviz Musayev – Xidmət rəisinin müavini', '', '', '', '', '', 0, 3, 1, 3, 3, 0),
(10, 'Elman Əliyev – Xidmət rəisinin müavini', '', '', '', '', '', 0, 4, 1, 1, 4, 0),
(11, 'Elman Əliyev – Xidmət rəisinin müavini', '', '', '', '', '', 0, 4, 1, 2, 4, 0),
(12, 'Elman Əliyev – Xidmət rəisinin müavini', '', '', '', '', '', 0, 4, 1, 3, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE `links` (
  `id` int(3) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, 'Miqrasiya sayti', '', 'https://www.migration.gov.az/home/calculator', '', '', '', 0, 1, 1, 1, 1, 0),
(2, 'Miqrasiya sayti', '', 'https://www.migration.gov.az/home/calculator', '', '', '', 0, 1, 1, 2, 1, 0),
(3, 'Miqrasiya sayti', '', 'https://www.migration.gov.az/home/calculator', '', '', '', 0, 1, 1, 3, 1, 0),
(4, 'Miqrasiya xidmeti', '', 'https://www.migration.gov.az/useful/freedom', '', '', '', 0, 2, 1, 1, 2, 0),
(5, 'Miqrasiya xidmeti', '', 'https://www.migration.gov.az/useful/freedom', '', '', '', 0, 2, 1, 2, 2, 0),
(6, 'Miqrasiya xidmeti', '', 'https://www.migration.gov.az/useful/freedom', '', '', '', 0, 2, 1, 3, 2, 0),
(7, 'Elektron hokumet portali', '', 'https://www.e-gov.az/', '', '', '', 0, 3, 1, 1, 3, 0),
(8, 'Elektron hokumet portali', '', 'https://www.e-gov.az/', '', '', '', 0, 3, 1, 2, 3, 0),
(9, 'Elektron hokumet portali', '', 'https://www.e-gov.az/', '', '', '', 0, 3, 1, 3, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mektublar`
--

CREATE TABLE `mektublar` (
  `id` int(11) NOT NULL,
  `applytype` varchar(50) NOT NULL,
  `sendto` varchar(50) NOT NULL,
  `doctype` varchar(50) NOT NULL,
  `docnum` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `patronymicname` varchar(50) NOT NULL,
  `socialstatus` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `region` varchar(50) NOT NULL,
  `birthday` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `file` varchar(50) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mektublar`
--

INSERT INTO `mektublar` (`id`, `applytype`, `sendto`, `doctype`, `docnum`, `name`, `surname`, `patronymicname`, `socialstatus`, `country`, `region`, `birthday`, `email`, `address`, `phone`, `file`, `description`) VALUES
(1, 'Şikayət', 'Vəli Nağıyev – Xidmət rəisinin birinci müavini', 'Vətəndaşlığı olmayan şəxsin şəxsiyyət vəsiqəsi', 'qw63636', 'asda', 'asda', 'asadasda', 'fəhlə', 'Almaniya', 'Xizi rayonu', '1992-02-06', 'fhesenli92@gmail.com', 'asdas', '546456', '5bae293d8cfee.pdf', 'asdasda<br />\r\ndas<br />\r\ndasdasdas');

-- --------------------------------------------------------

--
-- Table structure for table `menyular`
--

CREATE TABLE `menyular` (
  `id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menyular`
--

INSERT INTO `menyular` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, 'Əsas səhifə', '', '/', '', '', '', 0, 1, 1, 1, 1, 0),
(2, 'Əsas səhifə', '', '/', '', '', '', 0, 1, 1, 2, 1, 0),
(3, 'Əsas səhifə', '', '/', '', '', '', 0, 1, 1, 3, 1, 0),
(4, 'Xidmət Haqqında', '', '#', '', '', '', 0, 2, 1, 1, 2, 0),
(5, 'Xidmət Haqqında', '', '#', '', '', '', 0, 2, 1, 2, 2, 0),
(6, 'Xidmət Haqqında', '', '#', '', '', '', 0, 2, 1, 3, 2, 0),
(7, 'Dövlət Miqrasiya Xidməti', '', 'dmx', '', '', '', 2, 1, 1, 1, 3, 0),
(8, 'Dövlət Miqrasiya Xidməti', '', 'dmx', '', '', '', 2, 1, 1, 2, 3, 0),
(9, 'Dövlət Miqrasiya Xidməti', '', 'dmx', '', '', '', 2, 1, 1, 3, 3, 0),
(10, 'Struktur', '', 'structure', '', '', '', 2, 2, 1, 1, 4, 0),
(11, 'Struktur', '', 'structure', '', '', '', 2, 2, 1, 2, 4, 0),
(12, 'Struktur', '', 'structure', '', '', '', 2, 2, 1, 3, 4, 0),
(13, 'Rəhbər', '', 'leadership', '', '', '', 2, 3, 1, 1, 5, 0),
(14, 'Rəhbər', '', 'leadership', '', '', '', 2, 3, 1, 2, 5, 0),
(15, 'Rəhbər', '', 'leadership', '', '', '', 2, 3, 1, 3, 5, 0),
(16, 'Qanunvericilik', '', '#', '', '', '', 0, 3, 1, 1, 6, 0),
(17, 'Qanunvericilik', '', '#', '', '', '', 0, 3, 1, 2, 6, 0),
(18, 'Qanunvericilik', '', '#', '', '', '', 0, 3, 1, 3, 6, 0),
(19, 'Azərbaycan Respublikasının hüquqi aktları', '', 'legislation/azacts', '', '', '', 6, 1, 1, 1, 7, 0),
(20, 'Azərbaycan Respublikasının hüquqi aktları', '', 'legislation/azacts', '', '', '', 6, 1, 1, 2, 7, 0),
(21, 'Azərbaycan Respublikasının hüquqi aktları', '', 'legislation/azacts', '', '', '', 6, 1, 1, 3, 7, 0),
(22, 'Naxçıvan Muxtar Respublikasının hüquqi aktları', '', 'legislation/naxacts', '', '', '', 6, 2, 1, 1, 8, 0),
(23, 'Naxçıvan Muxtar Respublikasının hüquqi aktları', '', 'legislation/naxacts', '', '', '', 6, 2, 1, 2, 8, 0),
(24, 'Naxçıvan Muxtar Respublikasının hüquqi aktları', '', 'legislation/naxacts', '', '', '', 6, 2, 1, 3, 8, 0),
(25, 'Beytnəlxalq hüquqi aktlar', '', 'legislation/globacts', '', '', '', 6, 3, 1, 1, 9, 0),
(26, 'Beytnəlxalq hüquqi aktlar', '', 'legislation/globacts', '', '', '', 6, 3, 1, 2, 9, 0),
(27, 'Beytnəlxalq hüquqi aktlar', '', 'legislation/globacts', '', '', '', 6, 3, 1, 3, 9, 0),
(28, 'Mətbuat Mərkəzi', '', '#', '', '', '', 0, 4, 1, 1, 10, 0),
(29, 'Mətbuat Mərkəzi', '', '#', '', '', '', 0, 4, 1, 2, 10, 0),
(30, 'Mətbuat Mərkəzi', '', '#', '', '', '', 0, 4, 1, 3, 10, 0),
(31, 'Xəbərlər', '', 'news', '', '', '', 10, 1, 1, 1, 11, 0),
(32, 'Xəbərlər', '', 'news', '', '', '', 10, 1, 1, 2, 11, 0),
(33, 'Xəbərlər', '', 'news', '', '', '', 10, 1, 1, 3, 11, 0),
(34, 'Fotoalbom', '', 'photo', '', '', '', 10, 2, 1, 1, 12, 0),
(35, 'Fotoalbom', '', 'photo', '', '', '', 10, 2, 1, 2, 12, 0),
(36, 'Fotoalbom', '', 'photo', '', '', '', 10, 2, 1, 3, 12, 0),
(37, 'Video', '', 'video', '', '', '', 10, 3, 1, 1, 13, 0),
(38, 'Video', '', 'video', '', '', '', 10, 3, 1, 2, 13, 0),
(39, 'Video', '', 'video', '', '', '', 10, 3, 1, 3, 13, 0),
(40, 'Audio', '', 'audio', '', '', '', 10, 4, 1, 1, 14, 0),
(41, 'Audio', '', 'audio', '', '', '', 10, 4, 1, 2, 14, 0),
(42, 'Audio', '', 'audio', '', '', '', 10, 4, 1, 3, 14, 0),
(43, 'Müsahibə', '', 'interviews', '', '', '', 10, 5, 1, 1, 15, 0),
(44, 'Müsahibə', '', 'interviews', '', '', '', 10, 5, 1, 2, 15, 0),
(45, 'Müsahibə', '', 'interviews', '', '', '', 10, 5, 1, 3, 15, 0),
(46, 'Məqalələr', '', 'articles', '', '', '', 10, 6, 1, 1, 16, 0),
(47, 'Məqalələr', '', 'articles', '', '', '', 10, 6, 1, 2, 16, 0),
(48, 'Məqalələr', '', 'articles', '', '', '', 10, 6, 1, 3, 16, 0),
(49, 'Əlaqə', '', '#', '', '', '', 0, 5, 1, 1, 17, 0),
(50, 'Əlaqə', '', '#', '', '', '', 0, 5, 1, 2, 17, 0),
(51, 'Əlaqə', '', '#', '', '', '', 0, 5, 1, 3, 17, 0),
(52, 'Məktub yazmaq', '', 'letter', '', '', '', 17, 1, 1, 1, 18, 0),
(53, 'Məktub yazmaq', '', 'letter', '', '', '', 17, 1, 1, 2, 18, 0),
(54, 'Məktub yazmaq', '', 'letter', '', '', '', 17, 1, 1, 3, 18, 0),
(55, 'Qəbul günləri', '', 'days', '', '', '', 17, 2, 1, 1, 19, 0),
(56, 'Qəbul günləri', '', 'days', '', '', '', 17, 2, 1, 2, 19, 0),
(57, 'Qəbul günləri', '', 'days', '', '', '', 17, 2, 1, 3, 19, 0),
(58, 'Bizi necə tapmalı', '', 'contact', '', '', '', 17, 3, 1, 1, 20, 0),
(59, 'Bizi nece tapmali', '', 'contact', '', '', '', 17, 3, 1, 2, 20, 0),
(60, 'Bizi nece tapmali', '', 'contact', '', '', '', 17, 3, 1, 3, 20, 0);

-- --------------------------------------------------------

--
-- Table structure for table `muraciet_mektub`
--

CREATE TABLE `muraciet_mektub` (
  `id` int(3) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `muraciet_mektub`
--

INSERT INTO `muraciet_mektub` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, 'Şikayət', '', '', '', '', '', 0, 1, 1, 1, 1, 0),
(2, 'Şikayət', '', '', '', '', '', 0, 1, 1, 2, 1, 0),
(3, 'Şikayət', '', '', '', '', '', 0, 1, 1, 3, 1, 0),
(4, 'Ərizə', '', '', '', '', '', 0, 2, 1, 1, 2, 0),
(5, 'Ərizə', '', '', '', '', '', 0, 2, 1, 2, 2, 0),
(6, 'Ərizə', '', '', '', '', '', 0, 2, 1, 3, 2, 0),
(7, 'Təklif', '', '', '', '', '', 0, 3, 1, 1, 3, 0),
(8, 'Təklif', '', '', '', '', '', 0, 3, 1, 2, 3, 0),
(9, 'Təklif', '', '', '', '', '', 0, 3, 1, 3, 3, 0),
(10, 'Sorğu', '', '', '', '', '', 0, 4, 1, 1, 4, 0),
(11, 'Sorğu', '', '', '', '', '', 0, 4, 1, 2, 4, 0),
(12, 'Sorğu', '', '', '', '', '', 0, 4, 1, 3, 4, 0),
(13, 'Fərdi məlumat', '', '', '', '', '', 0, 5, 1, 1, 5, 0),
(14, 'Fərdi məlumat', '', '', '', '', '', 0, 5, 1, 2, 5, 0),
(15, 'Fərdi məlumat', '', '', '', '', '', 0, 5, 1, 3, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `muraciet_novbe`
--

CREATE TABLE `muraciet_novbe` (
  `id` int(3) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `muraciet_novbe`
--

INSERT INTO `muraciet_novbe` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, 'Müraciətlərin qəbulu', '', '', '', '', '', 0, 1, 1, 1, 1, 0),
(2, 'Müraciətlərin qəbulu', '', '', '', '', '', 0, 1, 1, 2, 1, 0),
(3, 'Müraciətlərin qəbulu', '', '', '', '', '', 0, 1, 1, 3, 1, 0),
(4, 'Hazır vəsiqələrin və sənədlərin götürülməsi', '', '', '', '', '', 0, 2, 1, 1, 2, 0),
(5, 'Hazır vəsiqələrin və sənədlərin götürülməsi', '', '', '', '', '', 0, 2, 1, 2, 2, 0),
(6, 'Hazır vəsiqələrin və sənədlərin götürülməsi', '', '', '', '', '', 0, 2, 1, 3, 2, 0),
(7, 'Məlumat əldə edilməsi', '', '', '', '', '', 0, 3, 1, 1, 3, 0),
(8, 'Məlumat əldə edilməsi', '', '', '', '', '', 0, 3, 1, 2, 3, 0),
(9, 'Məlumat əldə edilməsi', '', '', '', '', '', 0, 3, 1, 3, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `naxcivan`
--

CREATE TABLE `naxcivan` (
  `id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `naxcivan`
--

INSERT INTO `naxcivan` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, 'Naxçıvan tarixi', '', '', '', '', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam maximus turpis laoreet justo scelerisque hendrerit. Maecenas ornare pharetra eros. Fusce ac diam in augue dapibus maximus et vitae sem. Sed ullamcorper tortor quis tellus placerat, et aliquet mi hendrerit. Proin non lacus commodo, facilisis neque ut, mollis libero. In scelerisque fermentum ipsum, eu ullamcorper elit sollicitudin elementum. Aenean sagittis imperdiet justo et lacinia. Proin rhoncus turpis lacus, vitae tempus lorem elementum ac. Aenean vel ligula odio. Nunc vitae sem dolor. Donec ac euismod massa. Donec et ultrices leo.&lt;/p&gt;\r\n&lt;p&gt;Etiam pulvinar urna vel posuere maximus. Nulla iaculis, mi id tempus mattis, augue lacus pharetra eros, vulputate viverra urna enim et nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet quam at neque bibendum ultricies. Nunc luctus accumsan bibendum. Aenean lacinia lorem vitae libero porta euismod. Donec eget sem interdum, condimentum dolor a, suscipit felis. Quisque a velit aliquam, porttitor lectus sit amet, gravida diam. Suspendisse at nisl neque.&lt;/p&gt;\r\n&lt;p&gt;Pellentesque eros leo, viverra nec rhoncus quis, ultricies sed tellus. Nulla a volutpat eros, sed mattis est. Sed egestas ut dui at cursus. Nunc elementum velit justo, non dictum tortor sollicitudin vitae. Donec leo nisl, rutrum ut finibus eu, malesuada ut est. Vestibulum cursus fermentum diam sit amet tincidunt. Etiam feugiat rutrum tempor. Donec quis iaculis magna. Nulla facilisi. Suspendisse maximus et elit consectetur hendrerit.&lt;/p&gt;\r\n&lt;p&gt;In nec ligula neque. Nulla orci nisi, pretium sed dui in, placerat rutrum ex. Donec in hendrerit ex. Vestibulum ultricies semper arcu sed egestas. Praesent lectus lacus, finibus sed neque at, viverra vehicula justo. Integer leo lectus, ultrices eget molestie ultricies, euismod in eros. Nullam efficitur orci ac lacinia lobortis. Donec placerat sapien vel vehicula euismod.&lt;/p&gt;\r\n&lt;p&gt;Mauris hendrerit nunc lectus, non blandit mauris posuere eget. Aenean mattis imperdiet nibh. Donec pellentesque aliquet velit at accumsan. Curabitur pharetra ipsum sit amet nisi hendrerit lacinia. Vivamus vel mollis elit. Nam ut convallis lectus. Etiam eget egestas odio, a egestas orci. Praesent vel enim id augue varius rhoncus at in felis. Quisque et augue nec dui lobortis ultricies. Mauris consequat fermentum augue, ut commodo libero ultrices eu.1&lt;/p&gt;', 0, 1, 1, 1, 1, 0),
(2, 'Naxçıvan tarixi', '', '', '', '', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam maximus turpis laoreet justo scelerisque hendrerit. Maecenas ornare pharetra eros. Fusce ac diam in augue dapibus maximus et vitae sem. Sed ullamcorper tortor quis tellus placerat, et aliquet mi hendrerit. Proin non lacus commodo, facilisis neque ut, mollis libero. In scelerisque fermentum ipsum, eu ullamcorper elit sollicitudin elementum. Aenean sagittis imperdiet justo et lacinia. Proin rhoncus turpis lacus, vitae tempus lorem elementum ac. Aenean vel ligula odio. Nunc vitae sem dolor. Donec ac euismod massa. Donec et ultrices leo.&lt;/p&gt;\r\n&lt;p&gt;Etiam pulvinar urna vel posuere maximus. Nulla iaculis, mi id tempus mattis, augue lacus pharetra eros, vulputate viverra urna enim et nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet quam at neque bibendum ultricies. Nunc luctus accumsan bibendum. Aenean lacinia lorem vitae libero porta euismod. Donec eget sem interdum, condimentum dolor a, suscipit felis. Quisque a velit aliquam, porttitor lectus sit amet, gravida diam. Suspendisse at nisl neque.&lt;/p&gt;\r\n&lt;p&gt;Pellentesque eros leo, viverra nec rhoncus quis, ultricies sed tellus. Nulla a volutpat eros, sed mattis est. Sed egestas ut dui at cursus. Nunc elementum velit justo, non dictum tortor sollicitudin vitae. Donec leo nisl, rutrum ut finibus eu, malesuada ut est. Vestibulum cursus fermentum diam sit amet tincidunt. Etiam feugiat rutrum tempor. Donec quis iaculis magna. Nulla facilisi. Suspendisse maximus et elit consectetur hendrerit.&lt;/p&gt;\r\n&lt;p&gt;In nec ligula neque. Nulla orci nisi, pretium sed dui in, placerat rutrum ex. Donec in hendrerit ex. Vestibulum ultricies semper arcu sed egestas. Praesent lectus lacus, finibus sed neque at, viverra vehicula justo. Integer leo lectus, ultrices eget molestie ultricies, euismod in eros. Nullam efficitur orci ac lacinia lobortis. Donec placerat sapien vel vehicula euismod.&lt;/p&gt;\r\n&lt;p&gt;Mauris hendrerit nunc lectus, non blandit mauris posuere eget. Aenean mattis imperdiet nibh. Donec pellentesque aliquet velit at accumsan. Curabitur pharetra ipsum sit amet nisi hendrerit lacinia. Vivamus vel mollis elit. Nam ut convallis lectus. Etiam eget egestas odio, a egestas orci. Praesent vel enim id augue varius rhoncus at in felis. Quisque et augue nec dui lobortis ultricies. Mauris consequat fermentum augue, ut commodo libero ultrices eu.&lt;/p&gt;', 0, 1, 1, 2, 1, 0),
(3, 'Naxçıvan tarixi', '', '', '', '', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam maximus turpis laoreet justo scelerisque hendrerit. Maecenas ornare pharetra eros. Fusce ac diam in augue dapibus maximus et vitae sem. Sed ullamcorper tortor quis tellus placerat, et aliquet mi hendrerit. Proin non lacus commodo, facilisis neque ut, mollis libero. In scelerisque fermentum ipsum, eu ullamcorper elit sollicitudin elementum. Aenean sagittis imperdiet justo et lacinia. Proin rhoncus turpis lacus, vitae tempus lorem elementum ac. Aenean vel ligula odio. Nunc vitae sem dolor. Donec ac euismod massa. Donec et ultrices leo.&lt;/p&gt;\r\n&lt;p&gt;Etiam pulvinar urna vel posuere maximus. Nulla iaculis, mi id tempus mattis, augue lacus pharetra eros, vulputate viverra urna enim et nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet quam at neque bibendum ultricies. Nunc luctus accumsan bibendum. Aenean lacinia lorem vitae libero porta euismod. Donec eget sem interdum, condimentum dolor a, suscipit felis. Quisque a velit aliquam, porttitor lectus sit amet, gravida diam. Suspendisse at nisl neque.&lt;/p&gt;\r\n&lt;p&gt;Pellentesque eros leo, viverra nec rhoncus quis, ultricies sed tellus. Nulla a volutpat eros, sed mattis est. Sed egestas ut dui at cursus. Nunc elementum velit justo, non dictum tortor sollicitudin vitae. Donec leo nisl, rutrum ut finibus eu, malesuada ut est. Vestibulum cursus fermentum diam sit amet tincidunt. Etiam feugiat rutrum tempor. Donec quis iaculis magna. Nulla facilisi. Suspendisse maximus et elit consectetur hendrerit.&lt;/p&gt;\r\n&lt;p&gt;In nec ligula neque. Nulla orci nisi, pretium sed dui in, placerat rutrum ex. Donec in hendrerit ex. Vestibulum ultricies semper arcu sed egestas. Praesent lectus lacus, finibus sed neque at, viverra vehicula justo. Integer leo lectus, ultrices eget molestie ultricies, euismod in eros. Nullam efficitur orci ac lacinia lobortis. Donec placerat sapien vel vehicula euismod.&lt;/p&gt;\r\n&lt;p&gt;Mauris hendrerit nunc lectus, non blandit mauris posuere eget. Aenean mattis imperdiet nibh. Donec pellentesque aliquet velit at accumsan. Curabitur pharetra ipsum sit amet nisi hendrerit lacinia. Vivamus vel mollis elit. Nam ut convallis lectus. Etiam eget egestas odio, a egestas orci. Praesent vel enim id augue varius rhoncus at in felis. Quisque et augue nec dui lobortis ultricies. Mauris consequat fermentum augue, ut commodo libero ultrices eu.&lt;/p&gt;', 0, 1, 1, 3, 1, 0),
(4, 'Səyahət bələdçisi', '', '', '', '', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam maximus turpis laoreet justo scelerisque hendrerit. Maecenas ornare pharetra eros. Fusce ac diam in augue dapibus maximus et vitae sem. Sed ullamcorper tortor quis tellus placerat, et aliquet mi hendrerit. Proin non lacus commodo, facilisis neque ut, mollis libero. In scelerisque fermentum ipsum, eu ullamcorper elit sollicitudin elementum. Aenean sagittis imperdiet justo et lacinia. Proin rhoncus turpis lacus, vitae tempus lorem elementum ac. Aenean vel ligula odio. Nunc vitae sem dolor. Donec ac euismod massa. Donec et ultrices leo.&lt;/p&gt;\r\n&lt;p&gt;Etiam pulvinar urna vel posuere maximus. Nulla iaculis, mi id tempus mattis, augue lacus pharetra eros, vulputate viverra urna enim et nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet quam at neque bibendum ultricies. Nunc luctus accumsan bibendum. Aenean lacinia lorem vitae libero porta euismod. Donec eget sem interdum, condimentum dolor a, suscipit felis. Quisque a velit aliquam, porttitor lectus sit amet, gravida diam. Suspendisse at nisl neque. seyahet&lt;/p&gt;', 0, 2, 1, 1, 2, 0),
(5, 'Səyahət bələdçisi', '', '', '', '', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam maximus turpis laoreet justo scelerisque hendrerit. Maecenas ornare pharetra eros. Fusce ac diam in augue dapibus maximus et vitae sem. Sed ullamcorper tortor quis tellus placerat, et aliquet mi hendrerit. Proin non lacus commodo, facilisis neque ut, mollis libero. In scelerisque fermentum ipsum, eu ullamcorper elit sollicitudin elementum. Aenean sagittis imperdiet justo et lacinia. Proin rhoncus turpis lacus, vitae tempus lorem elementum ac. Aenean vel ligula odio. Nunc vitae sem dolor. Donec ac euismod massa. Donec et ultrices leo.&lt;/p&gt;\r\n&lt;p&gt;Etiam pulvinar urna vel posuere maximus. Nulla iaculis, mi id tempus mattis, augue lacus pharetra eros, vulputate viverra urna enim et nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet quam at neque bibendum ultricies. Nunc luctus accumsan bibendum. Aenean lacinia lorem vitae libero porta euismod. Donec eget sem interdum, condimentum dolor a, suscipit felis. Quisque a velit aliquam, porttitor lectus sit amet, gravida diam. Suspendisse at nisl neque. seyahet&lt;/p&gt;', 0, 2, 1, 2, 2, 0),
(6, 'Səyahət bələdçisi', '', '', '', '', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam maximus turpis laoreet justo scelerisque hendrerit. Maecenas ornare pharetra eros. Fusce ac diam in augue dapibus maximus et vitae sem. Sed ullamcorper tortor quis tellus placerat, et aliquet mi hendrerit. Proin non lacus commodo, facilisis neque ut, mollis libero. In scelerisque fermentum ipsum, eu ullamcorper elit sollicitudin elementum. Aenean sagittis imperdiet justo et lacinia. Proin rhoncus turpis lacus, vitae tempus lorem elementum ac. Aenean vel ligula odio. Nunc vitae sem dolor. Donec ac euismod massa. Donec et ultrices leo.&lt;/p&gt;\r\n&lt;p&gt;Etiam pulvinar urna vel posuere maximus. Nulla iaculis, mi id tempus mattis, augue lacus pharetra eros, vulputate viverra urna enim et nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet quam at neque bibendum ultricies. Nunc luctus accumsan bibendum. Aenean lacinia lorem vitae libero porta euismod. Donec eget sem interdum, condimentum dolor a, suscipit felis. Quisque a velit aliquam, porttitor lectus sit amet, gravida diam. Suspendisse at nisl neque. seyahet&lt;/p&gt;', 0, 2, 1, 3, 2, 0),
(7, 'Mədəniyyət, həyət tərzi və ənənələr', '', '', '', '', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam maximus turpis laoreet justo scelerisque hendrerit. Maecenas ornare pharetra eros. Fusce ac diam in augue dapibus maximus et vitae sem. Sed ullamcorper tortor quis tellus placerat, et aliquet mi hendrerit. Proin non lacus commodo, facilisis neque ut, mollis libero. In scelerisque fermentum ipsum, eu ullamcorper elit sollicitudin elementum. Aenean sagittis imperdiet justo et lacinia. Proin rhoncus turpis lacus, vitae tempus lorem elementum ac. Aenean vel ligula odio. Nunc vitae sem dolor. Donec ac euismod massa. Donec et ultrices leo.&lt;/p&gt;\r\n&lt;p&gt;Etiam pulvinar urna vel posuere maximus. Nulla iaculis, mi id tempus mattis, augue lacus pharetra eros, vulputate viverra urna enim et nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet quam at neque bibendum ultricies. Nunc luctus accumsan bibendum. Aenean lacinia lorem vitae libero porta euismod. Donec eget sem interdum, condimentum dolor a, suscipit felis. Quisque a velit aliquam, porttitor lectus sit amet, gravida diam. Suspendisse at nisl neque. medeniiyyet&lt;/p&gt;', 0, 3, 1, 1, 3, 0),
(8, 'Mədəniyyət, həyət tərzi və ənənələr', '', '', '', '', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam maximus turpis laoreet justo scelerisque hendrerit. Maecenas ornare pharetra eros. Fusce ac diam in augue dapibus maximus et vitae sem. Sed ullamcorper tortor quis tellus placerat, et aliquet mi hendrerit. Proin non lacus commodo, facilisis neque ut, mollis libero. In scelerisque fermentum ipsum, eu ullamcorper elit sollicitudin elementum. Aenean sagittis imperdiet justo et lacinia. Proin rhoncus turpis lacus, vitae tempus lorem elementum ac. Aenean vel ligula odio. Nunc vitae sem dolor. Donec ac euismod massa. Donec et ultrices leo.&lt;/p&gt;\r\n&lt;p&gt;Etiam pulvinar urna vel posuere maximus. Nulla iaculis, mi id tempus mattis, augue lacus pharetra eros, vulputate viverra urna enim et nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet quam at neque bibendum ultricies. Nunc luctus accumsan bibendum. Aenean lacinia lorem vitae libero porta euismod. Donec eget sem interdum, condimentum dolor a, suscipit felis. Quisque a velit aliquam, porttitor lectus sit amet, gravida diam. Suspendisse at nisl neque.&lt;/p&gt;', 0, 3, 1, 2, 3, 0),
(9, 'Mədəniyyət, həyət tərzi və ənənələr', '', '', '', '', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam maximus turpis laoreet justo scelerisque hendrerit. Maecenas ornare pharetra eros. Fusce ac diam in augue dapibus maximus et vitae sem. Sed ullamcorper tortor quis tellus placerat, et aliquet mi hendrerit. Proin non lacus commodo, facilisis neque ut, mollis libero. In scelerisque fermentum ipsum, eu ullamcorper elit sollicitudin elementum. Aenean sagittis imperdiet justo et lacinia. Proin rhoncus turpis lacus, vitae tempus lorem elementum ac. Aenean vel ligula odio. Nunc vitae sem dolor. Donec ac euismod massa. Donec et ultrices leo.&lt;/p&gt;\r\n&lt;p&gt;Etiam pulvinar urna vel posuere maximus. Nulla iaculis, mi id tempus mattis, augue lacus pharetra eros, vulputate viverra urna enim et nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet quam at neque bibendum ultricies. Nunc luctus accumsan bibendum. Aenean lacinia lorem vitae libero porta euismod. Donec eget sem interdum, condimentum dolor a, suscipit felis. Quisque a velit aliquam, porttitor lectus sit amet, gravida diam. Suspendisse at nisl neque.&lt;/p&gt;', 0, 3, 1, 3, 3, 0),
(10, 'İqtisadiyyat', '', '', '', '', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam maximus turpis laoreet justo scelerisque hendrerit. Maecenas ornare pharetra eros. Fusce ac diam in augue dapibus maximus et vitae sem. Sed ullamcorper tortor quis tellus placerat, et aliquet mi hendrerit. Proin non lacus commodo, facilisis neque ut, mollis libero. In scelerisque fermentum ipsum, eu ullamcorper elit sollicitudin elementum. Aenean sagittis imperdiet justo et lacinia. Proin rhoncus turpis lacus, vitae tempus lorem elementum ac. Aenean vel ligula odio. Nunc vitae sem dolor. Donec ac euismod massa. Donec et ultrices leo.&lt;/p&gt;\r\n&lt;p&gt;Etiam pulvinar urna vel posuere maximus. Nulla iaculis, mi id tempus mattis, augue lacus pharetra eros, vulputate viverra urna enim et nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet quam at neque bibendum ultricies. Nunc luctus accumsan bibendum. Aenean lacinia lorem vitae libero porta euismod. Donec eget sem interdum, condimentum dolor a, suscipit felis. Quisque a velit aliquam, porttitor lectus sit amet, gravida&amp;nbsp; iqtisadiyyat&lt;/p&gt;', 0, 4, 1, 1, 4, 0),
(11, 'İqtisadiyyat', '', '', '', '', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam maximus turpis laoreet justo scelerisque hendrerit. Maecenas ornare pharetra eros. Fusce ac diam in augue dapibus maximus et vitae sem. Sed ullamcorper tortor quis tellus placerat, et aliquet mi hendrerit. Proin non lacus commodo, facilisis neque ut, mollis libero. In scelerisque fermentum ipsum, eu ullamcorper elit sollicitudin elementum. Aenean sagittis imperdiet justo et lacinia. Proin rhoncus turpis lacus, vitae tempus lorem elementum ac. Aenean vel ligula odio. Nunc vitae sem dolor. Donec ac euismod massa. Donec et ultrices leo.&lt;/p&gt;\r\n&lt;p&gt;Etiam pulvinar urna vel posuere maximus. Nulla iaculis, mi id tempus mattis, augue lacus pharetra eros, vulputate viverra urna enim et nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet quam at neque bibendum ultricies. Nunc luctus accumsan bibendum. Aenean lacinia lorem vitae libero porta euismod. Donec eget sem interdum, condimentum dolor a, suscipit felis. Quisque a velit aliquam, porttitor lectus sit amet, gravida&amp;nbsp; iqtisadiyyat&lt;/p&gt;', 0, 4, 1, 2, 4, 0),
(12, 'İqtisadiyyat', '', '', '', '', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam maximus turpis laoreet justo scelerisque hendrerit. Maecenas ornare pharetra eros. Fusce ac diam in augue dapibus maximus et vitae sem. Sed ullamcorper tortor quis tellus placerat, et aliquet mi hendrerit. Proin non lacus commodo, facilisis neque ut, mollis libero. In scelerisque fermentum ipsum, eu ullamcorper elit sollicitudin elementum. Aenean sagittis imperdiet justo et lacinia. Proin rhoncus turpis lacus, vitae tempus lorem elementum ac. Aenean vel ligula odio. Nunc vitae sem dolor. Donec ac euismod massa. Donec et ultrices leo.&lt;/p&gt;\r\n&lt;p&gt;Etiam pulvinar urna vel posuere maximus. Nulla iaculis, mi id tempus mattis, augue lacus pharetra eros, vulputate viverra urna enim et nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet quam at neque bibendum ultricies. Nunc luctus accumsan bibendum. Aenean lacinia lorem vitae libero porta euismod. Donec eget sem interdum, condimentum dolor a, suscipit felis. Quisque a velit aliquam, porttitor lectus sit amet, gravida&amp;nbsp; iqtisadiyyat&lt;/p&gt;', 0, 4, 1, 3, 4, 0),
(13, 'Əlavə məlumat', '', '', '', '', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam maximus turpis laoreet justo scelerisque hendrerit. Maecenas ornare pharetra eros. Fusce ac diam in augue dapibus maximus et vitae sem. Sed ullamcorper tortor quis tellus placerat, et aliquet mi hendrerit. Proin non lacus commodo, facilisis neque ut, mollis libero. In scelerisque fermentum ipsum, eu ullamcorper elit sollicitudin elementum. Aenean sagittis imperdiet justo et lacinia. Proin rhoncus turpis lacus, vitae tempus lorem elementum ac. Aenean vel ligula odio. Nunc vitae sem dolor. Donec ac euismod massa. Donec et ultrices leo.&lt;/p&gt;\r\n&lt;p&gt;Etiam pulvinar urna vel posuere maximus. Nulla iaculis, mi id tempus mattis, augue lacus pharetra eros, vulputate viverra urna enim et nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet quam at neque bibendum ultricies. Nunc luctus accumsan bibendum. Aenean lacinia lorem vitae libero porta euismod. Donec eget sem interdum, condimentum dolor a, suscipit felis. Quisque a velit aliquam, porttitor lectus sit amet, gravida&amp;nbsp; elave&lt;/p&gt;', 0, 5, 1, 1, 5, 0),
(14, 'Əlavə məlumat', '', '', '', '', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam maximus turpis laoreet justo scelerisque hendrerit. Maecenas ornare pharetra eros. Fusce ac diam in augue dapibus maximus et vitae sem. Sed ullamcorper tortor quis tellus placerat, et aliquet mi hendrerit. Proin non lacus commodo, facilisis neque ut, mollis libero. In scelerisque fermentum ipsum, eu ullamcorper elit sollicitudin elementum. Aenean sagittis imperdiet justo et lacinia. Proin rhoncus turpis lacus, vitae tempus lorem elementum ac. Aenean vel ligula odio. Nunc vitae sem dolor. Donec ac euismod massa. Donec et ultrices leo.&lt;/p&gt;\r\n&lt;p&gt;Etiam pulvinar urna vel posuere maximus. Nulla iaculis, mi id tempus mattis, augue lacus pharetra eros, vulputate viverra urna enim et nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet quam at neque bibendum ultricies. Nunc luctus accumsan bibendum. Aenean lacinia lorem vitae libero porta euismod. Donec eget sem interdum, condimentum dolor a, suscipit felis. Quisque a velit aliquam, porttitor lectus sit amet, gravida&amp;nbsp; elave&lt;/p&gt;', 0, 5, 1, 2, 5, 0),
(15, 'Əlavə məlumat', '', '', '', '', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam maximus turpis laoreet justo scelerisque hendrerit. Maecenas ornare pharetra eros. Fusce ac diam in augue dapibus maximus et vitae sem. Sed ullamcorper tortor quis tellus placerat, et aliquet mi hendrerit. Proin non lacus commodo, facilisis neque ut, mollis libero. In scelerisque fermentum ipsum, eu ullamcorper elit sollicitudin elementum. Aenean sagittis imperdiet justo et lacinia. Proin rhoncus turpis lacus, vitae tempus lorem elementum ac. Aenean vel ligula odio. Nunc vitae sem dolor. Donec ac euismod massa. Donec et ultrices leo.&lt;/p&gt;\r\n&lt;p&gt;Etiam pulvinar urna vel posuere maximus. Nulla iaculis, mi id tempus mattis, augue lacus pharetra eros, vulputate viverra urna enim et nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet quam at neque bibendum ultricies. Nunc luctus accumsan bibendum. Aenean lacinia lorem vitae libero porta euismod. Donec eget sem interdum, condimentum dolor a, suscipit felis. Quisque a velit aliquam, porttitor lectus sit amet, gravida&amp;nbsp; elave&lt;/p&gt;', 0, 5, 1, 3, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `news_gallery`
--

CREATE TABLE `news_gallery` (
  `id` int(10) NOT NULL,
  `tarix` int(11) DEFAULT NULL,
  `tip` varchar(5) NOT NULL,
  `blog_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news_gallery`
--

INSERT INTO `news_gallery` (`id`, `tarix`, `tip`, `blog_id`) VALUES
(1, 1537777968, 'jpg', 2),
(2, 1537777968, 'jpg', 2),
(3, 1537778300, 'jpg', 3),
(4, 1537778300, 'jpg', 3);

-- --------------------------------------------------------

--
-- Table structure for table `novbe`
--

CREATE TABLE `novbe` (
  `id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `manset` int(2) NOT NULL DEFAULT '0',
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL,
  `created_at` int(13) NOT NULL,
  `updated_at` int(13) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `novbe`
--

INSERT INTO `novbe` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `manset`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`, `created_at`, `updated_at`) VALUES
(1, '', '', '', '', '', '&lt;p&gt;&lt;span&gt;Azərbaycan Respublikasında 15 g&amp;uuml;ndən artıq m&amp;uuml;vəqqəti qalmaq istəyən əcnəbilər və ya vətəndaşlığı olmayan şəxslər olduğu yer &amp;uuml;zrə qeydiyyata alınmalıdırlar. Qeydiyyat &amp;uuml;&amp;ccedil;&amp;uuml;n əcnəbi &amp;ouml;lkəyə gəldiyi vaxtdan 15 g&amp;uuml;n ərzində elektron qaydada D&amp;ouml;vlət Miqrasiya Xidmətinin rəsmi saytının&lt;/span&gt;&lt;/p&gt;', 0, 0, 1, 1, 1, 1, 0, 1538462357, 1538462374),
(2, '', '', '', '', '', '&lt;p&gt;&lt;span&gt;Azərbaycan Respublikasında 15 g&amp;uuml;ndən artıq m&amp;uuml;vəqqəti qalmaq istəyən əcnəbilər və ya vətəndaşlığı olmayan şəxslər olduğu yer &amp;uuml;zrə qeydiyyata alınmalıdırlar. Qeydiyyat &amp;uuml;&amp;ccedil;&amp;uuml;n əcnəbi &amp;ouml;lkəyə gəldiyi vaxtdan 15 g&amp;uuml;n ərzində elektron qaydada D&amp;ouml;vlət Miqrasiya Xidmətinin rəsmi saytının&lt;/span&gt;&lt;/p&gt;', 0, 0, 1, 1, 2, 1, 0, 1538462357, 1538462374),
(3, '', '', '', '', '', '&lt;p&gt;&lt;span&gt;Azərbaycan Respublikasında 15 g&amp;uuml;ndən artıq m&amp;uuml;vəqqəti qalmaq istəyən əcnəbilər və ya vətəndaşlığı olmayan şəxslər olduğu yer &amp;uuml;zrə qeydiyyata alınmalıdırlar. Qeydiyyat &amp;uuml;&amp;ccedil;&amp;uuml;n əcnəbi &amp;ouml;lkəyə gəldiyi vaxtdan 15 g&amp;uuml;n ərzində elektron qaydada D&amp;ouml;vlət Miqrasiya Xidmətinin rəsmi saytının&lt;/span&gt;&lt;/p&gt;', 0, 0, 1, 1, 3, 1, 0, 1538462357, 1538462374);

-- --------------------------------------------------------

--
-- Table structure for table `novbeler`
--

CREATE TABLE `novbeler` (
  `id` int(11) NOT NULL,
  `applytype` varchar(50) NOT NULL,
  `docnum` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `patronymicname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL,
  `hour` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `novbeler`
--

INSERT INTO `novbeler` (`id`, `applytype`, `docnum`, `name`, `surname`, `patronymicname`, `email`, `address`, `date`, `hour`) VALUES
(1, 'Müraciətlərin qəbulu', 'qw63636', 'Fuad', 'Hasanli', 'Hidayat', 'fhesenli92@gmail.com', 'asdas', '02/20/2018', '16:30'),
(2, 'Müraciətlərin qəbulu', 'qw63636', 'Fuad', 'Hasanli', 'Hidayat', 'fhesenli92@gmail.com', 'asdas', '02/20/2018', '16:30'),
(3, 'Məlumat əldə edilməsi', 'qw63636', 'asda', 'asda', 'asadasda', 'fhesenli92@gmail.com', 'asdas', '10/11/2018', '18:00'),
(4, 'Məlumat əldə edilməsi', 'qw63636', 'asda', 'asda', 'asadasda', 'fhesenli92@gmail.com', 'asdas', '10/11/2018', '18:00'),
(5, 'Məlumat əldə edilməsi', 'qw63636', 'asda', 'asda', 'asadasda', 'fhesenli92@gmail.com', 'asdas', '10/11/2018', '18:00'),
(6, 'Məlumat əldə edilməsi', 'qw63636', 'asda', 'asda', 'asadasda', 'fhesenli92@gmail.com', 'asdas', '10/03/2018', '17:30');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(10) NOT NULL,
  `tarix` int(11) NOT NULL,
  `basliq` varchar(255) DEFAULT NULL,
  `link` varchar(255) NOT NULL,
  `tam_metn` text,
  `tip` varchar(5) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) DEFAULT '0',
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `tarix`, `basliq`, `link`, `tam_metn`, `tip`, `sira`, `aktivlik`, `lang_id`, `auto_id`) VALUES
(1, 1538049024, '', 'https://www.migration.gov.az/useful/freedom', '', 'png', 1, 1, 1, 1),
(2, 1538049024, '', 'https://www.migration.gov.az/useful/freedom', '', 'png', 1, 1, 2, 1),
(3, 1538049024, '', 'https://www.migration.gov.az/useful/freedom', '', 'png', 1, 1, 3, 1),
(4, 1538049071, '', 'https://www.migration.gov.az/home/calculator', '', 'png', 2, 1, 1, 2),
(5, 1538049071, '', 'https://www.migration.gov.az/home/calculator', '', 'png', 2, 1, 2, 2),
(6, 1538049071, '', 'https://www.migration.gov.az/home/calculator', '', 'png', 2, 1, 3, 2),
(7, 1538049079, '', 'https://www.e-gov.az/', '', 'png', 3, 1, 1, 3),
(8, 1538049079, '', 'https://www.e-gov.az/', '', 'png', 3, 1, 2, 3),
(9, 1538049079, '', 'https://www.e-gov.az/', '', 'png', 3, 1, 3, 3),
(10, 1538049092, '', 'https://www.migration.gov.az/', '', 'png', 4, 1, 1, 4),
(11, 1538049092, '', 'https://www.migration.gov.az/', '', 'png', 4, 1, 2, 4),
(12, 1538049092, '', 'https://www.migration.gov.az/', '', 'png', 4, 1, 3, 4);

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE `regions` (
  `id` int(3) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `parent_auto_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `name`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `parent_auto_id`) VALUES
(1, 'Baki səhəri', 1, 1, 1, 1, 0),
(2, 'Baki səhəri', 1, 1, 2, 1, 0),
(3, 'Baki səhəri', 1, 1, 3, 1, 0),
(4, 'Binəqədi rayonu ', 2, 1, 1, 2, 0),
(5, 'Binəqədi rayonu ', 2, 1, 2, 2, 0),
(6, 'Binəqədi rayonu ', 2, 1, 3, 2, 0),
(7, 'Qaradag rayonu ', 3, 1, 1, 3, 0),
(8, 'Qaradag rayonu ', 3, 1, 2, 3, 0),
(9, 'Qaradag rayonu ', 3, 1, 3, 3, 0),
(10, 'Xəzər rayonu ', 4, 1, 1, 4, 0),
(11, 'Xəzər rayonu ', 4, 1, 2, 4, 0),
(12, 'Xəzər rayonu ', 4, 1, 3, 4, 0),
(13, 'Səbail rayonu ', 5, 1, 1, 5, 0),
(14, 'Səbail rayonu ', 5, 1, 2, 5, 0),
(15, 'Səbail rayonu ', 5, 1, 3, 5, 0),
(16, 'Sabunçu rayonu ', 6, 1, 1, 6, 0),
(17, 'Sabunçu rayonu ', 6, 1, 2, 6, 0),
(18, 'Sabunçu rayonu ', 6, 1, 3, 6, 0),
(19, 'Suraxani rayonu ', 7, 1, 1, 7, 0),
(20, 'Suraxani rayonu ', 7, 1, 2, 7, 0),
(21, 'Suraxani rayonu ', 7, 1, 3, 7, 0),
(22, 'Nərimanov rayonu ', 8, 1, 1, 8, 0),
(23, 'Nərimanov rayonu ', 8, 1, 2, 8, 0),
(24, 'Nərimanov rayonu ', 8, 1, 3, 8, 0),
(25, 'Nəsimi rayonu ', 9, 1, 1, 9, 0),
(26, 'Nəsimi rayonu ', 9, 1, 2, 9, 0),
(27, 'Nəsimi rayonu ', 9, 1, 3, 9, 0),
(28, 'Nizami rayonu ', 10, 1, 1, 10, 0),
(29, 'Nizami rayonu ', 10, 1, 2, 10, 0),
(30, 'Nizami rayonu ', 10, 1, 3, 10, 0),
(31, 'Pirallahi rayonu ', 11, 1, 1, 11, 0),
(32, 'Pirallahi rayonu ', 11, 1, 2, 11, 0),
(33, 'Pirallahi rayonu ', 11, 1, 3, 11, 0),
(34, 'Xətai rayonu ', 12, 1, 1, 12, 0),
(35, 'Xətai rayonu ', 12, 1, 2, 12, 0),
(36, 'Xətai rayonu ', 12, 1, 3, 12, 0),
(37, 'Yasamal rayonu ', 13, 1, 1, 13, 0),
(38, 'Yasamal rayonu ', 13, 1, 2, 13, 0),
(39, 'Yasamal rayonu ', 13, 1, 3, 13, 0),
(40, 'Abseron rayonu ', 14, 1, 1, 14, 0),
(41, 'Abseron rayonu ', 14, 1, 2, 14, 0),
(42, 'Abseron rayonu ', 14, 1, 3, 14, 0),
(43, 'Sumqayit səhəri ', 15, 1, 1, 15, 0),
(44, 'Sumqayit səhəri ', 15, 1, 2, 15, 0),
(45, 'Sumqayit səhəri ', 15, 1, 3, 15, 0),
(46, 'Xizi rayonu ', 16, 1, 1, 16, 0),
(47, 'Xizi rayonu ', 16, 1, 2, 16, 0),
(48, 'Xizi rayonu ', 16, 1, 3, 16, 0),
(49, 'Gəncə səhəri ', 17, 1, 1, 17, 0),
(50, 'Gəncə səhəri ', 17, 1, 2, 17, 0),
(51, 'Gəncə səhəri ', 17, 1, 3, 17, 0),
(52, 'Qazax rayonu ', 18, 1, 1, 18, 0),
(53, 'Qazax rayonu ', 18, 1, 2, 18, 0),
(54, 'Qazax rayonu ', 18, 1, 3, 18, 0),
(55, 'Agstafa rayonu ', 19, 1, 1, 19, 0),
(56, 'Agstafa rayonu ', 19, 1, 2, 19, 0),
(57, 'Agstafa rayonu ', 19, 1, 3, 19, 0),
(58, 'Tovuz rayonu ', 20, 1, 1, 20, 0),
(59, 'Tovuz rayonu ', 20, 1, 2, 20, 0),
(60, 'Tovuz rayonu ', 20, 1, 3, 20, 0),
(61, 'Səmkir rayonu ', 21, 1, 1, 21, 0),
(62, 'Səmkir rayonu ', 21, 1, 2, 21, 0),
(63, 'Səmkir rayonu ', 21, 1, 3, 21, 0),
(64, 'Gədəbəy rayonu ', 22, 1, 1, 22, 0),
(65, 'Gədəbəy rayonu ', 22, 1, 2, 22, 0),
(66, 'Gədəbəy rayonu ', 22, 1, 3, 22, 0),
(67, 'Daskəsən rayonu ', 23, 1, 1, 23, 0),
(68, 'Daskəsən rayonu ', 23, 1, 2, 23, 0),
(69, 'Daskəsən rayonu ', 23, 1, 3, 23, 0),
(70, 'Samux rayonu ', 24, 1, 1, 24, 0),
(71, 'Samux rayonu ', 24, 1, 2, 24, 0),
(72, 'Samux rayonu ', 24, 1, 3, 24, 0),
(73, 'Göygöl rayonu ', 25, 1, 1, 25, 0),
(74, 'Göygöl rayonu ', 25, 1, 2, 25, 0),
(75, 'Göygöl rayonu ', 25, 1, 3, 25, 0),
(76, 'Goranboy rayonu ', 26, 1, 1, 26, 0),
(77, 'Goranboy rayonu ', 26, 1, 2, 26, 0),
(78, 'Goranboy rayonu ', 26, 1, 3, 26, 0),
(79, 'Naftalan səhəri ', 27, 1, 1, 27, 0),
(80, 'Naftalan səhəri ', 27, 1, 2, 27, 0),
(81, 'Naftalan səhəri ', 27, 1, 3, 27, 0),
(82, 'Balakən rayonu ', 28, 1, 1, 28, 0),
(83, 'Balakən rayonu ', 28, 1, 2, 28, 0),
(84, 'Balakən rayonu ', 28, 1, 3, 28, 0),
(85, 'Zaqatala rayonu ', 29, 1, 1, 29, 0),
(86, 'Zaqatala rayonu ', 29, 1, 2, 29, 0),
(87, 'Zaqatala rayonu ', 29, 1, 3, 29, 0),
(88, 'Qax rayonu ', 30, 1, 1, 30, 0),
(89, 'Qax rayonu ', 30, 1, 2, 30, 0),
(90, 'Qax rayonu ', 30, 1, 3, 30, 0),
(91, 'Səki səhəri ', 31, 1, 1, 31, 0),
(92, 'Səki səhəri ', 31, 1, 2, 31, 0),
(93, 'Səki səhəri ', 31, 1, 3, 31, 0),
(94, 'Oguz rayonu ', 32, 1, 1, 32, 0),
(95, 'Oguz rayonu ', 32, 1, 2, 32, 0),
(96, 'Oguz rayonu ', 32, 1, 3, 32, 0),
(97, 'Qəbələ rayonu ', 33, 1, 1, 33, 0),
(98, 'Qəbələ rayonu ', 33, 1, 2, 33, 0),
(99, 'Qəbələ rayonu ', 33, 1, 3, 33, 0),
(100, 'Astara rayonu ', 34, 1, 1, 34, 0),
(101, 'Astara rayonu ', 34, 1, 2, 34, 0),
(102, 'Astara rayonu ', 34, 1, 3, 34, 0),
(103, 'Lənkəran səhəri ', 35, 1, 1, 35, 0),
(104, 'Lənkəran səhəri ', 35, 1, 2, 35, 0),
(105, 'Lənkəran səhəri ', 35, 1, 3, 35, 0),
(106, 'Lerik rayonu ', 36, 1, 1, 36, 0),
(107, 'Lerik rayonu ', 36, 1, 2, 36, 0),
(108, 'Lerik rayonu ', 36, 1, 3, 36, 0),
(109, 'Yardimli rayonu ', 37, 1, 1, 37, 0),
(110, 'Yardimli rayonu ', 37, 1, 2, 37, 0),
(111, 'Yardimli rayonu ', 37, 1, 3, 37, 0),
(112, 'Masalli rayonu ', 38, 1, 1, 38, 0),
(113, 'Masalli rayonu ', 38, 1, 2, 38, 0),
(114, 'Masalli rayonu ', 38, 1, 3, 38, 0),
(115, 'Cəlilabad rayonu ', 39, 1, 1, 39, 0),
(116, 'Cəlilabad rayonu ', 39, 1, 2, 39, 0),
(117, 'Cəlilabad rayonu ', 39, 1, 3, 39, 0),
(118, 'Qusar rayonu ', 40, 1, 1, 40, 0),
(119, 'Qusar rayonu ', 40, 1, 2, 40, 0),
(120, 'Qusar rayonu ', 40, 1, 3, 40, 0),
(121, 'Xaçmaz rayonu ', 41, 1, 1, 41, 0),
(122, 'Xaçmaz rayonu ', 41, 1, 2, 41, 0),
(123, 'Xaçmaz rayonu ', 41, 1, 3, 41, 0),
(124, 'Quba rayonu ', 42, 1, 1, 42, 0),
(125, 'Quba rayonu ', 42, 1, 2, 42, 0),
(126, 'Quba rayonu ', 42, 1, 3, 42, 0),
(127, 'Sabran rayonu ', 43, 1, 1, 43, 0),
(128, 'Sabran rayonu ', 43, 1, 2, 43, 0),
(129, 'Sabran rayonu ', 43, 1, 3, 43, 0),
(130, 'Siyəzən rayonu ', 44, 1, 1, 44, 0),
(131, 'Siyəzən rayonu ', 44, 1, 2, 44, 0),
(132, 'Siyəzən rayonu ', 44, 1, 3, 44, 0),
(133, 'Göyçay rayonu ', 45, 1, 1, 45, 0),
(134, 'Göyçay rayonu ', 45, 1, 2, 45, 0),
(135, 'Göyçay rayonu ', 45, 1, 3, 45, 0),
(136, 'Beyləqan rayonu ', 46, 1, 1, 46, 0),
(137, 'Beyləqan rayonu ', 46, 1, 2, 46, 0),
(138, 'Beyləqan rayonu ', 46, 1, 3, 46, 0),
(139, 'Agcabədi rayonu ', 47, 1, 1, 47, 0),
(140, 'Agcabədi rayonu ', 47, 1, 2, 47, 0),
(141, 'Agcabədi rayonu ', 47, 1, 3, 47, 0),
(142, 'Bərdə rayonu ', 48, 1, 1, 48, 0),
(143, 'Bərdə rayonu ', 48, 1, 2, 48, 0),
(144, 'Bərdə rayonu ', 48, 1, 3, 48, 0),
(145, 'Neftçala rayonu ', 49, 1, 1, 49, 0),
(146, 'Neftçala rayonu ', 49, 1, 2, 49, 0),
(147, 'Neftçala rayonu ', 49, 1, 3, 49, 0),
(148, 'Biləsuvar rayonu ', 50, 1, 1, 50, 0),
(149, 'Biləsuvar rayonu ', 50, 1, 2, 50, 0),
(150, 'Biləsuvar rayonu ', 50, 1, 3, 50, 0),
(151, 'Salyan rayonu ', 51, 1, 1, 51, 0),
(152, 'Salyan rayonu ', 51, 1, 2, 51, 0),
(153, 'Salyan rayonu ', 51, 1, 3, 51, 0),
(154, 'Yevlax səhəri ', 52, 1, 1, 52, 0),
(155, 'Yevlax səhəri ', 52, 1, 2, 52, 0),
(156, 'Yevlax səhəri ', 52, 1, 3, 52, 0),
(157, 'mingecevirseheri ', 53, 1, 1, 53, 0),
(158, 'mingecevirseheri ', 53, 1, 2, 53, 0),
(159, 'mingecevirseheri ', 53, 1, 3, 53, 0),
(160, 'Agdas rayonu ', 54, 1, 1, 54, 0),
(161, 'Agdas rayonu ', 54, 1, 2, 54, 0),
(162, 'Agdas rayonu ', 54, 1, 3, 54, 0),
(163, 'Ucar rayonu ', 55, 1, 1, 55, 0),
(164, 'Ucar rayonu ', 55, 1, 2, 55, 0),
(165, 'Ucar rayonu ', 55, 1, 3, 55, 0),
(166, 'Zərdab rayonu ', 56, 1, 1, 56, 0),
(167, 'Zərdab rayonu ', 56, 1, 2, 56, 0),
(168, 'Zərdab rayonu ', 56, 1, 3, 56, 0),
(169, 'Kürdəmir rayonu ', 57, 1, 1, 57, 0),
(170, 'Kürdəmir rayonu ', 57, 1, 2, 57, 0),
(171, 'Kürdəmir rayonu ', 57, 1, 3, 57, 0),
(172, 'Imisli rayonu ', 58, 1, 1, 58, 0),
(173, 'Imisli rayonu ', 58, 1, 2, 58, 0),
(174, 'Imisli rayonu ', 58, 1, 3, 58, 0),
(175, 'Saatli rayonu ', 59, 1, 1, 59, 0),
(176, 'Saatli rayonu ', 59, 1, 2, 59, 0),
(177, 'Saatli rayonu ', 59, 1, 3, 59, 0),
(178, 'Sabirabad rayonu ', 60, 1, 1, 60, 0),
(179, 'Sabirabad rayonu ', 60, 1, 2, 60, 0),
(180, 'Sabirabad rayonu ', 60, 1, 3, 60, 0),
(181, 'Haciqabul rayonu ', 61, 1, 1, 61, 0),
(182, 'Haciqabul rayonu ', 61, 1, 2, 61, 0),
(183, 'Haciqabul rayonu ', 61, 1, 3, 61, 0),
(184, 'Sirvan səhəri ', 62, 1, 1, 62, 0),
(185, 'Sirvan səhəri ', 62, 1, 2, 62, 0),
(186, 'Sirvan səhəri ', 62, 1, 3, 62, 0),
(187, 'Cəbrayil rayonu ', 63, 1, 1, 63, 0),
(188, 'Cəbrayil rayonu ', 63, 1, 2, 63, 0),
(189, 'Cəbrayil rayonu ', 63, 1, 3, 63, 0),
(190, 'Füzuli rayonu ', 64, 1, 1, 64, 0),
(191, 'Füzuli rayonu ', 64, 1, 2, 64, 0),
(192, 'Füzuli rayonu ', 64, 1, 3, 64, 0),
(193, 'Agdam rayonu ', 65, 1, 1, 65, 0),
(194, 'Agdam rayonu ', 65, 1, 2, 65, 0),
(195, 'Agdam rayonu ', 65, 1, 3, 65, 0),
(196, 'Tərtər rayonu ', 66, 1, 1, 66, 0),
(197, 'Tərtər rayonu ', 66, 1, 2, 66, 0),
(198, 'Tərtər rayonu ', 66, 1, 3, 66, 0),
(199, 'Xocali rayonu ', 67, 1, 1, 67, 0),
(200, 'Xocali rayonu ', 67, 1, 2, 67, 0),
(201, 'Xocali rayonu ', 67, 1, 3, 67, 0),
(202, 'Susa rayonu ', 68, 1, 1, 68, 0),
(203, 'Susa rayonu ', 68, 1, 2, 68, 0),
(204, 'Susa rayonu ', 68, 1, 3, 68, 0),
(205, 'Xocavənd rayonu ', 69, 1, 1, 69, 0),
(206, 'Xocavənd rayonu ', 69, 1, 2, 69, 0),
(207, 'Xocavənd rayonu ', 69, 1, 3, 69, 0),
(208, 'Xankəndi səhəri ', 70, 1, 1, 70, 0),
(209, 'Xankəndi səhəri ', 70, 1, 2, 70, 0),
(210, 'Xankəndi səhəri ', 70, 1, 3, 70, 0),
(211, 'Kəlbəcər rayonu ', 71, 1, 1, 71, 0),
(212, 'Kəlbəcər rayonu ', 71, 1, 2, 71, 0),
(213, 'Kəlbəcər rayonu ', 71, 1, 3, 71, 0),
(214, 'Laçin rayonu ', 72, 1, 1, 72, 0),
(215, 'Laçin rayonu ', 72, 1, 2, 72, 0),
(216, 'Laçin rayonu ', 72, 1, 3, 72, 0),
(217, 'Qubadli rayonu ', 73, 1, 1, 73, 0),
(218, 'Qubadli rayonu ', 73, 1, 2, 73, 0),
(219, 'Qubadli rayonu ', 73, 1, 3, 73, 0),
(220, 'Zəngilan rayonu ', 74, 1, 1, 74, 0),
(221, 'Zəngilan rayonu ', 74, 1, 2, 74, 0),
(222, 'Zəngilan rayonu ', 74, 1, 3, 74, 0),
(223, 'Qobustan rayonu ', 75, 1, 1, 75, 0),
(224, 'Qobustan rayonu ', 75, 1, 2, 75, 0),
(225, 'Qobustan rayonu ', 75, 1, 3, 75, 0),
(226, 'Ismayilli rayonu ', 76, 1, 1, 76, 0),
(227, 'Ismayilli rayonu ', 76, 1, 2, 76, 0),
(228, 'Ismayilli rayonu ', 76, 1, 3, 76, 0),
(229, 'Agsu rayonu ', 77, 1, 1, 77, 0),
(230, 'Agsu rayonu ', 77, 1, 2, 77, 0),
(231, 'Agsu rayonu ', 77, 1, 3, 77, 0),
(232, 'Samaxi rayonu ', 78, 1, 1, 78, 0),
(233, 'Samaxi rayonu ', 78, 1, 2, 78, 0),
(234, 'Samaxi rayonu ', 78, 1, 3, 78, 0),
(235, 'Naxçivan səhəri ', 79, 1, 1, 79, 0),
(236, 'Naxçivan səhəri ', 79, 1, 2, 79, 0),
(237, 'Naxçivan səhəri ', 79, 1, 3, 79, 0),
(238, 'Sərur rayonu ', 80, 1, 1, 80, 0),
(239, 'Sərur rayonu ', 80, 1, 2, 80, 0),
(240, 'Sərur rayonu ', 80, 1, 3, 80, 0),
(241, 'Babək rayonu ', 81, 1, 1, 81, 0),
(242, 'Babək rayonu ', 81, 1, 2, 81, 0),
(243, 'Babək rayonu ', 81, 1, 3, 81, 0),
(244, 'Ordubad rayonu ', 82, 1, 1, 82, 0),
(245, 'Ordubad rayonu ', 82, 1, 2, 82, 0),
(246, 'Ordubad rayonu ', 82, 1, 3, 82, 0),
(247, 'Culfa rayonu ', 83, 1, 1, 83, 0),
(248, 'Culfa rayonu ', 83, 1, 2, 83, 0),
(249, 'Culfa rayonu ', 83, 1, 3, 83, 0),
(250, 'Kəngərli rayonu ', 84, 1, 1, 84, 0),
(251, 'Kəngərli rayonu ', 84, 1, 2, 84, 0),
(252, 'Kəngərli rayonu ', 84, 1, 3, 84, 0),
(253, 'Sahbuz rayonu ', 85, 1, 1, 85, 0),
(254, 'Sahbuz rayonu ', 85, 1, 2, 85, 0),
(255, 'Sahbuz rayonu ', 85, 1, 3, 85, 0),
(256, 'Sədərək rayonu', 86, 1, 1, 86, 0),
(257, 'Sədərək rayonu', 86, 1, 2, 86, 0),
(258, 'Sədərək rayonu', 86, 1, 3, 86, 0),
(259, 'Baki səhəri ', 87, 1, 1, 87, 0),
(260, 'Baki səhəri ', 87, 1, 2, 87, 0),
(261, 'Baki səhəri ', 87, 1, 3, 87, 0),
(262, 'Binəqədi rayonu ', 88, 1, 1, 88, 0),
(263, 'Binəqədi rayonu ', 88, 1, 2, 88, 0),
(264, 'Binəqədi rayonu ', 88, 1, 3, 88, 0),
(265, 'Qaradag rayonu ', 89, 1, 1, 89, 0),
(266, 'Qaradag rayonu ', 89, 1, 2, 89, 0),
(267, 'Qaradag rayonu ', 89, 1, 3, 89, 0),
(268, 'Xəzər rayonu ', 90, 1, 1, 90, 0),
(269, 'Xəzər rayonu ', 90, 1, 2, 90, 0),
(270, 'Xəzər rayonu ', 90, 1, 3, 90, 0),
(271, 'Səbail rayonu ', 91, 1, 1, 91, 0),
(272, 'Səbail rayonu ', 91, 1, 2, 91, 0),
(273, 'Səbail rayonu ', 91, 1, 3, 91, 0),
(274, 'Sabunçu rayonu ', 92, 1, 1, 92, 0),
(275, 'Sabunçu rayonu ', 92, 1, 2, 92, 0),
(276, 'Sabunçu rayonu ', 92, 1, 3, 92, 0),
(277, 'Suraxani rayonu ', 93, 1, 1, 93, 0),
(278, 'Suraxani rayonu ', 93, 1, 2, 93, 0),
(279, 'Suraxani rayonu ', 93, 1, 3, 93, 0),
(280, 'Nərimanov rayonu ', 94, 1, 1, 94, 0),
(281, 'Nərimanov rayonu ', 94, 1, 2, 94, 0),
(282, 'Nərimanov rayonu ', 94, 1, 3, 94, 0),
(283, 'Nəsimi rayonu ', 95, 1, 1, 95, 0),
(284, 'Nəsimi rayonu ', 95, 1, 2, 95, 0),
(285, 'Nəsimi rayonu ', 95, 1, 3, 95, 0),
(286, 'Nizami rayonu ', 96, 1, 1, 96, 0),
(287, 'Nizami rayonu ', 96, 1, 2, 96, 0),
(288, 'Nizami rayonu ', 96, 1, 3, 96, 0),
(289, 'Pirallahi rayonu ', 97, 1, 1, 97, 0),
(290, 'Pirallahi rayonu ', 97, 1, 2, 97, 0),
(291, 'Pirallahi rayonu ', 97, 1, 3, 97, 0),
(292, 'Xətai rayonu ', 98, 1, 1, 98, 0),
(293, 'Xətai rayonu ', 98, 1, 2, 98, 0),
(294, 'Xətai rayonu ', 98, 1, 3, 98, 0),
(295, 'Yasamal rayonu ', 99, 1, 1, 99, 0),
(296, 'Yasamal rayonu ', 99, 1, 2, 99, 0),
(297, 'Yasamal rayonu ', 99, 1, 3, 99, 0),
(298, 'Abseron rayonu ', 100, 1, 1, 100, 0),
(299, 'Abseron rayonu ', 100, 1, 2, 100, 0),
(300, 'Abseron rayonu ', 100, 1, 3, 100, 0),
(301, 'Sumqayit səhəri ', 101, 1, 1, 101, 0),
(302, 'Sumqayit səhəri ', 101, 1, 2, 101, 0),
(303, 'Sumqayit səhəri ', 101, 1, 3, 101, 0),
(304, 'Xizi rayonu ', 102, 1, 1, 102, 0),
(305, 'Xizi rayonu ', 102, 1, 2, 102, 0),
(306, 'Xizi rayonu ', 102, 1, 3, 102, 0),
(307, 'Gəncə səhəri ', 103, 1, 1, 103, 0),
(308, 'Gəncə səhəri ', 103, 1, 2, 103, 0),
(309, 'Gəncə səhəri ', 103, 1, 3, 103, 0),
(310, 'Qazax rayonu ', 104, 1, 1, 104, 0),
(311, 'Qazax rayonu ', 104, 1, 2, 104, 0),
(312, 'Qazax rayonu ', 104, 1, 3, 104, 0),
(313, 'Agstafa rayonu ', 105, 1, 1, 105, 0),
(314, 'Agstafa rayonu ', 105, 1, 2, 105, 0),
(315, 'Agstafa rayonu ', 105, 1, 3, 105, 0),
(316, 'Tovuz rayonu ', 106, 1, 1, 106, 0),
(317, 'Tovuz rayonu ', 106, 1, 2, 106, 0),
(318, 'Tovuz rayonu ', 106, 1, 3, 106, 0),
(319, 'Səmkir rayonu ', 107, 1, 1, 107, 0),
(320, 'Səmkir rayonu ', 107, 1, 2, 107, 0),
(321, 'Səmkir rayonu ', 107, 1, 3, 107, 0),
(322, 'Gədəbəy rayonu ', 108, 1, 1, 108, 0),
(323, 'Gədəbəy rayonu ', 108, 1, 2, 108, 0),
(324, 'Gədəbəy rayonu ', 108, 1, 3, 108, 0),
(325, 'Daskəsən rayonu ', 109, 1, 1, 109, 0),
(326, 'Daskəsən rayonu ', 109, 1, 2, 109, 0),
(327, 'Daskəsən rayonu ', 109, 1, 3, 109, 0),
(328, 'Samux rayonu ', 110, 1, 1, 110, 0),
(329, 'Samux rayonu ', 110, 1, 2, 110, 0),
(330, 'Samux rayonu ', 110, 1, 3, 110, 0),
(331, 'Göygöl rayonu ', 111, 1, 1, 111, 0),
(332, 'Göygöl rayonu ', 111, 1, 2, 111, 0),
(333, 'Göygöl rayonu ', 111, 1, 3, 111, 0),
(334, 'Goranboy rayonu ', 112, 1, 1, 112, 0),
(335, 'Goranboy rayonu ', 112, 1, 2, 112, 0),
(336, 'Goranboy rayonu ', 112, 1, 3, 112, 0),
(337, 'Naftalan səhəri ', 113, 1, 1, 113, 0),
(338, 'Naftalan səhəri ', 113, 1, 2, 113, 0),
(339, 'Naftalan səhəri ', 113, 1, 3, 113, 0),
(340, 'Balakən rayonu ', 114, 1, 1, 114, 0),
(341, 'Balakən rayonu ', 114, 1, 2, 114, 0),
(342, 'Balakən rayonu ', 114, 1, 3, 114, 0),
(343, 'Zaqatala rayonu ', 115, 1, 1, 115, 0),
(344, 'Zaqatala rayonu ', 115, 1, 2, 115, 0),
(345, 'Zaqatala rayonu ', 115, 1, 3, 115, 0),
(346, 'Qax rayonu ', 116, 1, 1, 116, 0),
(347, 'Qax rayonu ', 116, 1, 2, 116, 0),
(348, 'Qax rayonu ', 116, 1, 3, 116, 0),
(349, 'Səki səhəri ', 117, 1, 1, 117, 0),
(350, 'Səki səhəri ', 117, 1, 2, 117, 0),
(351, 'Səki səhəri ', 117, 1, 3, 117, 0),
(352, 'Oguz rayonu ', 118, 1, 1, 118, 0),
(353, 'Oguz rayonu ', 118, 1, 2, 118, 0),
(354, 'Oguz rayonu ', 118, 1, 3, 118, 0),
(355, 'Qəbələ rayonu ', 119, 1, 1, 119, 0),
(356, 'Qəbələ rayonu ', 119, 1, 2, 119, 0),
(357, 'Qəbələ rayonu ', 119, 1, 3, 119, 0),
(358, 'Astara rayonu ', 120, 1, 1, 120, 0),
(359, 'Astara rayonu ', 120, 1, 2, 120, 0),
(360, 'Astara rayonu ', 120, 1, 3, 120, 0),
(361, 'Lənkəran səhəri ', 121, 1, 1, 121, 0),
(362, 'Lənkəran səhəri ', 121, 1, 2, 121, 0),
(363, 'Lənkəran səhəri ', 121, 1, 3, 121, 0),
(364, 'Lerik rayonu ', 122, 1, 1, 122, 0),
(365, 'Lerik rayonu ', 122, 1, 2, 122, 0),
(366, 'Lerik rayonu ', 122, 1, 3, 122, 0),
(367, 'Yardimli rayonu ', 123, 1, 1, 123, 0),
(368, 'Yardimli rayonu ', 123, 1, 2, 123, 0),
(369, 'Yardimli rayonu ', 123, 1, 3, 123, 0),
(370, 'Masalli rayonu ', 124, 1, 1, 124, 0),
(371, 'Masalli rayonu ', 124, 1, 2, 124, 0),
(372, 'Masalli rayonu ', 124, 1, 3, 124, 0),
(373, 'Cəlilabad rayonu ', 125, 1, 1, 125, 0),
(374, 'Cəlilabad rayonu ', 125, 1, 2, 125, 0),
(375, 'Cəlilabad rayonu ', 125, 1, 3, 125, 0),
(376, 'Qusar rayonu ', 126, 1, 1, 126, 0),
(377, 'Qusar rayonu ', 126, 1, 2, 126, 0),
(378, 'Qusar rayonu ', 126, 1, 3, 126, 0),
(379, 'Xaçmaz rayonu ', 127, 1, 1, 127, 0),
(380, 'Xaçmaz rayonu ', 127, 1, 2, 127, 0),
(381, 'Xaçmaz rayonu ', 127, 1, 3, 127, 0),
(382, 'Quba rayonu ', 128, 1, 1, 128, 0),
(383, 'Quba rayonu ', 128, 1, 2, 128, 0),
(384, 'Quba rayonu ', 128, 1, 3, 128, 0),
(385, 'Sabran rayonu ', 129, 1, 1, 129, 0),
(386, 'Sabran rayonu ', 129, 1, 2, 129, 0),
(387, 'Sabran rayonu ', 129, 1, 3, 129, 0),
(388, 'Siyəzən rayonu ', 130, 1, 1, 130, 0),
(389, 'Siyəzən rayonu ', 130, 1, 2, 130, 0),
(390, 'Siyəzən rayonu ', 130, 1, 3, 130, 0),
(391, 'Göyçay rayonu ', 131, 1, 1, 131, 0),
(392, 'Göyçay rayonu ', 131, 1, 2, 131, 0),
(393, 'Göyçay rayonu ', 131, 1, 3, 131, 0),
(394, 'Beyləqan rayonu ', 132, 1, 1, 132, 0),
(395, 'Beyləqan rayonu ', 132, 1, 2, 132, 0),
(396, 'Beyləqan rayonu ', 132, 1, 3, 132, 0),
(397, 'Agcabədi rayonu ', 133, 1, 1, 133, 0),
(398, 'Agcabədi rayonu ', 133, 1, 2, 133, 0),
(399, 'Agcabədi rayonu ', 133, 1, 3, 133, 0),
(400, 'Bərdə rayonu ', 134, 1, 1, 134, 0),
(401, 'Bərdə rayonu ', 134, 1, 2, 134, 0),
(402, 'Bərdə rayonu ', 134, 1, 3, 134, 0),
(403, 'Neftçala rayonu ', 135, 1, 1, 135, 0),
(404, 'Neftçala rayonu ', 135, 1, 2, 135, 0),
(405, 'Neftçala rayonu ', 135, 1, 3, 135, 0),
(406, 'Biləsuvar rayonu ', 136, 1, 1, 136, 0),
(407, 'Biləsuvar rayonu ', 136, 1, 2, 136, 0),
(408, 'Biləsuvar rayonu ', 136, 1, 3, 136, 0),
(409, 'Salyan rayonu ', 137, 1, 1, 137, 0),
(410, 'Salyan rayonu ', 137, 1, 2, 137, 0),
(411, 'Salyan rayonu ', 137, 1, 3, 137, 0),
(412, 'Yevlax səhəri ', 138, 1, 1, 138, 0),
(413, 'Yevlax səhəri ', 138, 1, 2, 138, 0),
(414, 'Yevlax səhəri ', 138, 1, 3, 138, 0),
(415, 'mingecevirseheri ', 139, 1, 1, 139, 0),
(416, 'mingecevirseheri ', 139, 1, 2, 139, 0),
(417, 'mingecevirseheri ', 139, 1, 3, 139, 0),
(418, 'Agdas rayonu ', 140, 1, 1, 140, 0),
(419, 'Agdas rayonu ', 140, 1, 2, 140, 0),
(420, 'Agdas rayonu ', 140, 1, 3, 140, 0),
(421, 'Ucar rayonu ', 141, 1, 1, 141, 0),
(422, 'Ucar rayonu ', 141, 1, 2, 141, 0),
(423, 'Ucar rayonu ', 141, 1, 3, 141, 0),
(424, 'Zərdab rayonu ', 142, 1, 1, 142, 0),
(425, 'Zərdab rayonu ', 142, 1, 2, 142, 0),
(426, 'Zərdab rayonu ', 142, 1, 3, 142, 0),
(427, 'Kürdəmir rayonu ', 143, 1, 1, 143, 0),
(428, 'Kürdəmir rayonu ', 143, 1, 2, 143, 0),
(429, 'Kürdəmir rayonu ', 143, 1, 3, 143, 0),
(430, 'Imisli rayonu ', 144, 1, 1, 144, 0),
(431, 'Imisli rayonu ', 144, 1, 2, 144, 0),
(432, 'Imisli rayonu ', 144, 1, 3, 144, 0),
(433, 'Saatli rayonu ', 145, 1, 1, 145, 0),
(434, 'Saatli rayonu ', 145, 1, 2, 145, 0),
(435, 'Saatli rayonu ', 145, 1, 3, 145, 0),
(436, 'Sabirabad rayonu ', 146, 1, 1, 146, 0),
(437, 'Sabirabad rayonu ', 146, 1, 2, 146, 0),
(438, 'Sabirabad rayonu ', 146, 1, 3, 146, 0),
(439, 'Haciqabul rayonu ', 147, 1, 1, 147, 0),
(440, 'Haciqabul rayonu ', 147, 1, 2, 147, 0),
(441, 'Haciqabul rayonu ', 147, 1, 3, 147, 0),
(442, 'Sirvan səhəri ', 148, 1, 1, 148, 0),
(443, 'Sirvan səhəri ', 148, 1, 2, 148, 0),
(444, 'Sirvan səhəri ', 148, 1, 3, 148, 0),
(445, 'Cəbrayil rayonu ', 149, 1, 1, 149, 0),
(446, 'Cəbrayil rayonu ', 149, 1, 2, 149, 0),
(447, 'Cəbrayil rayonu ', 149, 1, 3, 149, 0),
(448, 'Füzuli rayonu ', 150, 1, 1, 150, 0),
(449, 'Füzuli rayonu ', 150, 1, 2, 150, 0),
(450, 'Füzuli rayonu ', 150, 1, 3, 150, 0),
(451, 'Agdam rayonu ', 151, 1, 1, 151, 0),
(452, 'Agdam rayonu ', 151, 1, 2, 151, 0),
(453, 'Agdam rayonu ', 151, 1, 3, 151, 0),
(454, 'Tərtər rayonu ', 152, 1, 1, 152, 0),
(455, 'Tərtər rayonu ', 152, 1, 2, 152, 0),
(456, 'Tərtər rayonu ', 152, 1, 3, 152, 0),
(457, 'Xocali rayonu ', 153, 1, 1, 153, 0),
(458, 'Xocali rayonu ', 153, 1, 2, 153, 0),
(459, 'Xocali rayonu ', 153, 1, 3, 153, 0),
(460, 'Susa rayonu ', 154, 1, 1, 154, 0),
(461, 'Susa rayonu ', 154, 1, 2, 154, 0),
(462, 'Susa rayonu ', 154, 1, 3, 154, 0),
(463, 'Xocavənd rayonu ', 155, 1, 1, 155, 0),
(464, 'Xocavənd rayonu ', 155, 1, 2, 155, 0),
(465, 'Xocavənd rayonu ', 155, 1, 3, 155, 0),
(466, 'Xankəndi səhəri ', 156, 1, 1, 156, 0),
(467, 'Xankəndi səhəri ', 156, 1, 2, 156, 0),
(468, 'Xankəndi səhəri ', 156, 1, 3, 156, 0),
(469, 'Kəlbəcər rayonu ', 157, 1, 1, 157, 0),
(470, 'Kəlbəcər rayonu ', 157, 1, 2, 157, 0),
(471, 'Kəlbəcər rayonu ', 157, 1, 3, 157, 0),
(472, 'Laçin rayonu ', 158, 1, 1, 158, 0),
(473, 'Laçin rayonu ', 158, 1, 2, 158, 0),
(474, 'Laçin rayonu ', 158, 1, 3, 158, 0),
(475, 'Qubadli rayonu ', 159, 1, 1, 159, 0),
(476, 'Qubadli rayonu ', 159, 1, 2, 159, 0),
(477, 'Qubadli rayonu ', 159, 1, 3, 159, 0),
(478, 'Zəngilan rayonu ', 160, 1, 1, 160, 0),
(479, 'Zəngilan rayonu ', 160, 1, 2, 160, 0),
(480, 'Zəngilan rayonu ', 160, 1, 3, 160, 0),
(481, 'Qobustan rayonu ', 161, 1, 1, 161, 0),
(482, 'Qobustan rayonu ', 161, 1, 2, 161, 0),
(483, 'Qobustan rayonu ', 161, 1, 3, 161, 0),
(484, 'Ismayilli rayonu ', 162, 1, 1, 162, 0),
(485, 'Ismayilli rayonu ', 162, 1, 2, 162, 0),
(486, 'Ismayilli rayonu ', 162, 1, 3, 162, 0),
(487, 'Agsu rayonu ', 163, 1, 1, 163, 0),
(488, 'Agsu rayonu ', 163, 1, 2, 163, 0),
(489, 'Agsu rayonu ', 163, 1, 3, 163, 0),
(490, 'Samaxi rayonu ', 164, 1, 1, 164, 0),
(491, 'Samaxi rayonu ', 164, 1, 2, 164, 0),
(492, 'Samaxi rayonu ', 164, 1, 3, 164, 0),
(493, 'Naxçivan səhəri ', 165, 1, 1, 165, 0),
(494, 'Naxçivan səhəri ', 165, 1, 2, 165, 0),
(495, 'Naxçivan səhəri ', 165, 1, 3, 165, 0),
(496, 'Sərur rayonu ', 166, 1, 1, 166, 0),
(497, 'Sərur rayonu ', 166, 1, 2, 166, 0),
(498, 'Sərur rayonu ', 166, 1, 3, 166, 0),
(499, 'Babək rayonu ', 167, 1, 1, 167, 0),
(500, 'Babək rayonu ', 167, 1, 2, 167, 0),
(501, 'Babək rayonu ', 167, 1, 3, 167, 0),
(502, 'Ordubad rayonu ', 168, 1, 1, 168, 0),
(503, 'Ordubad rayonu ', 168, 1, 2, 168, 0),
(504, 'Ordubad rayonu ', 168, 1, 3, 168, 0),
(505, 'Culfa rayonu ', 169, 1, 1, 169, 0),
(506, 'Culfa rayonu ', 169, 1, 2, 169, 0),
(507, 'Culfa rayonu ', 169, 1, 3, 169, 0),
(508, 'Kəngərli rayonu ', 170, 1, 1, 170, 0),
(509, 'Kəngərli rayonu ', 170, 1, 2, 170, 0),
(510, 'Kəngərli rayonu ', 170, 1, 3, 170, 0),
(511, 'Sahbuz rayonu ', 171, 1, 1, 171, 0),
(512, 'Sahbuz rayonu ', 171, 1, 2, 171, 0),
(513, 'Sahbuz rayonu ', 171, 1, 3, 171, 0),
(514, 'Sədərək rayonu', 172, 1, 1, 172, 0),
(515, 'Sədərək rayonu', 172, 1, 2, 172, 0),
(516, 'Sədərək rayonu', 172, 1, 3, 172, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rehber`
--

CREATE TABLE `rehber` (
  `id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rehber`
--

INSERT INTO `rehber` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(3, 'Şahin Hidayət oğlu Nəbiyev', '', '', '', 'jpg', '&lt;p dir=&quot;ltr&quot;&gt;&lt;span&gt;12 yanvar 1980-ci ildə Bakı şəhərində anadan olmuşdur.&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;Təhsili&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;2001-ci ildə Azərbaycan D&amp;ouml;vlət İqtisad Universitetinin &amp;uuml;mumi iqtisadiyyat &amp;uuml;zrə bakalavr pilləsini bitirmişdir.2002-2003-c&amp;uuml; illərdə Azərbaycan Respublikası Silahlı Q&amp;uuml;vvələrində hərbi xidmətdə olmuşdur. 2003-2005-ci illərdə Azərbaycan D&amp;ouml;vlət İqtisad Universitetinin beynəlxalq iqtisadi m&amp;uuml;nasibətlər &amp;uuml;zrə magistr təhsili almışdır. 2008-2012-ci illərdə Bakı D&amp;ouml;vlət Universitetinin h&amp;uuml;quq fak&amp;uuml;ltəsində təhsilini davam etdirmiş və h&amp;uuml;quqş&amp;uuml;nas ixtisası almışdır. 2010-cu ildə Harvard Universitetinin John F. Kennedy adına idarə&amp;ccedil;ilik məktəbinin magistr proqramına qəbul olmuş və 2011-ci ildə d&amp;ouml;vlət idarə&amp;ccedil;iliyi &amp;uuml;zrə magistr dərəcəsi almışdır.&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;Əmək fəaliyyəti&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;2005-2010-cu illərdə Azərbaycan Respublikası Korrupsiyaya qarşı m&amp;uuml;barizə &amp;uuml;zrə Komissiyasının katibliyində aparıcı məsləhət&amp;ccedil;i və b&amp;ouml;y&amp;uuml;k məsləhət&amp;ccedil;i olmuşdur. 2007-2009-cu illərdə Azərbaycan D&amp;ouml;vlət İqtisad Universiteti və Xəzər Universitetində pedaqoji fəaliyyətlə məşğul olmuşdur. 2011-2012-ci illərdə Azərbaycan Respublikası Prezidentinin Administrasiyasının H&amp;uuml;quq m&amp;uuml;hafizə orqanları ilə iş ş&amp;ouml;bəsinin &amp;ldquo;Miqrasiya və vətəndaşlıq məsələləri&amp;rdquo; sektorunda məsləhət&amp;ccedil;i və b&amp;ouml;y&amp;uuml;k məsləhət&amp;ccedil;i vəzifələrində &amp;ccedil;alışmışdır. 2012-2015-ci illərdə Azərbaycan Respublikasının Korrupsiyaya qarşı m&amp;uuml;barizə &amp;uuml;zrə Komissiyasının Katibi və Azərbaycan Respublikası Prezidentinin Administrasiyasının H&amp;uuml;quq m&amp;uuml;hafizə orqanları ilə iş ş&amp;ouml;bəsinin b&amp;ouml;y&amp;uuml;k məsləhət&amp;ccedil;isi olmuşdur. 2015-ci ildə Yeni Azərbaycan Partiyasının namizədi olaraq 91 saylı Ucar se&amp;ccedil;ki dairəsindən beşinci &amp;ccedil;ağırış Azərbaycan Respublikası Milli Məclisinin deputatı se&amp;ccedil;ilmişdir. Azərbaycan Respublikası Prezidentinin 20 aprel 2016-cı il tarixli Sərəncamı ilə İqtisadi İslahatların Təhlili və Kommunikasiya Mərkəzinin M&amp;uuml;şahidə Şurası &amp;uuml;zv&amp;uuml; təyin edilmişdir. Azərbaycan Respublikası Prezidentinin 23 aprel 2018-ci il tarixli Sərəncamı ilə Azərbaycan Respublikası D&amp;ouml;vlət Miqrasiya Xidmətinin rəisi təyin edilmişdir.&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;Beynəlxalq təcr&amp;uuml;bə&amp;nbsp;&lt;/span&gt;&lt;br /&gt;&lt;span&gt;2012-ci ildən Avropa Şurasının Korrupsiyaya qarşı D&amp;ouml;vlətlər Qrupunda Azərbaycan n&amp;uuml;mayəndə heyətinin rəhbəridir. 2012-2017-ci illərdə A&amp;ccedil;ıq H&amp;ouml;kumət Tərəfdaşlığı Beynəlxalq Təşəbb&amp;uuml;s&amp;uuml;n&amp;uuml;n Azərbaycan &amp;uuml;zrə Milli koordinatoru olmuşdur. 2015-ci ildən Beynəlxalq Antikorrupsiya Akademiyasının İdarə Heyətinin &amp;uuml;zv&amp;uuml;d&amp;uuml;r. 2015-2018-ci illər arasında Avropa Şurası Parlament Assambleyasında Azərbaycan n&amp;uuml;mayəndə heyətinin sədr m&amp;uuml;avini olmuşdur. 2018-ci ildə Avropa Şurası Parlament Assambleyasının H&amp;uuml;quq məsələləri və insan h&amp;uuml;quqları komitəsinin sədr m&amp;uuml;avini, Hakimlərin se&amp;ccedil;ki komitəsinin &amp;uuml;zv&amp;uuml; se&amp;ccedil;ilmişdir. İngilis və rus dillərini bilir. Evlidir, iki &amp;ouml;vladı var.&lt;/span&gt;&lt;/p&gt;', 0, 2, 1, 1, 2, 0),
(4, 'О НАС', '', '', '', 'jpg', '&lt;p dir=&quot;ltr&quot;&gt;&lt;span&gt;A.SET Kredit &amp;ndash; это универсальный продукт создан на базе BOKT FINtrend, с помощью которого Вы сможете купить товар и/или услугу в кредит, во всех торговых сетях партнерах электроники и бытовой техники, мебели, автозапчастей, автомобильных сервисных центрах с максимально выгодными для Вас условиями. &lt;/span&gt;&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;span&gt;Основное преимущество данного продукта &amp;ndash; это приобретение товара или услуги в кредит прямо в магазине.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;span&gt;На сегодняшний день мы предлагаем конкурентные условия работы на рынке потребительского кредитования со сроком кредитования до 36 месяцев без первого взноса.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;span&gt;Вы можете детально ознакомится со всеми нашими партнерами в разделе ПАРТНЕРЫ.&lt;/span&gt;&lt;/p&gt;', 0, 2, 1, 2, 2, 0),
(5, 'ABOUT COMPANY', '', '', '', 'jpg', '&lt;p dir=&quot;ltr&quot;&gt;A.SET Kredit &amp;ndash; это универсальный продукт создан на базе BOKT FINtrend, с помощью которого Вы сможете купить товар и/или услугу в кредит, во всех торговых сетях партнерах электроники и бытовой техники, мебели, автозапчастей, автомобильных сервисных центрах с максимально выгодными для Вас условиями.&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;Основное преимущество данного продукта &amp;ndash; это приобретение товара или услуги в кредит прямо в магазине.&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;На сегодняшний день мы предлагаем конкурентные условия работы на рынке потребительского кредитования со сроком кредитования до 36 месяцев без первого взноса.&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;Вы можете детально ознакомится со всеми нашими партнерами в разделе ПАРТНЕРЫ.&lt;/p&gt;', 0, 2, 1, 3, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, 'Тунзаля Агаева', '', '', '&quot;С тех пор, как я стала делать &quot;капельницы молодости Лаеннек&quot; я стала ощущать прилив бодрости и энергии,  прошло чувство хронической усталости. Высыпаюсь, хотя сплю меньше обычного. Лаеннек помогает мне восстанавливаться после концертов и съемок, хочется творить и радовать своих зрителей еще больше&quot;', 'jpg', '', 0, 1, 1, 1, 1, 0),
(2, 'Dianna ru 2', '', '', '&quot; С тех пор, как я стала делать &quot;капельницы молодости Лаеннек&quot; я стала ощущать прилив бодрости и энергии,  прошло чувство хронической усталости. Высыпаюсь, хотя сплю меньше обычного. Лаеннек помогает мне восстанавливаться после концертов и съемок, хочется творить и радовать своих зрителей еще больше&quot;', 'jpg', '', 0, 1, 1, 2, 1, 0),
(3, 'Нура Сури', '', '', '&quot;Обычно после занятий спортом, я чувствую усталость в конце тренировки, после прохождения курса Лаеннек-терапии, я могу заниматься спортом по 3 часа и больше, при этом не ощущая никакой усталости, а наоборот прилив сил. Делаю эту процедуру ежегодно, так как замечаю, что после прохождения курса становлюсь более стрессоустойчивой, повышается иммунитет, улучшается качество кожи, нормализуется сон.&quot; ', 'jpg', '', 0, 2, 1, 1, 2, 0),
(4, 'Medical Massage', '', '', 'in in augue placerat, ligula quis, elementum augue. lorem ipsum dolor sit amet, consectetur adipiscing elit. integer gravida velit quis dolor tristique accumsan. pellentesque lla nec faucibus est. in in augue placerat, ligula quis, elementum augue.\r\n\r\nin in augue placerat, ligula quis, elementum augue. lorem ipsum dolor sit amet, consectetur adipiscing elit. integer gravida velit quis dolor tristique accumsan. pellentesque lla nec faucibus est. in in augue placerat, ligula quis, elementum augue. en', 'jpg', '', 0, 2, 1, 2, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rightlinks`
--

CREATE TABLE `rightlinks` (
  `id` int(10) NOT NULL,
  `tarix` int(11) NOT NULL,
  `basliq` varchar(255) DEFAULT NULL,
  `link` varchar(255) NOT NULL,
  `tam_metn` text,
  `tip` varchar(5) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) DEFAULT '0',
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rightlinks`
--

INSERT INTO `rightlinks` (`id`, `tarix`, `basliq`, `link`, `tam_metn`, `tip`, `sira`, `aktivlik`, `lang_id`, `auto_id`) VALUES
(1, 1538045406, 'sds adasdas dasdasd asd as intern', 'https://www.migration.gov.az/home/calculator', '', 'jpg', 2, 1, 1, 1),
(2, 1538045406, 'sds adasdas dasdasd asd as intern', 'https://www.migration.gov.az/home/calculator', '', 'jpg', 2, 1, 2, 1),
(3, 1538045406, 'sds adasdas dasdasd asd as intern', 'https://www.migration.gov.az/home/calculator', '', 'jpg', 2, 1, 3, 1),
(4, 1538045456, 'sds adasdas dasdasd asd as', 'https://www.migration.gov.az/useful/freedom', '', 'jpg', 1, 1, 1, 2),
(5, 1538045456, 'sds adasdas dasdasd asd as656', 'https://www.migration.gov.az/useful/freedom', '', 'jpg', 1, 1, 2, 2),
(6, 1538045456, 'sds adasdas dasdasd asd as656', 'https://www.migration.gov.az/useful/freedom', '', 'jpg', 1, 1, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `rusum`
--

CREATE TABLE `rusum` (
  `id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `manset` int(2) NOT NULL DEFAULT '0',
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL,
  `created_at` int(13) NOT NULL,
  `updated_at` int(13) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rusum`
--

INSERT INTO `rusum` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `manset`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`, `created_at`, `updated_at`) VALUES
(1, '', '', '', 'Azərbaycan Respublikasının “Dövlət rüsumu haqqında” Qanununa əsasən, əcnəbilərə və vətəndaşlığı olmayan şəxslərə ölkə ərazisində müvəqqəti olma müddətinin uzadılması barədə qərarların verilməsinə, müvəqqəti və ya daimi yaşamaq, həmçinin haqqı ödənilən əmək fəaliyyəti ilə məşğul olmaq üçün müvafiq icazələrin verilməsinə və müddətlərinin uzadılmasına, Azərbaycan Respublikasının vətəndaşlığı məsələlərinə, o cümlədən Azərbaycan Respublikasında qaçqın statusu almış şəxslərə Qaçqın vəsiqəsinin və ya Yol sənədinin verilməsinə görə ödəniləcək dövlət rüsumları aşağıdakı məbləğlərdə müəyyən edilmişdir:', '', '&lt;table class=&quot;table table-bordered table-responsive table-hover&quot; align=&quot;justify&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;1. Yetkinlik yaşına &amp;ccedil;atmış əcnəbilərə və vətəndaşlığı olmayan şəxslərə &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılması barədə qərarın verilməsinə g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;30 g&amp;uuml;nədək&lt;/td&gt;\r\n&lt;td&gt;30 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;60 g&amp;uuml;nədək&lt;/td&gt;\r\n&lt;td&gt;60 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;2. Yetkinlik yaşına &amp;ccedil;atmayan əcnəbilərə və vətəndaşlığı olmayan şəxslərə &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılması barədə qərarın verilməsinə g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;30 g&amp;uuml;nədək&lt;/td&gt;\r\n&lt;td&gt;15 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;60 g&amp;uuml;nədək&lt;/td&gt;\r\n&lt;td&gt;30 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;3. Yetkinlik yaşına &amp;ccedil;atmış əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqəsinin verilməsinə və m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;3 ayadək&lt;/td&gt;\r\n&lt;td&gt;30 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;6 ayadək&lt;/td&gt;\r\n&lt;td&gt;60 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;1 ilədək&lt;/td&gt;\r\n&lt;td&gt;120 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;1 il 6 ayadək&lt;/td&gt;\r\n&lt;td&gt;180 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;2 ilədək&lt;/td&gt;\r\n&lt;td&gt;240 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;2 il 6 ayadək&lt;/td&gt;\r\n&lt;td&gt;300 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;3 ilədək&lt;/td&gt;\r\n&lt;td&gt;360 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;4. Yetkinlik yaşına &amp;ccedil;atmayan əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqəsinin verilməsinə və m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;3 ayadək&lt;/td&gt;\r\n&lt;td&gt;15 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;6 ayadək&lt;/td&gt;\r\n&lt;td&gt;30 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;1 ilədək&lt;/td&gt;\r\n&lt;td&gt;60 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;1 il 6 ayadək&lt;/td&gt;\r\n&lt;td&gt;90 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;2 ilədək&lt;/td&gt;\r\n&lt;td&gt;120 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;2 il 6 ayadək&lt;/td&gt;\r\n&lt;td&gt;150 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;3 ilədək&lt;/td&gt;\r\n&lt;td&gt;180 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;5. Azərbaycan Respublikasının ali və orta ixtisas təhsili m&amp;uuml;əssisələrində əyani formada təhsil alan əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqəsinin verilməsinə və m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;1 ilədək&lt;/td&gt;\r\n&lt;td&gt;40 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;2 ilədək (m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqəsinin m&amp;uuml;ddəti uzadıldıqda)&lt;/td&gt;\r\n&lt;td&gt;80 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;6. Yetkinlik yaşına &amp;ccedil;atmış əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqəsinin verilməsinə və m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;300 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;7. Yetkinlik yaşına &amp;ccedil;atmayan əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqəsinin verilməsinə və m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;150 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;8. Əcnəbilərə və vətəndaşlığı olmayan şəxslərə itirilmiş Azərbaycan Respublikasında m&amp;uuml;vəqqəti və ya daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqələrinin və m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılması barədə qərarın yenidən verilməsinə g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;10 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;9. Azərbaycan Respublikasının ərazisində haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq &amp;uuml;&amp;ccedil;&amp;uuml;n əcnəbilərə və vətəndaşlığı olmayan şəxslərə iş icazəsinin verilməsinə və m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə (işəg&amp;ouml;t&amp;uuml;rən tərəfindən &amp;ouml;dənilir):&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;3 ayadək&lt;/td&gt;\r\n&lt;td&gt;350 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;6 ayadək&lt;/td&gt;\r\n&lt;td&gt;600 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;1 ilədək&lt;/td&gt;\r\n&lt;td&gt;1000 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;10. Azərbaycan Respublikası vətəndaşlığına qəbul edilmək, bərpa edilmək və ya ondan &amp;ccedil;ıxmaq barədə ərizələrə g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;110 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;11. Azərbaycan Respublikasında qa&amp;ccedil;qın statusu almış şəxslərə Qa&amp;ccedil;qın vəsiqəsinin və ya Yol sənədinin verilməsinə g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;5 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;', 0, 0, 1, 1, 1, 1, 0, 1537861989, 1537862363),
(2, '', '', '', 'Azərbaycan Respublikasının “Dövlət rüsumu haqqında” Qanununa əsasən, əcnəbilərə və vətəndaşlığı olmayan şəxslərə ölkə ərazisində müvəqqəti olma müddətinin uzadılması barədə qərarların verilməsinə, müvəqqəti və ya daimi yaşamaq, həmçinin haqqı ödənilən əmək fəaliyyəti ilə məşğul olmaq üçün müvafiq icazələrin verilməsinə və müddətlərinin uzadılmasına, Azərbaycan Respublikasının vətəndaşlığı məsələlərinə, o cümlədən Azərbaycan Respublikasında qaçqın statusu almış şəxslərə Qaçqın vəsiqəsinin və ya Yol sənədinin verilməsinə görə ödəniləcək dövlət rüsumları aşağıdakı məbləğlərdə müəyyən edilmişdir:', '', '&lt;table class=&quot;table table-bordered table-responsive table-hover&quot; align=&quot;justify&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;1. Yetkinlik yaşına &amp;ccedil;atmış əcnəbilərə və vətəndaşlığı olmayan şəxslərə &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılması barədə qərarın verilməsinə g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;30 g&amp;uuml;nədək&lt;/td&gt;\r\n&lt;td&gt;30 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;60 g&amp;uuml;nədək&lt;/td&gt;\r\n&lt;td&gt;60 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;2. Yetkinlik yaşına &amp;ccedil;atmayan əcnəbilərə və vətəndaşlığı olmayan şəxslərə &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılması barədə qərarın verilməsinə g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;30 g&amp;uuml;nədək&lt;/td&gt;\r\n&lt;td&gt;15 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;60 g&amp;uuml;nədək&lt;/td&gt;\r\n&lt;td&gt;30 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;3. Yetkinlik yaşına &amp;ccedil;atmış əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqəsinin verilməsinə və m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;3 ayadək&lt;/td&gt;\r\n&lt;td&gt;30 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;6 ayadək&lt;/td&gt;\r\n&lt;td&gt;60 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;1 ilədək&lt;/td&gt;\r\n&lt;td&gt;120 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;1 il 6 ayadək&lt;/td&gt;\r\n&lt;td&gt;180 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;2 ilədək&lt;/td&gt;\r\n&lt;td&gt;240 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;2 il 6 ayadək&lt;/td&gt;\r\n&lt;td&gt;300 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;3 ilədək&lt;/td&gt;\r\n&lt;td&gt;360 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;4. Yetkinlik yaşına &amp;ccedil;atmayan əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqəsinin verilməsinə və m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;3 ayadək&lt;/td&gt;\r\n&lt;td&gt;15 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;6 ayadək&lt;/td&gt;\r\n&lt;td&gt;30 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;1 ilədək&lt;/td&gt;\r\n&lt;td&gt;60 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;1 il 6 ayadək&lt;/td&gt;\r\n&lt;td&gt;90 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;2 ilədək&lt;/td&gt;\r\n&lt;td&gt;120 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;2 il 6 ayadək&lt;/td&gt;\r\n&lt;td&gt;150 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;3 ilədək&lt;/td&gt;\r\n&lt;td&gt;180 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;5. Azərbaycan Respublikasının ali və orta ixtisas təhsili m&amp;uuml;əssisələrində əyani formada təhsil alan əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqəsinin verilməsinə və m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;1 ilədək&lt;/td&gt;\r\n&lt;td&gt;40 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;2 ilədək (m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqəsinin m&amp;uuml;ddəti uzadıldıqda)&lt;/td&gt;\r\n&lt;td&gt;80 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;6. Yetkinlik yaşına &amp;ccedil;atmış əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqəsinin verilməsinə və m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;300 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;7. Yetkinlik yaşına &amp;ccedil;atmayan əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqəsinin verilməsinə və m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;150 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;8. Əcnəbilərə və vətəndaşlığı olmayan şəxslərə itirilmiş Azərbaycan Respublikasında m&amp;uuml;vəqqəti və ya daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqələrinin və m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılması barədə qərarın yenidən verilməsinə g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;10 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;9. Azərbaycan Respublikasının ərazisində haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq &amp;uuml;&amp;ccedil;&amp;uuml;n əcnəbilərə və vətəndaşlığı olmayan şəxslərə iş icazəsinin verilməsinə və m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə (işəg&amp;ouml;t&amp;uuml;rən tərəfindən &amp;ouml;dənilir):&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;3 ayadək&lt;/td&gt;\r\n&lt;td&gt;350 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;6 ayadək&lt;/td&gt;\r\n&lt;td&gt;600 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;1 ilədək&lt;/td&gt;\r\n&lt;td&gt;1000 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;10. Azərbaycan Respublikası vətəndaşlığına qəbul edilmək, bərpa edilmək və ya ondan &amp;ccedil;ıxmaq barədə ərizələrə g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;110 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;11. Azərbaycan Respublikasında qa&amp;ccedil;qın statusu almış şəxslərə Qa&amp;ccedil;qın vəsiqəsinin və ya Yol sənədinin verilməsinə g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;5 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;', 0, 0, 1, 1, 2, 1, 0, 1537861989, 1537862363),
(3, '', '', '', 'Azərbaycan Respublikasının “Dövlət rüsumu haqqında” Qanununa əsasən, əcnəbilərə və vətəndaşlığı olmayan şəxslərə ölkə ərazisində müvəqqəti olma müddətinin uzadılması barədə qərarların verilməsinə, müvəqqəti və ya daimi yaşamaq, həmçinin haqqı ödənilən əmək fəaliyyəti ilə məşğul olmaq üçün müvafiq icazələrin verilməsinə və müddətlərinin uzadılmasına, Azərbaycan Respublikasının vətəndaşlığı məsələlərinə, o cümlədən Azərbaycan Respublikasında qaçqın statusu almış şəxslərə Qaçqın vəsiqəsinin və ya Yol sənədinin verilməsinə görə ödəniləcək dövlət rüsumları aşağıdakı məbləğlərdə müəyyən edilmişdir:', '', '&lt;table class=&quot;table table-bordered table-responsive table-hover&quot; align=&quot;justify&quot;&gt;\r\n&lt;tbody&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;1. Yetkinlik yaşına &amp;ccedil;atmış əcnəbilərə və vətəndaşlığı olmayan şəxslərə &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılması barədə qərarın verilməsinə g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;30 g&amp;uuml;nədək&lt;/td&gt;\r\n&lt;td&gt;30 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;60 g&amp;uuml;nədək&lt;/td&gt;\r\n&lt;td&gt;60 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;2. Yetkinlik yaşına &amp;ccedil;atmayan əcnəbilərə və vətəndaşlığı olmayan şəxslərə &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılması barədə qərarın verilməsinə g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;30 g&amp;uuml;nədək&lt;/td&gt;\r\n&lt;td&gt;15 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;60 g&amp;uuml;nədək&lt;/td&gt;\r\n&lt;td&gt;30 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;3. Yetkinlik yaşına &amp;ccedil;atmış əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqəsinin verilməsinə və m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;3 ayadək&lt;/td&gt;\r\n&lt;td&gt;30 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;6 ayadək&lt;/td&gt;\r\n&lt;td&gt;60 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;1 ilədək&lt;/td&gt;\r\n&lt;td&gt;120 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;1 il 6 ayadək&lt;/td&gt;\r\n&lt;td&gt;180 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;2 ilədək&lt;/td&gt;\r\n&lt;td&gt;240 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;2 il 6 ayadək&lt;/td&gt;\r\n&lt;td&gt;300 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;3 ilədək&lt;/td&gt;\r\n&lt;td&gt;360 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;4. Yetkinlik yaşına &amp;ccedil;atmayan əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqəsinin verilməsinə və m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;3 ayadək&lt;/td&gt;\r\n&lt;td&gt;15 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;6 ayadək&lt;/td&gt;\r\n&lt;td&gt;30 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;1 ilədək&lt;/td&gt;\r\n&lt;td&gt;60 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;1 il 6 ayadək&lt;/td&gt;\r\n&lt;td&gt;90 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;2 ilədək&lt;/td&gt;\r\n&lt;td&gt;120 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;2 il 6 ayadək&lt;/td&gt;\r\n&lt;td&gt;150 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;3 ilədək&lt;/td&gt;\r\n&lt;td&gt;180 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;5. Azərbaycan Respublikasının ali və orta ixtisas təhsili m&amp;uuml;əssisələrində əyani formada təhsil alan əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqəsinin verilməsinə və m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;1 ilədək&lt;/td&gt;\r\n&lt;td&gt;40 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;2 ilədək (m&amp;uuml;vəqqəti yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqəsinin m&amp;uuml;ddəti uzadıldıqda)&lt;/td&gt;\r\n&lt;td&gt;80 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;6. Yetkinlik yaşına &amp;ccedil;atmış əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqəsinin verilməsinə və m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;300 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;7. Yetkinlik yaşına &amp;ccedil;atmayan əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqəsinin verilməsinə və m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;150 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;8. Əcnəbilərə və vətəndaşlığı olmayan şəxslərə itirilmiş Azərbaycan Respublikasında m&amp;uuml;vəqqəti və ya daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazə vəsiqələrinin və m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılması barədə qərarın yenidən verilməsinə g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;10 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;9. Azərbaycan Respublikasının ərazisində haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq &amp;uuml;&amp;ccedil;&amp;uuml;n əcnəbilərə və vətəndaşlığı olmayan şəxslərə iş icazəsinin verilməsinə və m&amp;uuml;ddətinin uzadılmasına g&amp;ouml;rə (işəg&amp;ouml;t&amp;uuml;rən tərəfindən &amp;ouml;dənilir):&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;3 ayadək&lt;/td&gt;\r\n&lt;td&gt;350 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;6 ayadək&lt;/td&gt;\r\n&lt;td&gt;600 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td&gt;1 ilədək&lt;/td&gt;\r\n&lt;td&gt;1000 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;10. Azərbaycan Respublikası vətəndaşlığına qəbul edilmək, bərpa edilmək və ya ondan &amp;ccedil;ıxmaq barədə ərizələrə g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;110 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td class=&quot;active&quot; colspan=&quot;2&quot;&gt;11. Azərbaycan Respublikasında qa&amp;ccedil;qın statusu almış şəxslərə Qa&amp;ccedil;qın vəsiqəsinin və ya Yol sənədinin verilməsinə g&amp;ouml;rə:&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;tr&gt;\r\n&lt;td colspan=&quot;2&quot;&gt;5 manat&lt;/td&gt;\r\n&lt;/tr&gt;\r\n&lt;/tbody&gt;\r\n&lt;/table&gt;', 0, 0, 1, 1, 3, 1, 0, 1537861989, 1537862363);

-- --------------------------------------------------------

--
-- Table structure for table `senedler`
--

CREATE TABLE `senedler` (
  `id` int(3) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `senedler`
--

INSERT INTO `senedler` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, 'Dövlət orqanları tərəfindən müşayiət məktubu ilə aşağıdakı sənədlər təqdim olunur', '', '', '', '', '&lt;p&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;/p&gt;', 0, 1, 1, 1, 1, 0),
(2, 'Dövlət orqanları tərəfindən müşayiət məktubu ilə aşağıdakı sənədlər təqdim olunur', '', '', '', '', '&lt;p&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;/p&gt;', 0, 1, 1, 2, 1, 0),
(3, 'Dövlət orqanları tərəfindən müşayiət məktubu ilə aşağıdakı sənədlər təqdim olunur', '', '', '', '', '&lt;p&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;strong&gt;D&amp;ouml;vlət orqanları tərəfindən m&amp;uuml;şayiət məktubu ilə aşağıdakı sənədlər təqdim olunur&lt;/strong&gt;&lt;/p&gt;', 0, 1, 1, 3, 1, 0),
(4, 'Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!', '', '', '', '', '&lt;p&gt;Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!&lt;/p&gt;', 0, 2, 1, 1, 2, 0),
(5, 'Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!', '', '', '', '', '&lt;p&gt;Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!&lt;/p&gt;', 0, 2, 1, 2, 2, 0),
(6, 'Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!', '', '', '', '', '&lt;p&gt;Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!&lt;/p&gt;', 0, 2, 1, 3, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sened_mektub`
--

CREATE TABLE `sened_mektub` (
  `id` int(3) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sened_mektub`
--

INSERT INTO `sened_mektub` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, 'Vətəndaşlığı olmayan şəxsin şəxsiyyət vəsiqəsi', '', '', '', '', '', 0, 1, 1, 1, 1, 0),
(2, 'Vətəndaşlığı olmayan şəxsin şəxsiyyət vəsiqəsi', '', '', '', '', '', 0, 1, 1, 2, 1, 0),
(3, 'Vətəndaşlığı olmayan şəxsin şəxsiyyət vəsiqəsi', '', '', '', '', '', 0, 1, 1, 3, 1, 0),
(4, 'Qaçqın vəsiqəsi', '', '', '', '', '', 0, 2, 1, 1, 2, 0),
(5, 'Qaçqın vəsiqəsi', '', '', '', '', '', 0, 2, 1, 2, 2, 0),
(6, 'Qaçqın vəsiqəsi', '', '', '', '', '', 0, 2, 1, 3, 2, 0),
(7, 'Yol sənədi', '', '', '', '', '', 0, 3, 1, 1, 3, 0),
(8, 'Yol sənədi', '', '', '', '', '', 0, 3, 1, 2, 3, 0),
(9, 'Yol sənədi', '', '', '', '', '', 0, 3, 1, 3, 3, 0),
(10, 'Azərbaycan vətəndaşının şəxsiyyət vəsiqəsi', '', '', '', '', '', 0, 4, 1, 1, 4, 0),
(11, 'Azərbaycan vətəndaşının şəxsiyyət vəsiqəsi', '', '', '', '', '', 0, 4, 1, 2, 4, 0),
(12, 'Azərbaycan vətəndaşının şəxsiyyət vəsiqəsi', '', '', '', '', '', 0, 4, 1, 3, 4, 0),
(13, 'Azərbaycan vətəndaşının pasportu', '', '', '', '', '', 0, 5, 1, 1, 5, 0),
(14, 'Azərbaycan vətəndaşının pasportu', '', '', '', '', '', 0, 5, 1, 2, 5, 0),
(15, 'Azərbaycan vətəndaşının pasportu', '', '', '', '', '', 0, 5, 1, 3, 5, 0),
(16, 'Qayıdış şəhadətnaməsi', '', '', '', '', '', 0, 6, 1, 1, 6, 0),
(17, 'Qayıdış şəhadətnaməsi', '', '', '', '', '', 0, 6, 1, 2, 6, 0),
(18, 'Qayıdış şəhadətnaməsi', '', '', '', '', '', 0, 6, 1, 3, 6, 0),
(19, 'Əcnəbinin pasportu', '', '', '', '', '', 0, 7, 1, 1, 7, 0),
(20, 'Əcnəbinin pasportu', '', '', '', '', '', 0, 7, 1, 2, 7, 0),
(21, 'Əcnəbinin pasportu', '', '', '', '', '', 0, 7, 1, 3, 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `social_mektub`
--

CREATE TABLE `social_mektub` (
  `id` int(3) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `social_mektub`
--

INSERT INTO `social_mektub` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, 'təhsil işçisi', '', '', '', '', '', 0, 1, 1, 1, 1, 0),
(2, 'təhsil işçisi', '', '', '', '', '', 0, 1, 1, 2, 1, 0),
(3, 'təhsil işçisi', '', '', '', '', '', 0, 1, 1, 3, 1, 0),
(4, 'özəl sektorun işçisi', '', '', '', '', '', 0, 2, 1, 1, 2, 0),
(5, 'özəl sektorun işçisi', '', '', '', '', '', 0, 2, 1, 2, 2, 0),
(6, 'özəl sektorun işçisi', '', '', '', '', '', 0, 2, 1, 3, 2, 0),
(7, 'fəhlə', '', '', '', '', '', 0, 3, 1, 1, 3, 0),
(8, 'fəhlə', '', '', '', '', '', 0, 3, 1, 2, 3, 0),
(9, 'fəhlə', '', '', '', '', '', 0, 3, 1, 3, 3, 0),
(10, 'evdar qadın', '', '', '', '', '', 0, 4, 1, 1, 4, 0),
(11, 'evdar qadın', '', '', '', '', '', 0, 4, 1, 2, 4, 0),
(12, 'evdar qadın', '', '', '', '', '', 0, 4, 1, 3, 4, 0),
(13, 'işsiz', '', '', '', '', '', 0, 5, 1, 1, 5, 0),
(14, 'işsiz', '', '', '', '', '', 0, 5, 1, 2, 5, 0),
(15, 'işsiz', '', '', '', '', '', 0, 5, 1, 3, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `statistics`
--

CREATE TABLE `statistics` (
  `id` int(11) NOT NULL,
  `daily` varchar(255) NOT NULL,
  `loan` varchar(255) NOT NULL,
  `partners` varchar(255) NOT NULL,
  `customers` varchar(255) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `statistics`
--

INSERT INTO `statistics` (`id`, `daily`, `loan`, `partners`, `customers`, `lang_id`, `auto_id`) VALUES
(1, '365', '20', '200', '2000', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `structure`
--

CREATE TABLE `structure` (
  `id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `structure`
--

INSERT INTO `structure` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(3, '', '', '', '', 'jpg', '&lt;p dir=&quot;ltr&quot;&gt;&lt;span&gt;A.SET Kredit &amp;ndash; FinTrend BOKT bazasında yaradılmış universal bir məhsuldur. Həmin məhsuldan istifadə etməklə siz partnyor elektronika və məişət texnikasının ticarət şəbəkələrində, mebel mağazalarında, avtomobillərin servis mərkəzlərinda istənilən məhsulu və ya xidməti Sizə maksimal sərfəli şərtlərlə kreditlə əldə edə bilərsiniz. Bu məhsulun əsas &amp;uuml;st&amp;uuml;nl&amp;uuml;y&amp;uuml; mağazadan ayrılmadan kreditə mal və xidmətlərin alınmasıdır.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;span&gt;Bu g&amp;uuml;n biz, Sizə istehlak krediti bazarında se&amp;ccedil;ilən ilkin &amp;ouml;dənişsiz m&amp;uuml;ddəti 30 aya qədər olan rəqabət şərtləri ilə se&amp;ccedil;ilən kredit təklif edirik.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;span&gt;Siz tərəfdaşlarımız barədə ətraflı məlumatı PARTNYORLAR b&amp;ouml;l&amp;uuml;m&amp;uuml;ndən əldə edə bilərsiz. sad sadsa dsa dad&lt;/span&gt;&lt;/p&gt;', 0, 1, 1, 1, 2, 0),
(4, '', '', '', '', 'jpg', '&lt;p dir=&quot;ltr&quot;&gt;&lt;span&gt;A.SET Kredit &amp;ndash; это универсальный продукт создан на базе BOKT FINtrend, с помощью которого Вы сможете купить товар и/или услугу в кредит, во всех торговых сетях партнерах электроники и бытовой техники, мебели, автозапчастей, автомобильных сервисных центрах с максимально выгодными для Вас условиями. &lt;/span&gt;&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;span&gt;Основное преимущество данного продукта &amp;ndash; это приобретение товара или услуги в кредит прямо в магазине.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;span&gt;На сегодняшний день мы предлагаем конкурентные условия работы на рынке потребительского кредитования со сроком кредитования до 36 месяцев без первого взноса.&lt;/span&gt;&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;span&gt;Вы можете детально ознакомится со всеми нашими партнерами в разделе ПАРТНЕРЫ.&lt;/span&gt;&lt;/p&gt;', 0, 1, 1, 2, 2, 0),
(5, '', '', '', '', 'jpg', '&lt;p dir=&quot;ltr&quot;&gt;A.SET Kredit &amp;ndash; это универсальный продукт создан на базе BOKT FINtrend, с помощью которого Вы сможете купить товар и/или услугу в кредит, во всех торговых сетях партнерах электроники и бытовой техники, мебели, автозапчастей, автомобильных сервисных центрах с максимально выгодными для Вас условиями.&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;Основное преимущество данного продукта &amp;ndash; это приобретение товара или услуги в кредит прямо в магазине.&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;На сегодняшний день мы предлагаем конкурентные условия работы на рынке потребительского кредитования со сроком кредитования до 36 месяцев без первого взноса.&lt;/p&gt;\r\n&lt;p dir=&quot;ltr&quot;&gt;Вы можете детально ознакомится со всеми нашими партнерами в разделе ПАРТНЕРЫ.&lt;/p&gt;', 0, 1, 1, 3, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sual`
--

CREATE TABLE `sual` (
  `id` int(3) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sual`
--

INSERT INTO `sual` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, 'Azərbaycan Respublikası  DÖVLƏT MİQRASİYA XİDMƏTİ', '', '', '', '', '&lt;center&gt;&lt;span id=&quot;basliq1&quot;&gt;Azərbaycan Respublikası&amp;nbsp;&lt;br /&gt;D&amp;Ouml;VLƏT MİQRASİYA XİDMƏTİ&lt;/span&gt;&lt;/center&gt;\r\n&lt;p&gt;&lt;span&gt;D&amp;ouml;vlət miqrasiya siyasəti&lt;/span&gt;&lt;br /&gt;Azərbaycan Respublikasında miqrasiya sahəsində d&amp;ouml;vlət siyasətinin həyata ke&amp;ccedil;irilməsi, qanunvericiliyin beynəlxalq normalara və m&amp;uuml;asir d&amp;ouml;vr&amp;uuml;n tələblərinə uyğun təkmilləşdirilməsi, qanunların tətbiqində Azərbaycan Respublikasının milli təhl&amp;uuml;kəsizliyinin və sabit sosial-iqtisadi, demoqrafik inkişafının təmin edilməsi, əmək ehtiyatlarından səmərəli istifadə edilməsi, &amp;ouml;lkə ərazisində əhalinin m&amp;uuml;tənasib yerləşdirilməsi, miqrantların intellektual və əmək potensialından istifadə edilməsi, tənzimlənməyən miqrasiya proseslərinin neqativ təsirinin aradan qaldırılması, insan alveri də daxil olmaqla, qeyri-qanuni miqrasiyanın qarşısının alınması məqsədi ilə Azərbaycan Respublikası Prezidentinin 25 iyul 2006-cı il tarixli Sərəncamı ilə &amp;ldquo;Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Proqramı (2006-2008-ci illər)&amp;rdquo; təsdiq edilmişdir. Bu D&amp;ouml;vlət Proqramının əsas məqsədi miqrasiya sahəsində d&amp;ouml;vlət siyasətinin həyata ke&amp;ccedil;irilməsi, miqrasiya idarəetmə sisteminin inkişaf etdirilməsi, miqrasiya proseslərinin tənzimlənməsi və proqnozlaşdırılması, bu sahədə qanunvericiliyin beynəlxalq normalara və m&amp;uuml;asir d&amp;ouml;vr&amp;uuml;n tələblərinə uyğun təkmilləşdirilməsi, qanunların tətbiqinin effektivliyinin təmin edilməsi, miqrasiya sahəsində vahid məlumat bankının, m&amp;uuml;asir avtomatlaşdırılmış nəzarət sisteminin yaradılması, qeyri-qanuni miqrasiyanın qarşısının alınması, beynəlxalq əməkdaşlığın inkişaf etdirilməsi tədbirlərinin həyata ke&amp;ccedil;irilməsindən ibarətdir. Proqramda miqrasiya proseslərinin x&amp;uuml;susiyyətləri, d&amp;ouml;vlət siyasətinin prioritet istiqamətləri &amp;ouml;z əksini tapmışdır. Bununla yanaşı proqramın əhatə etdiyi d&amp;ouml;vr ərzində ayrı-ayrı d&amp;ouml;vlət qurumları ilə əlaqəli şəkildə həyata ke&amp;ccedil;irməli olduqları tədbirlər qeyd edilmişdir&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;D&amp;ouml;vlət Miqrasiya Xidməti haqqında&lt;/span&gt;&amp;nbsp;&lt;br /&gt;Təsdiq edilmiş &amp;ldquo;Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Proqramı&amp;rdquo; əsasında miqrasiya sahəsində vahid d&amp;ouml;vlət siyasətini həyata ke&amp;ccedil;irən x&amp;uuml;susi d&amp;ouml;vlət orqanının yaradılması zərurətini nəzərə alan Azərbaycan Respublikasının Prezidenti cənab İlham Əliyev 19 mart 2007-ci il tarixdə Azərbaycan Respublikası D&amp;ouml;vlət Miqrasiya Xidmətinin yaradılması haqqında 560 n&amp;ouml;mrəli Fərman imzalamışdır. Eyni zamanda, Fərmana əsasən Xidmətin Əsasnaməsi təsdiq edilmişdir. D&amp;ouml;vlət Miqrasiya Xidmətinin yaradıldığı vaxtdan miqrasiya proseslərinə d&amp;ouml;vlət nəzarətinin g&amp;uuml;cləndirilməsi məqsədi ilə zəruri normativ h&amp;uuml;quqi aktlar qəbul edilmiş, bir sıra institusional və təşkilati tədbirlər həyata ke&amp;ccedil;irilmişdir. Miqrasiya proseslərinin vahid və &amp;ccedil;evik prosedurlar əsasında tənzimlənməsi, sənədləşmənin sadələşdirilməsi məqsədilə Azərbaycan Respublikasının Prezidenti cənab İlham Əliyev tərəfindən &quot;Miqrasiya proseslərinin idarəolunmasında &quot;bir pəncərə&quot; prinsipinin tətbiqi haqqında&quot; 4 mart 2009-cu il tarixli 69 n&amp;ouml;mrəli Fərman imzalanmışdır. Adı&amp;ccedil;əkilən Fərmanın tətbiqi &amp;ouml;lkədə miqrasiya proseslərinin daha &amp;ccedil;evik və işlək mexanizmlər əsasında idarə edilməsinə, bu sahədə operativliyin təmin edilməsinə və həllini g&amp;ouml;zləyən problemlərin aradan qaldırılmasına səbəb olmuşdur. Fərmana əsasən, 2009-cu il iyulun 1-dən miqrasiya proseslərinin idarəolunmasında &amp;laquo;bir pəncərə&amp;raquo; prinsipinin tətbiq edilməsinə başlanılmış və bu prinsip &amp;uuml;zrə vahid d&amp;ouml;vlət orqanının səlahiyyətləri Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Xidmətinə həvalə edilmişdir. &amp;ldquo;Bir pəncərə&amp;rdquo; prinsipi &amp;ccedil;ər&amp;ccedil;ivəsində D&amp;ouml;vlət Miqrasiya Xidməti əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti və daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazələrin və m&amp;uuml;vafiq vəsiqələrin verilməsini, onların qeydiyyata alınmasını, Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılmasını, həm&amp;ccedil;inin &amp;ouml;lkə ərazisində haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq &amp;uuml;&amp;ccedil;&amp;uuml;n iş icazələrinin verilməsini həyata ke&amp;ccedil;irir. Bununla belə, Xidmət vətəndaşlıq məsələlərində iştirak edir və qa&amp;ccedil;qın statusunu m&amp;uuml;əyyənləşdirir. Qeyri-qanuni miqrasiyaya qarşı m&amp;uuml;barizə tədbirlərinin g&amp;uuml;cləndirilməsi, Xidmətin fəaliyyətinin təkmilləşdirilməsi məqsədi ilə Prezident cənab İlham Əliyevin 8 aprel 2009-cu il tarixdə imzaladığı 76 n&amp;ouml;mrəli Fərmanla D&amp;ouml;vlət Miqrasiya Xidmətinə h&amp;uuml;quq-m&amp;uuml;hafizə orqanı statusu verilmişdir.&lt;/p&gt;', 0, 1, 1, 1, 1, 0),
(2, 'Azərbaycan Respublikası  DÖVLƏT MİQRASİYA XİDMƏTİ', '', '', '', '', '&lt;p&gt;&lt;strong&gt;D&amp;ouml;vlət miqrasiya siyasəti&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;&lt;/strong&gt;&lt;br /&gt;Azərbaycan Respublikasında miqrasiya sahəsində d&amp;ouml;vlət siyasətinin həyata ke&amp;ccedil;irilməsi, qanunvericiliyin beynəlxalq normalara və m&amp;uuml;asir d&amp;ouml;vr&amp;uuml;n tələblərinə uyğun təkmilləşdirilməsi, qanunların tətbiqində Azərbaycan Respublikasının milli təhl&amp;uuml;kəsizliyinin və sabit sosial-iqtisadi, demoqrafik inkişafının təmin edilməsi, əmək ehtiyatlarından səmərəli istifadə edilməsi, &amp;ouml;lkə ərazisində əhalinin m&amp;uuml;tənasib yerləşdirilməsi, miqrantların intellektual və əmək potensialından istifadə edilməsi, tənzimlənməyən miqrasiya proseslərinin neqativ təsirinin aradan qaldırılması, insan alveri də daxil olmaqla, qeyri-qanuni miqrasiyanın qarşısının alınması məqsədi ilə Azərbaycan Respublikası Prezidentinin 25 iyul 2006-cı il tarixli Sərəncamı ilə &amp;ldquo;Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Proqramı (2006-2008-ci illər)&amp;rdquo; təsdiq edilmişdir. Bu D&amp;ouml;vlət Proqramının əsas məqsədi miqrasiya sahəsində d&amp;ouml;vlət siyasətinin həyata ke&amp;ccedil;irilməsi, miqrasiya idarəetmə sisteminin inkişaf etdirilməsi, miqrasiya proseslərinin tənzimlənməsi və proqnozlaşdırılması, bu sahədə qanunvericiliyin beynəlxalq normalara və m&amp;uuml;asir d&amp;ouml;vr&amp;uuml;n tələblərinə uyğun təkmilləşdirilməsi, qanunların tətbiqinin effektivliyinin təmin edilməsi, miqrasiya sahəsində vahid məlumat bankının, m&amp;uuml;asir avtomatlaşdırılmış nəzarət sisteminin yaradılması, qeyri-qanuni miqrasiyanın qarşısının alınması, beynəlxalq əməkdaşlığın inkişaf etdirilməsi tədbirlərinin həyata ke&amp;ccedil;irilməsindən ibarətdir. Proqramda miqrasiya proseslərinin x&amp;uuml;susiyyətləri, d&amp;ouml;vlət siyasətinin prioritet istiqamətləri &amp;ouml;z əksini tapmışdır. Bununla yanaşı proqramın əhatə etdiyi d&amp;ouml;vr ərzində ayrı-ayrı d&amp;ouml;vlət qurumları ilə əlaqəli şəkildə həyata ke&amp;ccedil;irməli olduqları tədbirlər qeyd edilmişdir&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;D&amp;ouml;vlət Miqrasiya Xidməti haqqında&lt;/span&gt;&amp;nbsp;&lt;br /&gt;Təsdiq edilmiş &amp;ldquo;Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Proqramı&amp;rdquo; əsasında miqrasiya sahəsində vahid d&amp;ouml;vlət siyasətini həyata ke&amp;ccedil;irən x&amp;uuml;susi d&amp;ouml;vlət orqanının yaradılması zərurətini nəzərə alan Azərbaycan Respublikasının Prezidenti cənab İlham Əliyev 19 mart 2007-ci il tarixdə Azərbaycan Respublikası D&amp;ouml;vlət Miqrasiya Xidmətinin yaradılması haqqında 560 n&amp;ouml;mrəli Fərman imzalamışdır. Eyni zamanda, Fərmana əsasən Xidmətin Əsasnaməsi təsdiq edilmişdir. D&amp;ouml;vlət Miqrasiya Xidmətinin yaradıldığı vaxtdan miqrasiya proseslərinə d&amp;ouml;vlət nəzarətinin g&amp;uuml;cləndirilməsi məqsədi ilə zəruri normativ h&amp;uuml;quqi aktlar qəbul edilmiş, bir sıra institusional və təşkilati tədbirlər həyata ke&amp;ccedil;irilmişdir. Miqrasiya proseslərinin vahid və &amp;ccedil;evik prosedurlar əsasında tənzimlənməsi, sənədləşmənin sadələşdirilməsi məqsədilə Azərbaycan Respublikasının Prezidenti cənab İlham Əliyev tərəfindən &quot;Miqrasiya proseslərinin idarəolunmasında &quot;bir pəncərə&quot; prinsipinin tətbiqi haqqında&quot; 4 mart 2009-cu il tarixli 69 n&amp;ouml;mrəli Fərman imzalanmışdır. Adı&amp;ccedil;əkilən Fərmanın tətbiqi &amp;ouml;lkədə miqrasiya proseslərinin daha &amp;ccedil;evik və işlək mexanizmlər əsasında idarə edilməsinə, bu sahədə operativliyin təmin edilməsinə və həllini g&amp;ouml;zləyən problemlərin aradan qaldırılmasına səbəb olmuşdur. Fərmana əsasən, 2009-cu il iyulun 1-dən miqrasiya proseslərinin idarəolunmasında &amp;laquo;bir pəncərə&amp;raquo; prinsipinin tətbiq edilməsinə başlanılmış və bu prinsip &amp;uuml;zrə vahid d&amp;ouml;vlət orqanının səlahiyyətləri Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Xidmətinə həvalə edilmişdir. &amp;ldquo;Bir pəncərə&amp;rdquo; prinsipi &amp;ccedil;ər&amp;ccedil;ivəsində D&amp;ouml;vlət Miqrasiya Xidməti əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti və daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazələrin və m&amp;uuml;vafiq vəsiqələrin verilməsini, onların qeydiyyata alınmasını, Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılmasını, həm&amp;ccedil;inin &amp;ouml;lkə ərazisində haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq &amp;uuml;&amp;ccedil;&amp;uuml;n iş icazələrinin verilməsini həyata ke&amp;ccedil;irir. Bununla belə, Xidmət vətəndaşlıq məsələlərində iştirak edir və qa&amp;ccedil;qın statusunu m&amp;uuml;əyyənləşdirir. Qeyri-qanuni miqrasiyaya qarşı m&amp;uuml;barizə tədbirlərinin g&amp;uuml;cləndirilməsi, Xidmətin fəaliyyətinin təkmilləşdirilməsi məqsədi ilə Prezident cənab İlham Əliyevin 8 aprel 2009-cu il tarixdə imzaladığı 76 n&amp;ouml;mrəli Fərmanla D&amp;ouml;vlət Miqrasiya Xidmətinə h&amp;uuml;quq-m&amp;uuml;hafizə orqanı statusu verilmişdir.&lt;/p&gt;', 0, 1, 1, 2, 1, 0),
(3, 'Azərbaycan Respublikası  DÖVLƏT MİQRASİYA XİDMƏTİ', '', '', '', '', '&lt;p&gt;&lt;strong&gt;D&amp;ouml;vlət miqrasiya siyasəti&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;&lt;/strong&gt;&lt;br /&gt;Azərbaycan Respublikasında miqrasiya sahəsində d&amp;ouml;vlət siyasətinin həyata ke&amp;ccedil;irilməsi, qanunvericiliyin beynəlxalq normalara və m&amp;uuml;asir d&amp;ouml;vr&amp;uuml;n tələblərinə uyğun təkmilləşdirilməsi, qanunların tətbiqində Azərbaycan Respublikasının milli təhl&amp;uuml;kəsizliyinin və sabit sosial-iqtisadi, demoqrafik inkişafının təmin edilməsi, əmək ehtiyatlarından səmərəli istifadə edilməsi, &amp;ouml;lkə ərazisində əhalinin m&amp;uuml;tənasib yerləşdirilməsi, miqrantların intellektual və əmək potensialından istifadə edilməsi, tənzimlənməyən miqrasiya proseslərinin neqativ təsirinin aradan qaldırılması, insan alveri də daxil olmaqla, qeyri-qanuni miqrasiyanın qarşısının alınması məqsədi ilə Azərbaycan Respublikası Prezidentinin 25 iyul 2006-cı il tarixli Sərəncamı ilə &amp;ldquo;Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Proqramı (2006-2008-ci illər)&amp;rdquo; təsdiq edilmişdir. Bu D&amp;ouml;vlət Proqramının əsas məqsədi miqrasiya sahəsində d&amp;ouml;vlət siyasətinin həyata ke&amp;ccedil;irilməsi, miqrasiya idarəetmə sisteminin inkişaf etdirilməsi, miqrasiya proseslərinin tənzimlənməsi və proqnozlaşdırılması, bu sahədə qanunvericiliyin beynəlxalq normalara və m&amp;uuml;asir d&amp;ouml;vr&amp;uuml;n tələblərinə uyğun təkmilləşdirilməsi, qanunların tətbiqinin effektivliyinin təmin edilməsi, miqrasiya sahəsində vahid məlumat bankının, m&amp;uuml;asir avtomatlaşdırılmış nəzarət sisteminin yaradılması, qeyri-qanuni miqrasiyanın qarşısının alınması, beynəlxalq əməkdaşlığın inkişaf etdirilməsi tədbirlərinin həyata ke&amp;ccedil;irilməsindən ibarətdir. Proqramda miqrasiya proseslərinin x&amp;uuml;susiyyətləri, d&amp;ouml;vlət siyasətinin prioritet istiqamətləri &amp;ouml;z əksini tapmışdır. Bununla yanaşı proqramın əhatə etdiyi d&amp;ouml;vr ərzində ayrı-ayrı d&amp;ouml;vlət qurumları ilə əlaqəli şəkildə həyata ke&amp;ccedil;irməli olduqları tədbirlər qeyd edilmişdir&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;D&amp;ouml;vlət Miqrasiya Xidməti haqqında&lt;/span&gt;&amp;nbsp;&lt;br /&gt;Təsdiq edilmiş &amp;ldquo;Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Proqramı&amp;rdquo; əsasında miqrasiya sahəsində vahid d&amp;ouml;vlət siyasətini həyata ke&amp;ccedil;irən x&amp;uuml;susi d&amp;ouml;vlət orqanının yaradılması zərurətini nəzərə alan Azərbaycan Respublikasının Prezidenti cənab İlham Əliyev 19 mart 2007-ci il tarixdə Azərbaycan Respublikası D&amp;ouml;vlət Miqrasiya Xidmətinin yaradılması haqqında 560 n&amp;ouml;mrəli Fərman imzalamışdır. Eyni zamanda, Fərmana əsasən Xidmətin Əsasnaməsi təsdiq edilmişdir. D&amp;ouml;vlət Miqrasiya Xidmətinin yaradıldığı vaxtdan miqrasiya proseslərinə d&amp;ouml;vlət nəzarətinin g&amp;uuml;cləndirilməsi məqsədi ilə zəruri normativ h&amp;uuml;quqi aktlar qəbul edilmiş, bir sıra institusional və təşkilati tədbirlər həyata ke&amp;ccedil;irilmişdir. Miqrasiya proseslərinin vahid və &amp;ccedil;evik prosedurlar əsasında tənzimlənməsi, sənədləşmənin sadələşdirilməsi məqsədilə Azərbaycan Respublikasının Prezidenti cənab İlham Əliyev tərəfindən &quot;Miqrasiya proseslərinin idarəolunmasında &quot;bir pəncərə&quot; prinsipinin tətbiqi haqqında&quot; 4 mart 2009-cu il tarixli 69 n&amp;ouml;mrəli Fərman imzalanmışdır. Adı&amp;ccedil;əkilən Fərmanın tətbiqi &amp;ouml;lkədə miqrasiya proseslərinin daha &amp;ccedil;evik və işlək mexanizmlər əsasında idarə edilməsinə, bu sahədə operativliyin təmin edilməsinə və həllini g&amp;ouml;zləyən problemlərin aradan qaldırılmasına səbəb olmuşdur. Fərmana əsasən, 2009-cu il iyulun 1-dən miqrasiya proseslərinin idarəolunmasında &amp;laquo;bir pəncərə&amp;raquo; prinsipinin tətbiq edilməsinə başlanılmış və bu prinsip &amp;uuml;zrə vahid d&amp;ouml;vlət orqanının səlahiyyətləri Azərbaycan Respublikasının D&amp;ouml;vlət Miqrasiya Xidmətinə həvalə edilmişdir. &amp;ldquo;Bir pəncərə&amp;rdquo; prinsipi &amp;ccedil;ər&amp;ccedil;ivəsində D&amp;ouml;vlət Miqrasiya Xidməti əcnəbilərə və vətəndaşlığı olmayan şəxslərə Azərbaycan Respublikasının ərazisində m&amp;uuml;vəqqəti və daimi yaşamaq &amp;uuml;&amp;ccedil;&amp;uuml;n icazələrin və m&amp;uuml;vafiq vəsiqələrin verilməsini, onların qeydiyyata alınmasını, Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin &amp;ouml;lkədə m&amp;uuml;vəqqəti olma m&amp;uuml;ddətinin uzadılmasını, həm&amp;ccedil;inin &amp;ouml;lkə ərazisində haqqı &amp;ouml;dənilən əmək fəaliyyəti ilə məşğul olmaq &amp;uuml;&amp;ccedil;&amp;uuml;n iş icazələrinin verilməsini həyata ke&amp;ccedil;irir. Bununla belə, Xidmət vətəndaşlıq məsələlərində iştirak edir və qa&amp;ccedil;qın statusunu m&amp;uuml;əyyənləşdirir. Qeyri-qanuni miqrasiyaya qarşı m&amp;uuml;barizə tədbirlərinin g&amp;uuml;cləndirilməsi, Xidmətin fəaliyyətinin təkmilləşdirilməsi məqsədi ilə Prezident cənab İlham Əliyevin 8 aprel 2009-cu il tarixdə imzaladığı 76 n&amp;ouml;mrəli Fərmanla D&amp;ouml;vlət Miqrasiya Xidmətinə h&amp;uuml;quq-m&amp;uuml;hafizə orqanı statusu verilmişdir.&lt;/p&gt;', 0, 1, 1, 3, 1, 0),
(6, 'Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!', '', '', '', '', '&lt;p&gt;Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!&lt;/p&gt;', 0, 2, 1, 1, 2, 0),
(7, 'Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!', '', '', '', '', '&lt;p&gt;Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!&lt;/p&gt;', 0, 2, 1, 2, 2, 0),
(8, 'Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!', '', '', '', '', '&lt;p&gt;Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!Viza ilə Azərbaycan Respublikasına gələn əcnəbilərin və vətəndaşlığı olmayan şəxslərin nəzərinə!&lt;/p&gt;', 0, 2, 1, 3, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `reg_date` int(11) NOT NULL,
  `last_visit` int(11) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `tokenCode` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `pass`, `email`, `phone`, `reg_date`, `last_visit`, `ip`, `status`, `tokenCode`) VALUES
(1, 'Fuad1', 'Hasanli', '', 'fhesenli92@gmail.com', '', 1484731818, 0, '127.0.0.1', '1', '22dc12f84a06f40090ef1f8cb5983948');

-- --------------------------------------------------------

--
-- Table structure for table `vetendasliq`
--

CREATE TABLE `vetendasliq` (
  `id` int(3) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qisa_metn` text NOT NULL,
  `tip` varchar(5) NOT NULL,
  `text` text NOT NULL,
  `parent_auto_id` int(11) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `vacib_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vetendasliq`
--

INSERT INTO `vetendasliq` (`id`, `name`, `title`, `link`, `qisa_metn`, `tip`, `text`, `parent_auto_id`, `sira`, `aktivlik`, `lang_id`, `auto_id`, `vacib_menu`) VALUES
(1, 'Azərbaycan Respublikası vətəndaşlığının itirilməsi halları ', '', '', '', '', '&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;&amp;ldquo;Vətəndaşsızlığın ixtisar edilməsi haqqında&amp;rdquo; 1961-ci il 30 avqust tarixli Konvensiyanın m&amp;uuml;ddəaları nəzərə alınmaqla, aşağıdakılar Azərbaycan Respublikası vətəndaşlığının itirilməsi &amp;uuml;&amp;ccedil;&amp;uuml;n əsas hesab edilir:&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;1) Azərbaycan Respublikası vətəndaşının digər d&amp;ouml;vlətin vətəndaşlığını k&amp;ouml;n&amp;uuml;ll&amp;uuml; əldə etməsi;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;2) Azərbaycan Respublikası vətəndaşının xarici d&amp;ouml;vlətin d&amp;ouml;vlət və ya bələdiyyə orqanlarında, yaxud silahlı q&amp;uuml;vvələrində və ya digər silahlı birləşmələrində k&amp;ouml;n&amp;uuml;ll&amp;uuml; xidmət etməsi;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;3) Azərbaycan Respublikası vətəndaşının d&amp;ouml;vlətin təhl&amp;uuml;kəsizliyinə ciddi zərər vuran davranışı;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;3-1) Azərbaycan Respublikası vətəndaşının terror fəaliyyətində iştirakı və ya Azərbaycan Respublikasının konstitusiya quruluşunun zorla dəyişdirilməsinə y&amp;ouml;nələn hərəkətləri həyata ke&amp;ccedil;irməsi;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;3-2) Azərbaycan Respublikası vətəndaşının dini ekstremist fəaliyyətdə iştirakı, o c&amp;uuml;mlədən dini d&amp;uuml;şmən&amp;ccedil;ilik zəminində dini məzhəblərin yayılması və ya dini ayinlərin icrası adı altında xarici d&amp;ouml;vlətdə silahlı m&amp;uuml;naqişədə iştirak etməsi və ya həmin m&amp;uuml;naqişəyə digər şəxsi cəlb etməsi, yaxud bu məqsədlə sabit qrup yaratması, habelə Azərbaycan Respublikası vətəndaşının dini təhsil adı altında xarici d&amp;ouml;vlətdə hərbi təlim ke&amp;ccedil;məsi;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;4) Azərbaycan Respublikasının vətəndaşlığını əldə etmiş şəxsin vətəndaşlığa qəbul olunmaq &amp;uuml;&amp;ccedil;&amp;uuml;n zəruri olan məlumatı qəsdən saxtalaşdırması və ya saxta sənəd təqdim etməsi.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;Azərbaycan Respublikası vətəndaşlığının itirilməsi məsələsi &amp;ldquo;Azərbaycan Respublikasının vətəndaşlığı haqqında&amp;rdquo; Qanunun 17-ci maddəsinin ikinci və &amp;uuml;&amp;ccedil;&amp;uuml;nc&amp;uuml; hissələrində g&amp;ouml;stərilən məhdudiyyətlər nəzərə alınmaqla məhkəmə tərəfindən həll edilir.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;', 0, 1, 1, 1, 1, 0),
(2, 'Azərbaycan Respublikası vətəndaşlığının itirilməsi halları ', '', '', '', '', '&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;&amp;ldquo;Vətəndaşsızlığın ixtisar edilməsi haqqında&amp;rdquo; 1961-ci il 30 avqust tarixli Konvensiyanın m&amp;uuml;ddəaları nəzərə alınmaqla, aşağıdakılar Azərbaycan Respublikası vətəndaşlığının itirilməsi &amp;uuml;&amp;ccedil;&amp;uuml;n əsas hesab edilir:&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;1) Azərbaycan Respublikası vətəndaşının digər d&amp;ouml;vlətin vətəndaşlığını k&amp;ouml;n&amp;uuml;ll&amp;uuml; əldə etməsi;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;2) Azərbaycan Respublikası vətəndaşının xarici d&amp;ouml;vlətin d&amp;ouml;vlət və ya bələdiyyə orqanlarında, yaxud silahlı q&amp;uuml;vvələrində və ya digər silahlı birləşmələrində k&amp;ouml;n&amp;uuml;ll&amp;uuml; xidmət etməsi;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;3) Azərbaycan Respublikası vətəndaşının d&amp;ouml;vlətin təhl&amp;uuml;kəsizliyinə ciddi zərər vuran davranışı;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;3-1) Azərbaycan Respublikası vətəndaşının terror fəaliyyətində iştirakı və ya Azərbaycan Respublikasının konstitusiya quruluşunun zorla dəyişdirilməsinə y&amp;ouml;nələn hərəkətləri həyata ke&amp;ccedil;irməsi;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;3-2) Azərbaycan Respublikası vətəndaşının dini ekstremist fəaliyyətdə iştirakı, o c&amp;uuml;mlədən dini d&amp;uuml;şmən&amp;ccedil;ilik zəminində dini məzhəblərin yayılması və ya dini ayinlərin icrası adı altında xarici d&amp;ouml;vlətdə silahlı m&amp;uuml;naqişədə iştirak etməsi və ya həmin m&amp;uuml;naqişəyə digər şəxsi cəlb etməsi, yaxud bu məqsədlə sabit qrup yaratması, habelə Azərbaycan Respublikası vətəndaşının dini təhsil adı altında xarici d&amp;ouml;vlətdə hərbi təlim ke&amp;ccedil;məsi;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;4) Azərbaycan Respublikasının vətəndaşlığını əldə etmiş şəxsin vətəndaşlığa qəbul olunmaq &amp;uuml;&amp;ccedil;&amp;uuml;n zəruri olan məlumatı qəsdən saxtalaşdırması və ya saxta sənəd təqdim etməsi.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;Azərbaycan Respublikası vətəndaşlığının itirilməsi məsələsi &amp;ldquo;Azərbaycan Respublikasının vətəndaşlığı haqqında&amp;rdquo; Qanunun 17-ci maddəsinin ikinci və &amp;uuml;&amp;ccedil;&amp;uuml;nc&amp;uuml; hissələrində g&amp;ouml;stərilən məhdudiyyətlər nəzərə alınmaqla məhkəmə tərəfindən həll edilir.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;', 0, 1, 1, 2, 1, 0),
(3, 'Azərbaycan Respublikası vətəndaşlığının itirilməsi halları ', '', '', '', '', '&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;&amp;ldquo;Vətəndaşsızlığın ixtisar edilməsi haqqında&amp;rdquo; 1961-ci il 30 avqust tarixli Konvensiyanın m&amp;uuml;ddəaları nəzərə alınmaqla, aşağıdakılar Azərbaycan Respublikası vətəndaşlığının itirilməsi &amp;uuml;&amp;ccedil;&amp;uuml;n əsas hesab edilir:&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;1) Azərbaycan Respublikası vətəndaşının digər d&amp;ouml;vlətin vətəndaşlığını k&amp;ouml;n&amp;uuml;ll&amp;uuml; əldə etməsi;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;2) Azərbaycan Respublikası vətəndaşının xarici d&amp;ouml;vlətin d&amp;ouml;vlət və ya bələdiyyə orqanlarında, yaxud silahlı q&amp;uuml;vvələrində və ya digər silahlı birləşmələrində k&amp;ouml;n&amp;uuml;ll&amp;uuml; xidmət etməsi;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;3) Azərbaycan Respublikası vətəndaşının d&amp;ouml;vlətin təhl&amp;uuml;kəsizliyinə ciddi zərər vuran davranışı;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;3-1) Azərbaycan Respublikası vətəndaşının terror fəaliyyətində iştirakı və ya Azərbaycan Respublikasının konstitusiya quruluşunun zorla dəyişdirilməsinə y&amp;ouml;nələn hərəkətləri həyata ke&amp;ccedil;irməsi;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;3-2) Azərbaycan Respublikası vətəndaşının dini ekstremist fəaliyyətdə iştirakı, o c&amp;uuml;mlədən dini d&amp;uuml;şmən&amp;ccedil;ilik zəminində dini məzhəblərin yayılması və ya dini ayinlərin icrası adı altında xarici d&amp;ouml;vlətdə silahlı m&amp;uuml;naqişədə iştirak etməsi və ya həmin m&amp;uuml;naqişəyə digər şəxsi cəlb etməsi, yaxud bu məqsədlə sabit qrup yaratması, habelə Azərbaycan Respublikası vətəndaşının dini təhsil adı altında xarici d&amp;ouml;vlətdə hərbi təlim ke&amp;ccedil;məsi;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;4) Azərbaycan Respublikasının vətəndaşlığını əldə etmiş şəxsin vətəndaşlığa qəbul olunmaq &amp;uuml;&amp;ccedil;&amp;uuml;n zəruri olan məlumatı qəsdən saxtalaşdırması və ya saxta sənəd təqdim etməsi.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;&lt;span&gt;&lt;span&gt;Azərbaycan Respublikası vətəndaşlığının itirilməsi məsələsi &amp;ldquo;Azərbaycan Respublikasının vətəndaşlığı haqqında&amp;rdquo; Qanunun 17-ci maddəsinin ikinci və &amp;uuml;&amp;ccedil;&amp;uuml;nc&amp;uuml; hissələrində g&amp;ouml;stərilən məhdudiyyətlər nəzərə alınmaqla məhkəmə tərəfindən həll edilir.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;', 0, 1, 1, 3, 1, 0),
(4, 'Dövlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş hüquq və azadlıqlar', '', '', '', '', '&lt;p&gt;&amp;nbsp;D&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlar&amp;nbsp;D&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarvvvvD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlar&lt;/p&gt;', 0, 2, 1, 1, 2, 0),
(5, 'Dövlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş hüquq və azadlıqlar', '', '', '', '', '&lt;p&gt;&amp;nbsp;D&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlar&amp;nbsp;D&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarvvvvD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlar&lt;/p&gt;', 0, 2, 1, 2, 2, 0),
(6, 'Dövlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş hüquq və azadlıqlar', '', '', '', '', '&lt;p&gt;&amp;nbsp;D&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlar&amp;nbsp;D&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlarvvvvD&amp;ouml;vlət Miqrasiya Xidməti fəaliyyətini insan və vətəndaş h&amp;uuml;quq və azadlıqlar&lt;/p&gt;', 0, 2, 1, 3, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(10) NOT NULL,
  `tarix` int(11) NOT NULL,
  `basliq` varchar(255) DEFAULT NULL,
  `tam_metn` text,
  `tip` varchar(5) NOT NULL,
  `video_link` varchar(255) NOT NULL,
  `sira` int(11) NOT NULL,
  `aktivlik` int(11) DEFAULT '0',
  `lang_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `tarix`, `basliq`, `tam_metn`, `tip`, `video_link`, `sira`, `aktivlik`, `lang_id`, `auto_id`) VALUES
(1, 1537377059, 'Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi 1', '', 'jpeg', 'https://www.youtube.com/watch?v=v2Iv1jAQZ5Q', 1, 1, 1, 1),
(2, 1537377059, 'Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi 2', '', 'jpeg', 'https://www.youtube.com/watch?v=v2Iv1jAQZ5Q', 1, 1, 2, 1),
(3, 1537377059, 'Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi 3', '', 'jpeg', 'https://www.youtube.com/watch?v=v2Iv1jAQZ5Q', 1, 1, 3, 1),
(4, 1538303013, 'Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi intern', '', 'jpg', 'https://www.youtube.com/watch?v=e-GqvWZKvq0', 2, 1, 1, 2),
(5, 1538303013, 'Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi intern', '', 'jpg', 'https://www.youtube.com/watch?v=e-GqvWZKvq0', 2, 1, 2, 2),
(6, 1538303014, 'Azərbaycan Respublikasının Cinayət-Prosessual Məcəlləsi intern', '', 'jpg', 'https://www.youtube.com/watch?v=e-GqvWZKvq0', 2, 1, 3, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `about` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `acts`
--
ALTER TABLE `acts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_pass`
--
ALTER TABLE `admin_pass`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alboms`
--
ALTER TABLE `alboms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `articles` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `audio`
--
ALTER TABLE `audio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `beledci`
--
ALTER TABLE `beledci`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `beledci` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `blog` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `calculator`
--
ALTER TABLE `calculator`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `calculator` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `callcenter`
--
ALTER TABLE `callcenter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `callcenter` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `countries` ADD FULLTEXT KEY `name` (`name`);

--
-- Indexes for table `days`
--
ALTER TABLE `days`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `days` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `description`
--
ALTER TABLE `description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diller`
--
ALTER TABLE `diller`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dmx`
--
ALTER TABLE `dmx`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `dmx` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `elanlar`
--
ALTER TABLE `elanlar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `elanlar` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `elaqe`
--
ALTER TABLE `elaqe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faydali`
--
ALTER TABLE `faydali`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `faydali` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `faydali_alt`
--
ALTER TABLE `faydali_alt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_footer`
--
ALTER TABLE `gallery_footer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `gallery_footer` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `interviews`
--
ALTER TABLE `interviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `interviews` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `karabag`
--
ALTER TABLE `karabag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `karabag` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `kime_mektub`
--
ALTER TABLE `kime_mektub`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `kime_mektub` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `links` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `mektublar`
--
ALTER TABLE `mektublar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menyular`
--
ALTER TABLE `menyular`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `menyular` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `muraciet_mektub`
--
ALTER TABLE `muraciet_mektub`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `muraciet_mektub` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `muraciet_novbe`
--
ALTER TABLE `muraciet_novbe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `muraciet_novbe` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `naxcivan`
--
ALTER TABLE `naxcivan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `naxcivan` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `news_gallery`
--
ALTER TABLE `news_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `novbe`
--
ALTER TABLE `novbe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `novbe` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `novbeler`
--
ALTER TABLE `novbeler`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `regions` ADD FULLTEXT KEY `name` (`name`);

--
-- Indexes for table `rehber`
--
ALTER TABLE `rehber`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `rehber` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rightlinks`
--
ALTER TABLE `rightlinks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rusum`
--
ALTER TABLE `rusum`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `rusum` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `senedler`
--
ALTER TABLE `senedler`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `senedler` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `sened_mektub`
--
ALTER TABLE `sened_mektub`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `sened_mektub` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `social_mektub`
--
ALTER TABLE `social_mektub`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `social_mektub` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `statistics`
--
ALTER TABLE `statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `structure`
--
ALTER TABLE `structure`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `structure` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `sual`
--
ALTER TABLE `sual`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `sual` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vetendasliq`
--
ALTER TABLE `vetendasliq`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
ALTER TABLE `vetendasliq` ADD FULLTEXT KEY `name` (`name`,`text`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `acts`
--
ALTER TABLE `acts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `admin_pass`
--
ALTER TABLE `admin_pass`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `alboms`
--
ALTER TABLE `alboms`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `audio`
--
ALTER TABLE `audio`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `beledci`
--
ALTER TABLE `beledci`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `calculator`
--
ALTER TABLE `calculator`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `callcenter`
--
ALTER TABLE `callcenter`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=709;
--
-- AUTO_INCREMENT for table `days`
--
ALTER TABLE `days`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `description`
--
ALTER TABLE `description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `diller`
--
ALTER TABLE `diller`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `dmx`
--
ALTER TABLE `dmx`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `elanlar`
--
ALTER TABLE `elanlar`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `elaqe`
--
ALTER TABLE `elaqe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `faydali`
--
ALTER TABLE `faydali`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `faydali_alt`
--
ALTER TABLE `faydali_alt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `gallery_footer`
--
ALTER TABLE `gallery_footer`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `interviews`
--
ALTER TABLE `interviews`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `karabag`
--
ALTER TABLE `karabag`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `kime_mektub`
--
ALTER TABLE `kime_mektub`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `links`
--
ALTER TABLE `links`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `mektublar`
--
ALTER TABLE `mektublar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `menyular`
--
ALTER TABLE `menyular`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `muraciet_mektub`
--
ALTER TABLE `muraciet_mektub`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `muraciet_novbe`
--
ALTER TABLE `muraciet_novbe`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `naxcivan`
--
ALTER TABLE `naxcivan`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `news_gallery`
--
ALTER TABLE `news_gallery`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `novbe`
--
ALTER TABLE `novbe`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `novbeler`
--
ALTER TABLE `novbeler`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `regions`
--
ALTER TABLE `regions`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=517;
--
-- AUTO_INCREMENT for table `rehber`
--
ALTER TABLE `rehber`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `rightlinks`
--
ALTER TABLE `rightlinks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `rusum`
--
ALTER TABLE `rusum`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `senedler`
--
ALTER TABLE `senedler`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sened_mektub`
--
ALTER TABLE `sened_mektub`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `social_mektub`
--
ALTER TABLE `social_mektub`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `statistics`
--
ALTER TABLE `statistics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `structure`
--
ALTER TABLE `structure`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sual`
--
ALTER TABLE `sual`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vetendasliq`
--
ALTER TABLE `vetendasliq`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
